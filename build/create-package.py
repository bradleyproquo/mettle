#!/usr/bin/env python

# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import sys
import os.path

from mettle.genes import ZipBuddy


mver     = os.path.expandvars('${METTLE_VERSION}')
zip_file = '../bin/mettle-package.zip'
dest_dir = '../release'
zb       = ZipBuddy(zip_file)

zb.clean_dir(dest_dir.replace('/', os.path.sep))
zb.create_dir(dest_dir.replace('/', os.path.sep))

zb.add_exclude_path('../code/cpp/mettle/support')
zb.add_exclude_path('../code/cpp/mettle/addon')
zb.add_exclude_path('../code/cpp/mettle/db/postgresql/9.x')
zb.add_exclude_path('../code/cpp/mettle/db/sqlite/sqlite3')
zb.add_exclude_dir('_unittest')

zb.add_text_as_file('.', 'version', mver)

zb.add_files('bash',        '../code/bash',  ['.sh'], False)
zb.add_files('cpp/include', '../code/cpp',   ['.h'], True)
zb.add_files('cpp/lib',     '../bin/cpp',    ['.a', '.lib', '.so', '.dll'], True)
zb.add_files('js',          '../code/js',    ['.js'],  True)

if sys.platform.startswith('win'):
    zb.add_post_execute('cmd /c "cd %s&&winrar -y x %s"' % (
        dest_dir.replace('/', os.path.sep), zip_file.replace('/', os.path.sep)))
else:
    zb.add_post_execute('sh -c "cd %s&&unzip -o %s -d ."' % (
        dest_dir.replace('/', os.path.sep), zip_file.replace('/', os.path.sep)))

zb.run()
