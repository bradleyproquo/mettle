/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/xmlattr.h"

#include "mettle/lib/common.h"
#include "mettle/lib/macros.h"

#include <string.h>

using namespace Mettle::Lib;

namespace Mettle { namespace Xml {

XmlAttr::XmlAttr()
{
}

XmlAttr::~XmlAttr()
{
}

XmlTag *XmlAttr::add(const char *name, const char *value)
{
   return _list.append(new XmlTag(name, value));
}

XmlTag *XmlAttr::add(XmlTag *tag)
{
   return _list.append(tag);
}

bool XmlAttr::remove(const char *name)
{
   _FOREACH(XmlTag, t, _list)
      if (strcmp(name, t.obj->name()) == 0)
      {
         _list.remove(t.idx);
         return true;
      }

   return false;
}

bool XmlAttr::remove(const unsigned int idx)
{
   if (idx >= _list.count())
      return false;

   _list.remove(idx);

   return true;
}

void XmlAttr::clear()
{
   _list.clear();
}

unsigned int XmlAttr::count() const
{
   return _list.count();
}


XmlTag *XmlAttr::find(const char *name, char **value) const
{
   _FOREACH(XmlTag, t, _list)
      if (strcmp(name, t.obj->name()) == 0)
      {
         if (value)
            *value = (char *)t.obj->value();

         return t.obj;
      }

   return 0;
}

XmlTag *XmlAttr::findIndex(const char *name, unsigned int *outIdx) const
{
   _FOREACH(XmlTag, t, _list)
      if (strcmp(name, t.obj->name()) == 0)
      {
         if (outIdx)
            *outIdx = t.idx;

         return t.obj;
      }

   return 0;
}

XmlTag *XmlAttr::ofIndex(const unsigned int idx) const
{
   if (idx >= _list.count())
      return 0;

   return _list.get(idx);
}

}}
