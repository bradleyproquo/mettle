/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_XMLREADER_H_
#define __METTLE_XML_XMLREADER_H_

#include "mettle/io/ireader.h"

namespace Mettle {

namespace Xml
{
   class DomNode;
}

namespace IO
{

/** \class XmlReader xmlreader.h mettle/xml/xmlreader.h
 *  \brief An xml reader using a DomNode.
 */
class XmlReader : public IReader
{
public:

   /** \brief Default constructor requires a DomNode.
    * \param root the destination root xml to read from.
    */
   XmlReader(Mettle::Xml::DomNode *root) noexcept;

   /** \brief Destructor.
   */
   virtual ~XmlReader() noexcept;

   /** \brief Implements interface method.
    */
   void clear();

   /** \brief Implements interface method.
    */
   const char *name();

   /** \brief Implements interface method.
    */
   unsigned int readStart(const char *name);

   /** \brief Implements interface method.
    */
   unsigned int readEnd(const char *name);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, bool &v);

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, char &v);
#endif

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, int8_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, int16_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, int32_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, int64_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, uint8_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, uint16_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, uint32_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, uint64_t &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, double &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, float &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, Mettle::Lib::String &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, Mettle::Lib::UString &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, Mettle::Lib::DateTime &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, Mettle::Lib::Date &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, Mettle::Lib::Time &v);

   /** \brief Implements interface method.
    * \warning This method always throws an exception as binary data is not supported.
    */
   unsigned int read(const char *field, Mettle::Lib::MemoryBlock &v);

   /** \brief Implements interface method.
    */
   unsigned int read(const char *field, Mettle::Lib::Guid &v);

private:

   const char    *_getField(const char *field);
   unsigned int   _nextField();

   Mettle::Xml::DomNode  *_root;
   Mettle::Xml::DomNode  *_node;
   bool                   _firstRead;
};

}}

#endif
