/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/xmlreader.h"

#include "mettle/lib/common.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"

#include "mettle/xml/domnode.h"
#include "mettle/xml/xmltag.h"

using namespace Mettle::Lib;
using namespace Mettle::Xml;

namespace Mettle { namespace IO {

#define MODULE "XmlReader"

XmlReader::XmlReader(DomNode *root) noexcept
{
   _node      = 0;
   _root      = root;
   _firstRead = true;
}


XmlReader::~XmlReader() noexcept
{
}

void XmlReader::clear()
{
   _root->destroy();
   _node      = 0;
   _firstRead = true;
}

const char *XmlReader::name()
{
   "Mettle.XmlReader";
}

const char *XmlReader::_getField(const char *name)
{
   if (!_node)
      throw xMettle(__FILE__, __LINE__, MODULE, "Expected (%s) and found nothing", name);

   if (strcmp(name, _node->name()))
      throw xMettle(__FILE__, __LINE__, MODULE, "Expected (%s) and found (%s)", name, _node->name());

   return _node->value();
}

unsigned int XmlReader::_nextField()
{
   if (!_node)
      return 0;

   if (_node->sibling)
      _node = _node->sibling;
   else
      _node = _node->parent;

   return 0;
}

unsigned int XmlReader::readStart(const char *name)
{
   if (!_node)
   {
      if (!_firstRead)
         throw xMettle(__FILE__, __LINE__, MODULE, "Expected (%s) and found nothing", name);

      _node = _root;

      if (strcmp(name, _node->name()))
         throw xMettle(__FILE__, __LINE__, MODULE, "Root node expected (%s) and found (%s)", name, _node->name());

      _node      = _node->child;
      _firstRead = false;

      return 0;
   }

   if (strcmp(name, _node->name()))
      throw xMettle(__FILE__, __LINE__, MODULE, "Expected (%s) and found (%s)", name, _node->name());

   _node = _node->child;

   return 0;
}

unsigned int XmlReader::readEnd(const char *name)
{
   return _nextField();
}

unsigned int XmlReader::read(const char *field, bool &v)
{
   const char *val = _getField(field);

   v = String(val).toBool();

   return _nextField();
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int XmlReader::read(const char *field, char &v)
{
   const char *val = _getField(field);

   v = *val;

   return _nextField();
}
#endif

unsigned int XmlReader::read(const char *field, int8_t &v)
{
   const char *val = _getField(field);

   v = (int8_t) String(val).toInt();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, int16_t &v)
{
   const char *val = _getField(field);

   v = (int16_t) String(val).toInt();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, int32_t &v)
{
   const char *val = _getField(field);

   v = (int32_t) String(val).toInt();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, int64_t &v)
{
   const char *val = _getField(field);

   v = (int64_t) String(val).toInt();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, uint8_t &v)
{
   const char *val = _getField(field);

   v = (uint8_t) String(val).toUInt();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, uint16_t &v)
{
   const char *val = _getField(field);

   v = (uint16_t) String(val).toUInt();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, uint32_t &v)
{
   const char *val = _getField(field);

   v = (uint32_t) String(val).toUInt32();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, uint64_t &v)
{
   const char *val = _getField(field);

   v = (uint64_t) String(val).toUInt64();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, double &v)
{
   const char *val = _getField(field);

   v = String(val).toDouble();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, float &v)
{
   const char *val = _getField(field);

   v = String(val).toFloat();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, String &v)
{
   const char *val = _getField(field);

   v = val;

   return _nextField();
}

unsigned int XmlReader::read(const char *field, UString &v)
{
   const char *val = _getField(field);

   v = val;

   return _nextField();
}

unsigned int XmlReader::read(const char *field, DateTime &v)
{
   const char *val = _getField(field);

   v = *val ? DateTime(val) : DateTime();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, Date &v)
{
   const char *val = _getField(field);

   v = *val ? Date(val) : Date();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, Time &v)
{
   const char *val = _getField(field);

   v = *val ? Time(val) : Time();

   return _nextField();
}

unsigned int XmlReader::read(const char *field, Mettle::Lib::MemoryBlock &v)
{
   throw xMettle(__FILE__, __LINE__, MODULE, "Cannot deserialize binary data via XML Reader");
   return 0;
}

unsigned int XmlReader::read(const char *field, Mettle::Lib::Guid &v)
{
   const char *val = _getField(field);

   if (*val)
      v.nullify();
   else
      v = val;

   return _nextField();
}

#undef MODULE

}}
