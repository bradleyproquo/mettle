/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/domnode.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/filemanager.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"

#include "mettle/xml/parser.h"
#include "mettle/xml/xmlattr.h"
#include "mettle/xml/xmltag.h"
#include "mettle/xml/xxml.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <ctype.h>

using namespace Mettle::Lib;

namespace Mettle { namespace Xml {

#define XPATH_DELIM_ELEMENT '>'
#define XPATH_DELIM_ATTR    '<'

DomNode::DomNode()
        :IXmlNode()
{
   tag        = 0;
   attr       = 0;
   sibling    =
   parent     =
   child      = 0;
}

DomNode::DomNode(XmlTag *tag, DomNode *parent)
        :IXmlNode()
{
   if (tag == 0)
      throw xXml(__FILE__, __LINE__, "Cannot initialize a DomNode with a null object");

   this->tag        = tag;
   this->parent     = parent;
   this->attr       = 0;
   this->sibling    =
   this->child      = 0;
}

DomNode::~DomNode()
{
   destroy();
}


void DomNode::destroy()
{
   _DELETE(tag)
   _DELETE(attr)

   if (!child && !sibling)
      return;

   for (DomNode *curr = child ? child : sibling; curr;)
   {
      DomNode *node;
      DomNode *delNode;

      for (node = curr; node->parent != parent;)
      {
         for (;node->child; node = node->child, node->parent->child = 0);

         delNode = node;

         if (delNode->sibling)
            node = delNode->sibling;
         else
            node = delNode->parent;

         delNode->child    =
         delNode->sibling  = 0;

         _DELETE(delNode)
      }

      if (child)
      {
         child = 0;
         curr  = sibling;
      }
      else if (sibling)
      {
         curr = 0;
      }
   }
}

DomNode &DomNode::eat(DomNode &me)
{
   destroy();

   tag      = me.tag;
   attr     = me.attr;
   sibling  = me.sibling;
   child    = me.child;

   if (parent != 0 && me.parent != 0)
      parent = me.parent;

   me.tag     = 0;
   me.attr    = 0;
   me.sibling = 0;
   me.parent  = 0;
   me.child   = 0;

   return *this;
}

IXmlNode *DomNode::newSibling(const char *name)
{
   DomNode *node;

   for (node = this; node->sibling; node = node->sibling);

   node->sibling = new DomNode(new XmlTag(name), this->parent);

   return node->sibling;
}

IXmlNode *DomNode::newChild(const char *name)
{
   if (!child)
   {
      child = new DomNode(new XmlTag(name), this);

      return child;
   }

   return child->newSibling(name);
}


XmlTag *DomNode::newAttribute(const char *name,
                              const char *value)
{
   if (!attr)
      attr = new XmlAttr();

   XmlTag *data = attr->add(name, value);

   return data;
}


void DomNode::setName(const char *name, const char *value)
{
   if (!tag)
   {
      tag = new XmlTag(name, value);
      return;
   }

   tag->setName(name);

   if (value)
      tag->setValue(value);
}

void DomNode::setValue(const char *value)
{
   tag->setValue(value);
}

const char *DomNode::name() const
{
   if (tag)
      return tag->name();

   return String::empty();
}

const char *DomNode::value() const
{
   if (tag)
      return tag->value();

   return String::empty();
}

bool DomNode::isSAX() const
{
   return false;
}

bool DomNode::isDOM() const
{
   return true;
}

void DomNode::deleteSibling(const char *name)
{
   if (!name)
   {
      delete sibling;
      sibling = 0;
      return;
   }

   DomNode *node;
   DomNode *prior = 0;

   for (node = this; node; prior = node, node = node->sibling)
      if (strcmp(node->tag->name(), name) == 0)
      {
         if (prior)
            prior->sibling = node->sibling;
         else
            node->parent->child = node->sibling;

         if (node->sibling)
            node->sibling = 0;

         delete node;
         return;
      }
}

void DomNode::deleteChild(const char *name)
{
   if (!name)
   {
      delete child;
      child = 0;
      return;
   }

   child->deleteChild(name);
}

const char *DomNode::get(const char *xpath) const
{
   XmlTag *tag = exists(xpath);

   if (!tag)
      throw xXml(__FILE__, __LINE__, "Xml-path (%s) is not valid", xpath);

   return tag->value();
}

DomNode *DomNode::getNode(const char *xpath) const
{
   DomNode *node = existsNode(xpath);

   if (!node)
      throw xXml(__FILE__, __LINE__, "Xml-path (%s) is not valid", xpath);

   return node;
}

XmlTag *DomNode::exists(const char *xpath) const
{
   StringBuilder  xpathattr;
   DomNode       *node = _locateNode(xpath, &xpathattr);

   if (!node)
      return 0;

   if (!xpathattr.isEmpty())
   {
      if (!node->attr)
         return 0;

      return node->attr->find(xpathattr.value);
   }

   return node->tag;
}


DomNode *DomNode::existsNode(const char *xpath) const
{
   return _locateNode(xpath, 0);
}


DomNode *DomNode::existsChild(const char *name) const
{
   DomNode *node;

   for (node = this->child; node; node = node->sibling)
      if (strcmp(node->tag->name(), name) == 0)
         return node;

   return 0;
}

DomNode *DomNode::existsSibling(const char *name) const
{
   DomNode *node;

   for (node = this->sibling; node; node = node->sibling)
      if (strcmp(node->tag->name(), name) == 0)
         return node;

   return 0;
}

unsigned int DomNode::_readNodeCount(StringBuilder *jotter, const char *path) const
{
   char *s;
   char *e;

   for (s = jotter->value; *s; s++)
      if (*s == '[')
         break;

   if (!*s)
      return 1;

   for (e = s + 1; *e; e++)
      if (*e == ']')
         break;

   if (!*e)
      throw xXml(__FILE__, __LINE__, "XmlTag::_readNodeCount - Invalid Count (%s) detected in xml path (%s)", jotter, path);

   *e = 0;

   String cnt(s + 1);

   if (!cnt.isUnsigned())
      throw xXml(__FILE__, __LINE__, "XmlTag::_readNodeCount - Invalid Count (%s) detected in xml path (%s)", s + 1, path);

   *s = 0;

   return cnt.toUInt();
}


bool DomNode::_splitNodeData(char          *&ptr,
                             StringBuilder *dest,
                             StringBuilder *attr) const
{
   char *start = ptr;

   dest->clear();

   for (; *ptr; ptr++)
      if (*ptr == XPATH_DELIM_ELEMENT)
      {
         dest->add(start, ptr - start);
         ptr++;
         return false;
      }

   dest->add(start);

   if (attr)
   {
      char *a;

      attr->clear();

      for (a = dest->value; *a; a++)
         if (*a == XPATH_DELIM_ATTR)
         {
            *a = 0;
            a++;
            dest->add(a);
            break;
         }
   }

   return true;
}


DomNode *DomNode::_locateNode(const char *xpath, StringBuilder *attr) const
{
   StringBuilder   jotter;
   unsigned int    cnt;
   unsigned int    idx;
   bool            last = false;
   char           *ptr  = (char *) xpath;
   DomNode        *node = (DomNode *) this;

   while (!last)
   {
      last = _splitNodeData(ptr, &jotter, attr);
      cnt  = _readNodeCount(&jotter, xpath);

      if (!jotter.isEmpty())
      {
         idx  = 0;
         node = node->child;

         do
         {
            if (idx > 0)
               node = node->sibling;

            for (; node; node = node->sibling)
               if (strcmp(node->tag->name(), jotter.value) == 0)
                  break;

            if (!node)
               return 0;

         } while (++idx != cnt);

         if (idx != cnt)
            return 0;
      }

      if (last)
         return node;
   }

   return 0;
}

const char *DomNode::getXmlPath(StringBuilder *dest) const
{
   char     jotter[64];
   int      cnt;
   DomNode *n;

   for (DomNode *node = (DomNode *)this; node->parent; node = node->parent)
   {
      cnt = 1;

      for (n = node->parent->child; n != node; n = n->sibling)
         if (strcmp(n->name(), node->name()) == 0)
            cnt++;

      if (n != node)
         throw xXml(__FILE__, __LINE__, "GetXmlPath - Internal catastrophe");

      if (dest->size)
         dest->insert(">", 0);

      if (cnt > 1)
      {
         sprintf(jotter, "(%d)", cnt);
         dest->insert(jotter, 0);
      }

      dest->insert(node->name(), 0);
   }

   return dest->value;
}

void DomNode::loadFile(DomNode *root, const char *fileName)
{
   unsigned int  size;
   int8_t       *xml = 0;

   try
   {
      FileManager::readFile(fileName, xml, size);

      load(root, (char *) xml);

      free(xml);
   }
   catch (...)
   {
      _FREE(xml)
      throw;
   }
}

void DomNode::load(DomNode *root, const char *xml)
{
   Parser().parse(xml, root);
}

void DomNode::dataNodeWrite(DataNode *dest)
{
   dest->purge();

   DomNode  *xnode = this;
   DataNode *dnode = dest;

   dnode->nameSet(xnode->name());

   if (xnode->value()[0])
      dnode->valueSet(xnode->value());

   do
   {
      if (xnode->child)
      {
         xnode = xnode->child;

         dnode = dnode->newChild(dnode->name());

         if (xnode->value()[0])
            dnode->valueSet(xnode->value());

         continue;
      }

      if (xnode->sibling)
      {
         xnode = xnode->sibling;

         dnode = dnode->newSibling(xnode->name());

         if (xnode->value()[0])
            dnode->valueSet(xnode->value());

         continue;
      }

      while (xnode->parent)
      {
         if (xnode->parent->sibling)
         {
            xnode = xnode->parent->sibling;
            dnode = dnode->parent();

            dnode = dnode->newSibling(xnode->name());

            if (xnode->value()[0])
               dnode->valueSet(xnode->value());

            break;
         }

         xnode = xnode->parent;
         dnode = dnode->parent();
      }

   } while (xnode->parent);
}

void DomNode::dataNodeRead(DataNode *src)
{
   DomNode  *xnode = this;
   DataNode *dnode = src;
   String    jot;

   while (xnode->child)
      xnode->deleteChild();

   if (!src->valueIsEmpty())
      src->value()->read(jot);

   xnode->setName(src->name(), jot.c_str());

   do
   {
      if (dnode->child())
      {
         dnode = dnode->child();

         xnode = (DomNode *) xnode->newChild(dnode->name());

         if (!dnode->valueIsEmpty())
         {
            jot.clear();
            dnode->value()->read(jot);
            xnode->setValue(jot.c_str());
         }

         continue;
      }

      if (dnode->sibling())
      {
         dnode = dnode->sibling();

         xnode = (DomNode *) xnode->newSibling(dnode->name());

         if (!dnode->valueIsEmpty())
         {
            jot.clear();
            dnode->value()->read(jot);
            xnode->setValue(jot.c_str());
         }

         continue;
      }

      while (dnode->parent())
      {
         if (dnode->parent()->sibling())
         {
            dnode = dnode->parent()->sibling();
            xnode = xnode->parent;

            xnode = (DomNode *) xnode->newSibling(dnode->name());

            if (!dnode->valueIsEmpty())
            {
               jot.clear();
               dnode->value()->read(jot);
               xnode->setValue(jot.c_str());
            }

            break;
         }

         dnode = dnode->parent();
         xnode = xnode->parent;
      }

   } while (dnode->parent());
}

#undef XPATH_DELIM_ELEMENT
#undef XPATH_DELIM_ATTR

}}
