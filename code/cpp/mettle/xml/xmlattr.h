/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_XMLATTR_H_
#define __METTLE_XML_XMLATTR_H_

#include "mettle/lib/xmettle.h"

#include "mettle/xml/xmltag.h"

#include <stdio.h>

namespace Mettle { namespace Xml {

/** \class XmlAttr xmlattr.h mettle/io/xmlattr.h
 *  \brief A collection of XmlTag objects with helper methods.
 */
class XmlAttr
{
public:

   /** \brief Default constructor.
    */
   XmlAttr();

   /** \brief Destructor.
    */
   ~XmlAttr();

   /** \brief Creates a new attribute, with a name and optional value.
    *  \param name the name of the new attribute
    *  \param value an optional arguement, the value of the new attribute
    *  \returns the XmlTag object created for convenience.
    */
   XmlTag *add(const char *name, const char *value = 0);

   /** \brief Adds a newly created attribute to this attribute list.
    *  \param tag the object to add.
    *  \returns the XmlTag object that was passed in for convenience.
    */
   XmlTag *add(XmlTag *tag);

   /** \brief Deletes the first attribute that has the specified name.
    *  \param name the attribute to be deleted.
    *  \returns true if an attribute was deleted, else returns false.
    */
   bool remove(const char *name);

   /** \brief Deletes the attribute at the specified index.
    *  \param idx the index of the attribute to be deleted.
    *  \returns true if an attribute was deleted, else returns false if the index was out of range.
    */
   bool remove(const unsigned int idx);

   /** \brief Clears all the atrributes
    */
   void clear();

   /** \brief Gets the number of attributes.
    *  \returns the number of attributes.
    */
   unsigned int count() const;

   /** \brief Finds the attribute with the specified name.
    *  \param name the attribute to search for.
    *  \param value an optional out value pointer, if this arguement is not null, and an attribute is found, the pointer will be pointed to the value of the found attribute.
    *  \returns the XmlTag object found, or NULL if nothing is found.
    */
   XmlTag *find(const char *name, char **value = 0) const;

   /** \brief Finds the attribute with the specified name.
    *  \param name the named attribute to search for.
    *  \param outIdx an optional out index pointer, if this arguement is not null, and an attribute is found, the arguement will be set to the index the found attribute.
    *  \returns the XmlTag object found, or NULL if nothing is found.
    */
   XmlTag *findIndex(const char *name, unsigned int *outIdx = 0) const;

   /** \brief Finds the attribute with the specified name.
    *  \param idx the idx of the attribute to get
    *  \returns the XmlTag object found, or NULL if the idx is out of bounds.
    */
   XmlTag *ofIndex(const unsigned int idx) const;

protected:

    XmlTag::List _list;
};

}}

#endif
