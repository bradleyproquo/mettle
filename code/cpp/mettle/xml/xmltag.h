/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_XMLTAG_H_
#define __METTLE_XML_XMLTAG_H_

#include "mettle/lib/collection.h"

namespace Mettle { namespace Xml {

/** \class XmlTag xmltag.h mettle/io/xmltag.h
 *  \brief Name and value pair value for an xml tag
 */
class XmlTag
{
public:
   DECLARE_COLLECTION_LIST_CLASS(XmlTag);


   /** \brief Default constructor.
    */
   XmlTag();

   /** \brief Constrcuct tag with a name.
    *  \param name the name of the tag.
    */
   XmlTag(const char *name);

   /** \brief Constrcuct tag with a name and string value.
    *  \param name the name of the tag.
    *  \param value the value of tag.
    */
   XmlTag(const char *name, const char *value);

   /** \brief Constrcuct tag with a name and integer value.
    *  \param name the name of the tag.
    *  \param value the value of tag.
    */
   XmlTag(const char *name, const int value);

   /** \brief Constrcuct tag with a name and double value.
    *  \param name the name of the tag.
    *  \param value the value of tag.
    */
   XmlTag(const char *name, const double value);

   /** \brief Constrcuct tag with a name and bool value.
    *  \param name the name of the tag.
    *  \param value the value of tag.
    */
   XmlTag(const char *name, const bool value);

   /** \brief Destructor.
    */
   ~XmlTag();

   /** \brief Set a new name for the tag.
    *  \param name the new name of the tag.
    */
   void setName(const char   *name);

   /** \brief Set a new value for the tag.
    *  \param value the new value of the tag.
    */
   void setValue(const char   *value);

   /** \brief Set a new value for the tag.
    *  \param value the new value of the tag.
    */
   void setValue(const int     value);

   /** \brief Set a new value for the tag.
    *  \param value the new value of the tag.
    */
   void setValue(const double  value);

   /** \brief Set a new value for the tag.
    *  \param value the new value of the tag.
    */
   void setValue(const bool    value);

   /** \brief Gets the name of the tag
    *  \return the name of the tag.
    */
   const char *name() const;

   /** \brief Gets the value of the tag
    *  \return the name of the tag, this method never returns a null
    */
   const char *value() const;

private:

   char *_name;
   char *_value;
};

}}

#endif
