/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/xmltag.h"

#include "mettle/lib/common.h"
#include "mettle/lib/macros.h"

#include <stdio.h>

using namespace Mettle::Lib;

namespace Mettle { namespace Xml {

XmlTag::XmlTag()
{
   _value = 0;
   _name  = 0;
}

XmlTag::XmlTag(const char *name)
{
   _value = 0;
   _name  = 0;

   setName(name);
}


XmlTag::XmlTag(const char *name, const char *value)
{
   _value = 0;
   _name  = 0;

   setName(name);
   setValue(value);
}


XmlTag::XmlTag(const char *name, const int value)
{
   _value = 0;
   _name  = 0;

   setName(name);
   setValue(value);
}


XmlTag::XmlTag(const char *name, const double value)
{
   _value = 0;
   _name  = 0;

   setName(name);
   setValue(value);
}


XmlTag::XmlTag(const char *name, const bool value)
{
   _value = 0;
   _name  = 0;

   setName(name);
   setValue(value);
}


XmlTag::~XmlTag()
{
   _FREE(_value)
   _FREE(_name)
}


const char *XmlTag::value() const
{
   return _value == 0 ? _EMPTY_STRING : _value;
}

const char *XmlTag::name() const
{
   return _name;
}

void XmlTag::setName(const char *name)
{
   _FREE(_name)

   _name = (char *) Common::memoryAlloc(strlen(name) + 1);

   strcpy(_name, name);
}


void XmlTag::setValue(const char *value)
{
   _FREE(_value)

   if (!value)
      return;

   _value = (char *) Common::memoryAlloc(strlen(value) + 1);

   strcpy(_value, value);
}


void XmlTag::setValue(const int value)
{
   char jotter[64];

   sprintf(jotter, "%d", value);

   setValue(jotter);
}


void XmlTag::setValue(const double value)
{
   char jotter[64];

   sprintf(jotter, "%.2lf", value);

   setValue(jotter);
}


void XmlTag::setValue(const bool value)
{
   setValue(value ? "True" : "False");
}

}}
