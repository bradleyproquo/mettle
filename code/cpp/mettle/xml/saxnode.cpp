/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/saxnode.h"

#include "mettle/lib/common.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/filemanager.h"

#include "mettle/xml/parser.h"

using namespace Mettle::Lib;

namespace Mettle { namespace Xml {

SaxNode::SaxNode() : IXmlNode()
{
}

SaxNode::~SaxNode()
{
}

IXmlNode *SaxNode::newSibling(const char *name)
{
   _name = name;
   _value.clear();

   saxEvent(ElementStart, _name.c_str(), _value.c_str());

   return this;
}


IXmlNode *SaxNode::newChild(const char *name)
{
   _name = name;
   _value.clear();

   saxEvent(ElementStart, _name.c_str(), _value.c_str());

   return this;
}

XmlTag *SaxNode::newAttribute(const char *name,
                              const char *value)
{
   _name = name;
   _value.clear();

   if (value)
      _value = value;
   else
      _value.clear();

   saxEvent(ElementAttribute, _name.c_str(), _value.c_str());

   return 0;
}


void SaxNode::setValue(const char *value)
{
   if (value)
      _value = value;
   else
      _value.clear();

   saxEvent(ElementText, _name.c_str(), _value.c_str());
}


void SaxNode::setName(const char *name, const char *value)
{
   _name = name;

   if (value)
      _value = value;
   else
      _value.clear();

   saxEvent(DocumentStart, _name.c_str(), _value.c_str());
}

const char *SaxNode::name() const
{
   return _name.c_str();
}

const char *SaxNode::value() const
{
   return _value.c_str();
}

bool SaxNode::isSAX() const
{
   return true;
}

bool SaxNode::isDOM() const
{
   return false;
}

void SaxNode::loadFile(SaxNode *root, const char *fileName)
{
   unsigned int size;
   int8_t       *xml =  0;

   try
   {
      FileManager::readFile(fileName, xml, size);

      load(root, (char *) xml);

      free(xml);
   }
   catch (...)
   {
      _FREE(xml)
      throw;
   }
}

void SaxNode::load(SaxNode *root, const char *xml)
{
   Parser().parse(xml, root);
}

}}
