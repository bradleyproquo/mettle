/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_PARSER_H_
#define __METTLE_XML_PARSER_H_

namespace Mettle {

namespace Lib
{
   class DataNode;
   class StringBuilder;
}

namespace Xml
{

class IXmlNode;
class DomNode;
class SaxNode;

/** \class Parser parser.h mettle/xml/parser.h
 *  \brief The xml parser object, this object is UTF-8 compliant.
 */
class Parser
{
public:

   /** \brief Constrcutor.
    *
    */
   Parser();

   /** \brief Destructor.
    *
    */
   virtual ~Parser();

   /** \brief Parses the xml string into the provided DomNode implementation.
    *  \param xmlStr the null terminated xml string to parse.
    *  \param root the start of DOM tree
    */
   void parse(const char *xmlStr, DomNode *root);

   /** \brief Parses the xml string into the provided SaxNode implementation.
    *  \param xmlStr the null terminated xml string to parse.
    *  \param worker worker SaxNode to be used
    */
   void parse(const char *xmlStr, SaxNode *worker);

   /** \brief Parses a DataNode and builds a dom tree from that
    *  \param source the datanode to be parsed
    *  \param dest the destional and start of DOM tree
    */
   void parse(const Mettle::Lib::DataNode *source, DomNode *dest);

protected:

   Mettle::Lib::StringBuilder *_jotter;
   Mettle::Lib::StringBuilder *_attrIdent;

   IXmlNode      *_root;
   IXmlNode      *_node;
   const char    *_xmlStr;
   char          *_pos;

   int openClose(const bool openRoot);

   void readTag(const bool openTag);

   void readValue();

   bool whiteSpace(const char ch);

   bool readCData();

   bool readSpecial();

   int readAttributes();

   void readAttr();

   void findEquals();

   void readAttrValue();
};


}}

#endif
