/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/json/parser.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/common.h"
#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/filemanager.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"

#define JSMN_STATIC

#include "jsmn.h"

using namespace Mettle::Lib;

namespace Mettle { namespace Json {

#define SOURCE "Json::Parser"

Parser::Parser()
{
   _sb        = 0;
   _totTokens = 0;
   _tokens    = 0;
   _json      = 0;
}


Parser::~Parser()
{
   clear();
}


void Parser::clear()
{
   _DELETE(_sb)
   _FREE(_tokens)
   _totTokens = 0;
   _json      = 0;
}


DataNode *Parser::parseFile(const char *filename)
{
   int8_t        *ptr      = 0;
   unsigned int   jsonSize = 0;
   String         json;

   FileManager::readFile(filename, ptr, jsonSize);

   json.eat((char *) ptr);

   return parse(json.c_str(), json.length());
}


DataNode *Parser::parse(const char *json, const size_t jsonLen)
{
   clear();

   DataNode::Safe  root(new DataNode());
   jsmn_parser     jp;
   size_t          jlen = jsonLen == 0 ? strlen(json) : jsonLen;
   int             rc;
   int             parent = 0;
   int             idx    = 0;

   jsmn_init(&jp);

   rc = jsmn_parse(&jp, json, jlen, 0, 0);

   if (rc == 0)
      return root.forget();

   if (rc > 0)
   {
      _tokens = (jsmntok_t * ) Common::memoryAlloc(sizeof(jsmntok_t) * rc);

      jsmn_init(&jp);

      rc = jsmn_parse(&jp, json, jlen, (jsmntok_t * ) _tokens, rc);
   }

   if (rc < 0)
   {
      if (rc == JSMN_ERROR_NOMEM)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Jsmn Error [%d]: not enough tokens provided.", rc);

      if (rc == JSMN_ERROR_INVAL)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Jsmn Error [%d]: invalid character inside JSON string.", rc);

      if (rc == JSMN_ERROR_PART)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Jsmn Error [%d]: the string is not a full JSON packet, more bytes expected.", rc);
   }

   _totTokens = rc;
   _json      = (char *) json;
   _sb        = new StringBuilder();

   if (((jsmntok_t *) _tokens)[0].type != JSMN_OBJECT)
      throw xMettle(__FILE__, __LINE__, SOURCE, "First type should always be an object!.");

   for (++idx; idx < _totTokens;)
      parseTags(root.obj, idx);

   return root.forget();
}


void Parser::parseTags(DataNode *node, int &idx)
{
   jsmntok_t  *tok;
   DataNode   *child = 0;
   int         childCnt;

   while (idx < _totTokens)
   {
      tok = &((jsmntok_t *) _tokens)[idx];

      if (tok->type == JSMN_UNDEFINED)
      {
         readToken(tok);
         idx++;
         continue;
      }

      if (tok->size == 0)
      {
         readToken(tok);
         idx += tok->size + 1;
         continue;
      }

      if (tok->type != JSMN_STRING &&
          tok->type != JSMN_PRIMITIVE)
      {
         idx += tok->size + 1;
         readToken(tok);
         continue;
      }

      child    = node->newChild(readToken(tok));
      childCnt = tok->size;
      idx++;
      break;
   }

   while (childCnt > 0 && idx < _totTokens)
   {
      childCnt--;
      tok = &((jsmntok_t *) _tokens)[idx++];

      if (tok->type == JSMN_OBJECT)
      {
         for (int objCnt = tok->size; objCnt > 0; objCnt--)
            parseTags(child, idx);

         continue;
      }

      if (tok->type == JSMN_ARRAY)
      {
         DataVar       *dv  = child->valueSet(new DataVar(DataVar::tl_list));
         DataVar::List *arr = (DataVar::List *) dv->valuePtr();

         for (int arrIdx = tok->size; arrIdx > 0; arrIdx--)
            parseArrTag(arr, idx);

         continue;
      }

      if (tok->type == JSMN_STRING)
      {
         child->valueSet(readToken(tok));
         continue;
      }

      if (tok->type == JSMN_PRIMITIVE)
      {
         readToken(tok);

         DataVar *item = child->valueInit();

         if (!strcmp(_sb->value, "null"))
            item->nullify();
         else if (!strcmp(_sb->value, "true"))
            item->write(true);
         else if (!strcmp(_sb->value, "false"))
            item->write(false);
         else
         {
            item->write(_sb->value);
            item->toBestType();
         }

         continue;
      }

   }
}


void Parser::parseArrTag(DataVar::List *arr, int &idx)
{
   readToken(&((jsmntok_t *) _tokens)[idx]);

   jsmntok_t *tok  = &((jsmntok_t *) _tokens)[idx++];
   DataVar   *item = arr->append(new DataVar());

   if (tok->type == JSMN_STRING)
   {
      item->write(readToken(tok));
      return;
   }

   if (tok->type == JSMN_PRIMITIVE)
   {
      readToken(tok);

      if (!strcmp(_sb->value, "null"))
         item->nullify();
      else if (!strcmp(_sb->value, "true"))
         item->write(true);
      else if (!strcmp(_sb->value, "false"))
         item->write(false);
      else
      {
         item->write(_sb->value);
         item->toBestType();
      }

      return;
   }

   if (tok->type == JSMN_OBJECT)
   {
      item->valueSetType(DataVar::tl_dataNode);

      for (int objCnt = tok->size; objCnt > 0; objCnt--)
         parseTags((DataNode *) item->valuePtr(), idx);

      return;
   }

   if (tok->type == JSMN_ARRAY)
   {
      item->valueSetType(DataVar::tl_list);

      DataVar::List *subArr = (DataVar::List *) item->valuePtr();

      for (int arrIdx = tok->size; arrIdx > 0; arrIdx--)
         parseArrTag(subArr, idx);
   }
}


char *Parser::readToken(const void *token)
{
   jsmntok_t *tok = (jsmntok_t*) token;

   _sb->clear();
   _sb->add(_json + tok->start, tok->end - tok->start);

   return _sb->value;
}

#undef SOURCE

}}
