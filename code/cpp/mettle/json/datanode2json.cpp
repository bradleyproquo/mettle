/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/json/datanode2json.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/common.h"
#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"

#include <stdarg.h>

using namespace Mettle::Lib;

namespace Mettle { namespace Json {

#define MODULE "Mettle.Json.DataNode2Json"

DataNode2Json::DataNode2Json(DataNode *node)
{
   _node = node;
}

DataNode2Json::~DataNode2Json()
{
}

void DataNode2Json::print(StringBuilder *dest)
{
   toString(0, dest);
}

void DataNode2Json::print(FILE *dest)
{
   toString(dest, 0);
}

void DataNode2Json::toString(FILE *destFH, Mettle::Lib::StringBuilder *destSB)
{
   DataNode::Emitter              e(_node);
   DataNode::Emitter::Action      act;
   DataVar                       *dv;
   DataVar::TypeLegend            tl;
   const DataNode                *dn;
   String                         jot;
   const String                  *name;


   while (DataNode::Emitter::done != (act = e.emit()))
   {
      dn   = e.node();
      name = e.name();

      switch (act)
      {
         case DataNode::Emitter::element:
            if (!e.inList())
            {
               if (destFH)
                  writef(destFH, "\"%s\":", name ? name->c_str() : String::empty());

               if (destSB)
               {
                  destSB->add('"');
                  destSB->add(name ? name->c_str() : String::empty());
                  destSB->add("\":");
               }
            }

            dv = e.value();

            if (!dv || dv->valueType() == DataVar::tl_null)
            {
               if (destFH)
                  writef(destFH, "null");

               if (destSB)
                  destSB->add("null");
            }
            else
            {
               dv->toString(jot);
               tl = dv->valueType();

               if (dv->valueTypeIsInteger() || tl == DataVar::tl_double || tl == DataVar::tl_float || tl == DataVar::tl_bool)
               {
                  if (destFH)
                     writef(destFH, "%s", jot.c_str());

                  if (destSB)
                     destSB->add(jot.c_str());
               }
               else
               {
                  if (destFH)
                     writef(destFH, "\"%s\"", jot.c_str());

                  if (destSB)
                  {
                     destSB->add('"');
                     destSB->add(jot.c_str());
                     destSB->add('"');
                  }
               }
            }

            if (e.hasSibling())
            {
               if (destFH)
                  writef(destFH, ",");

               if (destSB)
                  destSB->add(",");
            }

            break;

         case DataNode::Emitter::listOpen:
            if (name && !name->isEmpty())
            {
               if (destFH)
                  writef(destFH, "\"%s\":", name->c_str());

               if (destSB)
               {
                  destSB->add('"');
                  destSB->add(name->c_str());
                  destSB->add("\":");
               }
            }

            if (destFH)
               writef(destFH, "[");

            if (destSB)
               destSB->add("[");

            break;

         case DataNode::Emitter::listClose:
            if (destFH)
               writef(destFH, "]");

            if (destSB)
               destSB->add("]");

            if (e.hasSibling())
            {
               if (destFH)
                  writef(destFH, ",");

               if (destSB)
                  destSB->add(",");
            }

            break;

         case DataNode::Emitter::objOpen:
            if (name && !name->isEmpty())
            {
               if (destFH)
                  writef(destFH, "\"%s\":", name->c_str());

               if (destSB)
               {
                  destSB->add('"');
                  destSB->add(name->c_str());
                  destSB->add("\":");
               }
            }

            if (destFH)
               writef(destFH, "{");

            if (destSB)
               destSB->add("{");

            break;

         case DataNode::Emitter::objClose:
            if (destFH)
               writef(destFH, "}");

            if (destSB)
               destSB->add("}");

            if (e.hasSibling())
            {
               if (destFH)
                  writef(destFH, ",");

               if (destSB)
                  destSB->add(",");
            }

            break;

         default:
            throw xMettle(__FILE__, __LINE__, MODULE, "Emitter error! Unexpected action [%d]", act);
      }
   }
}

void DataNode2Json::writef(FILE *fp, const char *fmt, ...)
{
   va_list ap;

   va_start(ap, fmt);

   if (vfprintf(fp, fmt, ap) == EOF)
   {
      va_end(ap);
      throw xMettle(__FILE__, __LINE__, MODULE, "Error writting to file, check disk space.");
   }

   va_end(ap);
}


#undef MODULE

}}
