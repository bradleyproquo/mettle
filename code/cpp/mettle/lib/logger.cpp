/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/logger.h"

#include <time.h>

namespace Mettle { namespace Lib {

Logger::Logger()
{
   _level = Debug;
}

Logger::~Logger() noexcept
{
}

Logger *Logger::trace(const char *file, const unsigned int line)
{
   return this;
}

void Logger::traceReset()
{
}


void Logger::traceUnwind()
{
}


void Logger::spot(const char *file, const int line, const std::exception &x) noexcept
{
   error("Spotted std::exception in File(%s) at Line(%d)\n Msg(%s)",
         file,
         line,
         x.what());
}

void Logger::spot(const char *file, const int line, const xMettle &x) noexcept
{
   error("Spotted xMettle in File(%s) at Line(%d)"
         "\n Msg(%s)"
         "\n File(%s)/Line(%d)/Source(%s)/ErrorCode(%d)/SysEC(%d)/SysMsg(%s)",
         file,
         line,
         x.errorMsg,
         x.filename,
         x.line,
         x.source,
         x.errorCode,
         x.sysErrorNo,
	      x.sysErrorString);
}

void Logger::spot(const char *file, const int line) noexcept
{
   error("Spotted UNKNOWN EXCEPTION in File(%s) at Line(%d)"
         "\n Msg(Unknown Exception!!!)",
         file,
         line);
}

Logger::Level Logger::getLogLevel() const noexcept
{
   return _level;
}

void Logger::setLogLevel(const Level level) noexcept
{
   _level = level;
}

}}
