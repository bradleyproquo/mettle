/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/datanode.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/linklist.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/uchar.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

#define MODULE "Mettle.DataNode"

DataNode::DataNode() noexcept
{
   _parent   =
   _sibling  =
   _child    = 0;
   _value    = 0;
}


DataNode::DataNode(const char *name, DataVar *val)
{
   _parent   =
   _sibling  =
   _child    = 0;
   _value    = val;
   _name     = name;
}


DataNode::~DataNode() noexcept
{
   purge();
}


void DataNode::purge() noexcept
{
   _DELETE(_value)

   if (!_child && !_sibling)
      return;

   for (DataNode *curr = _child ? _child : _sibling; curr;)
   {
      DataNode *node;
      DataNode *delNode;

      for (node = curr; node->_parent != _parent;)
      {
         for (; node->_child; node = node->_child, node->_parent->_child = 0);

         delNode = node;

         if (delNode->_sibling)
            node = delNode->_sibling;
         else
            node = delNode->_parent;

         delNode->_child    =
         delNode->_sibling  = 0;

         _DELETE(delNode)
      }

      if (_child)
      {
         _child = 0;
         curr   = _sibling;
      }
      else if (_sibling)
      {
         curr = 0;
      }
   }
}

void DataNode::clear() noexcept
{
   purge();
   _name.clear();
}

const String &DataNode::name() const noexcept
{
   return _name;
}

void DataNode::nameSet(const char *newName)
{
   _name = newName;
}

DataVar *DataNode::value() const
{
   return _value;
}

const bool DataNode::valueIsNull() const noexcept
{
   return _value == 0 || _value->valueIsNull();
}

const bool DataNode::valueIsEmpty() const noexcept
{
   return _value == 0 || _value->valueIsEmpty();
}

Mettle::Lib::DataVar *DataNode::valueInit()
{
   if (_value == 0)
      _value = new DataVar();

   return _value;
}

DataVar *DataNode::valueSet(DataVar *var)
{
   _DELETE(_value)

   _value = var;

   return var;
}

DataVar *DataNode::valueSet(const char *var)
{
   if (_value == 0)
      _value = new DataVar();

   _value->write(var);

   return _value;
}

const unsigned int DataNode::childCount() const noexcept
{
   if (!_child)
      return 0;

   unsigned int cnt = 0;

   for (DataNode *ptr = _child; ptr; ptr = ptr->_sibling, cnt++);

   return cnt;
}

const unsigned int DataNode::siblingCount() const noexcept
{
   if (!_sibling)
      return 0;

   unsigned int cnt = 0;

   for (DataNode *ptr = _sibling; ptr; ptr = ptr->_sibling, cnt++);

   return cnt;
}

DataNode *DataNode::parent() const noexcept
{
   return _parent;
}

DataNode *DataNode::sibling() const noexcept
{
   return _sibling;
}

DataNode *DataNode::child() const noexcept
{
   return _child;
}

DataNode *DataNode::get(const char *path)
{
   return _findNode(path, true, true);
}

DataNode *DataNode::indexOf(const unsigned int idx) const noexcept
{
   DataNode      *fnd;
   DataNode      *ptr   = (DataNode *) this;
   unsigned int   count = idx;

   for (fnd = _child; fnd; fnd = fnd->_sibling)
      if (--count == 0)
         break;

   return fnd;
}

DataNode *DataNode::newChild(const char *name, DataVar *value, const int pos)
{
   DataVar::Safe dvs(value);

   DataNode  *node = new DataNode(name, value);
   int        cnt = 0;

   dvs.forget();

   node->_parent = this;

   if (!_child)
   {
      _child = node;
      return node;
   }

   DataNode *ptr;
   DataNode *prev = 0;

   for (ptr = _child; ptr; ptr = ptr->_sibling)
   {
      prev = ptr;

      if (cnt++ == pos)
         break;
   }

   if (ptr == _child)
   {
      node->_sibling = _child;
      _child         = node;
   }
   else
   {
      if (!ptr)
         ptr = prev;

      node->_sibling = ptr->_sibling;
      ptr->_sibling  = node;
   }


   return node;
}

DataNode *DataNode::newSibling(const char *name, DataVar *value, const int pos)
{
   DataVar::Safe dvs(value);

   if (!_parent)
      throw xMettle(__FILE__, __LINE__, "DataNode", "Cannot add siblings to the top data node");

   DataNode  *node = new DataNode(name, value);
   int        cnt = 0;

   dvs.forget();

   node->_parent = this->_parent;

   if (!_sibling)
   {
      _sibling = node;
      return node;
   }

   DataNode *ptr;
   DataNode *prev = 0;

   for (ptr = _sibling; ptr; ptr = ptr->_sibling)
   {
      prev = ptr;

      if (cnt++ == pos)
         break;
   }

   if (!ptr)
      ptr = prev;

   node->_sibling = ptr->_sibling;
   ptr->_sibling  = node;

   return node;
}

Mettle::Lib::DataNode *DataNode::root() const noexcept
{
   DataNode *ptr;

   for (ptr = (DataNode *) this; ptr->_parent; ptr = ptr->_parent);

   return ptr;
}

bool DataNode::remove(const char *path) noexcept
{
   DataNode *ptr = _findNode(path, false, false);

   if (!ptr)
      return false;

   ptr->purge();

   return true;
}

DataNode *DataNode::write(const char *path, const char *val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const String   &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const UString  &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const double &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const int &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const unsigned int &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const bool &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const Date &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const Time &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const DateTime &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

DataNode *DataNode::write(const char *path, const Guid &val)
{
   DataNode *targ = _findNodeForWrite(path);

   if (!targ)
      return 0;

   targ->_value->write(val);

   return targ;
}

bool DataNode::read(const char *path, char *&val)
{
   val = (char *) String::empty();

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   const char *tmp = node->valueIsEmpty() ? 0 : node->_value->c_str();

   if (tmp)
      val = (char *) tmp;

   return true;
}

bool DataNode::read(const char *path, String &val)
{
   val.clear();

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

bool DataNode::read(const char *path, UString &val)
{
   val.clear();

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

bool DataNode::read(const char *path, double &val)
{
   val = 0.0;

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

bool DataNode::read(const char *path, int &val)
{
   val = 0;

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   int32_t tmp;

   if (!node->valueIsEmpty())
   {
      node->_value->read(tmp);
      val = tmp;
   }

   return true;
}

bool DataNode::read(const char *path, unsigned int &val)
{
   val = 0;

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   uint32_t tmp;

   if (!node->valueIsEmpty())
   {
      node->_value->read(tmp);
      val = tmp;
   }

   return true;
}

bool DataNode::read(const char *path, bool &val)
{
   val = false;

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

bool DataNode::read(const char *path, Date &val)
{
   val.nullify();

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

bool DataNode::read(const char *path, Time &val)
{
   val.nullify();

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

bool DataNode::read(const char *path, DateTime &val)
{
   val.nullify();

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

bool DataNode::read(const char *path, Guid &val)
{
   val.nullify();

   DataNode *node = _findNode(path);

   if (!node)
      return false;

   if (!node->valueIsEmpty())
      node->_value->read(val);

   return true;
}

DataNode *DataNode::_findNode(const char *path,
                              const bool  allowRoot,
                              const bool  allowParentNav)
{
   if (!path || !*path)
      return this;

   UString::List  nodeNames;
   DataNode      *ptr = this;
   DataNode      *fnd;
   char           jotter[128];
   unsigned int   arrStart;
   unsigned int   arrEnd;
   unsigned int   count;

   if (*path == '/')
   {
      if (!allowRoot)
         throw xMettle(__FILE__, __LINE__, "DataNode::Find", "Starting with '/' is not allowed for this operation. Relative paths only!", path);

      for (; ptr->_parent; ptr = ptr->_parent);

      UString(path + 1).split("/", nodeNames, '"');
   }
   else
      UString(path).split("/", nodeNames, '"');

   _FOREACH(UString, name, nodeNames)
   {
      if (0 == strcmp(name.obj->c_str(), ".."))
      {
         if (!allowParentNav)
            throw xMettle(__FILE__, __LINE__, "DataNode::Find", "Cannot use '..' for this operation. Relative paths only!", path);

         if (!ptr->_parent)
            return 0;

         ptr = ptr->_parent;
      }

      count  = 0;

      if (name.obj->endsWith("]"))
      {
         arrStart = name.obj->reverseFind("[");
         arrEnd   = name.obj->reverseFind("]");

         if (arrStart < 0);
            throw xMettle(__FILE__, __LINE__, "DataNode::Find", "Path not valid, missing opening '[' bracket (%s)", path);

         if (arrEnd - arrStart >= sizeof(jotter))
            throw xMettle(__FILE__, __LINE__, "DataNode::Find", "Path not valid, array value to large (%s)", path);

         memset(jotter, 0, sizeof(jotter));
         memcpy(jotter, name.obj->c_str() + arrStart, arrEnd);

         String cnt(jotter);

         if (!cnt.isUnsigned())
            throw xMettle(__FILE__, __LINE__, "DataNode::Find", "Path not valid, array number not valid (%s, %s)", path, cnt.c_str());

         count    = cnt.toUInt() + 1;
         arrStart = name.obj->calcIndexFromOffset(arrStart);

         name.obj->subString(0, arrStart);
      }

      if (!name.obj->isEmpty())
      {
         for (fnd = ptr->_child; fnd; fnd = fnd->_sibling)
            if (strcmp(name.obj->c_str(), fnd->name()) == 0)
            {
               if (count > 0)
               {
                  if (--count == 0)
                     break;

                  continue;
               }

               break;
            }

         if (!fnd)
            ptr = ptr->newChild(name.obj->c_str());
         else
            ptr = fnd;
      }
      else if (count > 0)
      {
         for (fnd = ptr->_child; fnd; fnd = fnd->_sibling)
            if (--count == 0)
               break;

         if (!fnd)
            return 0;

         ptr = fnd;
      }
   }

   return ptr;
}

DataNode *DataNode::_findNodeForWrite(const char *path)
{
   DataNode *targ = _findNode(path);

   if (targ == 0)
      return 0;

   if (targ->_value == 0)
      targ->_value = new DataVar();

   return targ;
}

unsigned int DataNode::toBestTypes()
{
   Emitter              e(this);
   Emitter::Action      act;
   unsigned int         cnt = 0;
   DataVar             *dv;
   DataVar::TypeLegend  bf;
   DataVar::TypeLegend  af;

   while (DataNode::Emitter::done != (act = e.emit()))
      if (act == DataNode::Emitter::element && (dv = e.value()))
      {
         bf = dv->valueType();
         af = dv->toBestType();

         if (bf != af)
            cnt++;
      }

   return cnt;
}

/*------------------------------------------------------------------------------
  Emitter
------------------------------------------------------------------------------*/

class _pivateDataNodeEmitter
{
public:
   typedef struct
   {
      DataNode                  *node;
      DataVar::List             *list;
      unsigned int               idx;
      DataNode::Emitter::Action  action;
   } NodeStack;

   LinkList<NodeStack>  *stack;
   DataNode             *root;
   NodeStack            *curr;

            _pivateDataNodeEmitter(DataNode *root);
   virtual ~_pivateDataNodeEmitter();

   NodeStack *push(DataNode *node);
   NodeStack *pop();
};


_pivateDataNodeEmitter::_pivateDataNodeEmitter(DataNode *root)
{
   this->root      = root;
   stack           = new LinkList<NodeStack>();

   push(root);
}

_pivateDataNodeEmitter::~_pivateDataNodeEmitter()
{
   _DELETE(stack)
}

_pivateDataNodeEmitter::NodeStack *_pivateDataNodeEmitter::push(DataNode *node)
{
   _pivateDataNodeEmitter::NodeStack *ns = stack->add(new _pivateDataNodeEmitter::NodeStack());

   memset(ns, 0, sizeof(_pivateDataNodeEmitter::NodeStack));

   ns->node   = node;
   ns->action = DataNode::Emitter::none;
   curr       = ns;

   return curr;
}

_pivateDataNodeEmitter::NodeStack *_pivateDataNodeEmitter::pop()
{
   stack->end();
   stack->remove();

   curr = stack->last();

   return curr;
}

#define PRVEMIT ((_pivateDataNodeEmitter *) _obj)

DataNode::Emitter::Emitter(DataNode *root)
{
   _pivateDataNodeEmitter *pe = new _pivateDataNodeEmitter(root);

   _obj = (void *) pe;
}


DataNode::Emitter::~Emitter()
{
   if (_obj == 0)
      return;

   _pivateDataNodeEmitter *pe = (_pivateDataNodeEmitter *) _obj;

   delete pe;

   _obj = 0;
}


const DataNode *DataNode::Emitter::node() const
{
   if (PRVEMIT->curr)
      return 0;

   return PRVEMIT->curr->node;
}


const String *DataNode::Emitter::name() const
{
   if (!PRVEMIT->curr || !PRVEMIT->curr->node)
      return 0;

   return &PRVEMIT->curr->node->name();
}


DataVar *DataNode::Emitter::value()
{
   if (!PRVEMIT->curr)
      return 0;

   if (PRVEMIT->curr->action == Emitter::element)
   {
      if (PRVEMIT->curr->list)
      {
         if (PRVEMIT->curr->list->count() < 1)
            return 0;

         return PRVEMIT->curr->list->get(PRVEMIT->curr->idx);
      }
   }

   if (PRVEMIT->curr->node)
      return PRVEMIT->curr->node->value();

   return 0;
}


const DataNode *DataNode::Emitter::rootNode() const
{
   return PRVEMIT->root;
}


DataNode::Emitter::Action DataNode::Emitter::action() const
{
   if (PRVEMIT->curr == 0)
      return Emitter::error;

   return PRVEMIT->curr->action;
}


unsigned int DataNode::Emitter::listIndex() const
{
   if (PRVEMIT->curr == 0)
      return 0;

   return PRVEMIT->curr->idx;
}


unsigned int DataNode::Emitter::listSize() const
{
   if (PRVEMIT->curr == 0 || !PRVEMIT->curr->list)
      return 0;

   return PRVEMIT->curr->list->count();
}


bool DataNode::Emitter::hasSibling() const
{
   if (!PRVEMIT->curr)
      return false;

   if (PRVEMIT->curr->action == DataNode::Emitter::element)
   {
      if (PRVEMIT->curr->list)
         return PRVEMIT->curr->idx + 1 < PRVEMIT->curr->list->count();

      if (PRVEMIT->curr->node)
         return PRVEMIT->curr->node->sibling() != 0;

      return false;
   }

   if (PRVEMIT->curr->action == DataNode::Emitter::listClose ||
       PRVEMIT->curr->action == DataNode::Emitter::objClose)
   {
      if (PRVEMIT->curr->node)
      {
         if (PRVEMIT->curr->node->sibling())
            return true;
      }

      PRVEMIT->stack->last();
      PRVEMIT->stack->prev();

      _pivateDataNodeEmitter::NodeStack *parent = PRVEMIT->stack->current();

      PRVEMIT->stack->last();

      if (parent)
      {
         if (parent->list)
            return parent->idx + 1 < parent->list->count();

         return false;
      }
   }

   return false;
}


bool DataNode::Emitter::inList() const
{
   if (PRVEMIT->curr)
      return PRVEMIT->curr->list != 0;

   return false;
}

DataNode::Emitter::Action DataNode::Emitter::emit()
{
   _pivateDataNodeEmitter *e = PRVEMIT;

   if (e->root == 0)
      return Emitter::error;

   if (e->curr == 0)
      return Emitter::done;

   _pivateDataNodeEmitter::NodeStack *curr = e->curr;

   for (;;)
   {
      if (curr->action == Emitter::none)
      {
         if (curr->list)
         {
            DataVar *dv = curr->list->get(curr->idx);

            if (dv == 0)
            {
               curr->action = Emitter::element;
               continue;
            }

            if (dv->valueType() == DataVar::tl_list)
            {
               curr         = e->push(0);
               curr->list   = (DataVar::List *) dv->valuePtr();
               curr->action = Emitter::listOpen;
               break;
            }

            if (dv->valueType() == DataVar::tl_dataNode)
            {
               curr         = e->push((DataNode *) dv->valuePtr());
               curr->action = Emitter::objOpen;
               break;
            }

            curr->action = Emitter::element;
            break;
         }

         if (curr->node)
         {
            curr->action = Emitter::objOpen;
            break;
         }

         throw xMettle(__FILE__, __LINE__, MODULE, "Emitter error: unexpected null node.");
      }

      if (curr->action == Emitter::element)
      {
         if (curr->list)
         {
            curr->idx++;

            if (curr->idx >= curr->list->count())
            {
               curr->action = Emitter::listClose;
               break;
            }

            curr->action = Emitter::none;
            continue;
         }

         if (curr->node && curr->node->sibling())
         {
            curr->node   = curr->node->sibling();
            curr->list   = 0;
            curr->idx    = 0;

            if (curr->node->child() || curr->node->value() == 0)
               curr->action = Emitter::none;
            else
               curr->action = Emitter::objOpen;

            continue;
         }

         if (!(curr = e->pop()))
            break;

         if (curr->list)
         {
            curr->action = Emitter::element;
            continue;
         }

         curr->action = Emitter::objClose;
         break;
      }

      if (curr->action == Emitter::objOpen)
      {
         if (curr->node->child())
         {
            curr = e->push(curr->node->child());

            if (curr->node->child())
               continue;
         }

         if (curr->node->value())
         {
            if (curr->node->value()->valueType() == DataVar::tl_list)
            {
               const void *tmp    = curr->node->value()->valuePtr();

               curr         = e->push(curr->node);
               curr->list   = (DataVar::List *) tmp;
               curr->idx    = 0;
               curr->action = Emitter::listOpen;
            }
            else
               curr->action = Emitter::element;

            break;
         }

         curr->action = Emitter::objClose;
         break;
      }

      if (curr->action == Emitter::objClose)
      {
         curr->action = Emitter::element;
         continue;
      }

      if (curr->action == Emitter::listOpen)
      {
         if (curr->list->count() < 1)
         {
            curr->action = Emitter::listClose;
            break;
         }

         curr->action = Emitter::none;
         continue;
      }

      if (curr->action == Emitter::listClose)
      {
         if (!(curr = e->pop()))
            break;

         curr->action = Emitter::element;
         continue;
      }

      throw xMettle(__FILE__, __LINE__, MODULE, "Emitter error: unhandled action [%d].", curr->action);
   }

   if (!curr)
      return Emitter::done;

   return curr->action;
}

#undef PRVEMIT

#undef MODULE

}}
