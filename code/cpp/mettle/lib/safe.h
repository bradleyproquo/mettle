/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_SAFE_H_
#define __METTLE_LIB_SAFE_H_


/** \def DECLARE_SAFE_CLASS(cls)
 * \brief A utility macro to more easily declare a Safe class
 *
 * This macro should generally be used in the public section of any class that requires a
 * Safe class.  This is in line with mettle standard of adding helper classes as sub classes
 * to the base class.
 */
#define DECLARE_SAFE_CLASS(cls) class Safe:public Mettle::Lib::Safe<cls> { public: Safe(cls *o) : Mettle::Lib::Safe<cls>(o){}}


namespace Mettle { namespace Lib {

/** \class Safe safe.h mettle/lib/safe.h
 * \brief A template class used for automated class pointer management
 *
 *  This template class is meant to simplify the management and cleaning
 *  up of dynamically allocated objects.
 *  The use of this template class also encourages developers to returned new objects to
 *  calling code, so long as the calling code is also making use of this class.
 *
 *  The final purpose of this template class is to
 *  entirely remove the need for try and catch when using dynamically created objects by
 *  using the stack.
 *
 *  Please note that any classes implementing the 'Safe' template should perform all
 *  resource cleanup in their respective destructor.
 */
template <class type> class Safe
{
public:

   /** \brief Constructor, takes a (new) instance of the object that will be managed.
    *
    * The \p me object that is passed in is typically a newed instance of the inherited
    * class.  Though there is no reason why an existing pointer that you want managed
    * cannot be used, just be sure to delete this object twice.
    * \param me a pointer to an instance of the inherited class.  NULL is acceptable if no object is readily avaialable.
    */
    Safe(type *me) noexcept;

   /** \brief Destructor will automatically delete the managed object.
    *
    * This intent of this class is to automatically delete it's managed object when it
    * falls out of scope on the stack.  If the member \p obj is NOT NULL than the destructor
    * will delete it.
    */
   virtual ~Safe() noexcept;

   /** \brief Assigns a new instance to be managed by this Safe class.
    *
    * This method acts almoast identically to the constructor itself.  The newed or assigned
    * will be assigned to this safe class.
    *
    * \param me a pointer to an instance of the inherited class.
    * \return the \p me pointer for conveniance
    * \warning Note that if an existing instance object '\p obj' is already being managed, that that
    * object will first be deleted before the new \p me object is assigned.
    */
   type *remember(type *me);

   /** \brief Unassigns without deleting the member \p obj.
    *
    * This method is used primarliy for return an object at the end of a function/method call.
    * The instance object stored in the Safe class will be forgotten and returned, thus the calling code
    * that receives the forgotten instance is now responsible for deleting that object.
    *
    * Good practice would be receive that forgotten object into another Safe class.
    *
    * \return the forgotten memember \p obj.  The member \p obj is set NULL and will not be deleted by the destructor.
    */
   type *forget();

   /** \brief The instance object that will be automatically deleted when this class falls out of scope.
    *
    * This member is publically available so that it might be used by the calling code.
    *
    * \warning This member should not be manually deleted, or assigned, instead use the remember & forget methods.
    */
   type           *obj;

private:

};


template <class type>
Safe<type>::Safe(type *me) noexcept
{
   obj = me;
}


template <class type>
Safe<type>::~Safe() noexcept
{
   if (obj)
   {
      delete obj;
      obj = 0;
   }
}


template <class type>
type *Safe<type>::remember(type *me)
{
   if (obj)
      delete obj;

   obj = me;

   return obj;
}


template <class type>
type *Safe<type>::forget()
{
   type *tmp = obj;

   obj = 0;

   return tmp;
}

}}

#endif
