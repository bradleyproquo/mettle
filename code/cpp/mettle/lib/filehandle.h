/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_FILEHANDLE_H_
#define __METTLE_LIB_FILEHANDLE_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include <stdio.h>

namespace Mettle { namespace Lib {

class DateTime;
class MemoryBlock;

/** \class FileHandle filehandle.h mettle/lib/filehandle.h
 * \brief A useful class for reading & writting a file.
 *
 *  Note that the destructor will close the file handle if it is open.
 */
class FileHandle
{
public:

   /** \class xFileHandle filehandle.h mettle/lib/filehandle.h
    * \brief Read and write exceptions thrown by FileHandle
    *
    */
   class xFileHandle : public xMettle
   {
   public:
      xFileHandle(const char *file, const int line, const char *fmt, ...) noexcept;
      xFileHandle(const xFileHandle &x) noexcept;
   };


   /** \brief File open mode
    *
    * Mode enumeration is used to determine in what mode the file will be opened
    */
   typedef enum
   {
      Binary,  /**< Binary mode is for read/writting non human readable files, ie bitmaps. */
      Text     /**< Text mode is use for read/writting human readable text files. */
   } Mode;

   /** \brief Default and only constructor.
    */
   FileHandle() noexcept;

   /** \brief Closes the internal file handle if it is open.
    */
   virtual         ~FileHandle() noexcept;

   /** \brief Opens the specified file in the specified mode
    *
    * This method will attempt to open the specified file, if the file does not exist, or if the
    * user does not have read access to the file an exceptionwill be thrown.
    *
    * \param fileName is a null terminated string pointing to the file that is to be opened.
    * \param mode determines in what mode the file will be opened.
    * \returns itself for convenience
    * \throw xMettle String memory allocation exception
    * \throw FileHandle::xFileHandle File read access exception.
    */
   FileHandle  &open(const char *fileName,
                     const Mode  mode);

   /** \brief Creates the specified file in the specified mode
    *
    * This method will attempt to create the specified file, if the already exists it will be
    * over-written.  If the user does not have write access to the file/path an exceptionwill be thrown.
    *
    * \param fileName is a null terminated string pointing to the file that is to be opened.
    * \param mode determines in what mode the file will be created.
    * \returns itself for convenience
    * \throw xMettle String memory allocation exception
    * \throw FileHandle::xFileHandle File write access exception.
    */
   FileHandle  &create(const char  *fileName,
                       const Mode   mode);

   /** \brief Returs the length of a specified file.
    *
    *  \return the length/size of the file in bytes.
    *  \warning this method returns the file pointer position to the start of the file after completetion
    *  \throw xMettle String memory allocation exception.
    *  \throw FileHandle::xFileHandle File read access exception.
    */
   unsigned int getLength();

   /** \brief Writes a formatted string to the file
    *
    *  This method works like the standard c 'fprinft' function.
    *
    *  \param fmt a null terminated format string.
    *  \param ... the arguements for the format string, see 'printf'
    *  \warning This function should be used for files open in Text Mode.
    *  \throw FileHandle::xFileHandle File write exception.
    */
   void writef(const char *fmt, ...);

   /** \brief Writes a plain string to the file.
    *
    *  \param str a null terminated string.
    *  \warning This function should be used for files created in Text Mode.
    *  \throw FileHandle::xFileHandle File write exception.
    */
   void write(const char *str);

   /** \brief Attempts to read the contents of the file into the destination.
    *
    *  This function will attempt to read the contents of the file into \p dest to
    *  the length specified by \p len.  If the function reaches the end of file before
    *  this occurs it returns the number of bytes it could read.  An optional \p throwOnError
    *  parameter is provided if the calling code would prefer the function to throw and
    *  exception if the read operation failed.
    *
    *  \param dest a void pointer preallocted with enough space to, or at least equal to the length of \p len.
    *  \param len the number of bytes to be read from the file into \p dest.
    *  \param throwOnError an optional boolean that is set to true will throw an xFileHandle exception if the entire length of \p cannot be read.
    *  \return the actual length of bytes read.  If function was successful this value will equal \p len.
    *  \throw FileHandle::xFileHandle File write exception.
    */
   unsigned int read(      void         *dest,
                     const unsigned int  len,
                     const bool          throwOnError = true);

   /** \brief Attempts to read the contents of the file into the destination.
    *
    *  This function will attempt to read the contents of the file into \p dest to
    *  the length specified by \p dest->Size().  If the function reaches the end of file before
    *  this occurs it returns the number of bytes it could read.  An optional \p throwOnError
    *  parameter is provided if the calling code would prefer the function to throw and
    *  exception if the read operation failed.
    *
    *  \param dest a memory block object preallocted with enough space to, or at least equal to the length of \p len.
    *  \param throwOnError an optional boolean that is set to true will throw an xFileHandle exception if the entire length of \p cannot be read.
    *  \return the actual length of bytes read.  If function was successful this value will equal \p dest->Size().
    *  \throw FileHandle::xFileHandle File write exception.
    */
   unsigned int read(      MemoryBlock  *dest,
                     const bool          throwOnError = true);

   /** \brief Attempts to read a full line into the destination buffer
    *
    *  This function will attempt to read a single line into the destionation buffer.
    *  If the line length is greater than \p len, then it only reads up length specified by \p len.
    *
    *  \param dest a void pointer preallocted with enough space to, or at least equal to the length of \p len.
    *  \param len the maximum number of bytes to be read into \p dest.
    *  \return false if not bytes were read, else returns true.
    *  \warning This function should be used for files open in Text Mode.
    *  \throw FileHandle::xFileHandle File write exception.
    */
   bool readLine(      char *dest,
                 const int   len);

   /** \brief Attempts to read a full line into the destination String object
    *
    *  This function will attempt to read a single line into the destionation string object.
    *  If the line length is greater than 4096, the method will only read 4096 characters at a time.
    *
    *  \param dest a reference to where the destioation line is to be stored.
    *  \return false if no bytes were read, else returns true.
    *  \warning This function should be used for files open in Text Mode.
    *  \throw FileHandle::xFileHandle File write exception.
    */
   bool readLine(String &dest);

   /** \brief Write the contents of source to the file.
    *
    *  This function will attempt to write the \p source buffer to file.  If the entire
    *  length specified by \p len cannot be written an exception will be thrown.
    *
    *  \param source the source memory to be written to the file.
    *  \param len the size of the source memory to be written.
    *  \warning This function should be used for files open in Binary Mode.
    *  \throw FileHandle::xFileHandle File write exception.
    */
   void  write(const void         *source,
               const unsigned int  len);

   /** \brief Write the contents of source to the file.
    *
    *  This function will attempt to write the \p source memory block to file.  If the entire
    *  length specified by \p source->Size() cannot be written an exception will be thrown.
    *
    *  \param source the source memory block to be written to the file.
    *  \throw FileHandle::xFileHandle File write exception.
    */
   void  write(const MemoryBlock *source);

   /** \brief Returns if the internal file handle is at the end of the file.
    *
    *  \return true if the the object is at the end of the file, else returns false.
    */
   bool endOfFile() const noexcept;

   /** \brief Manually close the file handle.
    *
    *  Note that the optional parameter \p checkForCloseError can turn off file close exceptions being
    *  thrown.  If this parameter is left as true, the class will throw an exception if there was an error
    *  close thing file.  It should be mentioned that the destructor calls this method with a false arguement
    *  to prevent the destructor from throwing exceptions.
    *
    *  \param checkForCloseError an optional parameter to toggle file close checking
    *  \throw FileHandle::xFileHandle file close error.
    */
   void close(const bool checkForCloseError = true);

   /** \brief Allows access to the raw file pointer.
    *
    *  \return the raw file pointer, NULL if not file is currently open.
    */
   FILE* filePointer() const noexcept;

   /** \brief Takes ownership of the specified file pointer
    *
    *  \param fptr the new file pointer that will now be owned by the object.
    *  \param fileName is a null termianted string of the file name belonging to \p fptr.
    *  \warning If there is existing file pointer open it will first be closed.
    */
   void eat(FILE* fptr, const char *fileName);

   /** \brief Forgets the file pointer, leaaving it to calling code.
    *
    *  \return the raw file pointer, NULL if not file is currently open.
    *  \warning It is up to the calling code to close the 'forgotten' file pointer.
    */
   FILE* poop() noexcept;

   /** \brief Moves the file position to the start of the file
    *  \returns itself for convenience
    *  \throw FileHandle::xFileHandle file access error.
    */
   FileHandle &seekStart();

   /** \brief Moves the file position to the end of the file.
    *  \returns itself for convenience.
    *  \throw FileHandle::xFileHandle file access error.
    */
   FileHandle &seekEnd();

   /** \brief Moves the file position forward or backward from current position.
   *   \param offset the amount of bytes to move forward or backward from the current position.
    *  \returns itself for convenience
    *  \throw xFileHandle if the seek was to large, or the seek failed
    */
   FileHandle &seekOffset(const long int offset);

   /** \brief Gets the current file position
    *  \returns itself the current file posittion
    */
   long int tell() noexcept;

   /** \brief Returns the file name of the open file pointer.
    *
    *  \return the current file pointer file name.  If not file is open the string will be empty.
    */
   const String &fileName() const noexcept;

  private:

    FILE     *_fptr;
    String    _fileName;
};


}}

#endif
