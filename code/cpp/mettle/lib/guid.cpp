/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/guid.h"
#include "mettle/lib/common.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

namespace Mettle { namespace Lib {

#define SOURCE    "Mettle::Guid"

Guid::Guid() noexcept
{
   uuid_clear(_guid);
}

Guid::Guid(const Guid &inGuid) noexcept
{
   uuid_copy(_guid, inGuid._guid);
}

Guid::Guid(const uuid_t inGuid)
{
   uuid_copy(_guid, inGuid);
}

Guid::Guid(const char *inGuid, const bool throwError)
{
   _from(inGuid, throwError);
}

Guid::Guid(const String &inGuid, const bool throwError)
{
   _from(inGuid.c_str(), throwError);
}

Guid::~Guid() noexcept
{
}

bool Guid::validGuid(const char *inGuid) noexcept
{
   uuid_t tmp;

   return 0 == uuid_parse(inGuid, tmp);
}

bool Guid::validGuid(const String &inGuid) noexcept
{
   uuid_t tmp;

   return 0 == uuid_parse(inGuid.c_str(), tmp);
}

void Guid::_from(const char *inGuid, const bool throwError)
{
   if (!*inGuid)
   {
      uuid_clear(_guid);
      return;
   }

   if (0 != uuid_parse(inGuid, _guid))
      throw xMettle(__FILE__, __LINE__, SOURCE, "'%s' is not a valid GUID", inGuid);
}

Guid Guid::newGuid(int timeSafe) noexcept
{
   uuid_t ng;

   if (timeSafe < 1)
      uuid_generate(ng);
   else
   {
      int rc = 0;

      do
      {
         if (0 != (rc = uuid_generate_time_safe(ng)))
            Common::sleepMili(timeSafe);

      } while (rc != 0);
   }

   return Guid(ng);
}

char *Guid::toString(char *dest, bool forceUpperCase, bool forceLowerCase) const noexcept
{
   if (isNull())
      strcpy(dest, String::empty());
   else if (forceUpperCase)
      uuid_unparse_upper(_guid, dest);
   else if (forceLowerCase)
      uuid_unparse_lower(_guid, dest);
   else
      uuid_unparse(_guid, dest);

   return dest;
}

Guid &Guid::operator = (const Guid &inGuid) noexcept
{
   uuid_copy(_guid, inGuid._guid);
   return *this;
}

Guid &Guid::operator = (const uuid_t inGuid) noexcept
{
   uuid_copy(_guid, inGuid);
   return *this;
}

Guid &Guid::operator = (const char *inGuid)
{
   _from(inGuid, true);
   return *this;
}

Guid &Guid::operator = (const String &inGuid)
{
   _from(inGuid.c_str(), true);
   return *this;
}

int Guid::compare(const Guid *inGuid) const  noexcept
{
   return uuid_compare(_guid, inGuid->_guid);
}

int Guid::_compare(const void *obj) const
{
   return compare((const Guid *) obj);
}

bool Guid::isNull() const noexcept
{
   return uuid_is_null(_guid);
}

void Guid::nullify() noexcept
{
   uuid_clear(_guid);
}

void Guid::clear() noexcept
{
   uuid_clear(_guid);
}

Guid &Guid::parse(const char *inGuid)
{
   _from(inGuid, true);
   return *this;
}

void Guid::assign(const uuid_t inGuid) noexcept
{
   uuid_copy(_guid, inGuid);
}

bool operator <  (const Guid &guid, const Guid &cmpGuid) noexcept
{
   return uuid_compare(guid._guid, cmpGuid._guid) < 0;
}

bool operator >  (const Guid &guid, const Guid &cmpGuid) noexcept
{
   return uuid_compare(guid._guid, cmpGuid._guid) > 0;
}

bool operator <= (const Guid &guid, const Guid &cmpGuid) noexcept
{
   return uuid_compare(guid._guid, cmpGuid._guid) <= 0;
}

bool operator >= (const Guid &guid, const Guid &cmpGuid) noexcept
{
   return uuid_compare(guid._guid, cmpGuid._guid) >= 0;
}

bool operator == (const Guid &guid, const Guid &cmpGuid) noexcept
{
   return uuid_compare(guid._guid, cmpGuid._guid) == 0;
}

bool operator != (const Guid &guid, const Guid &cmpGuid) noexcept
{
   return uuid_compare(guid._guid, cmpGuid._guid) != 0;
}

std::ostream &operator << (std::ostream &output, const Guid &inGuid) noexcept
{
   char jot[38];

   uuid_unparse(inGuid._guid, jot);

   output << jot;

   return output;
}

std::istream &operator >> (std::istream &input, Guid &outGuid)
{
   char *p;

   input >> p;

   outGuid._from(p, true);

   return input;
}

#undef SOURCE

}}
