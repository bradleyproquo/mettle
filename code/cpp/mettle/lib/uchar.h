/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_UCHAR_H_
#define __METTLE_LIB_UCHAR_H_

#include "mettle/lib/collection.h"
#include "mettle/lib/c99standard.h"
#include "mettle/lib/icomparable.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

/** \class UChar uchar.h mettle/lib/uchar.h
 * \brief A UChar, UTF-8, class object.
 *
 *  This the standard utf-8 character handling object in Mettle.
 */
class UChar : public IComparable
{
public:

   DECLARE_COLLECTION_LIST_CLASS(UChar);

   /** \brief Defualt constructor.
    */
   UChar() noexcept;

   /** \brief Copy construct.
    *  \param cp the value to copy.
    */
   UChar(const UChar &cp) noexcept;

   /** \brief Construct from an ascii char.
    *  \param val the value that represents the utf-8 character.
    */
   UChar(const char val) noexcept;

   /** \brief Construct from an unsigned 32 bit integer.
    *  \param val the value that represents the utf-8 character.
    */
   UChar(const uint32_t val) noexcept;

   /** \brief Destructor.
    */
   ~UChar() noexcept;

   /** \brief Implemented pure virtual function for ICompare interface.
    *  \param obj the string to compare to, note it is assumed obj is in fact a pointer to a valid UChar object.
    *  \returns 0 if the uchars match, -1 if this uchars is less than \p obj, +1 if this uchar is greater than \p obj.
    */
   int _compare(const void *obj) const;

   /** \brief Gets the value of this utf-8 character.
    *  \returns the utf8 char value.
    */
   uint32_t value() const { return _char; }

   /** \brief Converts this object to uppercase.
    *  \returns Itself for convenience.
    *  \warning This method does not work yet, place holder for now.
    */
   UChar &lower() noexcept;

   /** \brief Converts this object to lowercase.
    *  \returns Itself for convenience.
    *  \warning This method does not work yet, place holder for now.
    */
   UChar &upper() noexcept;

   /** \brief Operator overload for = to assign the date from an existing UChar.
    *  \param val the uchar to become.
    *  \returns itself for convenience.
    */
   UChar & operator = (const UChar &val) noexcept;

   /** \brief Operator overload for = to assign the date from an ascii character.
    *  \param val the char to become.
    *  \returns itself for convenience.
    */
   UChar & operator = (const char val) noexcept;

   /** \brief Operator overload for = to assign the date from an unsinged 32 integer.
    *  \param val the uchar to become.
    *  \returns itself for convenience.
    */
   UChar & operator = (const uint32_t val) noexcept;

   /** \brief Less than friend operator overload between two utf-8 chars.
    *  \param ch the first utf-8 char to compare.
    *  \param cmp the second utf-8 char to compare.
    *  \returns true if \p ch is less than \p cmp.
    */
   friend bool operator <  (const UChar &ch, const UChar &cmp) noexcept;

   /** \brief Greater than friend operator overload between two utf-8 chars.
    *  \param ch the first utf-8 char to compare.
    *  \param cmp the second utf-8 char to compare.
    *  \returns true if \p ch is greater than \p cmp.
    */
   friend bool operator >  (const UChar &ch, const UChar &cmp) noexcept;

   /** \brief Less Equal than friend operator overload between two utf-8 chars.
    *  \param ch the first utf-8 char to compare.
    *  \param cmp the second utf-8 char to compare.
    *  \returns true if \p ch is less than or eqaul to \p cmp.
    */
   friend bool operator <= (const UChar &ch, const UChar &cmp) noexcept;

   /** \brief Greater Equal than friend operator overload between two utf-8 chars.
    *  \param ch the first utf-8 char to compare.
    *  \param cmp the second utf-8 char to compare.
    *  \returns true if \p ch is greater than or equal to \p cmp.
    */
   friend bool operator >= (const UChar &ch, const UChar &cmp) noexcept;

   /** \brief Equals friend operator overload between two utf-8 chars.
    *  \param ch the first utf-8 char to compare.
    *  \param cmp the second utf-8 char to compare.
    *  \returns true if \p ch equals \p cmp.
    */
   friend bool operator == (const UChar &ch, const UChar &cmp) noexcept;

   /** \brief Not Equals friend operator overload between two utf-8 chars.
    *  \param ch the first utf-8 char to compare.
    *  \param cmp the second utf-8 char to compare.
    *  \returns true if \p ch does not equal \p cmp.
    */
   friend bool operator != (const UChar &ch, const UChar &cmp) noexcept;

   /** \brief Less than friend operator overload between two utf-8 char and an ascii char.
    *  \param ch the utf-8 char to compare.
    *  \param cmp the ascii char to compare.
    *  \returns true if \p ch is less than \p cmp.
    */
   friend bool operator <  (const UChar &ch, const char cmp) noexcept;

   /** \brief Greater than friend operator overload between two utf-8 char and an ascii char.
    *  \param ch the utf-8 char to compare.
    *  \param cmp the ascii char to compare.
    *  \returns true if \p ch is greater than \p cmp.
    */
   friend bool operator >  (const UChar &ch, const char cmp) noexcept;

   /** \brief Less Equal than friend operator overload between two utf-8 char and an ascii char.
    *  \param ch the utf-8 char to compare.
    *  \param cmp the ascii char to compare.
    *  \returns true if \p ch is less than or eqaul to \p cmp.
    */
   friend bool operator <= (const UChar &ch, const char cmp) noexcept;

   /** \brief Greater Equal than friend operator overload between two utf-8 char and an ascii char.
    *  \param ch the utf-8 char to compare.
    *  \param cmp the ascii char to compare.
    *  \returns true if \p ch is greater than or equal to \p cmp.
    */
   friend bool operator >= (const UChar &ch, const char cmp) noexcept;

   /** \brief Equals friend operator overload between two utf-8 char and an ascii char.
    *  \param ch the utf-8 char to compare.
    *  \param cmp the ascii char to compare.
    *  \returns true if \p ch equals \p cmp.
    */
   friend bool operator == (const UChar &ch, const char cmp) noexcept;

   /** \brief Not Equals friend operator overload between two utf-8 char and an ascii char.
    *  \param ch the utf-8 char to compare.
    *  \param cmp the ascii char to compare.
    *  \returns true if \p ch does not equal \p cmp.
    */
   friend bool operator != (const UChar &ch, const char cmp) noexcept;

   /** \brief Checks if a char * utf-8 sequence is an ascii char.
    *  \param ch the utf-8 sequence to check.
    *  \returns true if \p ch is a pure ascii sequence.
    */
   static bool u8_is_ascii(const char *ch) noexcept;

   /** \brief Checks if a char * utf-8 sequence is a utf-8 encode char.
    *  \param ch the utf-8 sequence to check.
    *  \returns true if \p ch is a pure utf-8 encoded sequence.
    */
   static bool u8_is_utf8(const char ch) noexcept;

   /** \brief Checks if a local is utf-8.
    *  \param local optionally pass in the local, else it is determined from 'setlocale()'.
    *  \returns true if the local is utf-8.
    */
   static bool u8_local_is_utf8(const char *local = 0) noexcept;

   /** \brief Get the string length of a utf-8 char array.
    *  \param str the utf-8 encoded string,
    *  \returns the length, number of utf-8 encode characters in the string.
    */
   static unsigned int u8_strlen(const char *str) noexcept;

   /** \brief Same as the sandard strstr(), but takes utf-8 encoded chracters into acount.
    *  \param src the source string to search inside of.
    *  \param find the sub string to match/find inside the source string.
    *  \returns the pointer to the first occurrence of \p find in \p src, or 0 if not found.
    */
   static const char *u8_strstr(const char *src, const char *str2) noexcept;

   /** \brief Same as the sandard strstr(), but takes utf-8 encoded chracters into acount.
    *  \param src the source string to search inside of.
    *  \param find the sub string to match/find inside the source string.
    *  \returns the pointer to the first occurrence of \p find in \p src, or 0 if not found.
    */
   static char *u8_strstr(char *src, const char *str2) noexcept;

   /** \brief Gets the next utf-8 char from the current string from the specified index.
    *  \param str the utf-8 endcoded string in question.
    *  \param idx the current position in the string, this value is incremented by the actual number of bytes it took to read the utf8 char.
    *  \returns a unsigned 32 bit integer representing the utf-8 char read, returns 0 when if the end of string is reached.
    */
   static uint32_t u8_next_char(const char *str, unsigned int &idx) noexcept;

   /** \brief Increment an indexing integer by utf-8 char forwards through the utf-8 encoded string.
    *  \param s the utf-8 encode in question to increment through.
    *  \param i the indexing integer to increment,  Will be increaded by the actuall number of bytes it takes to read one utf-8 char.
    *  \returns the new value of \p i.
    */
   static unsigned int u8_inc(const char *s, unsigned int &i) noexcept;

   /** \brief Decrement an indexing integer by utf-8 char backwards through the utf-8 encoded string.
    *  \param s the utf-8 encode in question to decrement through.
    *  \param i the indexing integer to decrement,  Will be increaded by the actuall number of bytes it takes to read one utf-8 char.
    *  \returns the new value of \p i, note when 0 is reached you have reached the start of the string.  Subsequent calls could has undefined behaviour.
    */
   static unsigned int u8_dec(const char *s, unsigned int &i) noexcept;

   /** \brief Increment a char pointer forwards through a utf-8 encoded string.
    *  \param s the pointer in the string to increment by one utf-8 char.
    *  \returns the new value of \p s. Note than 0 is returned when the end of the string is reached.
    */
   static char *u8_inc(char *&s) noexcept;

   /** \brief Decrement a char pointer backwards through a utf-8 encoded string.
    *  \param s the pointer in the string to decrement by one utf-8 char.
    *  \param i an index representing the current offset of the \p s relative to the start of the string.
    *  \returns the new value of \p s. Note travelling beyond the start has undefined behaviour.
    */
   static char *u8_dec(char *&s, unsigned int &i) noexcept;

private:

   uint32_t  _char;

};


}}

#endif
