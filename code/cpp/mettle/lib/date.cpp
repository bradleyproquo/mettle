/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/date.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "mettle/lib/datetime.h"

namespace Mettle { namespace Lib {

const char *Date::_weekDays[] =
{
   "Sunday",
   "Monday",
   "Tuesday",
   "Wednesday",
   "Thursday",
   "Friday",
   "Saturday"
};

const char *Date::_months[] =
{
   "January",
   "February",
   "March",
   "April",
   "May",
   "June",
   "July",
   "August",
   "September",
   "October",
   "November",
   "December"
};

const int8_t Date::_monthEnds[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

#define MODULE    "Mettle::Date"
#define STD_FMT   "%Y-%m-%d"


Date::Date() noexcept
{
   _from();
}


Date::Date(const unsigned int year,
           const unsigned int month,
           const unsigned int day,
           const bool         throwErrors)
{
   _from(year, month, day, throwErrors);
}


Date::Date(const struct tm &inDate, const bool throwErrors)
{
   _from(&inDate, throwErrors);
}



Date::Date(const Date &inDate) noexcept
{
   memcpy(this, &inDate, sizeof(Date));
}


Date::Date(const char *yyyymmdd, const bool throwErrors)
{
   _from(yyyymmdd, throwErrors);
}


Date::Date(const int32_t yyyymmdd, const bool throwErrors)
{
   if (yyyymmdd == 0)
      nullify();
   else
   {
      int y, m, d;

      y  = (int)(yyyymmdd * 0.0001);
      m  = ((int)(yyyymmdd * 0.01)) - (y * 100);
      d  = yyyymmdd - ((y * 10000) + (m * 100));

      _from(y, m, d, throwErrors);
   }
}


Date::~Date() noexcept
{
}


void Date::_from(const struct tm *tmStruct, const bool throwErrors)
{
   _year           = (uint16_t) tmStruct->tm_year + 1900;
   _month          = (uint8_t)  tmStruct->tm_mon;
   _day            = (uint8_t)  tmStruct->tm_mday;

   _validate(throwErrors);
}


void Date::_from(const unsigned int year,
                const unsigned int month,
                const unsigned int day,
                const bool         throwErrors)
{
   _year     = (uint16_t) year;
   _month    = (uint8_t)  month - 1;
   _day      = (uint8_t)  day;

   if (throwErrors)
      _validate();
}


void Date::_from(const char *yyyymmdd, const bool throwErrors)
{
   if (strlen(yyyymmdd) < 8)
      throw xMettle(__FILE__, __LINE__, MODULE, "Invalid date string passed '%s'", yyyymmdd);

   char jotter[9];

   memcpy(jotter, yyyymmdd, 8);
   jotter[8] = 0;

   _day      = atoi(jotter + 6);
   jotter[6] = 0;

   _month    = atoi(jotter + 4) - 1;
   jotter[4] = 0;

   _year     = atoi(jotter);

   if (throwErrors)
      _validate();
}


void Date::_from() noexcept
{
   _day          = 0;
   _month        = 0;
   _year         = 0;
}


bool Date::_validate(const bool throwErrors)
{
   if (_year > 9999)
      if (throwErrors)
         throw xMettle(__FILE__, __LINE__, MODULE, "'%u' is not a valid year", _year);
      else
         return false;

   if (_month > 11)
      if (throwErrors)
         throw xMettle(__FILE__, __LINE__, MODULE, "'%u' is not a valid month", _month + 1);
      else
         return false;

   if (_day > daysInMonth())
      if (throwErrors)
         throw xMettle(__FILE__, __LINE__, MODULE, "'%u' is not a valid day", _day);
      else
         return false;

   return true;
}


bool Date::validDate(const unsigned int year, const unsigned int month, const unsigned int day) noexcept
{
    return Date(year, month, day, false)._validate(false);
}


bool Date::validDate(const char *yyyymmdd)  noexcept
{
   return Date(yyyymmdd, false)._validate(false);
}


bool Date::validDate(const int32_t yyyymmdd) noexcept
{
   return Date(yyyymmdd, false)._validate(false);
}


Date & Date::operator = (const Date &inDate) noexcept
{
   memcpy(this, &inDate, sizeof(*this));
   return *this;
}


Date & Date::operator = (const struct tm &inDate)
{
   _from(&inDate);
   return *this;
}


Date & Date::operator = (const char *yyyymmdd)
{
   _from(yyyymmdd);
   return *this;
}


Date & Date::operator = (const int32_t yyyymmdd)
{
   if (yyyymmdd == 0)
      nullify();
   else
   {
      int y, m, d;

      y = (int)(yyyymmdd * 0.0001);
      m = ((int)(yyyymmdd * 0.01)) - (y * 100);
      d = yyyymmdd - ((y * 10000) + (m * 100));

      _from(y, m, d);
   }

   return *this;
}


const Date & Date::operator ++ () noexcept
{
   add(1, Days);
   return *this;
}


const Date & Date::operator -- () noexcept
{
   sub(1, Days);
   return *this;
}


Date Date::operator ++ (int) noexcept
{
   Date temp = *this;
   add(1, Days);
   return temp;
}


Date Date::operator -- (int) noexcept
{
   Date temp = *this;
   sub(1, Days);
   return temp;
}


Date &Date::operator += (const unsigned int numDays) noexcept
{
   add(numDays, Days);
   return *this;
}


Date &Date::operator -= (const unsigned int numDays) noexcept
{
   sub(numDays, Days);
   return *this;
}


Date &Date::add(unsigned int amount, const PeriodLegend legend) noexcept
{
   switch (legend)
   {
      case Weeks :
         amount *= 7;

      case Days :
         {
            unsigned int leap_check = (_month == 1 && isLeapYear(_year)) ? 1 : 0;

            for (; amount; amount--)
               if (_day == (uint8_t) (_monthEnds[_month] + leap_check))
               {
                  _day = 1;
                  if (_month == 11)
                  {
                     _month = 0;
                     _year++;
                  }
                  else
                     _month++;

                  leap_check = (_month == 1 && isLeapYear(_year)) ? 1 : 0;
               }
               else
                  _day++;
         }
         break;

      case Months :
         for (; amount; amount--)
            if (_month == 11)
            {
               _month = 0;
               _year++;
            }
            else
               _month++;

         // Adjust day is differant month end date
         if (_day > daysInMonth())
         {
            _day = _monthEnds[_month];
            if (_month == 1 && isLeapYear(_year))
               _day++;
         }
         break;

      case Years :
         _year += amount;

         // Check for the February hazard when doing years
         if (_month == 1 && _day == 29 && !isLeapYear(_year))
            _day = 28;
         break;
   }

   return *this;
}


Date &Date::sub(unsigned int amount, const PeriodLegend legend) noexcept
{
   switch (legend)
   {
      case Weeks :
         amount *= 7;

      case Days :
         for (; amount; amount--)
            if (_day == 1)
            {
                // check month
                if (_month)
                   _month--;
                else
                {
                   _month = 11;
                   _year--;
                }
                _day = _monthEnds[_month] + ((_month == 1 && isLeapYear(_year)) ? 1 : 0);
            }
            else
               _day--;
         break;

      case Months :
         for (; amount; amount--)
            if (_month)
               _month--;
            else
            {
               _month = 11;
               _year--;
            }

         // Adjust day is differant month end date
         if (_day > _monthEnds[_month])
         {
            _day = _monthEnds[_month];
            if (_month == 1 && isLeapYear(_year))
               _day++;
         }

         break;

      case Years :
         _year -= amount;

         // Check for the February hazard when doing years
         if (_month == 1 && _day == 29 && !isLeapYear(_year))
            _day = 28;
         break;
   }

   return *this;
}


char *Date::format(char *dest, const char *fmt) const noexcept
{
   return DateTime(*this).format(dest, fmt);
}


String &Date::format(String &dest, const char *fmt) const
{
   return DateTime(*this).format(dest, fmt);
}


bool Date::isLeapYear(const unsigned int year) noexcept
{
   if (!(year % 400))
      return true;

   if (!(year % 100))
      return false;

   if (!(year % 4))
      return true;

   return false;
}


unsigned int Date::daysInMonth(const unsigned int year, const unsigned int month)
{
   if (month > 11 || year > 9999)
      throw xMettle(__FILE__, __LINE__, MODULE, "Arguements out of range.");

   return _monthEnds[month] + ((month == 1 && isLeapYear(year)) ? 1 : 0);
}

unsigned int Date::daysInMonth() const noexcept
{
   return _monthEnds[_month] + ((_month == 1 && isLeapYear(_year)) ? 1 : 0);
}


unsigned int Date::dayOfYear() const noexcept
{
   struct tm tm_rec;

   memset(&tm_rec, 0, sizeof(tm_rec));

   tm_rec.tm_year  = _year - 1900;
   tm_rec.tm_mon   = _month;
   tm_rec.tm_mday  = _day;

#if defined(WINCE)
   _mktime64(&tm_rec);
#else
   mktime(&tm_rec);
#endif

   return (unsigned int) tm_rec.tm_yday;
}


char *Date::toString(char *yyyymmdd) const noexcept
{
   if (isNull())
      yyyymmdd[0] = 0;
   else
      sprintf(yyyymmdd, "%4.4d%2.2d%2.2d", _year, _month + 1, _day);

   return yyyymmdd;
}


int32_t Date::toInt32() const noexcept
{
   if (isNull())
      return 0;

   return (_year * 10000) + ((_month + 1) * 100) + _day;
}


int32_t Date::julianDate() const noexcept
{
   unsigned int short year;
   unsigned int short day;
   unsigned int short month;
   unsigned int short centuary;
   unsigned int short cy;

   year = _year < 1 ? 1 : _year;
   day  = (unsigned int short) _day;

   if (_month + 1 > 2)
      month = _month - 2;
   else
   {
      month = _month + 10;
      year--;
   }

   centuary = (unsigned int short) (year * 0.01);
   cy       = year - 100 * centuary;

   return (int32_t) ((146097 * centuary * 0.25)  +
                     (1461   * cy       * 0.25)  +
                     ((153   * month + 2) / 5)   +
                     day                         +
                     1721119);
}


Date::DayOfWeekLegend Date::dayOfWeek() const noexcept
{
   struct tm tm_rec;

   memset(&tm_rec, 0, sizeof(tm_rec));

   tm_rec.tm_year  = _year - 1900;
   tm_rec.tm_mon   = _month;
   tm_rec.tm_mday  = _day;

#if defined(WINCE)
   _mktime64(&tm_rec);
#else
   mktime(&tm_rec);
#endif

   return (DayOfWeekLegend) tm_rec.tm_wday; // 0 = Sunday, .... 6 = Saturday
}


Date Date::now() noexcept
{
#if defined(WINCE)

   __time64_t    now;
   struct tm     tm_rec;

   _time64(&now);
   _localtime64_s(&tm_rec, &now);

   return Date(tm_rec, false);

#else

   time_t      now;
   struct tm  *tm_rec;

   time(&now);
   tm_rec = localtime(&now);

   return Date(*tm_rec, false);

#endif
}


Date &Date::setDay(const unsigned int day)
{
   _day = (uint8_t) day;

   _validate();

   return *this;
}


Date &Date::setMonth(const unsigned int month)
{
   _month = (uint8_t) month;

   _validate();

   return *this;
}


Date &Date::setYear(const unsigned int year)
{
   _year = year;

   _validate();

   return *this;
}


bool Date::isNull() const noexcept
{
   return (_year == 0 && _month == 0 && _day == 0);
}


bool Date::isLeapYear() noexcept
{
   return isLeapYear(_year);
}

void Date::nullify() noexcept
{
   _year  = 0;
   _month = 0;
   _day   = 0;
}


void Date::clear() noexcept
{
   nullify();
}


void Date::assign(const unsigned int year, const unsigned int month, const unsigned int day)
{
   _from(year, month, day, true);
}


int Date::compare(const Date *cmpDate) const noexcept
{
   if (_year < cmpDate->_year)
      return -1;

   if (_year > cmpDate->_year)
      return +1;

   if (_month < cmpDate->_month)
      return -1;

   if (_month > cmpDate->_month)
      return +1;

   if (_day < cmpDate->_day)
      return -1;

   if (_day > cmpDate->_day)
      return +1;

   return 0;
}


int Date::_compare(const void *obj) const
{
   return compare((const Date *) obj);
}


bool operator < (const Date &date1, const Date &date2) noexcept
{
   return date1.compare(&date2) < 0;
}


bool operator > (const Date &date1, const Date &date2) noexcept
{
   return date1.compare(&date2) > 0;
}


bool operator <= (const Date &date1, const Date &date2) noexcept
{
   return date1.compare(&date2) <= 0;
}


bool operator >= (const Date &date1, const Date &date2) noexcept
{
   return date1.compare(&date2) >= 0;
}


bool operator == (const Date &date1, const Date &date2) noexcept
{
   return date1.compare(&date2) == 0;
}


bool operator != (const Date &date1, const Date &date2) noexcept
{
   return date1.compare(&date2) != 0;
}


Date::operator struct tm () const noexcept
{
   struct tm tm_rec;

   memset(&tm_rec ,0, sizeof(struct tm));

   tm_rec.tm_year  = _year - 1900;
   tm_rec.tm_mon   = _month;
   tm_rec.tm_mday  = _day;

   return tm_rec;
}


const char *Date::stdDateFormat()
{
   return STD_FMT;
}


std::ostream &operator << (std::ostream &output, const Date &dt) noexcept
{
   char jot[16];

   dt.format(jot, STD_FMT);

   output << jot;

   return output;
}


std::istream &operator >> (std::istream &input, Date &dt)
{
   char     *p;
   DateTime  tmp;

   input >> p;

   tmp.parse(p, STD_FMT);

   dt = tmp.date;

   return input;
}

#undef STD_FMT
#undef MODULE

}}
