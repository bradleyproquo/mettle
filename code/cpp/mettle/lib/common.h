/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_COMMON_H_
#define __METTLE_LIB_COMMON_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

#include <uuid/uuid.h>

#if defined (OS_AIX) || defined (OS_LINUX)

#include <unistd.h>

#endif

#include <stdlib.h>


namespace Mettle { namespace Lib {

/** \class Common common.h mettle/lib/common.h
 *  \brief A static class with commonly used static functions.
 */
class Common
{
public:

   /** \brief Trims spaces off the back of the target string starting at the size position.
    *  \param str the c string to trim.
    *  \param size the size of \p str.
    *  \returns \p str for convenience.
    */
   static char *trim(char *str, const unsigned int size) noexcept;

   /** \brief Does a byte swap of the integer 16 value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static int16_t swapBytes(int16_t v) noexcept;

   /** \brief Does a byte swap of the integer 32 value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static int32_t swapBytes(int32_t v) noexcept;

   /** \brief Does a byte swap of the integer 64 value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static int64_t swapBytes(int64_t v) noexcept;

   /** \brief Does a byte swap of the unsigned integer 16 value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static uint16_t swapBytes(uint16_t v) noexcept;

   /** \brief Does a byte swap of the unsigned integer 32 value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static uint32_t swapBytes(uint32_t v) noexcept;

   /** \brief Does a byte swap of the unsigned integer 64 value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static uint64_t swapBytes(uint64_t v) noexcept;

   /** \brief Does a byte swap of the double value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static double swapBytes(double v) noexcept;

   /** \brief Does a byte swap of the float value.
    *  \param v the value.
    *  \returns \p v for convenience.
    */
   static float swapBytes(float v) noexcept;

   /** \brief Compares two int 8 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const int8_t x, const int8_t y) noexcept;

   /** \brief Compares two int 16 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const int16_t x, const int16_t y) noexcept;

   /** \brief Compares two int 32 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const int32_t x, const int32_t y) noexcept;

   /** \brief Compares two int 64 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const int64_t x, const int64_t y) noexcept;

   /** \brief Compares two unsigned int 8 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const uint8_t x, const uint8_t y) noexcept;

   /** \brief Compares two unsigned int 16 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const uint16_t x, const uint16_t y) noexcept;

   /** \brief Compares two unsigned int 32 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const uint32_t x, const uint32_t y) noexcept;

   /** \brief Compares two unsigned int 64 types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const uint64_t x, const uint64_t y) noexcept;

   /** \brief Compares two unsigned double types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const double x, const double y) noexcept;

   /** \brief Compares two float types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const float x, const float y) noexcept;

   /** \brief Compares two uuid_t types.
    *  \param x the first value to compare.
    *  \param y the second value to compare.
    *  \returns -1 if \p is less than \p y, 0 if \p x == \p y or, +1 if \p x is greater than \p y
    */
   static int compare(const uuid_t x, const uuid_t y) noexcept;

   /** \brief Sleep the current process/thead for a defined period of seconds.
    *  \param seconds the number of seconds to sleep for.
    */
   static void sleep(const int time)  noexcept;

   /** \brief Sleep the current process/thead for a defined period of mili seconds.
    *  \param mili the number of mili seconds to sleep for.
    */
   static void sleepMili(const int time) noexcept;

   /** \brief Copy one string into another and ensure a null terminator at position \p size.
    *  \param dest the destination string.
    *  \param src the source string to copy from.
    *  \param size the maximum size of \p dest.
    *  \return \p dest for convenience.
    */
   static char *stringCopy(char *dest, const char *src, const int32_t size) noexcept;

   /** \brief Allocates a block of memory of the required size.  Raises an exception if out of memory.
    *  \param size in bytes of the memory block to allocate.
    *  \returns the pointer to the allocated memory.  This must be free'd by the calling process.
    *  \throws an xMettle exception if out of memory.
    */
   static void *memoryAlloc(const unsigned int size);

   /** \brief Calc the tenth power.
    *  \param n the numeric value to calc.
    *  \returns the tenth power of \p n.
    */
   static double tenPow(int n);

   /** \brief Round a double to the specified decimal places.
    *  \param val the value to round.
    *  \param decimals the number of decimals to round to.
    *  \returns the rounded value.
    */
   static double round(const double val, const int decimals = 2);

   /** \brief Truncate a double to the specified decimal places.
    *  \param val the value to truncate.
    *  \param decimals the number of decimals to round to.
    *  \returns the truncated value.
    */
   static double truncate(const double val, const int decimals = 2);

   /** \brief Rounds a double with banker rules to the specified decimal place.
    *  \param val the value to round.
    *  \param decimals the number of decimals to round to.
    *  \returns the rounded value.
    */
   static double roundBanker(double val, int decimals = 2);

   /** \brief Rounds a double symmetrically to the specified decimal place.
    *  \param val the value to round.
    *  \param decimals the number of decimals to round to.
    *  \returns the rounded value.
    */
   static double roundSymmetric(double val, int decimals = 2);

   /** \brief Rounds and the truncate a double to the specified decimal place.
    *  \param val the value to round and truncate.
    *  \param decimals the number of decimals to round and truncate to.
    *  \returns the rounded and truncated value.
    */
   static double roundTruncated(double val, int decimals = 2);

   /** \brief Check if a string is a pure unsigned digit.
    *  \param str the string the check.
    *  \returns true if the str is a pure unsigned digit.
    */
   static bool stringIsUnsigned(const char *str) noexcept;

   /** \brief Check if a string is a pure digit.
    *  \param str the string the check.
    *  \returns true if the str is a pure digit.
    */
   static bool stringIsInt(const char *str) noexcept;

   /** \brief Check if a string is a pure double.
    *  \param str the string the check.
    *  \returns true if the str is a pure double.
    */
   static bool stringIsDouble(const char *str) noexcept;

   /** \brief Check if a string is a uuid.
    *  \param str the string the check.
    *  \param checklen by default this also checks the string length, to not do this check set to false.
    *  \returns true if the str is a uuid.
    */
   static bool stringIsUUID(const char *str, bool checkLen = true) noexcept;


#if defined(OS_MS_WINDOWS)

   /** \brief Gets the process id of the current process.
    *  \return the process id of the current proceess.
    */
   static int getPID() noexcept;

   #if COMPILER_MSC > 6

      static void *bsearch(const void *key, const void *base, size_t num, size_t width, int (*compare)(const void *, const void *));
      static void  qsort(void *base, unsigned num, unsigned width, int (*comp)(const void *, const void *));

   #endif

#else

   static pid_t getPID() noexcept;

#endif

private:

   Common();
  ~Common();
};

}}

#endif
