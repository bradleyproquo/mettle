/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_LOGGER_STDFILE_H_
#define __METTLE_LIB_LOGGER_STDFILE_H_

#include <stdarg.h>
#include <stdio.h>

#include "mettle/lib/logger.h"

namespace Mettle { namespace Lib {

/** \class LoggerStdFile loggerstdfile.h mettle/lib/loggerstdfile.h
 * \brief Logs program information to a specified file.
 *
 * This is the most basic an often used logging class in servers or
 * batch programs.
 */
class LoggerStdFile : public Logger
{
public:

   /** \brief Constructor.
    *
    * If the destination log does not exist, it is created.  If it does exist the file
    * is appended.
    *
    * \param logFile a null termianted string point to the destination log file.
    * \param level the logging level to use.
    * \param display if true the LoggerStdFile will also print to the standard out.
    */
   LoggerStdFile(const char  *logFile,
                 const Level       level,
                 bool  display = true);

   /** \brief Desturctor, closes the log file if it is open.
   */
   virtual ~LoggerStdFile() noexcept;

   /** \brief Implements interface method.
    */
   virtual void error(const char *fmt, ...) noexcept;

   /** \brief Implements interface method.
    */
   virtual void warning(const char *fmt, ...) noexcept;

   /** \brief Implements interface method.
    */
   virtual void info(const char *fmt, ...)  noexcept;

   /** \brief Implements interface method.
    */
   virtual void debug(const char *fmt, ...) noexcept;

   /** \brief Implements interface method.
    */
   virtual Logger *trace(const char *file, const unsigned int line) noexcept;

protected:

   bool          _display;
   FILE         *_logFH;

   virtual void    _getPrefix(const char *lvl, char *prefix, int *prefixLen) noexcept;
   virtual void    _logPrefix(const char *prefix, const int prefixLen, FILE *fp) noexcept;
   virtual void    _log(const char *fmt, va_list ap, FILE *fp) noexcept;

};

}}

#endif
