/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_DATAVAR_H_
#define __METTLE_LIB_DATAVAR_H_

#define __STDC_FORMAT_MACROS 1

#include "mettle/lib/c99standard.h"
#include "mettle/lib/collection.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/string.h"
#include "mettle/lib/ustring.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

class DataNode;
class Date;
class DateTime;
class Time;
class Guid;

/** \class DataVar datavar.h mettle/lib/datavar.h
 * \brief A generic data tree node object.
 *
 * This object is used to store a generic data type.  Supported datatypes are
 * c strings, String, Ustring, all the integers, double, float, DataNode, and Date objects.
 *
 * The main purpose of this class is to store a data value of any form while loading items
 * from a configuration file, database query, etc.
 */
class DataVar
{
public:

   /** \class Safe datavar.h mettle/lib/datavar.h
    * \brief The safe class implementation.
    */
   class Safe : public Mettle::Lib::Safe<DataVar>
   {
   public:
      Safe(DataVar *o) : Mettle::Lib::Safe<DataVar>(o) {}
   };

   /** \class List datavar.h mettle/lib/datavar.h
    * \brief The list class implementation.
    */
   class List : public Collection<DataVar>
   {
   public:
      List() noexcept;
      virtual ~List() noexcept;

      /** \class Safe datavar.h mettle/lib/datavar.h
       * \brief The safe class implementation.
       */
      class Safe : public Mettle::Lib::Safe<List>
      {
      public:
         Safe(List *o) : Mettle::Lib::Safe<List>(o) {}
      };

   };

   typedef enum
   {
      tl_null,
      tl_string,
      tl_ustring,
      tl_c_string,
      tl_bool,
      tl_char,
      tl_int8,
      tl_int16,
      tl_int32,
      tl_int64,
      tl_uint8,
      tl_uint16,
      tl_uint32,
      tl_uint64,
      tl_double,
      tl_float,
      tl_date,
      tl_time,
      tl_datetime,
      tl_memoryBlock,
      tl_list,
      tl_dataNode,
      tl_guid
   } TypeLegend;


   /** \brief Destructor.
    */
   virtual ~DataVar() noexcept;

   /** \brief Default constructor.  Note the default type will be set to Null.
    */
   DataVar() noexcept;

   /** \brief Constructor setting the data node's name and value.
    *  \param tleg the data var type that this DataNode will automatically attempt to type cast to.
    */
   DataVar(const TypeLegend tleg);

   /** \brief Destroys the object, settings its type back to Null.
    */
   void destroy() noexcept;

   /** \brief Clears the value, does not delete/destroy it.
    */
   void clear() noexcept;

   /** \brief Gets the value pointer.
    *  \returns the value pointer, the user will have to use the valueType() method to determine how to type cast it.
    */
   const void *valuePtr() const noexcept;

   /** \brief Gets the node type.
    *  \returns the nodes type.
    */
   const TypeLegend valueType() const noexcept;

   /** \brief Checks if the value is null.
    *  \returns true if the value is a null.
    */
   const bool valueIsNull() const noexcept;

   /** \brief Checks if the value if empty or not.
    *  \returns true if the value is a null or is empty/zero, else return true
    */
   const bool valueIsEmpty() const noexcept;

   /** \brief Checks if the value is some kind of integer.
    *  \returns true if the value is an integer type.
    */
   const bool valueTypeIsInteger() const noexcept;

   /** \brief Checks if the value is some kind of string.
    *  \returns true if the value is a string type.
    */
   const bool valueTypeIsString() const noexcept;

   /** \brief Set the value type, if the type changes and is compatable with the type it is destroyed.
    *  \throws an xMettle exception if out of memory.
    */
   void valueSetType(const TypeLegend tleg) ;

   /** \brief Attepts to convert value to an integer.
    *  \returns int representing the value type.
    *  \throws an xMettle exception if out of memory.
    */
   int toInteger() const;

   /** \brief If the type is a string, attempt to become the best data type, ie int, bool, etc.
    *  \returns the new TypeLegend, or the same TypeLegend if it could not change.
    *  \throws an xMettle exception if out of memory.
    */
   TypeLegend toBestType();

   /** \brief Attepts to convert value to a string.
    *  \returns String representing the value type.
    *  \throws an xMettle exception if out of memory.
    */
   Mettle::Lib::String toString() const;

   /** \brief Attepts to convert value to a string.
    *  \param dest the target reference string that will be filled with the value.
    *  \returns String that was set.
    *  \throws an xMettle exception if out of memory.
    */
   Mettle::Lib::String &toString(Mettle::Lib::String &dest) const;

   /** \brief If the value type is a c_str, String, or UString return the char *.
    *  \returns the char * if the value type is c_str, String, or UString, else returns null.
    */
   const char *c_str() const noexcept;

   /** \brief Same as destroy.
    */
   void nullify() noexcept;

   /** \brief Reads the node as a String.
    *  \param val the String object to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(Mettle::Lib::String &val);

   /** \brief Reads the node as a UString.
    *  \param val the UString object to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(Mettle::Lib::UString &val);

   /** \brief Reads the node as a Date.
    *  \param val the Date object to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(Mettle::Lib::Date &val);

   /** \brief Reads the node as a Time.
    *  \param val the Time object to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(Mettle::Lib::Time &val);

   /** \brief Reads the node as a DateTime.
    *  \param val the DateTime object to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(Mettle::Lib::DateTime &val);

   /** \brief Reads the node as a Guid.
    *  \param val the Guid object to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(Mettle::Lib::Guid &val);

   /** \brief Reads the node as a char.
    *  \param val the char to read into.
    *  \returns true if the value was read successfully.
    */
   bool read(char &val) noexcept;

   /** \brief Reads the node as a bool.
    *  \param val the bool to read into.
    *  \returns true if the value was read successfully.
    */
   bool read(bool &val) noexcept;

   /** \brief Reads the node as a int8.
    *  \param val the int8 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(int8_t &val);

   /** \brief Reads the node as a int16.
    *  \param val the int16 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(int16_t &val);

   /** \brief Reads the node as a int32.
    *  \param val the int32 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(int32_t &val);

   /** \brief Reads the node as a int64.
    *  \param val the int64 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(int64_t &val);

   /** \brief Reads the node as a uint8.
    *  \param val the uint8 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(uint8_t &val);

   /** \brief Reads the node as a uint16.
    *  \param val the uint16 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(uint16_t &val);

   /** \brief Reads the node as a uint32.
    *  \param val the uint32 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(uint32_t &val);

   /** \brief Reads the node as a uint64.
    *  \param val the uint64 to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(uint64_t &val);

   /** \brief Reads the node as a float.
    *  \param val the float to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(float &val);

   /** \brief Reads the node as a double.
    *  \param val the double to read into.
    *  \returns true if the value was read successfully.
    *  \throws an xMettle exception if out of memory.
    */
   bool read(double &val);

   /** \brief Writes the value from a String.
    *  \param val the String object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const Mettle::Lib::String &val);

   /** \brief Writes the value from a UString.
    *  \param val the UString object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const Mettle::Lib::UString &val);

   /** \brief Writes the value from a Date.
    *  \param val the Date object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const Mettle::Lib::Date &val);

   /** \brief Writes the value from a Time.
    *  \param val the Time object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const Mettle::Lib::Time &val);

   /** \brief Writes the value from a DateTime.
    *  \param val the DateTime object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const Mettle::Lib::DateTime &val);

   /** \brief Writes the value from a MemoryBlock.
    *  \param val the MemoryBlock object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const Mettle::Lib::MemoryBlock &val);

   /** \brief Writes the value from a List.
    *  \param val the List object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const DataVar::List &val);

   /** \brief Writes the value from a DataNode.
    *  \param val the DataNode object to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const Mettle::Lib::DataNode &val);

   /** \brief Writes the value from a char *.
    *  \param val the char * to write.
    *  \throws an xMettle exception if out of memory.
    */
   void write(const char *val);

   /** \brief Writes the value from a bool.
    *  \param val the bool to write.
    */
   void write(const bool val) noexcept;

   /** \brief Writes the value from a char.
    *  \param val the char to write.
    */
   void write(const char val) noexcept;

   /** \brief Writes the value from a int8_t.
    *  \param val the int8_t to write.
    */
   void write(const int8_t val) noexcept;

   /** \brief Writes the value from a int16_t.
    *  \param val the int16_t to write.
    */
   void write(const int16_t val) noexcept;

   /** \brief Writes the value from a int32_t.
    *  \param val the int32_t to write.
    */
   void write(const int32_t val) noexcept;

   /** \brief Writes the value from a int64_t.
    *  \param val the int64_t to write.
    */
   void write(const int64_t val) noexcept;

   /** \brief Writes the value from a uint8_t.
    *  \param val the uint8_t to write.
    */
   void write(const uint8_t val) noexcept;

   /** \brief Writes the value from a uint16_t.
    *  \param val the uint16_t to write.
    */
   void write(const uint16_t val) noexcept;

   /** \brief Writes the value from a uint32_t.
    *  \param val the uint32_t to write.
    */
   void write(const uint32_t val) noexcept;

   /** \brief Writes the value from a uint64_t.
    *  \param val the uint64_t to write.
    */
   void write(const uint64_t val) noexcept;

   /** \brief Writes the value from a float.
    *  \param val the float to write.
    */
   void write(const float val) noexcept;

   /** \brief Writes the value from a double.
    *  \param val the double to write.
    */
   void write(const double val) noexcept;

   /** \brief Writes the value from a guid.
    *  \param val the guid to write.
    */
   void write(const Mettle::Lib::Guid &val) noexcept;

   /** \brief Detects what the best strong type the string is.
    *  \param val the string to evaluate.
    *  \param dest an optional parameter, if not null \p dest will automatically be assignedthe best value.
    *  \param valLen, optional parameter as to how much of the string to evalauate.
    *  \returns the TypeLegend that ths string should be.
    */
   static TypeLegend detectBestType(const char *val, DataVar *dest = 0, unsigned int valLen = 0);

   /** \brief Detects what the best strong type the string is.
    *  \param val the string to evaluate.
    *  \param dest an optional parameter, if not null \p dest will automatically be assignedthe best value.
    *  \param valLen, optional parameter as to how much of the string to evalauate.
    *  \returns the TypeLegend that ths string should be.
    */
   static TypeLegend detectBestType(const Mettle::Lib::String &val, DataVar *dest = 0);

protected:

   void _initType(const TypeLegend tleg);

   static bool _uintPart(uint64_t &out, const char *src, const uint64_t len, const uint64_t min, const uint64_t max);

   TypeLegend  _tleg;

   union
   {
      void                     *_void;
      char                     *_c_string;
      bool                      _bool;
      char                      _c_char;
      int8_t                    _int8;
      int16_t                   _int16;
      int32_t                   _int32;
      int64_t                   _int64;
      uint8_t                   _uint8;
      uint16_t                  _uint16;
      uint32_t                  _uint32;
      uint64_t                  _uint64;
      float                     _float;
      double                    _double;
      Mettle::Lib::String      *_string;
      Mettle::Lib::UString     *_ustring;
      Mettle::Lib::Date        *_date;
      Mettle::Lib::Time        *_time;
      Mettle::Lib::DateTime    *_datetime;
      Mettle::Lib::MemoryBlock *_memoryBlock;
      Mettle::Lib::Guid        *_guid;
      List                     *_list;
      DataNode                 *_dataNode;
   } _value;
};


}}

#endif
