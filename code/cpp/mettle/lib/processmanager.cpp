/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/processmanager.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/common.h"
#include "mettle/lib/logger.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined(OS_LINUX) || defined (OS_AIX)

   #include <unistd.h>
   #include <errno.h>

#elif defined (OS_MS_WINDOWS)

   #include <windows.h>
   #include <direct.h>

#else
   #error UNKNOWN OPERATING SYSTEM
#endif

namespace Mettle { namespace Lib {

#define MODULE "ProcessManager"

ProcessManager::ProcessManager() noexcept
{
}

ProcessManager::~ProcessManager() noexcept
{
   killChildren(3);

   _childProcs.purge();
}

unsigned int ProcessManager::numChildren() noexcept
{
   unsigned int cnt = 0;

   for (_childProcs.start(); _childProcs.current(); _childProcs.next())
      cnt++;

   return cnt;
}


void ProcessManager::houseKeeping()
{
   ChildProc *child;

   _childProcs.start();

   while (_childProcs.current())
   {
      child = _childProcs.current();

      if (child->completed)
      {
         _childProcs.next();
         continue;
      }

#if defined(OS_LINUX) || defined (OS_AIX)

      switch (waitpid(child->pid, &child->rc, WNOHANG))
      {
         case 0 :
            break;

         case -1:
            _childProcs.remove();
            continue;

         default :
            child->completed = 1;
            break;
      }

#elif defined (OS_MS_WINDOWS)

      DWORD rc = 0;

      if (!GetExitCodeThread(child->procInfo.hThread, &rc))
      {
         CloseHandle(child->procInfo.hThread);
         CloseHandle(child->procInfo.hProcess);
         child->completed = 1;
         child->rc        = 0;
      }
      else
      {
         if (rc != STILL_ACTIVE)
         {
            CloseHandle(child->procInfo.hProcess);
            CloseHandle(child->procInfo.hThread);
            child->completed = 1;
            child->rc        = rc;
         }
      }

#endif

      _childProcs.next();
   }
}

bool ProcessManager::fetchChildExit(ChildProc &childRec) noexcept
{
   ChildProc *child;

   for (_childProcs.start(); _childProcs.current(); _childProcs.next())
   {
      child = _childProcs.current();

      if (child->completed)
      {
         childRec = *child;
         _childProcs.remove();
         return true;
      }
   }

   return false;
}

ProcessManager::ChildProc *ProcessManager::spawnChild(const char *path,
                                                      const char *args,
                                                      const char *workingDir,
                                                      const bool  forceSigKill,
                                                      const bool  allowRunAway)
{
   ChildProc *child;

   child = _childProcs.add(new ChildProc());

   child->extPtr       = 0;
   child->forceSigKill = forceSigKill;
   child->allowRunAway = allowRunAway;

   String::safeCopy(child->name, path, sizeof(child->name));

#if defined(OS_LINUX) || defined (OS_AIX)

   pid_t pid;

   pid = fork();

   if (pid == 0)
   {
      char         *tmpArgs[256];
      String        tmp(args);
      String::List  arglist;
      unsigned int  idx;

      tmp.split(' ', arglist);

      memset(tmpArgs, 0, sizeof(tmpArgs));

      tmpArgs[0] = (char *) path;

      for (idx = 0; idx < arglist.count() && idx < 255; idx++)
         tmpArgs[idx + 1] = (char *) arglist[idx].c_str();

      //try
      //{
         if (workingDir && chdir(workingDir) == -1)
            throw xMettle(__FILE__, __LINE__, MODULE, "Could not change dir to '%s'", workingDir);

         if (execv(path, (char * const *) tmpArgs) == -1)
            throw xMettle(__FILE__, __LINE__, MODULE, "Could not exec child process '%s'", path);
      //}
      /*catch (xMettle &x)
      {
         SLOG->Error("Error creating child process '%d' - %s", getpid(), x.what());
         SLOG->Trace(__FILE__, __LINE__, x);
         exit(1);
      }
      catch (std::exception &x)
      {
         SLOG->Error("Error creating child process '%d' - %s", getpid(), x.what());
         SLOG->Trace(__FILE__, __LINE__);
         exit(1);
      }
      catch (...)
      {
         SLOG->Error("Unknown exception caught creating child process '%d'", getpid());
         SLOG->Trace(__FILE__, __LINE__);
         exit(1);
      }*/
   }
   else if (pid == -1)
   {
      throw xMettle(__FILE__, __LINE__, MODULE, "Could not spawn child process '%s'", path);
   }

   child->pid = pid;

#elif defined (OS_MS_WINDOWS)

   STARTUPINFO   startInfo;
   char         *currDir = 0;
   String        cmdline(path);

   cmdline += " ";
   cmdline += args;

   GetStartupInfo(&startInfo);

   if (workingDir && *workingDir)
   {
      if ((currDir = _getcwd(0, 0)) == 0)
         throw xMettle(__FILE__, __LINE__, MODULE, "Could not get current working dir to '%s'", workingDir);

      if (_chdir(workingDir) != 0)
      {
         free(currDir);
         throw xMettle(__FILE__, __LINE__, MODULE, "Could not change dir to '%s'", workingDir);
      }
   }

   if (!CreateProcess(0,
                      (char *) cmdline.c_str(),
                      0,
                      0,
                      false,
                      DETACHED_PROCESS,
                      0,
                      workingDir,
                      &startInfo,
                      &child->procInfo))
   {
      if (currDir)
      {
         _chdir(currDir);
         free(currDir);
      }

      throw xMettle(__FILE__, __LINE__, MODULE, "Could not spawn child process '%s' - %d/%s", path);
   }

   if (currDir)
   {
      _chdir(currDir);
      free(currDir);
   }

   child->pid = child->procInfo.dwProcessId;

#endif

   return child;
}

void ProcessManager::killChildren(const int  secondsCountDown,
                                  const bool autoPurgeChildProcs,
                                  const bool killRunAways) noexcept
{
   ChildProc  childRec;
   ChildProc *child;
   int        countdown;
   bool       allRunAways;

   for (countdown = secondsCountDown; countdown > 0; countdown--)
   {
      allRunAways = true;

      for (_childProcs.start(); _childProcs.current(); _childProcs.next())
      {
         child = _childProcs.current();

         if (!killRunAways && child->allowRunAway)
            continue;

         allRunAways = false;
         terminate(child, countdown);
      }

      houseKeeping();

      while (fetchChildExit(childRec));

      _childProcs.start();

      if (!_childProcs.current())
         break;

      Common::sleep(1);
   }

   if (autoPurgeChildProcs)
      _childProcs.purge();
}

void ProcessManager::terminate(ChildProc *child,
                               const int  countdown) noexcept
{
   if (child->completed)
      return;

#if defined(OS_LINUX) || defined (OS_AIX)

   kill(child->pid, countdown == 1 && child->forceSigKill ? SIGKILL : SIGTERM);

#elif defined (OS_MS_WINDOWS)

   DWORD exitCode = 0;

   if (!TerminateProcess(child->procInfo.hProcess, exitCode) &&
       countdown == 1                                        &&
       child->forceSigKill)
   {
      child->completed = 1;
      child->rc        = (int) exitCode;

      CloseHandle(child->procInfo.hProcess);
      CloseHandle(child->procInfo.hThread);
   }

#endif
}

unsigned int ProcessManager::currentChildProcs(ChildProc::List &childList)
{
   ChildProc *child;

   for (_childProcs.start(); _childProcs.current(); _childProcs.next())
   {
      child = _childProcs.current();

      if (child->completed == false)
         childList.append(new ChildProc(child));
   }

   return childList.count();
}


ProcessManager::ChildProc::ChildProc()
{
   rc           = 0;
   completed    =
   forceSigKill =
   allowRunAway = false;
   extRef       = 0;
   pid          = 0;

#if defined (OS_MS_WINDOWS)
   PROCESS_INFORMATION  procInfo;
#endif
}

ProcessManager::ChildProc::ChildProc(const ChildProc *cp)
{
   name         = cp->name;
   rc           = cp->rc;
   completed    = cp->completed;
   forceSigKill = cp->forceSigKill;
   allowRunAway = cp->allowRunAway;
   extRef       = cp->extRef;
   pid          = cp->pid;

#if defined (OS_MS_WINDOWS)
   memcpy(&procInfo, &cp->procInfo, sizeof(PROCESS_INFORMATION));
#endif
}

#undef MODULE

}}
