/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_SEMAPHORE_H_
#define __METTLE_LIB_SEMAPHORE_H_

#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#if defined (OS_MS_WINDOWS)

   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>

#else

   #include <semaphore.h>

#endif

namespace Mettle { namespace Lib {

/** \class Semaphore semaphore.h mettle/lib/semaphore.h
 * \brief A helper object to manage a semaphores.
 */
class Semaphore
{
public:

   /** \brief Default constructor.
    *  \param name the file name of the semaphore to open/create/
    *  \parem createIfMissing if the set to true, the constructor will creaet the semaphore if it is missing, else it will raise an exception if it is missing.
    *  \throws xMettle if the semephore was missing or if out of memory.
    */
   Semaphore(const char *name, const bool createIfMissing = true);

   /** \brief Destructor.
    */
   ~Semaphore() noexcept;

   /** \brief Locks the semephore, waits infinately on windows, and 15 seconds on unix/linux.
     * \throws xSemephore if the semaphore could not be locked.
    */
   void lock();

   /** \brief Unlocks the semephore.
     * \param throwErrors if set to true, an exception will be thrown if the unlock fails.
     * \throws xSemephore if the semaphore could not be unlocked.
    */
   void unlock(const bool throwErrors = false);

   /** \brief Get the semaphore name.
     * \returns the semaphore name.
    */
   const String &name() const noexcept;

   /** \class Auto semaphore.h mettle/lib/semaphore.h
    * \brief A helper object to auto lock and releae a semaphore using the stack.
    */
   class Auto
   {
   public:

      /** \brief Constructor, locks the semaphore.
       *  \p semaphore the semaphore to be auto locked & unlocked.
       *  \throws xMettle if a semaphore could not be created or locked.
       */
      Auto(Semaphore *semaphore);

      /** \brief Destructor, unlocks the semaphore.
       */
     ~Auto() noexcept;

     private:

        Semaphore *_semaphore;
   };

private:

   String  _name;

#if defined (OS_MS_WINDOWS)

   HANDLE _semaphore;

#else

   sem_t *_semaphore;

#endif
};

}}

#endif
