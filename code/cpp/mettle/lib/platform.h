/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_PLATFORM_H_
#define __METTLE_LIB_PLATFORM_H_

/*

Supported c++ Compilers:
   Gnu         = COMPILER_GNU
   Xlc         = COMPILER_XLC
   vc6         = COMPILER_MSC = 6/7/8/9
   borlandc    = COMPILER_BORLAND

Supported Platforms:
   linux       = OS_LINUX
   unix        = OS_AIX
   ms windows  = OS_MS_WINDOWS

Supported Environment/Arch Sizes:
   32bit    = ENV_32
   64bit    = ENV_64

Supported Endians
   'l' or 'b'    = _ENDIANNESS
   little endian = _ENDIAN_LITTLE
   big endian    = _ENDIAN_BIG

Misc
   compiler cannot handle dupl types = _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   directory seperator               = _DIR_SEPERATOR
   line ending                       = _LINE_ENDING
   pack push                         = PACK_PUSH
   pack pop                          = PACK_POP
*/

/*-----------------------------------
   Check Compiler, then OS
-----------------------------------*/
#if defined(__GNUC__)

   #if __x86_64__ || __ppc64_
      #define ENV_64 1
   #else
      #define ENV_32
   #endif

   #if defined(__linux)
      #define OS_LINUX
   #elif defined(__unix)
      #define OS_AIX
   #elif defined(_WIN32) || defined(_WIN64)
      #define OS_MS_WINDOWS
   #endif

   #define COMPILER_GNU
   #define PACK_PUSH(s)  pack(s)
   #define PACK_POP      pack(pop)

#elif defined(__xlC__)

   #if __x86_64__ || __ppc64_
      #define ENV_64
   #else
      #define ENV_32
   #endif

   #if defined (__TOS_WIN__)
      #define OS_MS_WINDOWS
   #else
      #define OS_AIX
   #endif

   #define COMPILER_XLC
   #define PACK_PUSH(s)  pack(s)
   #define PACK_POP      pack(pop)

#elif defined (_MSC_VER)

   #define OS_MS_WINDOWS

   #if _WIN64
      #define ENV_64
   #elif _WIN32
      #define ENV_32
   #else
      #error Uknown Platform Size
   #endif

   #define PACK_PUSH(s)  pack(push, s)
   #define PACK_POP      pack(pop)

   #if _MSC_VER >= 1700
      #define COMPILER_MSC  9
      #define _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   #elif _MSC_VER >= 1500
      #define COMPILER_MSC  9
      #define _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   #elif _MSC_VER >= 1400
      #define COMPILER_MSC  8
      #define _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   #elif _MSC_VER >= 1300
      #define COMPILER_MSC  7
   #else
      #define COMPILER_MSC  6
   #endif

   #if _MSC_VER < 1900

       #define access    _access
       #define snprintf  _snprintf
       #define vsnprintf _vsnprintf
       #define strupr    _strupr
       #define strlwr    _strlwr

   #endif

#elif defined (__BORLANDC__)

   #if _WIN64
      #define ENV_64
   #elif _WIN32
      #define ENV_32
   #else
      #error Uknown Platform Size
   #endif

   #if defined(__linux)
      #define OS_LINUX
   #elif defined(__unix)
      #define OS_AIX
   #elif defined(_WIN32) || defined(_WIN64)
      #define OS_MS_WINDOWS
   #endif

   #define COMPILER_BORLAND
   #define PACK_PUSH(s)  pack(s)
   #define PACK_POP      pack(pop)

#endif


/*-----------------------------------
   Set Little/Big Endian
-----------------------------------*/
#if defined(OS_LINUX) || defined(OS_AIX)

   #include <endian.h>

   #if BYTE_ORDER == LITTLE_ENDIAN
      #define _ENDIANNESS      'l'  // little
      #define _ENDIAN_LITTLE
   #elif BYTE_ORDER == BIG_ENDIAN
      #define _ENDIANNESS       'b'  // big
      #define _ENDIAN_BIG
   #else
      #error Unsupprted endianness
   #endif

   #define _DIR_SEPERATOR    '/'
   #define _LINE_ENDING      "\n"

#elif defined(OS_MS_WINDOWS)

   #define _ENDIANNESS      'l'  // little
   #define _ENDIAN_LITTLE
   #define _DIR_SEPERATOR   '\\'
   #define _LINE_ENDING      "\r\n"


#elif defined(OS_MAC)

   #define _DIR_SEPERATOR    ':'
   #define _LINE_ENDING      "\n\r"
   #define _ENDIANNESS      'l'  // little
   #define _ENDIAN_LITTLE

#endif

#endif
