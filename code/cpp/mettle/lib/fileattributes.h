/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_FILEATTRIBUTES_H_
#define __METTLE_LIB_FILEATTRIBUTES_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include <stdio.h>


namespace Mettle { namespace Lib {

/** \class FileAttributes fileattributes.h mettle/lib/fileattributes.h
 * \brief Reads the attributes of a system file.
 *
 *  A class containing file attributes for a specified file.  The attributes
 *  available are the file name, file type, file size (bytes),
 *  , file last accessed, file last modified.
 *
 *  A child collection 'List' class is provided which provides helper methods
 *  which allow sortying by file name and sorting by last modified.
 */
class FileAttributes
{
public:

  /** \brief FileType enum.
   *
   * FileType enumeration is used to determine the file type.
   */
  typedef enum
   {
      Unknown,      /**< File is of unkown type. */
      Special,      /**< File is a special file. */
      Regular,      /**< File is a normal/regular file. */
      Directory,    /**< File is a directory. */
      SymbolicLink, /**< File is a symbolic link. */
      Socket        /**< File is a socket handle. */
   } FileType;

   /** \brief Throws an xMettle exception if the specified path is not valid or could not be read.
    * \param path a null terminated string representing the full path of the file.
    */
   FileAttributes(const char *path);

   /** \brief Destructor.
    */
  ~FileAttributes();

   /** \brief The full path of the file.
    *
    */
   String       fileName;

   /** \brief The file type, see FileType.
    *
    */
   FileType     fileType;

   /** \brief The file size in bytes.
    *
    */
   unsigned int fileSize;

   /** \brief The last date/time this file was read or executed.
    *
    */
   DateTime     lastAccessed;

   /** \brief The last date/time this was modified.
    *
    */
   DateTime     lastModified;

   /** \brief A collection class for the FileAttributes class.
    */
   class List : public Collection<FileAttributes>
   {
   public:

      /** \brief Sorts the collection by lastModified ascending.
       */
      void sortByLastModified();

      /** \brief Sorts the collection by fileName ascending.
       */
      void sortByFileName();
   };
};


}}

#endif
