/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/time.h"

#include "mettle/lib/date.h"
#include "mettle/lib/datetime.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


namespace Mettle { namespace Lib {

#define SOURCE    "Mettle::Time"
#define STD_FMT   "%H:%M:%S"


Time::Time() noexcept
{
   _from();
}


Time::Time(const Time &inTime) noexcept
{
   _hour    = inTime._hour;
   _minute  = inTime._minute;
   _second  = inTime._second;
}

Time::Time(const struct tm &inTime, const bool throwError)
{
   _from(&inTime,  throwError);
}


Time::Time(const unsigned int hours, const unsigned int minutes, const unsigned int seconds, const bool throwError)
{
   _from(hours, minutes, seconds, throwError);
}


Time::Time(const char *hhmmss, const bool throwError)
{
   _from(hhmmss, throwError);
}


Time::Time(const int32_t hhmmss, const bool throwError)
{
   if (hhmmss == 0)
      nullify();
   else
   {
      int h, m, s;

      h = (int)(hhmmss * 0.0001);
      m = ((int)(hhmmss * 0.01)) - (h * 100);
      s = hhmmss - ((h * 10000) + (m * 100));

      _from(h, m, s, throwError);
   }
}


Time::~Time() noexcept
{
}


bool Time::validate(const bool throwError)
{
   if (_hour > 23)
      if (throwError)
         throw xMettle(__FILE__, __LINE__, SOURCE, "'%u' is not a valid hour", _hour);
      else
        return false;

   if (_minute > 59)
      if (throwError)
         throw xMettle(__FILE__, __LINE__, SOURCE, "'%u' is not a valid minute", _minute);
      else
        return false;

   if (_second > 59)
      if (throwError)
         throw xMettle(__FILE__, __LINE__, SOURCE, "'%u' is not a valid second", _second);
      else
        return false;

   return true;
}


bool Time::validTime(const unsigned int hours, const unsigned int minutes, const unsigned int seconds) noexcept
{
   return Time(hours, minutes, seconds, false).validate(false);
}


bool Time::validTime(const char *hhmmss) noexcept
{
   return Time(hhmmss, false).validate(false);
}


bool Time::validTime(const int32_t hhmmss) noexcept
{
   return Time(hhmmss, false).validate(false);
}


void Time::_from(const struct tm *tm_struct, const bool throwError)
{
   _hour           = (uint8_t)  tm_struct->tm_hour;
   _minute         = (uint8_t)  tm_struct->tm_min;
   _second         = (uint8_t)  tm_struct->tm_sec;

   validate(throwError);
}


void Time::_from(const unsigned int hours, const unsigned int minutes, const unsigned int seconds, const bool throwError)
{
   _hour   = (uint8_t) hours;
   _minute = (uint8_t) minutes;
   _second = (uint8_t) seconds;

   validate(throwError);
}


void Time::_from(const char *hhmmss, const bool throwError)
{
   char *ptr = (char *) hhmmss;
   char  tmp[3];
   char  done = 0;

   while (*ptr)
   {
      if (done == 3)
      {
         validate(throwError);
         return;
      }

      if (ptr[0] >= '0' && ptr[0] <= '9')
      {
         if (ptr[1] < '0' || ptr[1] > '9')
            throw xMettle(__FILE__, __LINE__, SOURCE, "'%s' is not a valid time", hhmmss);

         tmp[0] = ptr[0];
         tmp[1] = ptr[1];
         tmp[2] = 0;

         switch (done++)
         {
            case 0 : _hour   = (uint8_t) atoi(tmp); ptr++;break;
            case 1 : _minute = (uint8_t) atoi(tmp); ptr++;break;
            case 2 : _second = (uint8_t) atoi(tmp); break;
         }
      }
      ptr++;
   }

   throw xMettle(__FILE__, __LINE__, SOURCE, "'%s' is not a valid time", hhmmss);
}


void Time::_from()
{
   _hour   = 0;
   _minute = 0;
   _second = 0;
}


Time Time::now() noexcept
{
#if defined(WINCE)

   __time64_t    now;
   struct tm     tm_rec;

   _time64(&now);
   _localtime64_s(&tm_rec, &now);

   return Date(tm_rec, false);

#else
   time_t      now;
   struct tm  *tm_rec;

   time(&now);
   tm_rec = localtime(&now);

   return Time(*tm_rec);
#endif
}


Time &Time::setHour(const unsigned int hour)
{
   _hour = (uint8_t) hour;

   validate();

   return *this;
}


Time &Time::setMinute(const unsigned int minute)
{
   _minute = (uint8_t) minute;

   validate();

   return *this;
}


Time &Time::setSecond(const unsigned int second)
{
   _second = (uint8_t) second;
   validate();

   return *this;
}


Time &Time::add(unsigned int        amount,
                const PeriodLegend  legend,
                Date               *date) noexcept
{
   switch (legend)
   {
      case Seconds :

         for (; amount; amount--)
            if (_second == 59)
            {
               _second = 0;

               if (_minute == 59)
               {
                  _minute  = 0;

                  if (_hour == 23)
                  {
                     _hour = 0;

                     if (date)
                        date->add(1, Date::Days);
                  }
                  else
                     _hour++;
               }
               else
                  _minute++;
            }
            else
               _second++;

         break;

      case Minutes :

         for (; amount; amount--)
            if (_minute == 59)
            {
               _minute = 0;

               if (_hour == 23)
               {
                  _hour = 0;

                  if (date)
                     date->add(1, Date::Days);
               }
               else
                  _hour++;
            }
            else
               _minute++;

         break;

      case Hours :

         for (; amount; amount--)
            if (_hour == 23)
            {
               _hour = 0;

               if (date)
                  date->add(1, Date::Days);
            }
            else
               _hour++;
   }

   return *this;
}


Time &Time::sub(unsigned int        amount,
                const PeriodLegend  legend,
                Date               *date) noexcept
{
   switch (legend)
   {
      case Seconds :

         for (; amount; amount--)
            if (_second == 0)
            {
               _second = 59;

               if (_minute == 0)
               {
                  _minute  = 59;

                  if (_hour == 0)
                  {
                     _hour = 59;

                     if (date)
                        date->sub(1, Date::Days);
                  }
               }
               else
                  _minute--;
            }
            else
               _second--;

         break;

      case Minutes :

         for (; amount; amount--)
            if (_minute == 0)
            {
               _minute = 59;

               if (_hour == 0)
               {
                  _hour = 23;

                  if (date)
                     date->sub(1, Date::Days);
               }
            }
            else
               _minute--;

         break;

      case Hours :

         for (; amount; amount--)
            if (_hour == 0)
            {
               _hour = 23;

               if (date)
                  date->sub(1, Date::Days);
            }
            else
               _hour--;
   }

   return *this;
}


char *Time::toString(char *hhmmss) const noexcept
{
   if (isNull())
      hhmmss[0] = 0;
   else
      sprintf(hhmmss, "%2.2d%2.2d%2.2d", _hour, _minute, _second);

   return hhmmss;
}


char *Time::format(char *dest, const char *fmt) const noexcept
{
    return DateTime(*this).format(dest, fmt);
}


String &Time::format(String &dest, const char *fmt) const
{
    return DateTime(*this).format(dest, fmt);
}


int32_t Time::toInt32() const noexcept
{
   if (isNull())
      return 0;

   return (_hour * 10000) + (_minute * 100) + _second;
}


Time &Time::operator = (const Time &inTime) noexcept
{
   memcpy(this, &inTime, sizeof(*this));
   return *this;
}


Time &Time::operator = (const struct tm &inTime)
{
   _from(&inTime);
   return *this;
}


Time &Time::operator = (const char *hhmmss)
{
   _from(hhmmss);
   return *this;
}


Time &Time::operator = (const int32_t hhmmss)
{
   if (hhmmss == 0)
      nullify();
   else
   {
      int h, m, s;

      h = (int)(hhmmss * 0.0001);
      m = ((int)(hhmmss * 0.01)) - (h * 100);
      s = hhmmss - ((h * 10000) + (m * 100));

      _from(h, m, s);
   }

   return *this;
}


Time &Time::operator += (const unsigned int seconds) noexcept
{
   add(seconds, Time::Seconds);
   return *this;
}


Time &Time::operator -= (const unsigned int seconds) noexcept
{
   sub(seconds, Time::Seconds);
   return *this;
}


const Time &Time::operator ++ () noexcept
{
   add(1, Seconds);
   return *this;
}


const Time &Time::operator -- () noexcept
{
   sub(1, Seconds);
   return *this;
}


Time Time::operator ++ (int) noexcept
{
   Time tmp = *this;
   add(1, Seconds);
   return tmp;
}


Time Time::operator -- (int) noexcept
{
   Time tmp = *this;
   sub(1, Seconds);
   return tmp;
}


int Time::compare(const Time *cmpTime) const  noexcept
{
   if (_hour < cmpTime->_hour)
      return -1;

   if (_hour > cmpTime->_hour)
      return +1;

   if (_minute < cmpTime->_minute)
      return -1;

   if (_minute > cmpTime->_minute)
      return +1;

   if (_second < cmpTime->_second)
      return -1;

   if (_second > cmpTime->_second)
      return +1;

   return 0;
}


int Time::_compare(const void *obj) const
{
   return compare((const Time *) obj);
}


bool Time::isNull() const noexcept
{
   return (_hour == 0 && _minute == 0 && _second == 0);
}


void Time::nullify() noexcept
{
   _hour   = 0;
   _minute = 0;
   _second = 0;
}


void Time::clear() noexcept
{
   nullify();
}


void Time::assign(const unsigned int hours, const unsigned int minutes, const unsigned int seconds)
{
   _from(hours, minutes, seconds, true);
}


bool operator <  (const Time &time1, const Time &cmpTime) noexcept
{
   return time1.compare(&cmpTime) < 0;
}


bool operator >  (const Time &time1, const Time &cmpTime) noexcept
{
   return time1.compare(&cmpTime) > 0;
}


bool operator <= (const Time &time1, const Time &cmpTime) noexcept
{
   return time1.compare(&cmpTime) <= 0;
}


bool operator >= (const Time &time1, const Time &cmpTime) noexcept
{
   return time1.compare(&cmpTime) >= 0;
}


bool operator == (const Time &time1, const Time &cmpTime) noexcept
{
   return time1.compare(&cmpTime) == 0;
}


bool operator != (const Time &time1, const Time &cmpTime) noexcept
{
   return time1.compare(&cmpTime) != 0;
}


Time::operator struct tm () const noexcept
{
   struct tm tm_rec;

   memset(&tm_rec ,0, sizeof(struct tm));

   tm_rec.tm_hour = _hour;
   tm_rec.tm_min  = _minute;
   tm_rec.tm_sec  = _second;

   return tm_rec;
}


const char *Time::stdTimeFormat()
{
   return STD_FMT;
}


std::ostream &operator << (std::ostream &output, const Time &dt) noexcept
{
   char jot[16];

   dt.format(jot, STD_FMT);

   output << jot;

   return output;
}


std::istream &operator >> (std::istream &input, Time &dt)
{
   char     *p;
   DateTime  tmp;

   input >> p;

   tmp.parse(p, STD_FMT);

   dt = tmp.time;

   return input;
}

#undef STD_FMT
#undef SOURCE

}}
