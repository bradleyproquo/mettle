/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_LOGGER_H_
#define __METTLE_LIB_LOGGER_H_

#include <stdarg.h>
#include <stdio.h>

#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

/** \class Logger logger.h mettle/lib/logger.h
 * \brief A logging object to for recording debug, info, and error messages.
 *
 * This is the default logging object used in Mettle.  This object is typically used by
 * servers and batches, but could be used for client applications or anything application
 * that needs to log information.
 *
 * The vanilla version of this class logs nothing and can  be used as a 'void' logger/
 */
class Logger
{
public:

  /** \brief Log Level enum.
   *
   * Defines the three levels of logging.
   */
   enum Level
   {
     Debug   = 1, /**< Debug information. */
     Info    = 2, /**< More useful program information. */
     Warning = 3, /**< Warning messages. */
     Error   = 4  /**< Errors or severe warnings. */
   };

   /** \brief Constructor.
    */
   Logger();

   /** \brief Destructor.
    */
   virtual ~Logger() noexcept;

   /** \brief Gets the logging level.
    * \returns the current log level.
    */
   Level getLogLevel() const noexcept;

   /** \brief Sets the logging level.
    * \param level the new logging level.
    */
   void setLogLevel(const Level level) noexcept;

   /** \brief Logs an error message.
    * \param fmt the formated message,
    * \param ... the arguements used by \p fmt.
    */
   virtual void error(const char *fmt, ...)  noexcept = 0;

   /** \brief Logs a warning message.
    * \param fmt the formated message,
    * \param ... the arguements used by \p fmt.
    */
   virtual void warning(const char *fmt, ...)  noexcept = 0;

   /** \brief Logs an information message.
    * \param fmt the formated message.
    * \param ... the arguements used by \p fmt.
   */
   virtual void info(const char *fmt, ...)   noexcept = 0;

   /** \brief Logs a debug message.
    * \param fmt the formated message.
    * \param ... the arguements used by \p fmt.
   */
   virtual void debug(const char *fmt, ...)  noexcept = 0;

   /** \brief Logs an exception as an error
    *
    * This is helper method, it logs the standard exception as an Error with file, and line number for reference.
    * \param file the file where the exception was spotted, almost always use __FILE__ for this arguement.
    * \param line the line number the exception was spotted, almost always use __LINE__ for this arguement.
    * \param x the standard exception to be logged.
   */
   virtual void spot(const char *file, const int line, const std::exception &x) noexcept;

   /** \brief Logs a Mettle exception as an error
    *
    * This is helper method, it logs the Mettle exception as an Error with file, and line number for reference.
    * \param file the file where the exception was spotted, almost always use __FILE__ for this arguement.
    * \param line the line number the exception was spotted, almost always use __LINE__ for this arguement.
    * \param x the Mettle exception to be logged.
   */
   virtual void spot(const char *file, const int line, const xMettle &x) noexcept;

   /** \brief Logs an 'UNKNOWN EXCEPTION' error the log file.
    *
    * This is helper method, it logs an 'UNNOWN EXCEPTION' Error with file, and line number for reference.
    * \param file the file where the exception was spotted, almost always use __FILE__ for this arguement.
    * \param line the line number the exception was spotted, almost always use __LINE__ for this arguement.
   */
   virtual void spot(const char *file, const int line) noexcept;

   /** \brief Trace's that the file and line number have been executed.
    *
    * This method does nothing in this class, and should be implemented by a derrived class.
    *
    * \param file the file where the exception was spotted, almost always use __FILE__ for this arguement.
    * \param line the line number the exception was spotted, almost always use __LINE__ for this arguement.
   */
   virtual Logger *trace(const char *file, const unsigned int line);

   /** \brief Resets trace.
    *
    * This method does nothing in this class, and should be implemented by a derrived class.
   */
   virtual void    traceReset();

   /** \brief Unwinds the current trace into the log file or another medium.
    *
    * This method does nothing in this class, and should be implemented by a derrived class.
   */
   virtual void    traceUnwind();

protected:

   Level _level;
};

}}

#endif
