/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_PROCESSMANAGER_H_
#define __METTLE_LIB_PROCESSMANAGER_H_

#include "mettle/lib/platform.h"
#include "mettle/lib/appendlist.h"
#include "mettle/lib/linklist.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#if defined(OS_LINUX) || defined (OS_AIX)

   #include <sys/types.h>
   #include <sys/wait.h>

#elif defined (OS_MS_WINDOWS)

   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>

#endif

namespace Mettle { namespace Lib {

class ProcessManager
{
public:

   class ChildProc
   {
   public:
      DECLARE_COLLECTION_LIST_CLASS(ChildProc);
      DECLARE_LINK_LIST_CLASS(ChildProc);

      ChildProc();
      ChildProc(const ChildProc *cp);

      String name;
      int    rc;
      bool   completed;
      bool   forceSigKill;
      bool   allowRunAway;

      union
      {
         void    *extPtr;
         int      extRef;
      };

#if defined(OS_LINUX) || defined (OS_AIX)

         pid_t    pid;

#elif defined (OS_MS_WINDOWS)

         int                  pid;
         PROCESS_INFORMATION  procInfo;
#endif
      };

#pragma PACK_POP

   ProcessManager() noexcept;
   ~ProcessManager() noexcept;

   bool          fetchChildExit(ChildProc &childRec) noexcept;

   unsigned int  numChildren() noexcept;

   ChildProc    *spawnChild(const char *path,
                            const char *args,
                            const char *workingDir   = 0,
                            const bool  forceSigKill = true,
                            const bool  allowRunAway = false);

   void          houseKeeping();

   void          killChildren(const int  secondsCountDown    = 5,
                              const bool autoPurgeChildProcs = true,
                              const bool killRunAways        = false) noexcept;

   void          terminate(ChildProc *child,
                           const int  countdown = 0) noexcept;

   unsigned int currentChildProcs(ChildProc::List &childList);

private:

   ChildProc::LinkList _childProcs;
};

}}

#endif
