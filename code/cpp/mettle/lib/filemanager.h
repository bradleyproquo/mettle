/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_FILEMANAGER_H_
#define __METTLE_LIB_FILEMANAGER_H_

#include "mettle/lib/filehandle.h"

namespace Mettle { namespace Lib {

/** \class FileManager filemanager.h mettle/lib/filemanager.h
 * \brief A static file manager utility class.
 *
 *  This static class contains useful utility functions to perform
 *  basic file system operations.
 */
class FileManager
{
public:

   /** \brief Returs the length of a specified file.
    *
    *  \param fileName a null terminated string pointing to the specified file.
    *  \return the length/size of the file in bytes.
    *  \throw xMettle String memory allocation exception.
    *  \throw FileHandle::xFileHandle File read access exception.
    */
   static unsigned int getLength(const char *fileName);

   /** \brief Writes a null terminated string to the specified file.
    *
    *  If the destination file does not exist it is created.  If the destination
    *  file does exist it is over-written.  Note the file is created and
    *  written in text mode.
    *
    *  \param fileName a null terminated string pointing to the destination file.
    *  \param str a null terminated string to be written to \p fileName.
    *  \throw xMettle String memory allocation exception.
    *  \throw FileHandle::xFileHandle File read access exception.
    */
   static void writeStrToFile(const char *fileName,
                              const char *str);

   /** \brief Reads the entire contents of the specified file to memory.
    *
    *  The entire contents of the source file are read into the 'jotter' parameter.
    *  The jotter pointer is allocated to the total size of file plus one for the
    *  null terminator.  A null terminator is appended automatically to the end of the
    *  file.  Note the file is read in text mode.
    *
    *  \param fileName a null terminated string pointing to the specified file.
    *  \param jotter an output pointer reference to which the file will be allocated.
    *  \param jotterSize an output value of the total size of \p jotter.
    *  \return the \p jotter pointer for conveniance.
    *  \warning the calling routine is responsible for freeing the allocated memory (\p jotter) with the std 'free()' function.
    *  \throw xMettle String memory allocation exception.
    *  \throw FileHandle::xFileHandle File read access exception.
    */
   static int8_t *readFile(const char    *fileName,
                           int8_t       *&jotter,
                           unsigned int  &jotterSize);

   /** \brief Determines if the specified system path exists.
    *
    *  \param path a null terminated string pointing to the specified system path.
    *  \return true if the path exists, else returns false.
    */
   static bool pathExists(const char *path) noexcept;

   /** \brief Creates a system directory.
    *
    *  Note that if multiple directories are specified, this method will attempt to create all of them.
    *  \param path a null terminated string pointing to the specified system director to be created.  This path can be relative or full.
    *  \throw FileHandle::xFileHandle File write access exception.
    */
   static void createDir(const char *path);

   /** \brief Removes/Deletes the specified file from the operating system.
    *
    *  \param filePath a null terminated string pointing to the specified file to be deleted.
    *  \param ignoreMissing if set to false, the method  will raise an exception if the specified \p filePath is missing.
    *  \throw FileHandle::xFileHandle File read/write access exception.
    */
   static void removeFile(const char *filePath, const bool ignoreMissing = false);

   /** \brief Renames the source file to the new destination file.
    *
    *  \param dest a null terminated string pointing to new destination file.
    *  \param source a null terminated string pointing to the source file.
    *  \throw FileHandle::xFileHandle File write access exception.
    */
   static void renameFile(const char *dest, const char *source);

   /** \brief Copies the contents of the source file into destination file.
    *
    *  \param dest a null terminated string pointing to destination filen.
    *  \param source a null terminated string pointing to the source filename.
    *  \throw FileHandle::xFileHandle File write access exception.
    */
   static void fileCopy(const char *dest, const char *source);

   /** \brief Returns a list of all the files in the specified system path.
    *
    *  \param path a null terminated string point to the system path to search.  This string does not require a proceeding directory seperator.
    *  \param fileList a String collection to which the list of found files is stored.  Note this list is automatically cleared in the function.
    *  \param returnFullPath if true the full path of each filename found will be included.  If false only the relative path of the files found will be returned.
    *  \return The total number of files found and added to \p fileList.
    *  \throw xMettle String memory allocation exception.
    *  \throw xMettle memory allocation exception.
    */
   static unsigned int findFilesInPath(const char         *path,
                                             String::List &fileList,
                                       const bool          returnFullPath = true);

   /** \brief Returns a list of all the directories in the specified system path.
    *
    *  \param path a null terminated string point to the system path to search.  This string does not require a proceeding directory seperator.
    *  \param dirList a String collection to which the list of found directories is stored.  Note this list is automatically cleared in the function.
    *  \param returnFullPath if true the full path of each director found will be included.  If false only the relative path of the directories found will be returned.
    *  \return The total number of directories found and added to \p dirList.
    *  \throw xMettle String memory allocation exception.
    *  \throw xMettle memory allocation exception.
    */
   static unsigned int findDirsInPath(const char         *path,
                                            String::List &dirList,
                                      const bool          returnFullPath = true);

private:

   /** \brief Constructor is private.
    *
    *  Static classes do not allow construction.
    */
   FileManager();
};



}}

#endif
