/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_XMETTLE_H_
#define __METTLE_LIB_XMETTLE_H_

#include <exception>

#include "mettle/lib/platform.h"

namespace Mettle { namespace Lib {

/** \class xMettle xmettle.h mettle/lib/xmettle.h
 * \brief The base exception class for the mettle library.
 *
 * This is the base exception class for the entire mettle library.  The Error Codes are
 * kept in sync with all other languages, ie C#, java, python.
 */
class xMettle : public std::exception
{
public:

   enum eCode
   {
      NoError            = 0,      /**< This typically should never be thrown. */

      UnknownException   = -101,   /**< Indicates an unknown exception was thrown. */
      StandardException  = -102,   /**< Indicates a typical exception was thrown, this is the default eCode that is thrown by the constructors. */
      TerminalException  = -103,   /**< Indicates a terminal exception was thrown, some mettle code will terminate the process is this exception type is encountered. */
      OutOfMemory        = -104,   /**< Indicates an out of memory exception was thrown. */
      SocketTimeout      = -105,   /**< Indicates a socket timeout exception was thrown. */
      InternalException  = -106,   /**< Indicates an internal exception was thrown that should not been seen by users. */
      DavException       = -107,   /**< Indicates a DAV (Data Assurance Valaidator) exception was thrown. */
      CredExpired        = -108,   /*/< Indicate user login credentials have expired. */
      CredInvalid        = -109,   /*/< Indicate user login is not valid. */
      CredDenied         = -110,   /*/< Indicate user does not haves access the required function. */
      TokenExpired       = -111,   /*/< Indicate user token/session has expired. */
      TokenInvalid       = -112,   /*/< Indicate user token/session is no longer valid. */
      STANDARD_MAX_ERROR = -199,   /**< The end of the standard exceptions numbers. */

      ComsStandardError  = -200,   /**< Indicates a standard coms/braze exception was thrown. */
      ComsTimeOut        = -201,   /**< Indicates a coms/braze timeout occurred. */
      ComsInvalidHeader  = -202,   /**< Indicates the coms/braze message header was not valid . */
      ComsServerError    = -203,   /**< Indicates the coms/braze servers raise an exception. */
      ComsReceiveError   = -204,   /**< Indicates a coms/braze data recieve error was thrown. */
      ComsSignature      = -205,   /**< Indicates a coms/braze invalid server/client signature detected. */
      COMS_MAX_ERROR     = -299,   /**< The end of the coms/braze execption numbers. */

      DBStandardError       = -300, /**< Indicates a standard database exception was thrown. */
      DBForeignKeyViolation = -301, /**< Indicates a database threw a foreign key exception. */
      DBPrimaryKeyViolation = -302, /**< Indicates a database threw a primary key exception. */
      DBUniqueKeyViolation  = -303, /**< Indicates a database threw a unique key exception. */
      DBLockNoWaitFailed    = -304, /**< Indicates a database threw a database lock exception. */
      DBBokenConnection     = -305, /**< Indicates a connector lost its database connection. Try reset() */
      DB_MAX_ERROR          = -399  /**< The end of the database exception numbers */
   };


   /** \brief Most basic constructor.
    *
    * Raises and excelption with the file name, line number and 'source'.
    *
    * \param filename typically __FILE__ is passed here.
    * \param line typeically __LINE__ is passed here.
    * \param source a constant string identifying the source object/code from where this exception was thrown.
    */
   xMettle(const char *filename,
           const int   line,
           const char *source) noexcept;

   /** \brief Most commonly used constructor.
    *
    * Raises and excelption with the file name, line number, 'source', and a custom exception message.
    *
    * \param filename typically __FILE__ is passed here.
    * \param line typeically __LINE__ is passed here.
    * \param source a constant string identifying the source object/code from where this exception was thrown.
    * \param fmt the formated exception message.
    * \param ... the arguements used by \p fmt.
    */
    xMettle(const char *filename,
            const int   line,
            const char *source,
            const char *fmt, ...) noexcept;

   /** \brief Most comprehensive constructor.
    *
    * Raises and excelption with the file name, line number, error code, 'source', and a custom exception message.
    *
    * \param filename typically __FILE__ is passed here.
    * \param line typeically __LINE__ is passed here.
    * \param errorCode the specific type of exception being thrown.
    * \param source a constant string identifying the source object/code from where this exception was thrown.
    * \param fmt the formated exception message.
    * \param ... the arguements used by \p fmt.
    */
    xMettle(const char *filename,
            const int   line,
            const eCode errorCode,
            const char *source,
            const char *fmt, ...) noexcept;

   /** \brief Copy constructor.
    */
   xMettle(const xMettle &x) noexcept;

      // overload the what method
#if defined (COMPILER_GNU)

   /** \brief Overloads the standard 'what()' method.
    * \returns the error message for the specific exception.
    */
   const char *what() const noexcept;

#else

   /** \brief Overloads the standard 'what()' method.
    * \returns the error message for the specific exception.
    */
   const char *what() const;

#endif

   /** \brief Gets the last operating system error message and returns the operating systems current error code.
    * \param dest the destination buffer to store the error message.
    * \param destSize the size of the \p dest buffer.
    * \param errCode if left 0 this method gets the latest error code from the operating system, else it gets the error message for the value of \p errCode.
    * \returns the operating system last error code or \p errCode if it was not left as 0.
    */
   static int    getOSError(char *dest, unsigned int destSize, int errCode = 0);

   /** \brief Notify caller if the exception is a database exception.
    * \returns true if this a database exception.
    */
   bool isDBError()   const noexcept;

   /** \brief Notify caller if the exception is a coms exception.
    * \returns true if this a coms exception.
    */
   bool isComsError() const noexcept;

   /** \brief Notify caller if the exception is a standard exception.
    * \returns true if this a standard exception.
    */
   bool isStandardError() const noexcept;

   /** \brief Check specifically for a broken database connection.
    * \returns true if this a broken connection error.
    */
   bool isDBBrokenConnection() const noexcept;

   /** \brief The file name from where the exception was thrown.
    */
   const char   *filename;

   /** \brief The line number from where the exception as thrown.
    */
   int           line;

   /** \brief The exception error code 'eCode'.
    */
   eCode         errorCode;

   /** \brief The source name from where the exception was thrown.
    */
   char          source[64];

   /** \brief The error message of the exception.
    */
   char          errorMsg[1024];

   /** \brief The last operating system error code at the time the exception was thrown.
    */
   int           sysErrorNo;

   /** \brief The last operating system error message at the time the exception was thrown.
    */
   char          sysErrorString[256];
};


/** \class xTerminate xmettle.h mettle/lib/xmettle.h
 * \brief This helper terminate exception class set the eCode of the base xMettle class to 'TerminalException'.
 */
class xTerminate : public xMettle
{
public:

   /** \brief Constructor
    *
    * Raises and excelption with the file name, line number, 'source', and a custom exception message.
    *
    * \param filename typically __FILE__ is passed here.
    * \param line typeically __LINE__ is passed here.
    * \param source a constant string identifying the source object/code from where this exception was thrown.
    * \param fmt the formated exception message.
    * \param ... the arguements used by \p fmt.
    */
   xTerminate(const char *filename,
              const int   line,
              const char *source,
              const char *fmt, ...) noexcept;

   /** \brief Most comprehensive constructor.
    *
    * Raises and excelption with the file name, line number, error code, 'source', and a custom exception message.
    *
    * \param filename typically __FILE__ is passed here.
    * \param line typeically __LINE__ is passed here.
    * \param errorCode the specific type of exception being thrown.
    * \param source a constant string identifying the source object/code from where this exception was thrown.
    * \param fmt the formated exception message.
    * \param ... the arguements used by \p fmt.
    */
    xTerminate(const char *filename,
            const int   line,
            const eCode errorCode,
            const char *source,
            const char *fmt, ...) noexcept;

   /** \brief Copy constructor.
    */
   xTerminate(const xTerminate &x) noexcept;
};

}}


#endif
