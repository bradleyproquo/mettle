/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/common.h"

#include "mettle/lib/macros.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>

#if defined (OS_MS_WINDOWS)

   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>

   #if !defined (WINCE)

      #include <process.h>

   #endif

#endif

#include <stdlib.h>

namespace Mettle { namespace Lib {

char *Common::trim(char *str, const unsigned int size) noexcept
{
   char *p = str + size;

   for (p--; p > str; p--)
      if (*p == ' ')
         break;

   if (p >= str)
      *p = 0;

   return str;
}

int16_t Common::swapBytes(int16_t v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[1]);

   return v;
}

int32_t Common::swapBytes(int32_t v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[3]);
   _SWOP_VAL(p[1], p[2]);

   return v;
}

int64_t Common::swapBytes(int64_t v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[7]);
   _SWOP_VAL(p[1], p[6]);
   _SWOP_VAL(p[2], p[5]);
   _SWOP_VAL(p[3], p[4]);

   return v;
}

uint16_t Common::swapBytes(uint16_t v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[1]);

   return v;
}

uint32_t Common::swapBytes(uint32_t v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[3]);
   _SWOP_VAL(p[1], p[2]);

   return v;
}

uint64_t Common::swapBytes(uint64_t v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[7]);
   _SWOP_VAL(p[1], p[6]);
   _SWOP_VAL(p[2], p[5]);
   _SWOP_VAL(p[3], p[4]);

   return v;
}

double Common::swapBytes(double v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[7]);
   _SWOP_VAL(p[1], p[6]);
   _SWOP_VAL(p[2], p[5]);
   _SWOP_VAL(p[3], p[4]);

   return v;
}

float Common::swapBytes(float v) noexcept
{
   unsigned char *p = (unsigned char *) &v;

   _SWOP_VAL(p[0], p[3]);
   _SWOP_VAL(p[1], p[2]);

   return v;
}

#define COMPARE_METHOD(t) int Common::compare(const t x, const t y) noexcept\
{\
   if (x < y) return -1;\
   if (x > y) return 1;\
   return 0;\
}

COMPARE_METHOD(int8_t)
COMPARE_METHOD(int16_t)
COMPARE_METHOD(int32_t)
COMPARE_METHOD(int64_t)
COMPARE_METHOD(uint8_t)
COMPARE_METHOD(uint16_t)
COMPARE_METHOD(uint32_t)
COMPARE_METHOD(uint64_t)
COMPARE_METHOD(double)
COMPARE_METHOD(float)

#undef COMPARE_METHOD

int Common::compare(const uuid_t x, const uuid_t y) noexcept
{
   return uuid_compare(x, y);
}

void Common::sleep(const int seconds) noexcept
{
#if defined (OS_MS_WINDOWS)
   ::Sleep(seconds * 1000);
#else
   ::sleep(seconds);
#endif
}

void Common::sleepMili(const int mili) noexcept
{
#if defined (OS_MS_WINDOWS)
   ::Sleep(mili);
#else
   ::usleep((useconds_t)mili * 1000);
#endif
}

char *Common::stringCopy(char *dest, const char *src, const int32_t size) noexcept
{
   strncpy(dest, src, size-1);
   dest[size-1] = 0;

   return dest;
}

void *Common::memoryAlloc(const unsigned int size)
{
   void *p = malloc(size);

   if (p == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, "MemoryAlloc", "Error allocating [%d] bytes", size);

   return p;
}

int Common::getPID() noexcept
{
#if defined (OS_MS_WINDOWS)

   #if defined (WINCE)

      return -1;

   #else

      return _getpid();

   #endif

#else
   return getpid();
#endif
}

double Common::tenPow(int n)
{
   double y = 10.0;

   while (--n > 0)
      y *= 10.0;

   return y;
}

double Common::round(double val, int decimals)
{
   if (val == 0.0)
      return val;

   int  addDec = 3;
   char tempStr[64];

   sprintf(tempStr, "%.*lf", decimals + addDec, val);

   int len = strlen(tempStr);

   if (tempStr[len - addDec] < '5')
   {
      tempStr[len - addDec] = 0;
      val = atof(tempStr);
   }
   else
   {
      tempStr[len - addDec] = 0;
      double offset = 1 / (pow(10.0, (double)decimals));

      if (val >= 0)
         val = atof(tempStr) + offset;
      else
         val = atof(tempStr) - offset;
   }

   return val;
}

double Common::truncate(double val, int decimals)
{
   char tempStr[64];

   sprintf(tempStr, "%.*lf", decimals + 17, val);

   for (unsigned i = 0; tempStr[i]; i++)
      if (tempStr[i] == '.')
      {
         tempStr[i + decimals + 1] = 0;
         break;
      }

   sscanf(tempStr, "%lf", &val);

   return val;
}

double Common::roundBanker(double val, int decimals)
{
   int    n   = decimals;
   double x   = val;
   bool   neg = (x < 0.0);
   double ipart, fpart, y, p;
   int    id;

   if (neg)
      x = -x;

   if (n > 0)
   {
      double yy;
      fpart = modf(x, &ipart);

      if (n > DBL_DIG)
         n = DBL_DIG;

      p     = tenPow(n);
      y     = fpart * p;
      fpart = modf(y, &yy);

      if (fpart < 0.5)
         fpart = 0.0;
      else if (fpart > 0.5)
         fpart = 1.0;
      else
      { /* Banker's Method */
         id = (int)fmod(yy, 10.0);
         fpart = (id&1) ? 1.0 : 0.0;
      }

      yy += fpart;
      y = ipart + yy / p;
   }
   else if (n < 0)
   {
      if (n < DBL_DIG)
         n = -DBL_DIG;

      p = tenPow(-n);
      y = x / p;
      y = roundBanker(y, 0) * p;
   }
   else
   { /* n == 0 */
      fpart = modf(x, &ipart);
      if (fpart > 0.5)
         ipart += 1.0;
      else if (fpart == 0.5)
      { /* Banker's Method */
         id = (int)fmod(ipart, 10.0);
         if ((id&1) != 0)
            ipart += 1.0;
      }

      y = ipart;
   }

   return neg ? -y : y;
}

double Common::roundSymmetric(double val, int decimals)
{
   int    n = decimals;
   double x = val;

   bool neg = (x < 0.0);

   double ipart, fpart, y, p;

   if (neg)
      x = -x;

   if (n > 0)
   {
      double yy;
      fpart = modf(x, &ipart);
      if (n > DBL_DIG)
         n = DBL_DIG;

      p = tenPow(n);
      y = fpart * p;
      fpart = modf(y, &yy);
      if (fpart < 0.5)
         yy += 1.0;

      y = ipart + yy / p;

   }
   else if (n < 0)
   {
      if (n < -DBL_DIG)
         n = -DBL_DIG;

      p = tenPow(-n);
      y = x / p;
      y = roundSymmetric(y, 0) * p;
   }
   else
   { /* n == 0 */
      fpart = modf(x, &ipart);
      y = (fpart < 0.5) ? ipart : ipart + 1;
   }

   return neg ? -y : y;
}

double Common::roundTruncated(double val, int decimals)
{
   return truncate(round(val, decimals), decimals);
}

bool Common::stringIsUnsigned(const char *str) noexcept
{
   char *ptr = (char *) str;

   for (; *ptr; ptr++)
      if (*ptr < '0' || *ptr > '9')
         return false;

   return true;
}

bool Common::stringIsInt(const char *str) noexcept
{
   char *ptr = (char *) str;

   if (*ptr == '-')
      ptr++;

   if (*ptr == 0)
      return false;

   for (; *ptr; ptr++)
      if (*ptr < '0' || *ptr > '9')
         return false;

   return true;
}

bool Common::stringIsDouble(const char *str) noexcept
{
   char *ptr       = (char *) str;
   char  dot_found = 0;

   if (*ptr == '-')
      ptr++;

   if (*ptr == 0)
      return false;

   for (; *ptr; ptr++)
      if (*ptr < '0' || *ptr > '9')
      {
         if (*ptr == '.' && !dot_found)
            dot_found = 1;
         else
            return false;
      }

   return true;
}

bool Common::stringIsUUID(const char *str, bool checkLen) noexcept
{
   if (str == 0 || *str == 0)
      return false;

   if (checkLen && strlen(str) != 36)
      return false;

   if (str[8] != '-' || str[13] != '-' || str[18] != '-' || str[23] != '-')
      return false;

   char *ptr;
   int  idx = 0;

   for (ptr = (char *)str; idx < 8; idx++, ptr++)
      if (!((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z')) || (*ptr >= '0' && *ptr <= '9'))
         return false;

   for (idx++, ptr++; idx < 13; idx++, ptr++)
      if (!((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z')) || (*ptr >= '0' && *ptr <= '9'))
         return false;

   for (idx++, ptr++; idx < 18; idx++, ptr++)
      if (!((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z')) || (*ptr >= '0' && *ptr <= '9'))
         return false;

   for (idx++, ptr++; idx < 23; idx++, ptr++)
      if (!((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z')) || (*ptr >= '0' && *ptr <= '9'))
         return false;

   return true;
}

#if defined(OS_MS_WINDOWS)

#if COMPILER_MSC > 6

void *bsearch(const void *key, const void *base, size_t num, size_t width, int (*compare)(const void *, const void *))
{
   if (!key || !base || !num)
      return 0;

   uint8_t      *lo = (uint8_t *) base;
   uint8_t      *hi = (uint8_t *) base + (num - 1) * width;
   uint8_t      *mid;
   size_t        half;
   int           result;

   while (lo <= hi)
   {
      half = _FAST_INT_DIV_BY_2(num);

      if (half == 0)
      {
         return ((*compare)(key, lo) ? 0 : lo);
      }

      mid    = lo + (num & 1 ? half : (half - 1)) * width;
      result = (*compare)(key, mid);

      if (result == 0)
         return mid;

      if (result < 0)
      {
         hi  = mid - width;
         num = num & 1 ? half : half - 1;
      }
      else
      {
         lo  = mid + width;
         num = half;
      }
   }

   return 0;
}

static void Qsort_swap(char *a, char *b, unsigned width)
{
   char tmp;

   if (a != b)
   {
      while (width--)
      {
         tmp  = *a;
         *a++ = *b;
         *b++ = tmp;
      }
   }
}

static void Qsort_shortsort(char *lo, char *hi, unsigned width, int (*comp)(const void *, const void *))
{
   char *p, *max;

   while (hi > lo)
   {
      max = lo;

      for (p = lo+width; p <= hi; p += width)
         if (comp(p, max) > 0)
            max = p;

      Qsort_swap(max, hi, width);
      hi -= width;
   }
}

#define CUTOFF 8

void qsort(void *base, unsigned num, unsigned width, int (*comp)(const void *, const void *))
{
   char      *lo;
   char      *hi;
   char      *mid;
   char      *loguy;
   char      *higuy;
   unsigned   size;
   char      *lostk[30];
   char      *histk[30];
   int        stkptr;

   if (num < 2 || width == 0)
      return;

   stkptr = 0;

   lo = (char *) base;
   hi = (char *) base + width * (num - 1);

Qsort_Recurse:

   size = (hi - lo) / width + 1;

   if (size <= CUTOFF)
   {
      Qsort_shortsort(lo, hi, width, comp);
   }
   else
   {
      mid = lo + (size / 2) * width;

      Qsort_swap(mid, lo, width);

      loguy = lo;
      higuy = hi + width;

      for (;;)
      {
         do
         {
            loguy += width;
         } while (loguy <= hi && comp(loguy, lo) <= 0);

         do
         {
            higuy -= width;
         } while (higuy > lo && comp(higuy, lo) >= 0);

         if (higuy < loguy)
            break;

         Qsort_swap(loguy, higuy, width);
      }

      Qsort_swap(lo, higuy, width);

      if (higuy - 1 - lo >= hi - loguy)
      {
         if (lo + width < higuy)
         {
           lostk[stkptr] = lo;
           histk[stkptr] = higuy - width;
           ++stkptr;
         }

         if (loguy < hi)
         {
            lo = loguy;
            goto Qsort_Recurse;
         }
      }
      else
      {
         if (loguy < hi)
         {
            lostk[stkptr] = loguy;
            histk[stkptr] = hi;
            ++stkptr;
         }

         if (lo + width < higuy)
         {
            hi = higuy - width;
            goto Qsort_Recurse;
         }
      }
   }

   --stkptr;

   if (stkptr >= 0)
   {
      lo = lostk[stkptr];
      hi = histk[stkptr];
      goto Qsort_Recurse;
   }

   return;
}

#undef CUTOFF

#endif

#endif

}}
