/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_ICOMPARABLE_H_
#define __METTLE_LIB_ICOMPARABLE_H_

namespace Mettle { namespace Lib {

/** \interface IComparable icomparable.h mettle/core/icomparable.h
 * \brief An interface to allow objects to be generically compared to each other
 *
 *  The Comparable interface class is intended to be used by objects that
 *  are able to compare themselves for sequencing.  For example a string class
 *  would implement this function to provide an oo alternative to strcmp().
 */
class IComparable
{
public:
   /** \brief Pure vitual method to be implemented by the inhertited class.
     * \param obj is typically a pointer of the inherited class and type casted backed the inherited class in the implementation.
     * \return -1 if obj is less than, 0 if the obj is equal, 1 if the obj is greater than
    */
   virtual int _compare(const void *obj) const = 0;
};

}}

#endif
