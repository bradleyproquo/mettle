/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_COLLECTION_H_
#define __METTLE_LIB_COLLECTION_H_

#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>

#if defined(OS_MS_WINDOWS)

   #if COMPILER_MSC > 6

      #include <memory.h>

      #include "mettle/lib/common.h"

   #endif

#endif

#define DECLARE_COLLECTION_LIST_CLASS(cls) class List:public Mettle::Lib::Collection<cls> {}

#define __HEADER_SOURCE_ "Mettle::Collection"

namespace Mettle { namespace Lib {


/** \class Collection collection.h mettle/lib/collection.h
 * \brief A template collection class.
 *
 *  This is the standard list/collection object used in all the mettle libraries and
 *  code generators.
 */
template <class type> class Collection
{
public:

   /** \brief Public access is granted to the internal pointer list for reading.
    *  \warning Do not modify this member.
    */
   type **_list;


   /** \brief Constructor.
    */
   Collection() noexcept;

   /** \brief Destructor.
    */
   virtual ~Collection() noexcept;

   /** \brief Allocates the internal collection size to a predetermined size.  Usually this method
    *          is not required to be called, however if you want to pre-allocate a large array this
    *          method can provide a performance boost.
    * \param size the total number of elements to be created.
    * \warning The entire collection is Purged before the operation is begun, thus you will lose any collection contents.
    * \throws xMettle if out of memory.
    */
   void allocate(const unsigned int size);

   /** \brief Appends an object to the collection.  The method auto increments the internal array size
    *          by the value of \p incVal.  Note that if a NULL is passed, it is simply assigned.
    * \param rec the object to be appended to the collection, NULL is acceptable.
    * \param incVal the rate at which to grow the collection when it runs out of space.
    * \returns the object that was passed in via \p rec.
    * \throws xMettle if out of memory.
    */
   type *append(type *rec = 0, const unsigned int incVal = 16);

   /** \brief Swaps the objects at positions \p index1 and \p index2 around.
    * \param index1 the array offset of the first object.
    * \param index2 the array offset of the second object.
    * \returns true if the swap was succuessful or false if either \p index1 or \p index2 was out of range..
    */
   bool swap(const unsigned int index1, const unsigned int index2)  noexcept;

   /** \brief Removes the object at the specified index.
    * \param index the position of the object to be removed.
    * \returns true if the remove was successful or false if either \p index1 or \p index2 was out of range.
    */
   bool remove(const unsigned int index)  noexcept;

   /** \brief Removes the object from the back of the collection.
    * \returns true if the remove was successful, or false if the collection is empty.
    */
   bool remove() noexcept;

   /** \brief Deletes all the objects from the collection, sets the array size to zero but does not
    *          delete the internal pointer array.
    */
   void clear() noexcept;

   /** \brief Deletels all the object from the collection and then deletes the internal pointer array.
    */
   void purge() noexcept;

   /** \brief The total count of the number of objects in the collection.
    * \returns the number of objects in the collection.
    */
   unsigned int count() const noexcept;

   /** \brief The internally allocateed array element size of the collection.
    * \returns the internal pointer array size of the collection.
    */
   unsigned int arrCount()  const noexcept;

   /** \brief Calculates the shallow memory size of the collection's objects
    * \returns the calculates size.
    */
   unsigned int memUsed() const noexcept;

   /** \brief A default quick sort method for this collection.
     * \param sortFunc a function pointer to be used to sort this collection with.  This is a compare function.
    */
   void quickSort(int (*sortFunc)(const type **rec, const type **rec2)) noexcept;

   /** \brief A default binary search method for this collection.  If the collection is not sorted, a call to Qsort should be made first.
     * \param searchRec a template of the object to be searched for.
     * \param sortFunc a function pointer to be used to binary search this collection with.  This is a compare function.
     * \returns the object if it is found or NULL if it was not.
    */
   type *binarySearch(const type *searchRec, int (*sortFunc)(const type **rec, const type **rec2)) const noexcept;

   /** \brief Gets the last object in the array
    * \returns the last object in the array or NULL if the collection is empty.
    */
   type *last() const noexcept;

   /** \brief Operator overload for [] to get the object at the specified index.
    * \param index the index of the object to get.
    * \returns the object at position \p index.
    * \warning This method has undefined behaviour if \p index is out of bounds.
    */
   type &operator[](const unsigned int index) const noexcept;

   /** \brief Gets the object at the specified index.
    * \param index the index of the object to get.
    * \returns the object at position \p index.
    * \warning This method has undefined behaviour if \p index is out of bounds.
    */
   type *get(const unsigned int index) const noexcept;

   /** \brief Sets the object at position \p index, note the object at that position is first deleted.
    * \param index the index of the object to be set.
    * \param val the object to be set.
    * \returns the object passed in via \p val.
    * \warning This method has undefined behaviour if \p index is out of bounds.
    */
   type *set(const unsigned int index, type *val) noexcept;

   /** \brief Deletes the object at the specified index, but does not reduce the array size leaving the space null.
    * \param index the index of the object to be deleted.
    * \warning This method has undefined behaviour if \p index is out of bounds.
    */
   void deleteAt(const unsigned int index) noexcept;

private:

   unsigned int   _listArrCnt;   // number of array elements
   unsigned int   _listCnt;      // number of used array elements
};

template <class type>
Collection<type>::Collection() noexcept
{
   _list       = 0;
   _listArrCnt =
   _listCnt    = 0;
}

template <class type>
Collection<type>::~Collection() noexcept
{
   purge();
}

template <class type>
unsigned int Collection<type>::count() const noexcept
{
   return _listCnt;
}

template <class type>
unsigned int Collection<type>::arrCount() const noexcept
{
   return _listArrCnt;
}

template <class type>
unsigned int Collection<type>::memUsed() const noexcept
{
   return _listArrCnt * (sizeof(type *) + sizeof(type));
}

template <class type>
void Collection<type>::clear() noexcept
{
   for (unsigned int index = 0; index < _listCnt; index++)
      if (_list[index])
      {
         delete _list[index];
         _list[index] = 0;
      }

   _listCnt = 0;
}

template <class type>
void Collection<type>::purge() noexcept
{
   clear();

   if (_list)
   {
      free(_list);
      _list       = 0;
      _listArrCnt =
      _listCnt    = 0;
   }
}

template <class type>
void Collection<type>::allocate(const unsigned int size)
{
  unsigned int idx;

  purge();

  if (size < 1)
     return;

  _listCnt = _listArrCnt = size;

   if ((_list = (type**) realloc(_list, sizeof(type*) * _listArrCnt)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, __HEADER_SOURCE_, "Error allocating [%u] bytes.", sizeof(type*) * _listArrCnt);

   memset(_list, 0, sizeof(type*) * _listArrCnt);

   for (idx = 0; idx < size; idx++)
      _list[idx] = new type();
}

template <class type>
type *Collection<type>::append(type *rec, const unsigned int incVal)
{
   if (_listCnt == _listArrCnt)
   {
      type      **memPtr = _list;
      unsigned int    oldCnt = _listArrCnt;

      _listArrCnt += incVal;

      if ((_list = (type**) realloc(_list, sizeof(type*) * _listArrCnt)) == 0)
      {
         _list = memPtr;
         throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, __HEADER_SOURCE_, "Error allocating [%u] bytes.", sizeof(type*) * _listArrCnt);
      }

      memset(((char *)_list) + sizeof(type*) * oldCnt, 0, sizeof(type*) * incVal);
   }

   if (rec)
      _list[_listCnt] = rec;

   ++_listCnt;

   return rec;
}

template <class type>
bool Collection<type>::swap(const unsigned int index1, const unsigned int index2) noexcept
{
   if (index1 == index2 ||
       index1 >= _listCnt ||
       index2 >= _listCnt)
      return false;

   type *tmp = _list[index1];

   _list[index1] = _list[index2];
   _list[index2] = tmp;

   return true;
}

template <class type>
bool Collection<type>::remove() noexcept
{
   if (_listCnt < 1)
      return false;

   return remove(_listCnt - 1);
}

template <class type>
bool Collection<type>::remove(const unsigned int index) noexcept
{
   if (index >= _listCnt)
      return false;

   if (_list[index])
   {
      delete _list[index];
      _list[index] = 0;
   }

   if (index == _listCnt - 1)
   {
      _listCnt--;
      return true;
   }

   memcpy(&_list[index], &_list[index + 1], sizeof(type*) * (_listCnt - index - 1));
   _listCnt--;

   return true;
}

template <class type>
type &Collection<type>::operator[](const unsigned int index) const noexcept
{
   return *_list[index];
}

template <class type>
type *Collection<type>::last() const noexcept
{
   if (_listCnt < 1)
      return 0;

   return _list[_listCnt - 1];
}

template <class type>
type *Collection<type>::get(const unsigned int index) const noexcept
{
   return _list[index];
}

template <class type>
type *Collection<type>::set(const unsigned int index, type *val) noexcept
{
   if (_list[index])
      delete _list[index];

   _list[index] = val;

   return _list[index];
}

template <class type>
void Collection<type>::deleteAt(const unsigned int index) noexcept
{
   if (_list[index])
   {
      delete _list[index];
      _list[index] = 0;
   }
}

template <class type>
void Collection<type>::quickSort(int (*sortFunc)(const type **rec, const type **rec2)) noexcept
{
	qsort(_list,
         _listCnt,
         sizeof(type *),
         (int (*)(const void *, const void *)) sortFunc);
}

template <class type>
type *Collection<type>::binarySearch(const type *searchRec,
                                     int (*sortFunc)(const type **rec, const type **rec2)) const noexcept
{
   if (_listCnt < 100)
   {
      unsigned int idx;

      for (idx = 0; idx < _listCnt; idx++)
         if (sortFunc(&searchRec, (const type **) &_list[idx]) == 0)
            return _list[idx];

      return 0;
   }

   type **result = (type **) bsearch(&searchRec,
                                     _list,
                                     _listCnt,
                                     sizeof(type *),
                                     (int (*)(const void *, const void *)) sortFunc);

   if (result)
      return *result;

   return 0;
}

}}

#undef __HEADER_SOURCE_

#endif
