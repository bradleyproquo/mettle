/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/ustring.h"

#define __STDC_FORMAT_MACROS 1
#define __STDC_LIMIT_MACROS

#include <stdlib.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <limits.h>

#include "mettle/lib/common.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"

#define MAX_INT_STR_SIZE         128
#define USTRING_TRUE             "true"
#define USTRING_FALSE            "false"
#define USTRING_YES              "yes"
#define USTRING_NO               "no"
#define WHITE_SPACES             " \n\r\t\f\v"

namespace Mettle { namespace Lib {

#define MODULE "Mettle.UString"

UString::UString() : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   _newStr(_EMPTY_STRING);
}

UString::UString(const char *inStr) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   _newStr(inStr);
}

UString::UString(const UString &inStr) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   _newStr(inStr.c_str());
}

UString::UString(const int16_t strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId16, strVal);
   _newStr(jotter);
}

UString::UString(const uint16_t strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu16, strVal);
   _newStr(jotter);
}

UString::UString(const int32_t strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   _newStr(jotter);
}

UString::UString(const uint32_t strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   _newStr(jotter);
}

UString::UString(const int64_t strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   _newStr(jotter);
}

UString::UString(const uint64_t strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   _newStr(jotter);
}

UString::UString(const float strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%f", strVal);
   _newStr(jotter);
}

UString::UString(const double strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   _newStr(jotter);
}

UString::UString(const long double strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%Lf", strVal);
   _newStr(jotter);
}

UString::UString(const char ch) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char inStr[2];

   inStr[0] = ch;
   inStr[1] = 0;

   _newStr(inStr);
}

UString::UString(const bool strVal) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   if (strVal)
      _newStr(USTRING_TRUE);
   else
      _newStr(USTRING_FALSE);
}

UString::UString(const uuid_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   if (uuid_is_null(strVal))
      _newStr(_EMPTY_STRING);
   else
   {
      char jotter[38];

      uuid_unparse(strVal, jotter);

      _newStr(jotter);
   }
}

UString::UString(const char *str1, const char *str2) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   char         *tmp;
   unsigned int  len = 0;

   if (str1)
      len += strlen(str1);

   if (str2)
      len += strlen(str2);

   if (!len)
   {
      _newStr(_EMPTY_STRING);
      return;
   }

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len + 1);

   _str       = tmp;
   _byteLen   = len;
   _bytesUsed = len + 1;

   if (str1)
   {
      strcpy(_str, str1);

      if (str2)
         strcat(_str, str2);
   }
   else
      strcpy(_str, str2);

   _strLen = UChar::u8_strlen(_str);
}

UString::UString(const char *str, const unsigned len) : _str(0), _strLen(0), _byteLen(0), _bytesUsed(0)
{
   if (!str || !len)
   {
      _newStr(_EMPTY_STRING);
      return;
   }

   char *tmp;

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len + 1);

   _str       = tmp;
   _byteLen   = len;
   _bytesUsed = len + 1;

   memcpy(_str, str, len);

   _str[len] = 0;
   _strLen   = UChar::u8_strlen(_str);
}

UString::~UString() noexcept
{
   _freeStr();
}

void UString::_freeStr() noexcept
{
   if (!_str)
      return;

   free(_str);

   _str       = 0;
   _strLen    = 0;
   _byteLen   = 0;
   _bytesUsed = 0;
}

UString *UString::_newStr(const char *str)

{
   if (!str)
      return _newStr(_EMPTY_STRING);

   char     *tmp;
   unsigned  len = strlen(str);

   if (len < _bytesUsed)
   {
      strcpy(_str, str);
      _byteLen = len;
      _strLen  = UChar::u8_strlen(str);

      return this;
   }

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len + 1);

   _str       = tmp;
   _strLen    = UChar::u8_strlen(str);
   _byteLen   = len;
   _bytesUsed = len + 1;

   strcpy(_str, str);

   return this;
}

UString &UString::_appendStr(const char *str)
{
   if (!str || !str[0])
      return *this;

   char         *tmp;
   unsigned int  len = strlen(_str) + strlen(str);

   if (len < _bytesUsed)
   {
      strcat(_str, str);
      _byteLen = len;
      _strLen  = UChar::u8_strlen(_str);

      return *this;
   }

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len + 1);

   _str        = tmp;
   _strLen    += UChar::u8_strlen(str);
   _byteLen    = len;
   _bytesUsed  = len + 1;

   strcat(_str, str);

   return *this;
}

UString &UString::operator = (const char ch)
{
   char inStr[2];

   inStr[0] = ch;
   inStr[1] = 0;

   return *_newStr(inStr);
}

UString &UString::operator = (const char *inStr)
{
   return *_newStr(inStr);
}


UString &UString::operator = (const UString &inStr)
{
   return *_newStr(inStr._str);
}

UString &UString::operator = (const int32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   return *_newStr(jotter);
}

UString &UString::operator = (const uint32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return *_newStr(jotter);
}

UString &UString::operator = (const int64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   return *_newStr(jotter);
}

UString &UString::operator = (const uint64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   return *_newStr(jotter);
}

UString &UString::operator = (const float strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%f", strVal);
   return *_newStr(jotter);
}

UString &UString::operator = (const double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   return *_newStr(jotter);
}

UString &UString::operator = (const long double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%Lf", strVal);
   return *_newStr(jotter);
}

UString &UString::operator = (const bool strVal)
{
   if (strVal)
      return *_newStr(USTRING_TRUE);

   return *_newStr(USTRING_FALSE);
}

UString &UString::operator = (const uuid_t strVal)
{
   if (uuid_is_null(strVal))
      return *_newStr(_EMPTY_STRING);

   char jotter[38];

   uuid_unparse(strVal, jotter);

   return *_newStr(jotter);
}

UString &UString::operator += (const char ch)
{
   char inStr[2];

   inStr[0] = ch;
   inStr[1] = 0;

   return _appendStr(inStr);
}

UString &UString::operator += (const char *inStr)
{
   return _appendStr(inStr);
}

UString &UString::operator += (const UString &inStr)
{
   return _appendStr(inStr._str);
}

UString &UString::operator += (const int32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   return _appendStr(jotter);
}

UString &UString::operator += (const uint32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return _appendStr(jotter);
}

UString &UString::operator += (const int64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   return _appendStr(jotter);
}

UString &UString::operator += (const uint64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   return _appendStr(jotter);
}

UString &UString::operator += (const float strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%f", strVal);
   return _appendStr(jotter);
}

UString &UString::operator += (const double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   return _appendStr(jotter);
}

UString &UString::operator += (const long double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%Lf", strVal);
   return _appendStr(jotter);
}

UString &UString::operator += (const bool strVal)
{
   if (strVal)
      return _appendStr(USTRING_TRUE);

   return _appendStr(USTRING_FALSE);
}

UString &UString::operator += (const uuid_t strVal)
{
   if (uuid_is_null(strVal))
      return *this;

   char jotter[38];

   uuid_unparse(strVal, jotter);

   return _appendStr(jotter);
}

UString operator + (const UString &str, const char ch)
{
   char inStr[2];

   inStr[0] = ch;
   inStr[1] = 0;

   return UString(str._str, inStr);
}

UString operator + (const UString &str, const char *inStr)
{
   return UString(str._str, inStr);
}

UString operator + (const UString &str, const UString &inStr)
{
   return UString(str._str, inStr._str);
}

UString operator + (const UString &str, const int32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   return UString(str._str, jotter);
}

UString operator + (const UString &str, const uint32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return UString(str._str, jotter);
}

UString operator + (const UString &str, const int64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   return UString(str._str, jotter);
}

UString operator + (const UString &str, const uint64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   return UString(str._str, jotter);
}

UString operator + (const UString &str, const float strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%f", strVal);
   return UString(str._str, jotter);
}

UString operator + (const UString &str, const double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   return UString(str._str, jotter);
}

UString operator + (const UString &str, const long double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%Lf", strVal);
   return UString(str._str, jotter);
}

UString operator + (const char ch, const UString &str)
{
   char inStr[2];

   inStr[0] = ch;
   inStr[1] = 0;

   return UString(inStr, str._str);
}

UString operator + (const char *inStr, const UString &str)
{
   return UString(inStr, str._str);
}

UString operator + (const int32_t strVal, const UString &str)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   return UString(jotter, str._str);
}

UString operator + (const uint32_t strVal, const UString &str)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return UString(jotter, str._str);
}

UString operator + (const double strVal, const UString &str)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   return UString(jotter, str._str);
}

UString operator + (const bool strVal, const UString &str)
{
   if (strVal)
      return UString(USTRING_TRUE, str._str);

   return UString(USTRING_FALSE, str._str);
}

UString operator + (const UString &str, const bool strVal)
{
   if (strVal)
      return UString(USTRING_TRUE, str._str);

   return UString(USTRING_FALSE, str._str);
}

UString operator + (const uuid_t strVal, const UString &str)
{
   if (uuid_is_null(strVal))
      return UString(str._str);
   else
   {
      char jotter[38];

      uuid_unparse(strVal, jotter);

      return UString(jotter, str._str);
   }
}

UString operator + (const UString &str, const uuid_t strVal)
{
   if (uuid_is_null(strVal))
      return UString(str._str);
   else
   {
      char jotter[38];

      uuid_unparse(strVal, jotter);

      return UString(str._str, jotter);
   }
}

/*

String &UString::Format(const char *fmt, ...)
{
   va_list ap;
   char    jotter[4096];

   va_start(ap, fmt);

   int cnt = vsnprintf(jotter, sizeof(jotter), fmt, ap);

   va_end(ap);

   if (cnt > -1 && cnt < sizeof(jotter))
   {
      _newStr(jotter);
      return *this;
   }

   char *tmp = 0;

   try
   {
      if (cnt < 0)
         throw xMettle(__FILE__, __LINE__, "UString::Format - problem formating with vsnprintf '%s'", fmt);

      if ((tmp = (char *) realloc(tmp, cnt+1)) == 0)
         throw xMettle(__FILE__, __LINE__, "UString::Format - could not allocate memory for temp buffer '%d'", cnt + 1);

      va_start(ap, fmt);

      vsnprintf(tmp, cnt+1, fmt, ap);

      va_end(ap);

      _newStr(tmp);

      _FREE(tmp);

      return *this;
   }
   catch (...)
   {
      va_end(ap);
      _FREE(tmp)
      throw;
   }
}*/


bool UString::isDouble() const noexcept
{
   return Common::stringIsDouble(_str);
}

bool UString::isInt() const noexcept
{
   return Common::stringIsInt(_str);
}

bool UString::isUnsigned() const noexcept
{
   return Common::stringIsUnsigned(_str);
}

bool UString::isBool() const noexcept
{
   if (_isTrue(_str) || _isFalse(_str))
      return true;

   return false;
}

bool UString::isUUID() const noexcept
{
   if (_strLen != 36)
      return false;

   return Common::stringIsUUID(_str, false);
}

bool UString::isAlpha() const noexcept
{
   char *ptr;

   for (ptr = _str; *ptr; ptr++)
      if (!((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z')))
         return false;

   return true;
}

bool UString::isUpper() const noexcept
{
   char *ptr;

   for (ptr = _str; *ptr; ptr++)
      if (*ptr >= 'a' && *ptr <= 'z')
         return false;

   return true;
}

bool UString::isLower() const noexcept
{
   char *ptr;

   for (ptr = _str; *ptr; ptr++)
      if (*ptr >= 'A' && *ptr <= 'Z')
         return false;

   return true;
}

char UString::dirSep() noexcept
{
   return _DIR_SEPERATOR;
}

int UString::toInt() const
{
   return String::toInt(_str);
}


unsigned int UString::toUInt() const
{
   return String::toUInt(_str);
}


int32_t UString::toInt32() const
{
   return String::toInt32(_str);
}

uint32_t UString::toUInt32() const
{
   return String::toUInt32(_str);
}

int64_t UString::toInt64() const
{
   return String::toInt64(_str);
}

uint64_t UString::toUInt64() const
{
   return String::toUInt64(_str);
}

float UString::toFloat() const
{
   return String::toFloat(_str);
}

double UString::toDouble() const
{
   return String::toDouble(_str);
}

long double UString::toLongDouble() const
{
   return String::toLongDouble(_str);
}

bool UString::toBool() const noexcept
{
   if (isBool())
   {
      if (_isTrue(_str))
         return true;

      return false;
   }

   return false;
}

uuid_t *UString::toUUID(uuid_t *dest) const
{
   if (!isUUID())
      throw xMettle(__FILE__, __LINE__, MODULE, "Not a valid value for uuid conversion.");

   if (0 != uuid_parse(_str, *dest))
      throw xMettle(__FILE__, __LINE__, MODULE, "Value could not be parsed for uuid conversion.");

   return dest;
}

bool operator < (const UString &str1, const UString &str2) noexcept
{
   return strcmp(str1._str, str2._str) < 0 ? true : false;
}

bool operator > (const UString &str1, const UString &str2) noexcept
{
   return strcmp(str1._str, str2._str) > 0 ? true : false;
}

bool operator <= (const UString &str1, const UString &str2) noexcept
{
   return strcmp(str1._str, str2._str) <= 0 ? true : false;
}

bool operator >= (const UString &str1, const UString &str2) noexcept
{
   return strcmp(str1._str, str2._str) >= 0 ? true : false;
}

bool operator == (const UString &str1, const UString &str2) noexcept
{
   return strcmp(str1._str, str2._str) == 0 ? true : false;
}

bool operator != (const UString &str1, const UString &str2) noexcept
{
   return strcmp(str1._str, str2._str) != 0 ? true : false;
}

bool operator < (const UString &str1, const char *inStr) noexcept
{
   return strcmp(str1._str, inStr) < 0 ? true : false;
}

bool operator > (const UString &str1, const char *inStr) noexcept
{
   return strcmp(str1._str, inStr) > 0 ? true : false;
}

bool operator <= (const UString &str1, const char *inStr) noexcept
{
   return strcmp(str1._str, inStr) <= 0 ? true : false;
}

bool operator >= (const UString &str1, const char *inStr) noexcept
{
   return strcmp(str1._str, inStr) >= 0 ? true : false;
}

bool operator == (const UString &str1, const char *inStr) noexcept
{
   return strcmp(str1._str, inStr) == 0 ? true : false;
}

bool operator != (const UString &str1, const char *inStr) noexcept
{
   return strcmp(str1._str, inStr) != 0 ? true : false;
}

bool operator < (const UString &str1, const char ch) noexcept
{
   return str1._str[0] < ch ? true : false;
}

bool operator > (const UString &str1, const char ch) noexcept
{
   return str1._str[0] > ch ? true : false;
}

bool operator <= (const UString &str1, const char ch) noexcept
{
   return str1._str[0] <= ch ? true : false;
}

bool operator >= (const UString &str1, const char ch) noexcept
{
   return str1._str[0] >= ch ? true : false;
}

bool operator == (const UString &str1, const char ch) noexcept
{
   return str1._str[0] == ch ? true : false;
}

bool operator != (const UString &str1, const char ch) noexcept
{
   return str1._str[0] != ch ? true : false;
}

std::ostream &operator << (std::ostream &output, const UString &str) noexcept
{
   output << str._str;

   return output;
}

std::istream &operator >> (std::istream &input, UString &str)
{
   char *p;

   input >> p;

   str = p;

   return input;
}

bool UString::_isTrue(const char *str) noexcept
{
   if (!stricmp(str, "y")          ||
       !stricmp(str, "t")           ||
       !stricmp(str, USTRING_TRUE) ||
       !stricmp(str, USTRING_YES)  ||
       !stricmp(str, "1"))
      return true;

   return false;
}

bool UString::_isFalse(const char *str) noexcept
{
   if (!stricmp(str, "n")              ||
       !stricmp(str, "f")              ||
       !stricmp(str, USTRING_FALSE)    ||
       !stricmp(str, USTRING_NO)       ||
       !stricmp(str, "0"))
      return true;

   return false;
}

bool UString::isEmpty() const noexcept
{
   return _str == 0 || *_str == 0;
}

UString &UString::clear() noexcept
{
   *_str    = 0;
   _strLen  =
   _byteLen = 0;

   return *this;
}

UString &UString::eat(char *str) noexcept
{
   _freeStr();

   _str        = str;
   _strLen     = UChar::u8_strlen(str);
   _byteLen    = strlen(str);
   _bytesUsed  = _byteLen + 1;

   return *this;
}

UString &UString::eat(String &str) noexcept
{
   _freeStr();

   _str        = str._str;
   _strLen     = UChar::u8_strlen(_str);
   _byteLen    = str._strLen;
   _bytesUsed  = str._bytesUsed;

   str._str        = 0;
   str._strLen     =
   str._bytesUsed  = 0;

   return *this;
}

UString &UString::eat(UString &str) noexcept
{
   _freeStr();

   _str        = str._str;
   _strLen     = str._strLen;
   _byteLen    = str._byteLen;
   _bytesUsed  = str._bytesUsed;

   str._str        = 0;
   str._strLen     =
   str._byteLen    =
   str._bytesUsed  = 0;

   return *this;
}

UString &UString::eat(StringBuilder &str) noexcept
{
   _freeStr();

   _str        = str.value;
   _strLen     = UChar::u8_strlen(_str);
   _byteLen    = str.used;
   _bytesUsed  = str.size;

   str.value = 0;
   str.used  =
   str.size  = 0;

   if (!_str)
      _newStr(_EMPTY_STRING);

   return *this;
}

char *UString::poop() noexcept
{
   char *tmp = _str;

   _str       = 0;
   _strLen    =
   _bytesUsed = 0;

   _newStr(_EMPTY_STRING);

   return tmp;
}

unsigned int UString::toCharArray(UChar::List &list)    const
{
   list.allocate(_strLen);

   if (_strLen < 1)
      return 0;

   uint32_t     ch;
   unsigned int idx = 0;

   for (ch = UChar::u8_next_char(_str, idx); ch != 0; UChar::u8_next_char(_str, idx))
      list.append(new UChar(ch));

   return list.count();
}

/*String &UString::Lower() noexcept
{
   for (char *p = _str; *p; p++)
      *p = tolower(*p);

   return *this;
}*/


/*UString &UString::Upper()
{
   for (char *p = _str; *p; p++)
      *p = toupper(*p);

   return *this;
}*/

#if 0

static char *stoupper(const char *s)
{
   char *p = NULL, *oldp;
   size_t len;
   wchar_t wc;
   int wclen, mclen;

   if (s)
   {
      len = strlen(s);
      oldp = p = (char*)malloc(len + 1024 + 1);
      if (p)
      {
         while ((wclen = mbtowc(&wc, s, len)) > 0)
         {
            /* I know, too many casts, but makes -Wconversion flag happy */
            mclen = wctomb(p, (wchar_t)towupper((wint_t)wc));
            /* Strange ... but I always trust Ben :) */
            if (mclen > wclen)
            {
               len += (size_t)(mclen - wclen);
               mclen = (int)(p - oldp);
               /* realloc it's a pain, but what else can I do? */
               p = (char*)realloc(oldp, len);
               if (!p)
               {
                  free(oldp);
                  return NULL;
               }
               oldp = p;
            }
            p += mclen;
            s += wclen;
         }
         *p = '\0';
         p -= len;
      }
   }

   return p;
}

#endif


/*String &UString::JoinDirs(const char *firstDir, ...)
{
   va_list    ap;
   const char *ptr;

   va_start(ap, firstDir);

   *this = firstDir;

   while (1)
   {
      if ((ptr = va_arg(ap, const char*)) == 0)
         break;

      MakeDirectory();
      *this += ptr;
   }

   va_end(ap);

   return *this;
}

void UString::GetPathAndFileName(String &destPath, String &destfile) const
{
   char *p = &_str[_strLen - 1];

   for (; p > _str; p--)
      if (*p == _DIR_SEPERATOR)
         break;

   if (p < _str)
      return;

   if (p > _str)
   {
      destPath = this->Splice(0, p - _str);
      destfile = this->Splice((p - _str) + 1);
   }
   else
   {
      destPath = "";
      destfile = this->_str;
   }
}

*/


UString UString::fileExtension(const char extCh) const
{
   int pos = reverseFind(extCh);

   if (pos < 0)
      return UString();

   return UString(_str + pos);
}

UString &UString::makeDirectory()
{
   if (_strLen == 0)
   {
      char tmp[3];

      tmp[0] = '.';
      tmp[1] = _DIR_SEPERATOR;
      tmp[2] = 0;

      *this = tmp;
      return *this;
   }

   if (!endsWith(_DIR_SEPERATOR))
      *this += _DIR_SEPERATOR;

   return *this;
}

int UString::find(const UString &inStr, const unsigned int offset) const noexcept
{
   char *tmp;
   char *pos;

   if (offset >= _bytesUsed)
      return -1;

   pos = _str + offset;

   if ((tmp = UChar::u8_strstr(pos, inStr._str)) == 0)
      return -1;

   return tmp - _str;
}

int UString::reverseFind(const UString &inStr, const unsigned int offset) const noexcept
{
   char         *tmp = _str;
   unsigned int  len = inStr._byteLen;
   unsigned int  idx = _byteLen;

   if (offset >= _strLen || len < 1)
      return -1;

   if (offset == 0)
      tmp += _strLen;
   else
      tmp += offset;

   for (; tmp >= _str; UChar::u8_dec(tmp, idx))
      if (strncmp(tmp, inStr._str, len) == 0)
         return tmp - _str;

   return -1;
}

unsigned int UString::calcIndexFromOffset(const unsigned int offset) const noexcept
{
   if (offset > _byteLen || _strLen < 1)
      return 0;

   unsigned int  idx  = 0;
   char         *ptr = _str;
   char         *fnd = _str + offset;

   while (*ptr)
   {
      if (ptr >= fnd)
         break;

      UChar::u8_inc(ptr);
      idx++;
   }

   return idx;
}

unsigned int UString::count(const UString &inStr) const noexcept
{
   char         *tmp;
   char         *pos = _str;
   unsigned int  cnt = 0;

   while ((tmp = UChar::u8_strstr(pos, inStr._str)) != 0)
   {
      pos = tmp + 1;
      cnt++;
   }

   return cnt;
}

UString &UString::replace(const UString &oldStr, const UString &newStr)
{
   _replaceStr(oldStr, newStr);

   return *this;
}

bool UString::endsWith(const UString &inStr) const noexcept
{
   if (_strLen < inStr._strLen)
      return false;

   if (strcmp(_str + (_byteLen - inStr._byteLen), inStr._str) == 0)
      return true;

   return false;
}

bool UString::startsWith(const UString &inStr) const noexcept
{
   if (_strLen < inStr._strLen)
      return false;

   if (strncmp(_str, inStr._str, inStr._byteLen) == 0)
      return true;

   return false;
}

int UString::compare(const UString &cmpStr, const bool ignoreCase) const noexcept
{
   if (ignoreCase)
      return stricmp(_str, cmpStr._str); // needs to be fixed later

   return strcmp(_str, cmpStr._str);
}

int UString::_compare(const void *obj) const
{
   return strcmp(_str, ((const UString *) obj)->_str);
}

UString &UString::checkLength() noexcept
{
   _byteLen = strlen(_str);
   _strLen  = UChar::u8_strlen(_str);

   return *this;
}

UString &UString::setSize(const unsigned length, const bool clear)
{
   if (length >= _bytesUsed)
   {
      char *tmp;

      if ((tmp = (char *) realloc(_str, length + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", length + 1);

      _str       = tmp;
      _bytesUsed = length + 1;
   }

   if (clear)
      memset(_str, 0, _bytesUsed);

   return *this;
}

unsigned int UString::split(const UString &sep, UString::List &dest, const char enclosingChar) const
{
   dest.clear();

   if (*_str == 0)
      return 0;

   unsigned int offset = 0;
   int          pos;

   // TODO if (enclosingChar == 0)
   {
      for (;;)
      {
         if ((pos = find(sep, offset)) < 0)
            break;

         if (pos == offset)
            dest.append(new UString());
         else
            dest.append(new UString(_str + offset, (unsigned int) (pos - offset)));

         offset = pos + sep.byteLength();
      }

      dest.append(new UString(_str + offset));
   }
   /*else
   {
      // TODO.
   }*/

   return dest.count();
}

unsigned int UString::splitLines(UString::List &dest) const
{
   return split(_LINE_ENDING, dest);
}

void UString::_replaceStr(const UString &dest, const UString &source)
{
   char *to_ptr;

   if ((to_ptr = UChar::u8_strstr(_str, dest._str)) == 0)
      return;

   char          *from_ptr;
   char          *mem;
   UString        newStr;
   unsigned int   offset;

   from_ptr = _str;

   for (; to_ptr;)
   {
      if (to_ptr - from_ptr > 0)
      {
         offset           = newStr._byteLen;
         newStr._byteLen += to_ptr - from_ptr;

         if (newStr._byteLen >= newStr._bytesUsed)
         {
            newStr._bytesUsed = newStr._byteLen + 1;

            if ((mem = (char *) realloc(newStr._str, newStr._bytesUsed)) == 0)
               throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", newStr._bytesUsed);

            newStr._str = mem;
         }

         memcpy(newStr._str + offset, from_ptr, to_ptr - from_ptr);
      }

      offset          = newStr._byteLen;
      newStr._byteLen += source._byteLen;

      if (newStr._byteLen >= newStr._bytesUsed)
      {
        newStr._bytesUsed = newStr._byteLen + 1;

        if ((mem = (char *) realloc(newStr._str, newStr._bytesUsed)) == 0)
            throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", newStr._bytesUsed);

        newStr._str = mem;
      }

      memcpy(newStr._str + offset, source._str, source._byteLen);

      from_ptr  = to_ptr + dest._byteLen;
      to_ptr    = UChar::u8_strstr(from_ptr, dest._str);
   }

   if (*from_ptr)
   {
      offset = newStr._byteLen;

      newStr._byteLen += strlen(from_ptr);

      if (newStr._byteLen >= newStr._bytesUsed)
      {
         newStr._bytesUsed = newStr._byteLen + 1;

         if ((mem = (char *) realloc(newStr._str, newStr._bytesUsed)) == 0)
            throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", newStr._bytesUsed);

         newStr._str = mem;
      }

      strcpy(newStr._str + offset, from_ptr);
   }

   newStr._str[newStr._byteLen] = 0;

   _freeStr();

   _str              = newStr._str;
   _strLen           = UChar::u8_strlen(_str);
   _byteLen          = newStr._byteLen;
   _bytesUsed        = newStr._bytesUsed;

   newStr._str       = 0;
   newStr._strLen    = 0;
   newStr._byteLen   = 0;
   newStr._bytesUsed = 0;
}

UString &UString::subString(const unsigned int start, const unsigned int len)
{
   if (start >= _strLen)
      throw xMettle(__FILE__, __LINE__, MODULE, "UString::SubString - Start position (%u) is greater than the length (%d)", start, _strLen);

   if (len && len > _strLen - start)
      throw xMettle(__FILE__, __LINE__, MODULE, "UString::SubString - Length (%u) is greater than remaining string length (%d)", len, _strLen - start);

   unsigned int    idx;
   unsigned int    end = len ? start + len : _strLen;
   char           *startPtr = _str;
   char           *endPtr;

   for (idx = 0; idx < start; idx++)
      UChar::u8_inc(startPtr);

   endPtr = startPtr;

   for (; idx < end; idx++)
      UChar::u8_inc(endPtr);

   _byteLen = endPtr - startPtr;

   memmove(_str, startPtr, _byteLen);

   _str[endPtr - startPtr] = 0;
   _strLen                 = len ? len : UChar::u8_strlen(_str);

   return *this;
}

int UString::stricmp(const char *a, const char *b) noexcept
{
   int n;

   for (; ;a++, b++)
   {
      n = tolower((unsigned char)*a) - tolower((unsigned char)*b);

      if (n || !*a)
         break;
   }

   return n;
}

int UString::strnicmp(const char *a, const char *b, const unsigned int n) noexcept
{
   unsigned i, ret;

   for (i = 0; i < n; i++)
   {
      ret = tolower((unsigned char)*a) - tolower((unsigned char)*b);

      if (ret || !*a)
         break;

      a++;
      b++;
   }

   return ret;
}

const char *UString::empty() noexcept
{
   return _EMPTY_STRING;
}

UString::List::List() noexcept
{
}

UString::List::~List() noexcept
{
}

#undef MAX_INT_STR_SIZE
#undef USTRING_TRUE
#undef USTRING_FALSE
#undef USTRING_YES
#undef USTRING_NO
#undef WHITE_SPACES
#undef MODULE

}}
