/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_LINKLIST_H_
#define __METTLE_LIB_LINKLIST_H_

#include <memory.h>

/** \def DECLARE_LINK_LIST_CLASS(cls)
 * \brief A utility macro to more easily declare a LinkList class
 *
 * This macro should generally be used in the public section of any class that requires a
 * link list class.  This is in line with mettle standard of adding helper classes as sub classes
 * to the base class.
 */

#define DECLARE_LINK_LIST_CLASS(cls) class LinkList:public Mettle::Lib::LinkList<cls> {}

namespace Mettle { namespace Lib {

/** \class LinkList linklist.h mettle/lib/linklist.h
 * \brief A template link list class.
 *
 * This template link list class implements a basic link list object.  This class
 * will delete any objects passed to when they are removed from the link list.
 */
template <class type>
class LinkList
{
public:

   /** \brief Constructor.
    */
  LinkList() noexcept;

   /** \brief Destructor.
    */
   virtual ~LinkList() noexcept;

   /** \brief Spawns a new node in the linklist.
    *
    * This method adds the passed in object pointer to the link list.
    * \param obj the object to be added to the link list.
    * \param toCurrPos if set to true, the object will be inserted at the current link list position.  Else if left falst the object is appended to the end of the link list.
    * \returns the object passed in for conveince to the calling code.
    */
   type *add(      type *obj,
             const bool  toCurrPos = false) noexcept;

   /** \brief Delete the entire linklist along with all the objects in the link list.
   */
   void purge() noexcept;


   /** \brief Gets the next object in the link list.
    *
    * This method returns the next object in the list and move the internal position one forward.
    * \returns the next object in the list or NULL if the end of the list has been reached.
   */
   type *next() noexcept;

   /** \brief Gets the previous object in the link list.
    *
    * This method returns the previous object in the list and move the internal position one backward.
    * \returns the previous object in the list or NULL if the start of the list has been reached.
   */
    type *prev() noexcept;

   /** \brief Removes the object at the current location in the link list.
    *
    * After deleting the current object the link is automatically moved to the next available object.
    * If there is another object in front the list is moved there, otherwise it attempts to move backward.
    * \returns the object at the new current position or NULL if the list is empty.
   */
    type *remove() noexcept;

   /** \brief Gets the object at the current position.
    *
    * \returns the object at the current position or NULL if the current position is not valid.
   */
    type *current() noexcept;

   /** \brief Moves the intenal position to the start of the link list.
   */
   void start() noexcept;

   /** \brief Moves the intenal position to the end of the link list.
   */
   void end() noexcept;

   /** \brief Gets the first object in the link list
    * \returns the object at the start of the link list or NULL if the link list is empty.
   */
   type *first() noexcept;

   /** \brief Gets the last object in the link list
    * \returns the object at the end of the link list or NULL if the link list is empty.
   */
   type *last() noexcept;

   /** \brief Gets a reference to the lists current position.
    * \returns a pointer to the current link list position
   */
	void *positionGet()  noexcept { return (void *) _currPos; }

   /** \brief Sets the posiition of the link list that was derived from 'positionGet'
    * \param pos the poisition that will be assigned to the link lists internal position.
    * \warning passing in an invalid pointer will make your program go BOOM.
   */
	void positionSet(void *pos) noexcept { _currPos = (LinkListNode *) pos; }

protected:

      typedef struct LinkListTag
      {
         type        *_rec;
         LinkListTag *_next;
         LinkListTag *_prev;
      } LinkListNode;

      LinkListNode       *_first;      // start of list
      LinkListNode       *_last;       // end of list
      LinkListNode       *_currPos;    // current position in the list
};


template <class type>
LinkList<type>::LinkList() noexcept
{
   _currPos = _first = _last = 0;
}

template <class type>
LinkList<type>::~LinkList() noexcept
{
   purge();
}

template <class type>
void LinkList<type>::start() noexcept
{
   _currPos = _first;
}

template <class type>
void LinkList<type>::end() noexcept
{
   _currPos = _last;
}

template <class type>
type *LinkList<type>::first() noexcept
{
   if (_first)
      return _first->_rec;

   return 0;
}

template <class type>
type *LinkList<type>::last() noexcept
{
   if (_last)
      return _last->_rec;

   return 0;
}

template <class type>
type *LinkList<type>::current() noexcept
{
   if (_currPos)
      return _currPos->_rec;

   return 0;
}

template <class type>
type *LinkList<type>::add(type *rec, const bool fromCurrPos) noexcept
{
   LinkListNode *ptr;

   ptr = new LinkListNode;

   ptr->_rec = rec;

   // check if this is the _first one in the list
   if (!_first)
   {
      ptr->_next = 0;
      ptr->_prev = 0;
      _currPos  = _first = _last = ptr;
      return rec;
   }

   if (!fromCurrPos)
   {
      // else just append to the end of the list
      ptr->_next   = 0;
      ptr->_prev   = _last;
      _last->_next = ptr;
      _last        = ptr;

      return rec;
   }

   if (_currPos == 0)
   {
      // if we have landed here then we need to add it to the beginning
      _currPos = ptr;
      _currPos->_next = _first;
      _currPos->_prev = 0;

      _first = _currPos;

      if (_currPos->_next)
         _currPos->_next->_prev = _currPos;

      return rec;
   }

   ptr->_next        = _currPos->_next;
   ptr->_prev        = _currPos;
   _currPos->_next   = ptr;

   if (ptr->_next)
      ptr->_next->_prev = ptr;
   else
	  _last = ptr;

   _currPos = ptr;

   return rec;
}

template <class type>
void LinkList<type>::purge() noexcept
{
   if (!_first)
      return;

   LinkListNode *ptr;

   for (ptr = _first; ptr->_next;)
   {
      if (ptr->_rec)
         delete ptr->_rec;

      ptr = ptr->_next;

      delete ptr->_prev;
   }

   if (ptr->_rec)
      delete ptr->_rec;

   delete ptr;

   _currPos = _first = _last = 0;
}

template <class type>
type *LinkList<type>::remove() noexcept
{
   if (!_currPos)
      return 0;

   if (_currPos->_rec)
      delete _currPos->_rec;

   // check to see if we're the only instance in the list
   if (_first == _last)
   {
      delete _first;
      _currPos = _first = _last = 0;
      return 0;
   }

   // if _first element in list
   if (_first == _currPos)
   {
      LinkListNode *ptr = _first;

      _first = _first->_next;

      ptr = ptr->_next;

      delete _first->_prev;
      _first->_prev = 0;
      _currPos    = _first;
      return _currPos->_rec;
   }

   // else if last element in list
   if (_last == _currPos)
   {
      _last = _last->_prev;
      delete _last->_next;
      _last->_next = 0;
      _currPos   = _last;
      return 0;
   }

   // else frag this instance
   LinkListNode *ptr;
   ptr      = _currPos;
   _currPos = _currPos->_next;

   if (ptr->_next)
      ptr->_next->_prev = ptr->_prev;

   if (ptr->_prev)
      ptr->_prev->_next = ptr->_next;

   delete ptr;

   return _currPos->_rec;
}

template <class type>
type *LinkList<type>::prev() noexcept
{
   if (_currPos)
   {
      type *rv = _currPos->_rec;
      _currPos = _currPos->_prev;
      return rv;
   }

   return 0;
}

template <class type>
type *LinkList<type>::next() noexcept
{
   if (_currPos)
   {
      type *rv = _currPos->_rec;
      _currPos = _currPos->_next;
      return rv;
   }

   return 0;
}

}}

#endif
