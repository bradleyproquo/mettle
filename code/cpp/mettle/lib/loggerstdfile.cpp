/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/loggerstdfile.h"

#include <time.h>

namespace Mettle { namespace Lib {

LoggerStdFile::LoggerStdFile(const char *logFile, const Level level, const bool display)
{
   if ((_logFH = fopen(logFile, "at+")) == 0)
      throw xMettle(__FILE__, __LINE__, "LoggerStdFile", "Cannot open/create specified logfile(%s)", logFile);

   _level   = level;
   _display = display;
}


LoggerStdFile::~LoggerStdFile() noexcept
{
   if (_logFH != 0)
   {
      fclose(_logFH);
      _logFH = 0;
   }
}


void LoggerStdFile::error(const char *fmt, ...) noexcept
{
   if (_level > Error)
      return;

   va_list ap;
   char    prefix[64];
   int     prefixLen;

   _getPrefix("ERR", prefix, &prefixLen);

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, stdout);
      _log(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, _logFH);
      _log(fmt, ap, _logFH);
      va_end(ap);
   }
}


void LoggerStdFile::warning(const char *fmt, ...) noexcept
{
   if (_level > Warning)
      return;

   va_list ap;
   char    prefix[64];
   int     prefixLen;

   _getPrefix("WRN", prefix, &prefixLen);

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, stdout);
      _log(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, _logFH);
      _log(fmt, ap, _logFH);
      va_end(ap);
   }
}


void LoggerStdFile::info(const char *fmt, ...) noexcept
{
   if (_level > Info)
      return;

   va_list ap;
   char    prefix[64];
   int     prefixLen;

   _getPrefix("INF", prefix, &prefixLen);

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, stdout);
      _log(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, _logFH);
      _log(fmt, ap, _logFH);
      va_end(ap);
  }
}


void LoggerStdFile::debug(const char *fmt, ...) noexcept
{
   if (_level > Debug)
      return;

   va_list ap;
   char    prefix[64];
   int     prefixLen;

   _getPrefix("DBG", prefix, &prefixLen);

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, stdout);
      _log(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      _logPrefix(prefix, prefixLen, _logFH);
      _log(fmt, ap, _logFH);
      va_end(ap);
   }
}


Logger *LoggerStdFile::trace(const char *file, const unsigned int line) noexcept
{
   if (!_logFH || _display)
      fprintf(stdout, "Trace [%s/%d] ", file, line);

   if (_logFH)
      fprintf(_logFH, "Trace [%s/%d] ", file, line);

   return this;
}


void LoggerStdFile::_getPrefix(const char *lvl,
                               char       *prefix,
                               int        *prefixLen) noexcept
{
   time_t       now;
   tm          *nowTm;

   time(&now);

   nowTm = localtime(&now);

  *prefixLen = sprintf(prefix,
                       "%04d%02d%02d%02d%02d%02d %s ",
                       nowTm->tm_year + 1900,
                       nowTm->tm_mon + 1,
                       nowTm->tm_mday,
                       nowTm->tm_hour,
                       nowTm->tm_min,
                       nowTm->tm_sec,
                       lvl);
}


void LoggerStdFile::_logPrefix(const char *prefix,
                               const int   prefixLen,
                               FILE       *fp) noexcept
{
   fwrite(prefix, prefixLen, 1, fp);
}


void LoggerStdFile::_log(const char *fmt,
                         va_list     ap,
                         FILE       *fp) noexcept
{
   vfprintf(fp, fmt, ap);
   fputc('\n', fp);
   fflush(fp);
}


}}
