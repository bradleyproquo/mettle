/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_MUTEX_H_
#define __METTLE_LIB_MUTEX_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

#if defined (OS_MS_WINDOWS)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#elif defined (OS_LINUX) || defined (OS_AIX)

#include <pthread.h>
#include <unistd.h>

#endif


namespace Mettle { namespace Lib {

/** \class Mutex mutex.h mettle/lib/mutex.h
 * \brief A helper object to manage a mutex.
 */
class Mutex
{
public:

   /** \brief Default constructor.
    *  \throws xMettle if a mutex could not be created.
    */
   Mutex();

   /** \brief Destructor.
    */
   virtual ~Mutex() noexcept;

   /** \brief Releases/unlocks the mutex for other threads.
    *  \throws xMettle if the mutex could not be released.
    */
   void unlock();

   /** \brief Locks the mutex, waits up to the \p in mili seconds.
    *  \p waittime the amount of time to wait in mili-seconds for the mutex.
    *  \throws xMettle if the mutex could not be locked in the specified time.
    */
   void lock(const uint32_t waittime = 0);

   /** \class Auto mutex.h mettle/lib/mutex.h
    * \brief A helper object to auto lock and releae a mutex using the stack.
    */
   class Auto
   {
   public:

      /** \brief Constructor.
       *  \p mutex the mutex to be auto locked & released.
       *  \p waittime the amount of time to wait in mili-seconds for the mutex.
       *  \throws xMettle if a mutex could not be created or locked.
       */
      Auto(      Mutex    *mutex,
           const uint32_t  waittime = 0);

      /** \brief Destructor
       *  \throws xMettle if the mutex could not be released.
       */
      ~Auto();

     private:
        Mutex *_mutex;
   };

private:

      static void       _throwMutexError(int errCode, const char *str);

#if defined (OS_MS_WINDOWS)

      HANDLE _mutex;

#elif defined (OS_LINUX) || defined (OS_AIX)

      pthread_mutex_t _mutex;

#endif
};

}}

#endif
