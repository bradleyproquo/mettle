/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_IO_IWRITER_H_
#define __METTLE_IO_IWRITER_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/platform.h"

#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/string.h"
#include "mettle/lib/ustring.h"

namespace Mettle { namespace IO {

/** \interface IWriter iwriter.h mettle/io/iwriter.h
 *  \brief Writer interface used for serializing
 *
 * ISerializable uses any implementation of IWriter for writting (serializing)
 * data streams.
 */
class IWriter
{
public:

   /** \brief Declare Safe Class.
   */
   DECLARE_SAFE_CLASS(IWriter);

   /** destructor.
   *
   */
   virtual ~IWriter() throw ();

   /** \brief Implemented method should clear/reset the writer object.
    */
   virtual void  clear() = 0;

   /** \brief The name of the implemented writer.
    *
    * This name is usually the same as it's sister Reader implementation
    */
   virtual const char *name()  = 0;


   /** \brief This method is called at the start of a serialization.
     * \param name the name of the object being serialized.
     * \return the number of bytes actually written.
    */
   virtual unsigned int writeStart(const char *name) = 0;

   /** \brief This method is called at the end when the last data member has been serialized.
     * \param name the name of the object that has just completed being serialized.
     * \return the number of bytes actually written.
    */
   virtual unsigned int writeEnd(const char *name) = 0;

   /** \brief Implemented method to write a boolean.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const bool &v) = 0;

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   /** \brief Implemented method to write a char.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     = \warning on some version of Microsoft Visual C++ this method is not available because of known bug in the compiler.  If this is the case use the 'int8_t' method instead.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const char &v) = 0;
#endif

   /** \brief Implemented method to write an int8_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const int8_t &v) = 0;

   /** \brief Implemented method to write an int16_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const int16_t &v) = 0;

   /** \brief Implemented method to write an int32_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const int32_t &v) = 0;

   /** \brief Implemented method to write an int64_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const int64_t &v) = 0;

   /** \brief Implemented method to write an uint8_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const uint8_t &v) = 0;

   /** \brief Implemented method to write an uint16_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const uint16_t &v) = 0;

   /** \brief Implemented method to write an uint32_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const uint32_t &v) = 0;

   /** \brief Implemented method to write an uint64_t .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const uint64_t &v) = 0;

   /** \brief Implemented method to write a double .
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const double &v) = 0;

   /** \brief Implemented method to write a float.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const float &v) = 0;

   /** \brief Implemented method to write a String.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const Mettle::Lib::String &v) = 0;

   /** \brief Implemented method to write a UTF8 String.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const Mettle::Lib::UString &v) = 0;

   /** \brief Implemented method to write a Date Time.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const Mettle::Lib::DateTime &v) = 0;

   /** \brief Implemented method to write a Date.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const Mettle::Lib::Date &v) = 0;

   /** \brief Implemented method to write a Time.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const Mettle::Lib::Time &v) = 0;

   /** \brief Implemented method to write a memory block.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \warning Not all writers are able to write blobs.  Some may throw exceptions if this method is not possible.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const Mettle::Lib::MemoryBlock &v) = 0;

   /** \brief Implemented method to write a uuid.
     * \param field the name of the field to be written.
     * \param v the variable that will be written to \p field.
     * \return the number of bytes actually written.
    */
   virtual unsigned int write(const char *field, const Mettle::Lib::Guid &v) = 0;

protected:

   /** Constructor.
    * \brief Pure virtual class (Interface).
    */
   IWriter() throw ();
};

}}

#endif
