/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/memorystream.h"

#include "mettle/lib/macros.h"

#include <stdlib.h>
#include <string.h>

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

#define MODULE              "MemoryStream"
#define STREAM_SIZE         4096

MemoryStream::MemoryStream() noexcept
{
   _memory        = 0;
   _sizeUsed      = 0;
   _sizeAllocated = 0;
   _offset        = 0;
}


MemoryStream::MemoryStream(const void *mem, const unsigned int size)
{
   _memory        = 0;
   _sizeUsed      = 0;
   _sizeAllocated = 0;
   _offset        = 0;

   write(mem, size);
}


MemoryStream::~MemoryStream() noexcept
{
   purge();
}


void MemoryStream::clear()
{
   _offset   = _memory;
   _sizeUsed = 0;
}


void MemoryStream::purge()
{
   _FREE(_memory)
   _sizeUsed      = 0;
   _sizeAllocated = 0;
}


void MemoryStream::read(void *dest, const unsigned int size)
{
   if (size < 1)
      return;

   if (_offset == 0)
   {
      _offset = _memory;
   }
   else if (_offset < _memory)
   {
      throw xMettle(__FILE__, __LINE__, MODULE, "Attempted to read before memory boundry!");
   }

   if (_offset + size > this->_memory + this->_sizeUsed)
   {
      throw xMettle(__FILE__, __LINE__, MODULE, "Attempted to read beyond memory boundry!");
   }

   memcpy(dest, _offset, size);

   _offset += size;
}


void MemoryStream::write(const void *mem, const unsigned int size)
{
   if (size < 1)
      return;

   if (_sizeUsed + size > _sizeAllocated)
      _allocateMemory(size);

   memcpy(_memory + _sizeUsed, mem, size);

   _sizeUsed += size;
}


unsigned int MemoryStream::size() const
{
   return _sizeUsed;
}


void MemoryStream::eat(MemoryStream *me)
{
   purge();

   _memory        =
   _offset        = me->_memory;
   _sizeUsed      = me->_sizeUsed;
   _sizeAllocated = me->_sizeAllocated;

   me->_memory        = 0;
   me->_sizeUsed      = 0;
   me->_sizeAllocated = 0;
   me->_offset        = 0;
}


void MemoryStream::eat(unsigned int size, void *me)
{
   purge();

   _memory        =
   _offset        = (uint8_t *) me;
   _sizeUsed      =
   _sizeAllocated = size;
}


void MemoryStream::poop(      MemoryStream *dest,
                        const unsigned int  offset)
{
   if (_sizeUsed == 0)
      return;

   if (offset >= _sizeUsed)
      throw xMettle(__FILE__, __LINE__, MODULE, "Cannot poop [%u] beyond the memory bounds [%u].", offset, _sizeUsed);

   if (offset == 0)
   {
      dest->eat(this);

      return;
   }

   dest->purge();

   dest->write(_memory + offset, _sizeUsed - offset);

   uint8_t *tmp = (uint8_t *) realloc(this->_memory, offset);

   if (!tmp)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Could not allocate [%u] bytes for the poop shrink.", offset);

   _sizeUsed      = offset;
   _sizeAllocated = offset;
   _memory        =
   _offset        = tmp;
}


void MemoryStream::positionStart() noexcept
{
   _offset = _memory;
}


void MemoryStream::positionEnd() noexcept
{
   _offset = _memory + _sizeUsed;
}


void MemoryStream::positionMove(const unsigned int offset)
{
   uint8_t *dest = _offset + offset;

   if (dest - _offset > _sizeUsed)
      throw xMettle(__FILE__, __LINE__, MODULE, "Cannot move stream position [%u] beyond the memory bounds [%u].", dest - _offset, _sizeUsed);

   _offset = dest;
}


unsigned int MemoryStream::position() const noexcept
{
   return _offset - _memory;
}


void *MemoryStream::addMemory(const unsigned int memSize)
{
   _allocateMemory(memSize);

   _sizeUsed += memSize;

   return _memory;
}


void *MemoryStream::_allocateMemory(const unsigned int memSize)
{
   unsigned int needed = _sizeUsed + memSize;

   _sizeAllocated += STREAM_SIZE;

   if (needed >= _sizeAllocated)
   {
      int increments = (needed - _sizeAllocated) / STREAM_SIZE;

      increments++;

      _sizeAllocated += STREAM_SIZE * increments;
   }

   unsigned int   localOffset  = _offset - _memory;
   void          *tmp          = realloc(_memory, _sizeAllocated);

   if (!tmp)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes", _sizeAllocated);

   _memory = (uint8_t *) tmp;
   _offset = _memory + localOffset;

   return this->_memory;
}

#undef MODULE

}}
