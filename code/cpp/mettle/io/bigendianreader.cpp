/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/bigendianreader.h"

#include "mettle/io/istream.h"

#include "mettle/lib/common.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/xmettle.h"

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

#define MODULE "BigEndianReader"

BigEndianReader::BigEndianReader(IStream *strem, const int32_t tzTarg) noexcept
{
   _stream  = strem;

   DateTime::calcTZHourMin(tzTarg, _tzhour, _tzmin);
}

BigEndianReader::~BigEndianReader() noexcept
{
}

void BigEndianReader::clear()
{
   _stream->clear();
}


const char *BigEndianReader::name()
{
   return "Mettle.BigEndianReader";
}

unsigned int BigEndianReader::readStart(const char *name)
{
   return 0;
}

unsigned int BigEndianReader::readEnd(const char *name)
{
   return 0;
}

unsigned int BigEndianReader::read(const char *field, bool &v)
{
   int8_t boolval;

   read(field, boolval);

   v = boolval ? true : false;

   return sizeof(boolval);
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int BigEndianReader::read(const char *field, char &v)
{
   _stream->read(&v, sizeof(v));
   return sizeof(v);
}
#endif

unsigned int BigEndianReader::read(const char *field, int8_t &v)
{
   _stream->read(&v, sizeof(v));
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, int16_t &v)
{
   _stream->read(&v, sizeof(v));
#if defined(_ENDIAN_LITTLE)
   v = Common::swapBytes(v);
#endif
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, int32_t &v)
{
   _stream->read(&v, sizeof(v));
#if defined(_ENDIAN_LITTLE)
   v = Common::swapBytes(v);
#endif
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, int64_t &v)
{
   _stream->read(&v, sizeof(v));
#if defined(_ENDIAN_LITTLE)
   v = Common::swapBytes(v);
#endif
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, uint8_t &v)
{
   _stream->read(&v, sizeof(v));
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, uint16_t &v)
{
   _stream->read(&v, sizeof(v));
#if defined(_ENDIAN_LITTLE)
   v = Common::swapBytes(v);
#endif
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, uint32_t &v)
{
   _stream->read(&v, sizeof(v));
#if defined(_ENDIAN_LITTLE)
   v = Common::swapBytes(v);
#endif
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, uint64_t &v)
{
   _stream->read(&v, sizeof(v));
#if defined(_ENDIAN_LITTLE)
   v = Common::swapBytes(v);
#endif
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, double &v)
{
   _stream->read(&v, sizeof(v));
#if defined(_ENDIAN_LITTLE)
   v = Common::swapBytes(v);
#endif
   return sizeof(v);
}

unsigned int BigEndianReader::read(const char *field, float &v)
{
   double       d = 0;
   unsigned int size;

   size = read(field, d);
   v    = (float) d;

   return size;
}

unsigned int BigEndianReader::read(const char *field, String &v)
{
   int32_t size;

   read(field, size);

   _stream->read((char *) v.setSize(size, true).c_str(), size);

   //v.CheckLength();

   return sizeof(size) + size;
}

unsigned int BigEndianReader::read(const char *field, UString &v)
{
   int32_t size;

   read(field, size);

   _stream->read((char *) v.setSize(size, true).c_str(), size);

   v.checkLength();

   return sizeof(size) + size;
}

unsigned int BigEndianReader::read(const char *field, DateTime &v)
{
   int32_t date;
   int32_t time;
   int8_t  tzhour;
   int8_t  tzmin;

   read(field, date);
   read(field, time);
   read(field, tzhour);
   read(field, tzmin);

   v = DateTime(date, time, tzhour, tzmin, false);

   if (_tzhour != INT8_MIN)
      v.tzConverTo(_tzhour, _tzmin);

   return sizeof(date) + sizeof(time) + sizeof(tzhour) + sizeof(tzmin);
}

unsigned int BigEndianReader::read(const char *field, Date &v)
{
   int32_t date;

   read(field, date);

   v = Date(date, false);

   return sizeof(date);
}

unsigned int BigEndianReader::read(const char *field, Time &v)
{
   int32_t time;

   read(field, time);

   v = Time(time, false);

   return sizeof(time);
}

unsigned int BigEndianReader::read(const char *field, MemoryBlock &v)
{
   int32_t size;

   read(field, size);

   v.allocate(size);

   if (size > 0)
      _stream->read(v._block, size);

   return sizeof(size) + size;
}

unsigned int BigEndianReader::read(const char *field, Mettle::Lib::Guid &v)
{
   int8_t size;

   read(field, size);

   if (size == 0)
   {
      v.nullify();
      return sizeof(size);
   }

   if (size != 36)
      throw xMettle(__FILE__, __LINE__, MODULE, "Guid length should be [36], but read size of [%d] for field [%s]", size, field);

   char struuid[38];

   _stream->read(struuid, 36);

   struuid[37] = 0;

   v = struuid;

   return 36 + sizeof(int8_t);
}

#undef MODULE

}}
