/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/bigendianwriter.h"

#include "mettle/io/istream.h"

#include "mettle/lib/common.h"
#include "mettle/lib/platform.h"

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

BigEndianWriter::BigEndianWriter(IStream *strem, const int32_t tzTarg) noexcept
{
   _stream  = strem;

   DateTime::calcTZHourMin(tzTarg, _tzhour, _tzmin);
}

BigEndianWriter::~BigEndianWriter() noexcept
{
}

void BigEndianWriter::clear()
{
   _stream->clear();
}

const char *BigEndianWriter::name()
{
   return "Mettle.BigEndianWriter";
}

unsigned int BigEndianWriter::writeStart(const char *name)
{
   return 0;
}

unsigned int BigEndianWriter::writeEnd(const char *name)
{
   return 0;
}

unsigned int BigEndianWriter::write(const char *field, const bool &v)
{
   return write(field, v ? (int8_t) 1 : (int8_t) 0);
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int BigEndianWriter::write(const char *field, const char &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}
#endif

unsigned int BigEndianWriter::write(const char *field, const int8_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const int16_t &v)
{
#if defined(_ENDIAN_LITTLE)
    int16_t t = Common::swapBytes(v);
   _stream->write(&t, sizeof(v));
#else
   _stream->write(&v, sizeof(v));
#endif
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const int32_t &v)
{
#if defined(_ENDIAN_LITTLE)
    int32_t t = Common::swapBytes(v);
   _stream->write(&t, sizeof(v));
#else
   _stream->write(&v, sizeof(v));
#endif
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const int64_t &v)
{
#if defined(_ENDIAN_LITTLE)
    int64_t t = Common::swapBytes(v);
   _stream->write(&t, sizeof(v));
#else
   _stream->write(&v, sizeof(v));
#endif
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const uint8_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const uint16_t &v)
{
#if defined(_ENDIAN_LITTLE)
    uint16_t t = Common::swapBytes(v);
   _stream->write(&t, sizeof(v));
#else
   _stream->write(&v, sizeof(v));
#endif
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const uint32_t &v)
{
#if defined(_ENDIAN_LITTLE)
    uint32_t t = Common::swapBytes(v);
   _stream->write(&t, sizeof(v));
#else
   _stream->write(&v, sizeof(v));
#endif
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const uint64_t &v)
{
#if defined(_ENDIAN_LITTLE)
    uint64_t t = Common::swapBytes(v);
   _stream->write(&t, sizeof(v));
#else
   _stream->write(&v, sizeof(v));
#endif
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const double &v)
{
#if defined(_ENDIAN_LITTLE)
    double t = Common::swapBytes(v);
   _stream->write(&t, sizeof(v));
#else
   _stream->write(&v, sizeof(v));
#endif
   return sizeof(v);
}

unsigned int BigEndianWriter::write(const char *field, const float &v)
{
   double d = (double) v;

   return write(field, d);
}

unsigned int BigEndianWriter::write(const char *field, const String &v)
{
   int32_t size = (int32_t) v.length();

#if defined(_ENDIAN_LITTLE)
   int32_t t = Common::swapBytes(size);
   _stream->write(&t, sizeof(t));
#else
   _stream->write(&size, sizeof(size));
#endif

   _stream->write((char *) v.c_str(), size);

   return sizeof(size) + size;
}

unsigned int BigEndianWriter::write(const char *field, const UString &v)
{
   int32_t size = (int32_t) v.byteLength();

#if defined(_ENDIAN_LITTLE)
   int32_t t = Common::swapBytes(size);
   _stream->write(&t, sizeof(t));
#else
   _stream->write(&size, sizeof(size));
#endif

   _stream->write((char *) v, size);

   return sizeof(size) + size;
}

unsigned int BigEndianWriter::write(const char *field, const DateTime &v)
{
   if (_tzhour != INT8_MIN)
   {
      DateTime t(v);

      t.tzConverTo(_tzhour, _tzmin);

      return
         write(field, t.date.toInt32()) +
         write(field, t.time.toInt32()) +
         write(field, t.tzHour()) +
         write(field, t.tzMinute());
   }

   return
      write(field, v.date.toInt32()) +
      write(field, v.time.toInt32()) +
      write(field, v.tzHour()) +
      write(field, v.tzMinute());
}

unsigned int BigEndianWriter::write(const char *field, const Date &v)
{
   return write(field, v.toInt32());
}

unsigned int BigEndianWriter::write(const char *field, const Time &v)
{
   return write(field, v.toInt32());
}

unsigned int BigEndianWriter::write(const char *field, const Mettle::Lib::MemoryBlock &v)
{
   int32_t is = (int32_t) v.size();

#if defined(_ENDIAN_LITTLE)
   int32_t t = Common::swapBytes(is);
   _stream->write(&t, sizeof(t));
#else
   _stream->write(&is, sizeof(is));
#endif

   _stream->write(v.block(), is);

   return sizeof(is) + is;
}

unsigned int BigEndianWriter::write(const char *field, const Mettle::Lib::Guid &v)
{
   if (v.isNull())
      return write(field, (int8_t) 0);

   char struuid[38];

   v.toString(struuid);

   write(field, (int8_t) 36);
   _stream->write(struuid, 36);

   return 36 + sizeof(int8_t);
}

}}
