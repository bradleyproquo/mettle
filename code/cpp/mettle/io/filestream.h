/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_IO_FILESTREAM_H_
#define __METTLE_IO_FILESTREAM_H_

#include "mettle/lib/string.h"

#include "mettle/io/istream.h"

namespace Mettle {

namespace Lib {

class FileHandle;

}

namespace IO {

class MemoryStream;

class FileStream : public IStream
{
public:

   /** \brief Constructor.
   * \param fh intance to the file to be stream with.
   */
   FileStream(Mettle::Lib::FileHandle *fh)  noexcept;


   /** \brief Destructor.
   */
   virtual ~FileStream() noexcept;

   /** \brief Implements interface method.
    */
   void clear();

   /** \brief Implements interface method.
    */
   void purge();

   /** \brief Implements interface method.
    */
   void read(      void         *p,
             const unsigned int  size);

   /** \brief Implements interface method.
    */
   void write(const void         *p,
              const unsigned int  size);

   /** \brief Implements interface method.
    */
   unsigned int size() const;

   /** \brief Implements interface method.
    */
   void eat(MemoryStream *me);

   /** \brief Implements interface method.
    */
   void poop(      MemoryStream *dest,
             const unsigned int  offset);

   /** \brief Implements interface method.
    */
   void positionStart() noexcept;

   /** \brief Implements interface method.
    */
   void positionEnd() noexcept;

   /** \brief Implements interface method.
    */
   void positionMove(const unsigned int offset);

   /** \brief Implements interface method.
    */
   unsigned int position() const noexcept;

protected:

   /** \brief Internal file hanlde object
    */
   Mettle::Lib::FileHandle  *_fh;
   Mettle::Lib::String       _fileName;
};

}

}

#endif
