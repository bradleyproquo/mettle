/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/streamwriter.h"

#include "mettle/io/istream.h"

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

StreamWriter::StreamWriter(IStream *stream, const int32_t tzTarg) noexcept
{
   _stream  = stream;

   DateTime::calcTZHourMin(tzTarg, _tzhour, _tzmin);
}

StreamWriter::~StreamWriter() noexcept
{
}

const char *StreamWriter::name()
{
   return "Mettle.StreamWriter";
}

void StreamWriter::clear()
{
   _stream->clear();
}

unsigned int StreamWriter::writeStart(const char *name)
{
   return 0;
}

unsigned int StreamWriter::writeEnd(const char *name)
{
   return 0;
}

unsigned int StreamWriter::write(const char *field, const bool &v)
{
   return write(field, v ? (int8_t) 1 : (int8_t) 0);
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int StreamWriter::write(const char *field, const char &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}
#endif

unsigned int StreamWriter::write(const char *field, const int8_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const int16_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const int32_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const int64_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const uint8_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const uint16_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const uint32_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const uint64_t &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const double &v)
{
   _stream->write(&v, sizeof(v));
   return sizeof(v);
}

unsigned int StreamWriter::write(const char *field, const float &v)
{
   double d = (double) v;

   return write(field, d);
}

unsigned int StreamWriter::write(const char *field, const String &v)
{
   int32_t size = (int32_t) v.length();

   write(field, size);

   _stream->write((char *) v.c_str(), size);

   return sizeof(size) + size;
}

unsigned int StreamWriter::write(const char *field, const UString &v)
{
   int32_t size = (int32_t) v.byteLength();

   write(field, size);

   _stream->write((char *) v, size);

   return sizeof(size) + size;
}

unsigned int StreamWriter::write(const char *field, const DateTime &v)
{
   if (_tzhour != INT8_MIN)
   {
      DateTime t(v);

      t.tzConverTo(_tzhour, _tzmin);

      return
         write(field, t.date.toInt32()) +
         write(field, t.time.toInt32()) +
         write(field, t.tzHour()) +
         write(field, t.tzMinute());
   }

   return
      write(field, v.date.toInt32()) +
      write(field, v.time.toInt32()) +
      write(field, v.tzHour()) +
      write(field, v.tzMinute());
}

unsigned int StreamWriter::write(const char *field, const Date &v)
{
   return write(field, v.toInt32());
}

unsigned int StreamWriter::write(const char *field, const Time &v)
{
   return write(field, v.toInt32());
}

unsigned int StreamWriter::write(const char *field, const Mettle::Lib::MemoryBlock &v)
{
   int32_t is = (int32_t) v.size();

   _stream->write(&is, sizeof(is));
   _stream->write(v.block(), is);

   return sizeof(is) + is;
}

unsigned int StreamWriter::write(const char *field, const Mettle::Lib::Guid &v)
{
   if (v.isNull())
      return write(field, (int8_t) 0);

   char struuid[38];

   v.toString(struuid);

   write(field, (int8_t) 36);
   _stream->write(struuid, 36);

   return 36 + sizeof(int8_t);
}

}}
