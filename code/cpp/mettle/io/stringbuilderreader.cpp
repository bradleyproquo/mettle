/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/stringbuilderreader.h"

#include "mettle/lib/common.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/stringbuilder.h"

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

#define MODULE "StringBuilderReader"

StringBuilderReader::StringBuilderReader(const StringBuilder *sb,
                                         const char          *delim,
                                         const int32_t        tzTarg)
{
   _jotter   = new StringBuilder();
   _sb       = sb;
   _ptr      = _sb->value;
   _delim    = delim;
   _delimlen = strlen(delim);

   DateTime::calcTZHourMin(tzTarg, _tzhour, _tzmin);
}

StringBuilderReader::~StringBuilderReader() noexcept
{
   delete _jotter;
}

void StringBuilderReader::clear()
{
   _ptr = _sb->value;
}


const char *StringBuilderReader::name()
{
   return "Mettle.StringBuilderReader";
}

unsigned int StringBuilderReader::readStart(const char *name)
{
   return 0;
}

unsigned int StringBuilderReader::readEnd(const char *name)
{
   return 0;
}

char *StringBuilderReader::_readNextField()
{
   char *local;

   _jotter->clear();

   for (local = _ptr; *local; local++)
      if (strncmp(local, _delim, _delimlen) == 0)
         break;
      else
         _jotter->add(*local);

   if (local - _ptr == 0)
      throw xMettle(__FILE__, __LINE__, MODULE, "Error: Reached the end of the buffer.");

   _ptr = local + _delimlen;

   return _jotter->value;
}

unsigned int StringBuilderReader::read(const char *field, bool &v)
{
   v = String(_readNextField()).toBool();

   return 0;
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int StringBuilderReader::read(const char *field, char &v)
{
   v = *_readNextField();

   return 0;
}
#endif

unsigned int StringBuilderReader::read(const char *field, int8_t &v)
{
   v = (int8_t) String(_readNextField()).toInt();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, int16_t &v)
{
   v = (int16_t) String(_readNextField()).toInt();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, int32_t &v)
{
   v = String(_readNextField()).toInt32();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, int64_t &v)
{
   v = String(_readNextField()).toInt64();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, uint8_t &v)
{
   v = (uint8_t) String(_readNextField()).toUInt();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, uint16_t &v)
{
   v = (uint16_t) String(_readNextField()).toUInt();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, uint32_t &v)
{
   v = String(_readNextField()).toUInt32();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, uint64_t &v)
{
   v = String(_readNextField()).toUInt64();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, double &v)
{
   v = String(_readNextField()).toDouble();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, float &v)
{
   v = String(_readNextField()).toFloat();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, String &v)
{
   v = _readNextField();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, UString &v)
{
   v = _readNextField();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, DateTime &v)
{
   v = _readNextField();

   if (_tzhour != INT8_MIN)
      v.tzConverTo(_tzhour, _tzmin);

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, Date &v)
{
   v = _readNextField();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, Time &v)
{
   v = _readNextField();

   return 0;
}

unsigned int StringBuilderReader::read(const char *field, MemoryBlock &v)
{
   throw xMettle(__FILE__, __LINE__, MODULE, "MemoryBlock not supported.");
}

unsigned int StringBuilderReader::read(const char *field, Mettle::Lib::Guid &v)
{
   char *x = _readNextField();

   if (!*x)
      v.nullify();
   else
      v = x;

   return 0;
}


#undef MODULE

}}
