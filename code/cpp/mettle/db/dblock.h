/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_DB_DBLOCK_H_
#define __METTLE_DB_DBLOCK_H_

namespace Mettle { namespace DB {

/** \class DBLock dblock.h mettle/db/dblock.h
 *  \brief An object that contains a lock, timeout and retry information for row locking.
 */
class DBLock
{
public:

   /** \brief Constructor.
    *  \param miliSeconds the number of mili seconds to wait between retrying to lock the record.
    *  \param retrys the number of retries to perform on a lock before raising a time out exception.
    */
   DBLock(const unsigned int miliSeconds,
          const unsigned int retrys) noexcept;

   /** \brief Destructor.
    */
   virtual ~DBLock() noexcept;

   /** \brief miliseconds.
    */
   unsigned int  miliSeconds;

   /** \brief retrys.
    */
   unsigned int  retrys;
};

}}


#endif
