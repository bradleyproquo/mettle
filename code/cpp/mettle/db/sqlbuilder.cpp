/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/db/sqlbuilder.h"

#include "mettle/db/sqlparams.h"

#include "mettle/lib/macros.h"
#include "mettle/lib/xmettle.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

using namespace Mettle::Lib;

namespace Mettle { namespace DB {

SqlBuilder::SqlBuilder() noexcept
{
   map         = 0;
   mapCount    = 0;
   _allocated  = 0;
}


SqlBuilder::~SqlBuilder() noexcept
{
   destroy();
}


void SqlBuilder::destroy() noexcept
{
   _FREE(map)

   mapCount   = 0;
   _allocated = 0;
}


const char *SqlBuilder::sql() const
{
   return value;
}


bool SqlBuilder::isVaribleChar(const char ch) const noexcept
{
   if (ch == '_' || ch == '.'  ||
      (ch >= 'a' && ch <= 'z') ||
      (ch >= 'A' && ch <= 'Z') ||
      (ch >= '0' && ch <= '9'))
      return true;

   return false;
}


void SqlBuilder::subst(SubstDelegate  substDelegate,
                       SqlParams     *in)
{
   char         *p;
   char         *e;
   unsigned int  i;
   int           len;

   bool inCommentMulti  = false;
   bool inCommentSingle = false;
   bool inString        = false;

   for (p = value; *p; p++)
   {
      if (inCommentMulti)
      {
         if (p[0] == '*' && p[1] == '/')
         {
            inCommentMulti = false;
            p++;
         }

         continue;
      }

      if (inCommentSingle)
      {
         if (p[0] == '\n')
            inCommentSingle = false;

         continue;
      }

      if (inString)
      {
         if (p[0] == '\\')
            p++;
         else if (p[0] == '\'')
            inString = false;

         continue;
      }

      if (p[0] == '-' && p[1] == '-')
      {
         inCommentSingle = true;
         p++;
         continue;
      }

      if (p[0] == '/' && p[1] == '*')
      {
         inCommentMulti = true;
         p++;
         continue;
      }

      if (p[0] == '\'')
      {
         inString = true;
         continue;
      }

      if (p[0] != ':')
         continue;

      if (p[1] == ':')
      {
         p += 2;
         continue;
      }

      p++;

      for (e = p; *e; e++)
         if (!isVaribleChar(*e))
            break;

      if (e == p)
         throw xMettle(__FILE__, __LINE__, "DB::SqlBuilder", "SqlBuilder::Subst() - Missing identifier - %s", value);

      len = e - p;

      for (i = 0; i < in->count; i++)
         if (strlen(in->bindings[i].name)          == len &&
             strncmp(p, in->bindings[i].name, len) == 0)
         {
            if (substDelegate)
            {
               char jotter[256];
               int  jotterLen = substDelegate(i, in->bindings[i].name, jotter);
               int  offset;

               p--;

               offset = p - value;

               replace(offset, len + 1, jotter, jotterLen);

               p = value + (offset + jotterLen);
            }

            addMap(i);
            break;
         }

      if (i >= in->count)
         throw xMettle(__FILE__, __LINE__, "SqlBuilder::Subst()", "Unknown identifier (%*.*s) - %s", len, len, p, value);

      if (!*p)
         break;
   }
}


void SqlBuilder::dynamic(const char *dynamicLookup, const char *sql)
{
   char *p = strstr(value, dynamicLookup);

   if (!p)
      throw xMettle(__FILE__, __LINE__, "DB::SqlBuilder", "Dynamic lookup not found %s/%s", dynamicLookup, value);

   for (; p; p = strstr(value, dynamicLookup))
      replace(p - value, strlen(dynamicLookup), sql, strlen(sql));
}


void SqlBuilder::replace(const unsigned int  pos,
                         const unsigned int  posLen,
                         const char         *newStr,
                         const unsigned int  newLen)
{
   char *ptr;

   if (posLen > newLen)
   {
      char           *tmp;
      unsigned int    replacePos;

      ptr = value + pos;

      memcpy(ptr, newStr, newLen);

      tmp         =  ptr + posLen;
      ptr        += newLen;
      replacePos  = ptr - value;

      memmove(ptr, tmp, used - replacePos);

      used -= posLen - newLen;
      return;
   }

   if (posLen < newLen)
   {
      char           *from;
      char           *to;
      unsigned int    replaceOffset = newLen - posLen;

      add(replaceOffset);

      ptr = value + pos;

      from        = ptr + posLen;
      to          = ptr + posLen + replaceOffset;

      memmove(to, from, used - pos - posLen);
      memcpy(ptr, newStr, newLen);

      used += replaceOffset;
      value[used] = 0;
      return;
   }

   ptr = value + pos;

   memcpy(ptr, newStr, newLen);
}


void SqlBuilder::addMap(unsigned int i)
{
   unsigned idx;

   for (idx = 0; idx < mapCount; idx++)
      if (map[idx] == i)
         return;

   if (_allocated <= mapCount)
   {
      void *p;

      _allocated += 32;

      p = realloc(map, sizeof(unsigned int) * _allocated);

      if (!p)
         throw xMettle(__FILE__, __LINE__, "SqlBuilder::AddMap()", "Error allocating memory (%u/%u)", sizeof(unsigned int) * _allocated, _allocated);

      map = (unsigned int *) p;
   }

   map[mapCount++] = i;
}

}}
