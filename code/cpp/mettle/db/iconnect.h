/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_DB_ICONNECT_H_
#define __METTLE_DB_ICONNECT_H_

#include "mettle/lib/safe.h"
#include "mettle/lib/xmettle.h"

namespace Mettle {


namespace Lib
{
   class Date;
   class DateTime;
   class String;
}


namespace DB {

class Statement;
class SqlBuilder;

/** \interface IConnect iconnect.h mettle/io/iconnect.h
 *  \brief Interface to a database connection.
 *  \warning it is expected that implementing object does not auto commit statements.
 */
class IConnect
{
public:

   /** \brief Destructor.
    */
   virtual ~IConnect();

   /** \brief Performs a database commit.
    */
   virtual void commit() = 0;

   /** \brief Performs a database rollback.
    */
   virtual void rollback() = 0;

   /** \brief Fetches the next row from the statement object.
    */
   virtual bool fetch(Statement* stmnt) = 0;

   /** \brief Connect to the database using common logon parameters.
    *  \param user the user logging onto the database.
    *  \param password for the \p user.
    *  \param db_name the database name to connect to.
    *  \param host The server/host to connect to, sometimes can be null.
    *  \param port The port number to connect to, optional.
    *  \param schema The default schema/namespace to connect to, not always supported.
    */
   virtual void connect(const char* user,
                        const char* password,
                        const char* db_name,
                        const char* host,
                        const int   port = 0,
                        const char* schema = 0) = 0;

   /** \brief Connect to the database using a database specific connection string.
     * \param connectionStr a string with all the required connection information.
    */
   virtual void connect(const char* connectionStr) = 0;

   /** \brief Attempt to reset to the database connection if it was lost.
     * \param retrys the number of retry attempts
     * \param miliSeconds the time in mili seconds to wait between each retry.
     * \returns true if successful or false if not.
    */
   virtual bool reset(const unsigned int retrys, const unsigned int miliSeconds) = 0;

   /** \brief Closes the database connection.
    */
   virtual void close() = 0;

   /** \brief Executes the provided statement.
    *  \param stmnt the statement to be executed.
    */
   virtual void execute(Statement* stmnt) = 0;

   /** \brief Gets the next sequence number from the table if the database prior to insert.
    *  \param seqNo the output sequence number.
    *  \param seqNoSize the size of the \p seqNo pointer.
    *  \param stmnt the statement associated with this action.
    *  \param table the name of the table's who's sequence to get.
    *  \param col the name of the column for the on the table of who's sequence to get.
    *  \returns true if the next sequence was retreieved, else returns false this database instead gets sequence's post insert.
    *  \throw xMettle if the database raises an exception.
    */
   virtual bool preGetSequence(void* seqNo, const unsigned int seqNoSize, Statement* stmnt, const char* table, const char* col) = 0;

   /** \brief Gets the next sequence number from the table if the database post insert.
    *  \param seqNo the output sequence number.
    *  \param seqNoSize the size of the \p seqNo pointer.
    *  \param stmnt the statement associated with this action.
    *  \param table the name of the table's who's sequence to get.
    *  \param col the name of the column for the on the table of who's sequence to get.
    *  \returns true if the next sequence was retreieved, else returns false this database instead gets sequence's prior to insert.
    *  \throw xMettle if the database raises an exception.
    */
   virtual bool postGetSequence(void* seqNo, const unsigned int seqNoSize, Statement* stmnt, const char* table, const char* col) = 0;

   /** \brief Gets the current date & time from the database.
    *  \param dt the output datetime object.
    *  \throw xMettle if the database raises an exception.
    */
   virtual void getTimeStamp(Mettle::Lib::DateTime &dt) = 0;

   /** \brief Tells the db connector we are about to attempt a row lock on the database.
    *  \param stmnt optionally provide the statement, some db connectors require this.
    *  \throw xMettle if the database raises an exception.
    */
   virtual void lock(Statement *stmnt = 0) = 0;

   /** \brief Creates a new Statement object for this database connection.
    *  \param name is the name of the statement.
    *  \returns the newly created statement.
    *  \warning it is one the onus of the calling code to free the Statement.  Suggest the use of Statement::Safe class.
    *  \throw xMettle if the database raises an exception.
    */
   virtual Statement *createStatement(const char* name, const int stmntType = 0) = 0;

   /** \brief The name of this database connection.
    *  \param name The name of the statement.
    *  \param stmntType The statement type.
    *  \returns the name.
    */
   virtual const char* name() const noexcept = 0;

   /** \brief Converts a date object, into a query usable equivalent for this specific database.
    *  \param srcDate is the source date to be converted into a query compatiable string.
    *  \param destDate is the destination query compatiable string.
    *  \returns the converted string for convenience.
    *  \throw xMettle if the there was memory allocation error.
    */
   virtual const char* dateToStr(const Mettle::Lib::Date*   srcDate,
                                       Mettle::Lib::String* destDate) const = 0;


   /** \brief Converts a date time object, into a query usable equivalent for this specific database.
    *  \param srcDate is the source date to be converted into a query compatiable string.
    *  \param destDate is the destination query compatiable string.
    *  \returns the converted string for convenience.
    *  \throw xMettle if the there was memory allocation error.
    */
   virtual const char* dateTimeToStr(const Mettle::Lib::DateTime* srcDate,
                                           Mettle::Lib::String*   destDate) const = 0;

   /** \brief Check if the database connection is in transaction mode.  By default all database connections are
    *    created in transaction mode.
    *
    *  \returns true if the database connection is in transaction mode (default), else false.
    */
   virtual bool transactionModeGet() const = 0;

   /** \brief Change the transaction mode of the database connection.
    *  \param mode the new setting, true = turn on transaction mode, false turn it off.
    *  \throws xMettle if you attempted to turn transaction mode off while an open transaction was in progress.
    */
   virtual void transactionModeSet(const bool mode) = 0;

   /** \brief Get the backend processes id of the connector.

    *  \returns 0 if not connected, -1 if not applicable (IE sqlite), or the backend processed id.
    */
   virtual int backendPID() const = 0;

protected:

   /** \brief Destructor.
   */
   IConnect();
};

}}

#endif
