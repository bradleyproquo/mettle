/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_DB_SQLPARAMS_H_
#define __METTLE_DB_SQLPARAMS_H_


#include "mettle/lib/c99standard.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

namespace Mettle
{

namespace Lib
{
   class Date;
   class DateTime;
   class MemoryBlock;
   class String;
   class Time;
   class Guid;
}

namespace DB
{

/*-----------------------------------
   SqlParams
-----------------------------------*/
class SqlParams
{
public:

   enum DataType
   {
      UNKNOWN = 0,
      STRING,
      STRING_C_STR,
      CHAR,
      INT8,
      INT16,
      INT32,
      INT64,
      FLOAT,
      DOUBLE,
      MEMBLOCK,
      DATE,
      TIME,
      DATETIME,
      BOOL,
      UUID,
      JSON
   };

#pragma PACK_PUSH(1)

   struct Binding
   {
      char           name[64];
      DataType       type;

      bool           _freePtr;
      bool           _freeOrigPtr;
      unsigned int   ptrSize;

      void*          ptr;
      bool           isNull;
      bool*          _nullPtr;

      union
      {
         void*                     origPtr;
         Mettle::Lib::String*      origStringPtr;
         Mettle::Lib::Date*        origDatePtr;
         Mettle::Lib::Time*        origTimePtr;
         Mettle::Lib::DateTime*    origDateTimePtr;
         Mettle::Lib::MemoryBlock* origMemBlockPtr;
         Mettle::Lib::Guid*        origGuidPtr;
      } orig;

      Lib::String*      asString()       noexcept;
      const char*       as_c_str()       noexcept;
      char*             asChar()         noexcept;
      int8_t*           asInt8()         noexcept;
      int16_t*          asInt16()        noexcept;
      int32_t*          asInt32()        noexcept;
      int64_t*          asInt64()        noexcept;
      float*            asFloat()        noexcept;
      double*           asDouble()       noexcept;
      Lib::MemoryBlock* asMemoryBlock()  noexcept;
      Lib::Date*        asDate()         noexcept;
      Lib::Time*        asTime()         noexcept;
      Lib::DateTime*    asDateTime()     noexcept;
      bool*             asBool()         noexcept;
      Lib::Guid*        asGuid()         noexcept;
   };

#pragma PACK_POP

public:
   SqlParams() noexcept;

   virtual     ~SqlParams()  noexcept;

   void         Clean()      noexcept;
   void         Initialize() noexcept;
   void         Destroy()    noexcept;

   void         Bind(const char* name, bool*    p);
   void         Bind(const char* name, char*    p);
   void         Bind(const char* name, int8_t*  p, const bool isNull = false, bool* nullPtr = 0);
   void         Bind(const char* name, int16_t* p, const bool isNull = false, bool* nullPtr = 0);
   void         Bind(const char* name, int32_t* p, const bool isNull = false, bool* nullPtr = 0);
   void         Bind(const char* name, int64_t* p, const bool isNull = false, bool* nullPtr = 0);
   void         Bind(const char* name, double*  p);
   void         Bind(const char* name, float*   p);
   void         Bind(const char* name, Mettle::Lib::MemoryBlock* p);
   void         Bind(const char* name, Mettle::Lib::Date*        p);
   void         Bind(const char* name, Mettle::Lib::Time*        p);
   void         Bind(const char* name, Mettle::Lib::DateTime*    p);
   void         Bind(const char* name, Mettle::Lib::Guid*        p);

   void         Bind(const char*           name, Mettle::Lib::String *p, const unsigned int length = 0);
   void         BindJson(const char*       name, Mettle::Lib::String *p, const unsigned int length = 0);
   void         Bind_c_str(const char*     name, char* p,                const unsigned int length = 0);
   void         BindJson_c_str(const char* name, char* p,                const unsigned int length = 0);

   Binding*     Allocate(const DataType type, const char* name, const int len = 0);

   Binding*     bindings;

   unsigned int count;
   unsigned int _allocated;

protected:

   Binding* AllocateBinding(const char* name);

   void FreeBinding(Binding* bind) noexcept;

   Binding* Add(const char*         name,
                      void*         ptr,
                const unsigned int  ptrSize,
                const DataType      type,
                const bool          isNull    = false,
                      bool*         nullPtr   = 0);

   Binding* AddDate(const char         *name,
                          void         *ptr,
                    const DataType      type);

   Binding* AddGuid(const char         *name,
                    Mettle::Lib::Guid *p);

   Binding* AddString(const char*                name,
                            Mettle::Lib::String* p,
                     const unsigned int          length);

   Binding* Add_c_str(const char*         name,
                            char*         ptr,
                      const unsigned int  length);

   Binding* AddMemoryBlock(const char*                     name,
                                 Mettle::Lib::MemoryBlock* p,
                           const unsigned int              size);
};

}}

#endif
