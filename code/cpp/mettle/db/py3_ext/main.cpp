/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include <Python.h>
#include "structmember.h"

#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include "mettle/db/iconnect.h"
#include "mettle/db/sqlbuilder.h"
#include "mettle/db/sqlparams.h"
#include "mettle/db/statement.h"

#if defined (_METTLE_DB_POSTGRESQL)
   #define _DB_TYPE      "PostgresSQL"
   #define _MODULE_NAME  "mettledb_postgresql"
   #include "mettle/db/postgresql/dbconnect.h"
#elif defined (_METTLE_DB_SQLITE)
   #define _DB_TYPE      "SQLite"
   #define _MODULE_NAME  "mettledb_sqlite"
   #include "mettle/db/sqlite/dbconnect.h"
#else
   #error "No Database has been selected for compilation"
#endif

static PyObject* datetimeModule  = 0;
static PyObject* jsonModule      = 0;
static PyObject* uuidModule      = 0;
static PyObject* mettleLibModule = 0;
static PyObject* datetimeClass   = 0;
static PyObject* dateClass       = 0;
static PyObject* timeClass       = 0;
static PyObject* xMettleClass    = 0;
static PyObject* uuidClass       = 0;
static PyObject* jsonDumps       = 0;
static PyObject* jsonLoads       = 0;


#define CATCH_AND_THROW_XMETTLE(exitCmds) catch (Mettle::Lib::xMettle &x)\
{\
   PyObject* xobj      = 0;\
   PyObject* xobjArgs  = 0;\
\
   if ((xobjArgs = Py_BuildValue("(ssi)", x.what(), _MODULE_NAME, x.errorCode)) == 0)\
   {\
      PyErr_SetString(PyExc_Exception, x.what());\
      exitCmds;\
   }\
\
   if ((xobj = PyEval_CallObject(xMettleClass, xobjArgs)) == 0)\
   {\
      Py_DECREF(xobjArgs);\
      PyErr_SetString(PyExc_Exception, x.what());\
      exitCmds;\
   }\
\
   PyErr_SetObject(xMettleClass, xobj);\
   Py_DECREF(xobjArgs);\
   Py_DECREF(xobj);\
\
   exitCmds;\
}



/*------------------------------------------------------------------------------
   ConnectObject
------------------------------------------------------------------------------*/
typedef struct
{
   PyObject_HEAD

   Mettle::DB::IConnect* con;

   int32_t stmnt_type_na;
   int32_t stmnt_type_read;
   int32_t stmnt_type_cud;
} ConnectObject;

static PyTypeObject ConnectObjectType = {
   PyVarObject_HEAD_INIT(NULL, 0)
   "Connect",                 /*tp_name*/
   sizeof(ConnectObject),     /* tp_basicsize */
   0,                         /* tp_itemsize */
   0,                         /* tp_dealloc */
   0,                         /* tp_print */
   0,                         /* tp_getattr */
   0,                         /* tp_setattr */
   0,                         /* tp_reserved */
   0,                         /* tp_repr */
   0,                         /* tp_as_number */
   0,                         /* tp_as_sequence */
   0,                         /* tp_as_mapping */
   0,                         /* tp_hash  */
   0,                         /* tp_call */
   0,                         /* tp_str */
   0,                         /* tp_getattro */
   0,                         /* tp_setattro */
   0,                         /* tp_as_buffer */
   Py_TPFLAGS_DEFAULT,        /* tp_flags */
   "Mettle database connection object for " _DB_TYPE,  /*tp_doc */
};


/*------------------------------------------------------------------------------
   StatementObject
------------------------------------------------------------------------------*/
typedef struct
{
   PyObject_HEAD

   Mettle::DB::Statement* stmnt;
   PyObject*              columns;
   PyObject*              result;

} StatementObject;


static PyTypeObject StatementObjectType = {
   PyVarObject_HEAD_INIT(NULL, 0)
   "Statement",             /*tp_name*/
   sizeof(StatementObject), /*tp_basicsize*/
    0,                      /*tp_itemsize*/
    0,                      /*tp_dealloc*/
    0,                      /*tp_print*/
    0,                      /*tp_getattr*/
    0,                      /*tp_setattr*/
    0,                      /*tp_as_async*/
    0,                      /*tp_repr*/
    0,                      /*tp_as_number*/
    0,                      /*tp_as_sequence*/
    0,                      /*tp_as_mapping*/
    0,                      /*tp_hash */
    0,                      /*tp_call*/
    0,                      /*tp_str*/
    0,                      /*tp_getattro*/
    0,                      /*tp_setattro*/
    0,                      /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT,     /*tp_flags*/
    "Mettle database statement object for " _DB_TYPE,   /*tp_doc */
};


#define SET_FROM_PY_ATTR(dest, func, part, obj, sub) if ((val = PyObject_GetAttrString(dest, #part)) != 0)\
   {\
      func((int) PyLong_AsLong(val) - sub);\
      Py_DECREF(val);\
   }\
   else\
   {\
      PyErr_SetString(PyExc_Exception, "Error reading ("#part") from python "#obj" object!");\
      return 0;\
   }


static int util_SetStatementColumns(StatementObject* stmnt)
{
   Mettle::DB::SqlParams* outCols = stmnt->stmnt->out;

   if (outCols->count < 1)
      return 1;

   unsigned int  index;
   PyObject     *item;

   for (index = 0; index < outCols->count; index++)
   {
      if ((item = Py_BuildValue("s", outCols->bindings[index].name)) == 0)
         return 0;

      if (PyList_Append(stmnt->columns, item) == -1)
      {
         Py_DECREF(item);
         return 0;
      }

      Py_DECREF(item);
   }

   return 1;
}

static PyObject* util_MettleDateTime_2_Py(const Mettle::Lib::DateTime& dt)
{
   PyObject* returnObj      = 0;
   PyObject* datetimeArgs   = 0;

   if (dt.isNull())
   {
      if ((datetimeArgs = Py_BuildValue("(iii)", 1, 1, 1, 0)) == 0)
        goto CLEANUP_util_MettleDateTime_2_Py;
   }
   else if ((datetimeArgs = Py_BuildValue("(iiiiii)", dt.date.year(), dt.date.month(), dt.date.day(), dt.time.hour(), dt.time.minute(), dt.time.second())) == 0)
      goto CLEANUP_util_MettleDateTime_2_Py;

   if ((returnObj = PyEval_CallObject(datetimeClass, datetimeArgs)) == 0)
      goto CLEANUP_util_MettleDateTime_2_Py;

CLEANUP_util_MettleDateTime_2_Py:

   if (datetimeArgs)
      Py_DECREF(datetimeArgs);

   return returnObj;
}

static PyObject* util_MettleDate_2_Py(const Mettle::Lib::Date& dt)
{
   PyObject* returnObj  = 0;
   PyObject* dateArgs   = 0;

   if (dt.isNull())
   {
      if ((dateArgs = Py_BuildValue("(iii)", 1, 1, 1, 0)) == 0)
        goto CLEANUP_util_MettleDate_2_Py;
   }
   else if ((dateArgs = Py_BuildValue("(iii)", dt.year(), dt.month(), dt.day())) == 0)
      goto CLEANUP_util_MettleDate_2_Py;

   if ((returnObj = PyEval_CallObject(dateClass, dateArgs)) == 0)
      goto CLEANUP_util_MettleDate_2_Py;

CLEANUP_util_MettleDate_2_Py:

   if (dateArgs)
      Py_DECREF(dateArgs);

   return returnObj;
}

static PyObject* util_MettleTime_2_Py(const Mettle::Lib::Time& dt)
{
   PyObject* returnObj  = 0;
   PyObject* timeArgs   = 0;

   if (dt.isNull())
   {
      if ((timeArgs = Py_BuildValue("(ii)", 0, 0)) == 0)
         goto CLEANUP_util_MettleTime_2_Py;
   }
   else if ((timeArgs = Py_BuildValue("(iii)", dt.hour(), dt.minute(), dt.second())) == 0)
      goto CLEANUP_util_MettleTime_2_Py;

   if ((returnObj = PyEval_CallObject(timeClass, timeArgs)) == 0)
      goto CLEANUP_util_MettleTime_2_Py;

CLEANUP_util_MettleTime_2_Py:

   if (timeArgs)
      Py_DECREF(timeArgs);

   return returnObj;
}

static PyObject* util_MettleGuid_2_Py(const Mettle::Lib::Guid& guid)
{
   PyObject* returnObj  = 0;
   PyObject* guidArgs   = 0;
   char       jotter[64];

   if ((guidArgs = Py_BuildValue("(s)", guid.toString(jotter))) == 0)
      goto CLEANUP_util_MettleGuid_2_Py;

   if ((returnObj = PyEval_CallObject(uuidClass, guidArgs)) == 0)
      goto CLEANUP_util_MettleGuid_2_Py;

CLEANUP_util_MettleGuid_2_Py:

   if (guidArgs)
      Py_DECREF(guidArgs);

   return returnObj;
}


static int util_BindObject(Mettle::DB::SqlParams* params,
                           char*                  name,
                           PyObject*              obj,
                           PyObject*              objType,
                           int32_t                objSize)
{
   if ((objType && (void*) objType == (void*) &PyBool_Type) || (obj && PyBool_Check(obj)))
   {
      Mettle::DB::SqlParams::Binding* bind;

      bind = params->Allocate(Mettle::DB::SqlParams::BOOL, name, sizeof(bool));

      if (obj && obj != Py_None)
      {
         bool val = PyObject_IsTrue(obj) == 1;
         memcpy(bind->ptr, &val, sizeof(val));
         bind->isNull = false;
      }
      else
         bind->isNull = true;
   }
   else if ((objType && (void*)objType == (void*)&PyLong_Type) || (obj && PyLong_Check(obj)))
   {
      Mettle::DB::SqlParams::Binding* bind;

      if (objSize == sizeof(int64_t))
      {
         bind = params->Allocate(Mettle::DB::SqlParams::INT64, name, sizeof(int64_t));

         if (obj && obj != Py_None)
         {
            int64_t val = (int64_t) PyLong_AsLong(obj);
            memcpy(bind->ptr, &val, sizeof(val));
         }
         else
            bind->isNull = true;
      }
      else if (objSize == sizeof(int32_t))
      {
         bind = params->Allocate(Mettle::DB::SqlParams::INT32, name, sizeof(int32_t));

         if (obj && obj != Py_None)
         {
            int32_t val = (int32_t) PyLong_AsLong(obj);
            memcpy(bind->ptr, &val, sizeof(val));
         }
         else
            bind->isNull = true;
      }
      else if (objSize == sizeof(int16_t))
      {
         bind = params->Allocate(Mettle::DB::SqlParams::INT16, name, sizeof(int16_t));

         if (obj && obj != Py_None)
         {
            int16_t val = (int16_t) PyLong_AsLong(obj);
            memcpy(bind->ptr, &val, sizeof(val));
         }
         else
            bind->isNull = true;
      }
      else if (objSize == sizeof(int8_t))
      {
         bind = params->Allocate(Mettle::DB::SqlParams::INT8, name, sizeof(int8_t));

         if (obj && obj != Py_None)
         {
            int8_t val = (int8_t) PyLong_AsLong(obj);
            memcpy(bind->ptr, &val, sizeof(val));
         }
         else
            bind->isNull = true;
      }
      else if (PyLong_CheckExact(obj))
      {
         int64_t val = (int64_t) PyLong_AsLong(obj);

         bind = params->Allocate(Mettle::DB::SqlParams::INT64, name, sizeof(int64_t));

         if (obj && obj != Py_None)
         {
            int64_t val = (int64_t) PyLong_AsLong(obj);
            memcpy(bind->ptr, &val, sizeof(val));
         }
         else
            bind->isNull = true;
      }
      else
      {
         bind = params->Allocate(Mettle::DB::SqlParams::INT32, name, sizeof(int32_t));

         if (obj && obj != Py_None)
         {
            int32_t val = (int32_t) PyLong_AsLong(obj);
            memcpy(bind->ptr, &val, sizeof(val));
         }
         else
            bind->isNull = true;
      }
   }
   else if ((objType && (void*) objType == (void*) &PyUnicode_Type) || (obj && PyUnicode_Check(obj)))
   {
      if (obj && obj != Py_None)
      {
         if (0 != PyUnicode_READY(obj))
            return 0;

         Py_ssize_t  size;
         const char* val = PyUnicode_AsUTF8AndSize(obj, &size);

         params->Bind_c_str(name, (char*) val, objSize);
      }
      else
         params->Bind_c_str(name, (char*) Mettle::Lib::String::empty(), objSize);
   }
   else if ((objType && (void*) objType == (void*) &PyFloat_Type) || (obj && PyFloat_Check(obj)))
   {
      Mettle::DB::SqlParams::Binding* bind;

      bind = params->Allocate(Mettle::DB::SqlParams::DOUBLE, name, sizeof(double));

      if (obj && obj != Py_None)
      {
         double val = PyFloat_AsDouble(obj);
         memcpy(bind->ptr, &val, sizeof(val));
      }
      else
         bind->isNull = true;
   }
   else if ((objType && objType == datetimeClass) || (obj && PyObject_IsInstance(obj, datetimeClass) == 1))
   {
      Mettle::DB::SqlParams::Binding* bind;

      bind = params->Allocate(Mettle::DB::SqlParams::DATETIME, name, 0);

      if (obj && obj != Py_None)
      {
         PyObject* val;

         if ((val = PyObject_GetAttrString(obj, "min")) == 0)
         {
            PyErr_SetString(PyExc_Exception, "Could not get [min] attribute from datetime object!");
            return 0;
         }

         if (1 != PyObject_RichCompareBool(obj, val, Py_EQ))
         {
            Py_DECREF(val);

            Mettle::Lib::DateTime *dt = bind->asDateTime();

            SET_FROM_PY_ATTR(obj, dt->date.setYear,   year,   datetime, 0)
            SET_FROM_PY_ATTR(obj, dt->date.setMonth,  month,  datetime, 1)
            SET_FROM_PY_ATTR(obj, dt->date.setDay,    day,    datetime, 0)
            SET_FROM_PY_ATTR(obj, dt->time.setHour,   hour,   datetime, 0)
            SET_FROM_PY_ATTR(obj, dt->time.setMinute, minute, datetime, 0)
            SET_FROM_PY_ATTR(obj, dt->time.setSecond, second, datetime, 0)
         }
         else
         {
            Py_DECREF(val);
            bind->isNull = true;
         }
      }
      else
         bind->isNull = true;
   }
   else if ((objType && objType == dateClass) || (obj && PyObject_IsInstance(obj, dateClass) == 1))
   {
      Mettle::DB::SqlParams::Binding* bind;

      bind = params->Allocate(Mettle::DB::SqlParams::DATE, name, 0);

      if (obj && obj != Py_None)
      {
         PyObject* val;

         if ((val = PyObject_GetAttrString(obj, "min")) == 0)
         {
            PyErr_SetString(PyExc_Exception, "Could not get [min] attribute from date object!");
            return 0;
         }

         if (1 != PyObject_RichCompareBool(obj, val, Py_EQ))
         {
            Py_DECREF(val);

            Mettle::Lib::Date  *dt = bind->asDate();

            SET_FROM_PY_ATTR(obj, dt->setYear,   year,  date, 0)
            SET_FROM_PY_ATTR(obj, dt->setMonth,  month, date, 1)
            SET_FROM_PY_ATTR(obj, dt->setDay,    day,   date, 0)
         }
         else {
            Py_DECREF(val);
            bind->isNull = true;
         }
      }
      else
         bind->isNull = true;
   }
   else if ((objType && objType == timeClass) || (obj && PyObject_IsInstance(obj, timeClass) == 1))
   {
      Mettle::DB::SqlParams::Binding* bind;

      bind = params->Allocate(Mettle::DB::SqlParams::TIME, name, 0);

      if (obj && obj != Py_None)
      {
         PyObject* val;

         if ((val = PyObject_GetAttrString(obj, "min")) == 0)
         {
            PyErr_SetString(PyExc_Exception, "Could not get [min] attribute from time object!");
            return 0;
         }

         if (1 != PyObject_RichCompareBool(obj, val, Py_EQ))
         {
            Py_DECREF(val);

            Mettle::Lib::Time  *dt = bind->asTime();

            SET_FROM_PY_ATTR(obj, dt->setHour,   hour,   time, 0)
            SET_FROM_PY_ATTR(obj, dt->setMinute, minute, time, 0)
            SET_FROM_PY_ATTR(obj, dt->setSecond, second, time, 0)
         }
         else
         {
            Py_DECREF(val);
            bind->isNull = true;
         }
      }
      else
         bind->isNull = true;
   }
   else if ((objType && (void*)objType == (void*)&PyDict_Type) || (obj && PyDict_Check(obj)))
   {
      if (obj)
      {
         if (PyDict_Check(obj))
         {
            if (PyDict_Size(obj) > 0)
            {
               PyObject* strVal;

               if ((strVal = PyObject_CallFunctionObjArgs(jsonDumps, obj, 0)) == 0){
                  return 0;
               }

               Py_ssize_t  size;
               const char* val = PyUnicode_AsUTF8AndSize(strVal, &size);

               params->BindJson_c_str(name, (char*) val, objSize);

               Py_DECREF(strVal);
            }
            else
               params->BindJson_c_str(name, (char*) Mettle::Lib::String::empty(), objSize);
         }
         else if (PyUnicode_Check(obj))
         {
            if (0 != PyUnicode_READY(obj))
               return 0;

            Py_ssize_t  size;
            const char* val = PyUnicode_AsUTF8AndSize(obj, &size);

            params->BindJson_c_str(name, (char*) val, objSize);
         }
         else
            params->BindJson_c_str(name, (char*) Mettle::Lib::String::empty(), objSize);
      }
      else
      {
         params->BindJson_c_str(name, (char*) Mettle::Lib::String::empty(), objSize);
      }
   }
   else if ((objType && objType == uuidClass) || (obj && PyObject_IsInstance(obj, uuidClass) == 1))
   {
      Mettle::DB::SqlParams::Binding* bind;

      bind = params->Allocate(Mettle::DB::SqlParams::UUID, name, 0);

      if (obj && obj != Py_None)
      {
         Mettle::Lib::Guid* guid  = bind->asGuid();
         PyObject*          repr  = PyObject_Str(obj);
         PyObject*          str   = PyUnicode_AsEncodedString(repr, "utf-8", "~E~");
         const char*        bytes = PyBytes_AS_STRING(str);

         guid->parse(bytes);

         Py_DECREF(repr);
         Py_DECREF(str);
      }
      else
         bind->isNull = true;
   }
   else if ((objType && (void*) objType == (void*) &PyBytes_Type) || (obj && PyBytes_Check(obj)))
   {
      Mettle::DB::SqlParams::Binding* bind;
      size_t                          bytesLen;

      bind = params->Allocate(Mettle::DB::SqlParams::MEMBLOCK, name, 0);

      if (obj && obj != Py_None && (bytesLen = PyBytes_Size(obj)) > 0)
      {
        Mettle::Lib::MemoryBlock  *blob;

        blob = bind->asMemoryBlock();
        blob->allocate(bytesLen);
        memcpy(blob->block(), PyBytes_AsString(obj), bytesLen);
      }
      else
         bind->isNull = true;
   }
   else if ((objType && (void*) objType == (void*) &PyByteArray_Type) || (obj && PyByteArray_Check(obj)))
   {
      Mettle::DB::SqlParams::Binding* bind;
      size_t                          bytesLen;

      bind = params->Allocate(Mettle::DB::SqlParams::MEMBLOCK, name, 0);

      if (obj && obj != Py_None && (bytesLen = PyByteArray_Size(obj)) > 0)
      {
        Mettle::Lib::MemoryBlock  *blob;

        blob = bind->asMemoryBlock();
        blob->allocate(bytesLen);
        memcpy(blob->block(), PyByteArray_AsString(obj), bytesLen);
      }
      else
         bind->isNull = true;
   }
   else
   {
      PyErr_SetString(PyExc_Exception, Mettle::Lib::String().format("Unknown py object type for binding object(name:%s)", name).c_str());
      return 0;
   }

   return 1;
}

static int util_StatementInitialize(StatementObject* stmnt)
{
   if (PyList_Size(stmnt->columns) > 0)
   {
     Py_DECREF(stmnt->columns);

     if ((stmnt->columns = PyList_New(0)) == 0)
        return 0;
   }

   if (PyList_Size(stmnt->result) > 0)
   {
     Py_DECREF(stmnt->result);

     if ((stmnt->result = PyList_New(0)) == 0)
        return 0;
   }

   if (stmnt->stmnt)
      stmnt->stmnt->sql->clear();

   return 1;
}

static int util_SetStatementResult(StatementObject* stmnt)
 {
   Mettle::DB::SqlParams* outCols = stmnt->stmnt->out;

   if (outCols->count < 1)
      return 1;

   unsigned int                    index;
   PyObject*                       item;
   Mettle::DB::SqlParams::Binding* bind;
   Mettle::Lib::MemoryBlock*       blob;

   while (PyList_Size(stmnt->result) < outCols->count)
   {
      if (PyList_Append(stmnt->result, Py_None) == -1)
         return 0;
   }

   for (index = 0; index < outCols->count; index++)
   {
      bind = &outCols->bindings[index];
      item = 0;

      switch (bind->type)
      {
         case Mettle::DB::SqlParams::STRING:
            if (bind->isNull)
            {
               if ((item = PyUnicode_FromString("")) == 0)
                  return 0;
            }
            else
            {
               if ((item = PyUnicode_FromString(bind->asString()->c_str())) == 0)
                  return 0;
            }

            break;

         case Mettle::DB::SqlParams::CHAR:
            {
               char c[2];

               c[0] = *bind->asChar();
               c[1] = 0;

               if ((item = PyUnicode_FromString(c)) == 0)
                  return 0;
            }
            break;

         case Mettle::DB::SqlParams::INT8  :
            if (bind->isNull)
               break;

            if ((item = PyLong_FromLong((long) (*bind->asInt8()))) == 0)
               return 0;

            break;

         case Mettle::DB::SqlParams::INT16 :
            if (bind->isNull)
               break;

            if ((item = PyLong_FromLong((long) (*bind->asInt16()))) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::INT32 :
            if (bind->isNull)
               break;

            if ((item = PyLong_FromLong((long) (*bind->asInt32()))) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::INT64 :
            if (bind->isNull)
               break;

            if ((item = PyLong_FromLong((long) (*bind->asInt64()))) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::DOUBLE :
            if (bind->isNull)
               break;

            if ((item = PyFloat_FromDouble((*bind->asDouble()))) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::BOOL :
            if ((item = PyBool_FromLong((*bind->asBool()))) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::DATETIME :
            if (bind->isNull)
               break;

            if ((item = util_MettleDateTime_2_Py(*bind->asDateTime())) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::DATE     :
            if (bind->isNull)
               break;

            if ((item = util_MettleDate_2_Py(*bind->asDate())) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::TIME     :
            if (bind->isNull)
               break;

            if ((item = util_MettleTime_2_Py(*bind->asTime())) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::JSON:
            if (bind->isNull)
               break;

            if ((item = PyUnicode_FromString(bind->asString()->c_str())) != 0)
            {
               PyObject* jdict;

               if ((jdict = PyObject_CallFunctionObjArgs(jsonLoads, item, 0)) == 0) {
                  Py_DECREF(item);
                  return 0;
               }

               Py_DECREF(item);
               item = jdict;
            }
            else
               return 0;

            break;


         case Mettle::DB::SqlParams::UUID :
            if (bind->isNull || bind->asGuid()->isNull())
               break;

            if ((item = util_MettleGuid_2_Py(*bind->asGuid())) == 0)
               return 0;
            break;

         case Mettle::DB::SqlParams::MEMBLOCK :
            if (bind->isNull)
               break;

            blob = bind->asMemoryBlock();

            if ((item = PyBytes_FromStringAndSize((const char*)blob->block(), blob->size())) == 0)
              return 0;
            break;

         default :
            PyErr_SetString(PyExc_Exception, Mettle::Lib::String().format("Unknown type [%d] returned from database connection [param:%s]!", bind->type, bind->name).c_str());
            return 0;
      }

      if (item)
      {
         if (PyList_SetItem(stmnt->result, index, item) == -1)
         {
             Py_DECREF(item);
             return 0;
         }
      }
      else
      {
         Py_INCREF(Py_None);

         if (PyList_SetItem(stmnt->result, index, Py_None) == -1)
             return 0;
      }
   }

   return 1;
}


/*------------------------------------------------------------------------------
   ConnectObject Methods
------------------------------------------------------------------------------*/
static void ConnectObject_dealloc(ConnectObject* self)
{
   _DELETE(self->con)
}

static int ConnectObject_init(ConnectObject* self, PyObject* args, PyObject* kwds)
{
   return 0;
}

static PyObject* ConnectObject_new(PyTypeObject* type, PyObject* args, PyObject* kwds)
{
   ConnectObject*        self;
   Mettle::DB::IConnect* con;

   try
   {
#if defined (_METTLE_DB_POSTGRESQL)
      con = new Mettle::DB::PostgreSQL::DBConnect();
#elif defined (_METTLE_DB_SQLITE)
      con = new Mettle::DB::SQLite::DBConnect();
#endif
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   if ((self = (ConnectObject *)type->tp_alloc(type, 0)) == 0)
   {
      delete con;
      return 0;
   }

   self->con             = con;
   self->stmnt_type_na   = 0;
   self->stmnt_type_read = 1;
   self->stmnt_type_cud  = 2;

   return (PyObject*)self;
}

static PyObject* ConnectObject_close(ConnectObject* self)
{
   try
   {
      self->con->close();
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* ConnectObject_commit(ConnectObject* self)
{
   try
   {
      self->con->commit();
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* ConnectObject_connect_deft(ConnectObject* self, PyObject* args, PyObject* keywds)
{
   char* user;
   char* password;
   char* db_name;
   char* host;
   int   port = 0;
   char* schema = 0;

   static char* kwlist[] = {
      (char*) "user",
      (char*) "password",
      (char*) "db_name",
      (char*) "host",
      (char*) "port",
      (char*) "schema",
      0};

   if (!PyArg_ParseTupleAndKeywords(args, keywds, "ssss|is:connect_deft", kwlist, &user, &password, &db_name, &host, &port, &schema))
      return 0;

   try
   {
      self->con->connect(user, password, db_name, host, port, schema);
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* ConnectObject_connect(ConnectObject *self, PyObject* args, PyObject* keywds)
{
   const char* connStr;

   if (!PyArg_ParseTuple(args, "s:connect", &connStr))
      return 0;

   try
   {
      self->con->connect(connStr);
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* ConnectObject_reset(ConnectObject* self, PyObject* args)
{
   int32_t retrys      = 0;
   int32_t miliSeconds = 0;

   if (!PyArg_ParseTuple(args, "ii:reset", &retrys, &miliSeconds))
      return 0;

   if (self->con->reset((unsigned int)  retrys, (unsigned int) miliSeconds))
      return PyBool_FromLong(0L);

   return PyBool_FromLong(1L);
}

static PyObject* ConnectObject_rollback(ConnectObject* self)
{
   try
   {
      self->con->rollback();
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* ConnectObject_statement(ConnectObject* self, PyObject* args)
{
   char             *name;
   int32_t           stmntType = 0;
   StatementObject  *stmnt = 0;

   if (!PyArg_ParseTuple(args, "s|i:statement", &name, &stmntType))
      return 0;

   try
   {
      if ((stmnt = (StatementObject*) StatementObjectType.tp_new(&StatementObjectType, 0, 0)) == 0)
         return 0;

      StatementObjectType.tp_init((PyObject*)stmnt, 0, 0);

      stmnt->stmnt = self->con->createStatement(name, stmntType);

      if (!util_StatementInitialize(stmnt))
         goto CLEAN_ConnectObject_statement;

      return (PyObject*) stmnt;
   }
   CATCH_AND_THROW_XMETTLE(goto CLEAN_ConnectObject_statement)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      goto CLEAN_ConnectObject_statement;
   }

CLEAN_ConnectObject_statement:

   if (stmnt)
   {
      StatementObjectType.tp_dealloc((PyObject*)stmnt);
      Py_DECREF(stmnt);
   }

   return 0;
}

static PyObject* ConnectObject_execute(ConnectObject* self, PyObject* args, PyObject* keywds)
{
   StatementObject  *stmnt;
   PyObject         *statmentObj;

   if (!PyArg_ParseTuple(args, "O:execute", &statmentObj))
      return 0;

   if (!PyObject_TypeCheck(statmentObj, &StatementObjectType))
   {
      PyErr_SetString(PyExc_TypeError, "arg #1 not a " _MODULE_NAME ".statement");
      return 0;
   }

   try
   {
      stmnt = (StatementObject*) statmentObj;
      self->con->execute(stmnt->stmnt);

      if (!util_SetStatementColumns(stmnt))
         return 0;
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(self);

   return (PyObject*) self;
}

static PyObject* ConnectObject_fetch(ConnectObject* self, PyObject* args)
{
   StatementObject  *stmnt;
   PyObject         *statmentObj;
   bool              result;

   if (!PyArg_ParseTuple(args, "O:fetch", &statmentObj))
      return 0;

   if (!PyObject_TypeCheck(statmentObj, &StatementObjectType))
   {
      PyErr_SetString(PyExc_TypeError, "arg #1 not a " _MODULE_NAME ".statement");
      return 0;
   }

   try
   {
      stmnt   = (StatementObject*) statmentObj;
      result  = self->con->fetch(stmnt->stmnt);

      if (result && !util_SetStatementResult(stmnt))
         return 0;
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   return PyBool_FromLong(result ? 1L : 0L);
}

static PyObject* ConnectObject_name(ConnectObject* self)
{
   return Py_BuildValue("s", self->con->name());
}

static PyObject* ConnectObject_get_timestamp(ConnectObject* self)
{
   Mettle::Lib::DateTime   dt;
   PyObject               *returnObj = 0;

   try
   {
      self->con->getTimeStamp(dt);

      returnObj = util_MettleDateTime_2_Py(dt);
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   return returnObj;
}

static PyObject* ConnectObject_lock(ConnectObject* self, PyObject* args, PyObject* keywds)
{
   StatementObject*  stmnt;
   PyObject*         statmentObj;

   static char* kwlist[] = {
      (char*) "stmnt",
      0};

   if (!PyArg_ParseTupleAndKeywords(args, keywds, "|O:lock", kwlist, &statmentObj))
      return 0;

   if (statmentObj && !PyObject_TypeCheck(statmentObj, &StatementObjectType))
   {
      PyErr_SetString(PyExc_TypeError, "arg #1 not a " _MODULE_NAME ".statement");
      return 0;
   }

   try
   {
      stmnt = (StatementObject*) statmentObj;

      if (stmnt)
         self->con->lock(stmnt->stmnt);
      else
         self->con->lock(0);
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* ConnectObject_pre_get_sequence(ConnectObject* self, PyObject* args)
{
   StatementObject*  stmnt;
   PyObject*         statmentObj;
   const char*       table;
   const char*       column;
   int32_t           seqSize = 4;
   char              res[16];
   bool              result;

   if (!PyArg_ParseTuple(args, "Oss|i:pre_get_sequence", &statmentObj, &table, &column, &seqSize))
      return 0;

   if (!PyObject_TypeCheck(statmentObj, &StatementObjectType))
   {
      PyErr_SetString(PyExc_TypeError, "arg #1 not a " _MODULE_NAME ".statement");
      return 0;
   }

   if (seqSize != sizeof(int32_t) && seqSize != sizeof(int64_t))
   {
      PyErr_SetString(PyExc_Exception, "seqSize parameter, expected int size of 32 or 64");
      return 0;
   }

   try
   {
      stmnt  = (StatementObject*) statmentObj;
      result = self->con->preGetSequence(res, seqSize, stmnt->stmnt, table, column);
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   if (!result)
   {
      Py_INCREF(Py_None);

      return Py_None;
   }

   if (seqSize == 4)
      return Py_BuildValue("i", *((int32_t *) res));

   return Py_BuildValue("L", *((int64_t *) res));
}

static PyObject* ConnectObject_post_get_sequence(ConnectObject* self, PyObject* args)
{
   StatementObject*  stmnt;
   PyObject*         statmentObj;
   const char*       table;
   const char*       column;
   int32_t           seqSize = 4;
   char              res[16];
   bool              result;

   if (!PyArg_ParseTuple(args, "Oss|i:post_get_sequence", &statmentObj, &table, &column, &seqSize))
      return 0;

   if (!PyObject_TypeCheck(statmentObj, &StatementObjectType))
   {
      PyErr_SetString(PyExc_TypeError, "arg #1 not a " _MODULE_NAME ".statement");
      return 0;
   }

   if (seqSize != sizeof(int32_t) && seqSize != sizeof(int64_t))
   {
      PyErr_SetString(PyExc_Exception, "seqSize parameter, expected int size of 32 or 64");
      return 0;
   }

   try
   {
      stmnt  = (StatementObject*) statmentObj;
      result = self->con->postGetSequence(res, seqSize, stmnt->stmnt, table, column);
   }
   CATCH_AND_THROW_XMETTLE(return 0)
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   if (!result)
   {
      Py_INCREF(Py_None);

      return Py_None;
   }

   if (seqSize == 4)
      return Py_BuildValue("i", *((int32_t *) res));

   return Py_BuildValue("L", *((int64_t *) res));
}

static PyObject* ConnectObject_date_4_sql_inject(ConnectObject* self, PyObject* args)
{
   PyObject*            dateObj;
   PyObject*            val;
   Mettle::Lib::Date    mdate;
   Mettle::Lib::String  sdate;

   if (!PyArg_ParseTuple(args, "O:date_4_sql_inject", &dateObj))
      return 0;

   if (PyObject_IsInstance(dateObj, dateClass)     != 1 &&
       PyObject_IsInstance(dateObj, datetimeClass) != 1)
   {
      PyErr_SetString(PyExc_Exception, "Expected a datetime.date or datetime.datetime object arguement");
      return 0;
   }

   try
   {
      SET_FROM_PY_ATTR(dateObj, mdate.setYear,   year,   date_or_datetime, 0)
      SET_FROM_PY_ATTR(dateObj, mdate.setMonth,  month,  date_or_datetime, 1)
      SET_FROM_PY_ATTR(dateObj, mdate.setDay,    day,    date_or_datetime, 0)

      self->con->dateToStr(&mdate, &sdate);
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   return Py_BuildValue("s", sdate.c_str());
}

static PyObject* ConnectObject_datetime_4_sql_inject(ConnectObject* self, PyObject* args)
{
   PyObject*              dateObj;
   PyObject*              val;
   Mettle::Lib::DateTime  mdatetime;
   Mettle::Lib::String    sdatetime;

   if (!PyArg_ParseTuple(args, "O:datetime_4_sql_inject", &dateObj))
      return 0;

   if (PyObject_IsInstance(dateObj, dateClass)     != 1 &&
       PyObject_IsInstance(dateObj, datetimeClass) != 1)
   {
      PyErr_SetString(PyExc_Exception, "Expected a datetime.date or datetime.datetime object arguement");
      return 0;
   }

   try
   {
      SET_FROM_PY_ATTR(dateObj, mdatetime.date.setYear,   year,   date_or_datetime, 0)
      SET_FROM_PY_ATTR(dateObj, mdatetime.date.setMonth,  month,  date_or_datetime, 1)
      SET_FROM_PY_ATTR(dateObj, mdatetime.date.setDay,    day,    date_or_datetime, 0)

      if (PyObject_IsInstance(dateObj, datetimeClass) == 1)
      {
         SET_FROM_PY_ATTR(dateObj, mdatetime.time.setHour,   hour,   datetime, 0)
         SET_FROM_PY_ATTR(dateObj, mdatetime.time.setMinute, minute, datetime, 0)
         SET_FROM_PY_ATTR(dateObj, mdatetime.time.setSecond, second, datetime, 0)
      }

      self->con->dateTimeToStr(&mdatetime, &sdatetime);
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   return Py_BuildValue("s", sdatetime.c_str());
}

static PyObject* ConnectObject_transaction_mode_get(ConnectObject* self)
{
   int mode;

   try
   {
      mode = self->con->transactionModeGet();
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   return Py_BuildValue("p", mode);
}

static PyObject* ConnectObject_transaction_mode_set(ConnectObject* self, PyObject* args)
{
   int mode;

   if (!PyArg_ParseTuple(args, "p:transaction_mode_set", &mode))
      return 0;

   try
   {
      self->con->transactionModeSet(mode);
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* ConnectObject_backend_pid(ConnectObject* self)
{
   PyObject* returnObj = 0;
   int        bpid;

   try
   {
      bpid = self->con->backendPID();
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   return Py_BuildValue("i", bpid);
}

/*------------------------------------------------------------------------------
   StatementObject Methods
------------------------------------------------------------------------------*/
static void StatementObject_dealloc(StatementObject* self)
 {
   _DELETE(self->stmnt)

   if (self->columns)
   {
      Py_DECREF(self->columns);
      self->columns = 0;
   }

   if (self->result)
   {
      Py_DECREF(self->result);
      self->result = 0;
   }
}

static int StatementObject_init(StatementObject* self, PyObject* args, PyObject* kwds)
{
   return 0;
}

static PyObject* StatementObject_new(PyTypeObject* type, PyObject* args, PyObject* kwds)
{
   StatementObject* self;
   PyObject*        columns;
   PyObject*        result;

   if ((columns = PyList_New(0)) == 0)
      return 0;

   if ((result = PyList_New(0)) == 0)
   {
      Py_DECREF(columns);
      return 0;
   }

   if ((self = (StatementObject*) type->tp_alloc(type, 0)) == 0)
   {
      Py_DECREF(columns);
      Py_DECREF(result);
      return 0;
   }

   self->stmnt   = 0;
   self->columns = columns;
   self->result  = result;

   return (PyObject*)self;
}

static PyObject* StatementObject_name(StatementObject* self)
 {
   if (!self->stmnt)
   {
      PyErr_SetString(PyExc_Exception, _DB_TYPE ".statment object has not been initialized by " _MODULE_NAME ".connect.execute()");
      return 0;
   }

   return Py_BuildValue("s", self->stmnt->name.c_str());
}

static PyObject* StatementObject_destroy(StatementObject* self)
 {
   StatementObject_dealloc(self);

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* StatementObject_free(StatementObject* self)
{
   StatementObject_dealloc(self);

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* StatementObject_bind_in(StatementObject* self, PyObject* args, PyObject* keywds)
{
   char*            name;
   PyObject*        obj;
   PyObject*        objType   = 0;
   int32_t          objSize   = 0;
   StatementObject* stmnt     = 0;

   static char* kwlist[] = {(char*)"name", (char*) "val", (char*) "valType", (char*) "size", 0};

   if (!PyArg_ParseTupleAndKeywords(args, keywds, "sOO|i:bind_in", kwlist, &name, &obj, &objType, &objSize))
      return 0;

   if (objType && !PyType_Check(objType))
   {
      PyErr_SetString(PyExc_Exception, "Arguement 'valType' is not a type object.");
      return 0;
   }

   try
   {
      if (!util_BindObject(self->stmnt->in, name, obj, objType, objSize))
         return 0;
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}


static PyObject* StatementObject_bind_out(StatementObject* self, PyObject* args, PyObject* keywds)
{
   char*            name;
   int32_t          objSize = 0;
   PyObject*        objType = 0;
   StatementObject* stmnt   = 0;

   static char* kwlist[] = {(char*)"name", (char*)"valType", (char*) "size", 0};

   if (!PyArg_ParseTupleAndKeywords(args, keywds, "s|Oi:bind_out", kwlist, &name, &objType, &objSize))
      return 0;

   try
   {
      if (!util_BindObject(self->stmnt->out, name, 0, objType, objSize))
         return 0;
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}


static PyObject* StatementObject_dynamic(StatementObject* self, PyObject* args)
{
   const char* dynId;
   const char* dynVal;

   if (!PyArg_ParseTuple(args, "ss:dynamic", &dynId, &dynVal))
      return 0;

   try
   {
      self->stmnt->sql->dynamic(dynId, dynVal);
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}

static PyObject* StatementObject_initialize(StatementObject* self)
 {
   util_StatementInitialize(self);

   Py_INCREF(Py_None);

   return Py_None;
}


static PyObject* StatementObject_sql(StatementObject* self, PyObject* args)
{
   const char* sql;

   if (!PyArg_ParseTuple(args, "s:sql", &sql))
      return 0;

   try
   {
      self->stmnt->sql->add(sql);
   }
   catch (std::exception &x)
   {
      PyErr_SetString(PyExc_Exception, x.what());
      return 0;
   }

   Py_INCREF(Py_None);

   return Py_None;
}


#undef SET_FROM_PY_ATTR

/*------------------------------------------------------------------------------
   Methods and member structs
------------------------------------------------------------------------------*/
static PyMethodDef ConnectObject_Methods[] = {
   {"close",        (PyCFunction)ConnectObject_close,        METH_NOARGS,                    "Close the database connection"},
   {"commit",       (PyCFunction)ConnectObject_commit,       METH_NOARGS,                    "Database commit"},
   {"connect_deft", (PyCFunction)ConnectObject_connect_deft, METH_VARARGS | METH_KEYWORDS,   "Connect to the database (user, password, db_name, host, port, schema)"},
   {"connect",      (PyCFunction)ConnectObject_connect,      METH_VARARGS,                   "Connect to the database with a connection string"},
   {"reset",        (PyCFunction)ConnectObject_reset,        METH_VARARGS,                   "Reset the database connection, useful it the connection is broken"},
   {"execute",      (PyCFunction)ConnectObject_execute,      METH_VARARGS,                   "Execute a the database statement (dbstmnt), returns self"},
   {"fetch",        (PyCFunction)ConnectObject_fetch,        METH_VARARGS,                   "Fetch the next frow from specified statement object (dbstmnt)"},
   {"name",         (PyCFunction)ConnectObject_name,         METH_NOARGS,                    "Return the database connection name"},
   {"rollback",     (PyCFunction)ConnectObject_rollback,     METH_NOARGS,                    "Database rollback"},
   {"statement",    (PyCFunction)ConnectObject_statement,    METH_VARARGS,                   "Create a database statement(name), returns the object"},

   {"lock",                  (PyCFunction)ConnectObject_lock,                  METH_VARARGS | METH_KEYWORDS, "Tell the db connector we are about to attempt a row lock (stmnt)'"},
   {"pre_get_sequence",      (PyCFunction)ConnectObject_pre_get_sequence,      METH_VARARGS, "Get sequence from table pre row insert (sql)'"},
   {"post_get_sequence",     (PyCFunction)ConnectObject_post_get_sequence,     METH_VARARGS, "Get sequence from table post row insert (sql)'"},
   {"get_timestamp",         (PyCFunction)ConnectObject_get_timestamp,         METH_NOARGS,  "Get the current timestamp from the database'"},
   {"date_4_sql_inject",     (PyCFunction)ConnectObject_date_4_sql_inject,     METH_VARARGS, "Return a SQL injectible string equivelent of the date'"},
   {"datetime_4_sql_inject", (PyCFunction)ConnectObject_datetime_4_sql_inject, METH_VARARGS, "Return a SQL injectible string equivelent of the datetime'"},

   {"transaction_mode_get",  (PyCFunction)ConnectObject_transaction_mode_get,  METH_NOARGS,  "Returns true if the dbconnect is in transaction mode'"},
   {"transaction_mode_set",  (PyCFunction)ConnectObject_transaction_mode_set,  METH_VARARGS, "Set the dbconnect transaction mode'"},
   {"backend_pid",           (PyCFunction)ConnectObject_backend_pid,           METH_NOARGS,  "Get the backend PID if applicable.'"},

   {0}  /* Sentinel */
};

static PyMethodDef StatementObject_Methods[] = {
   {"bind_in",       (PyCFunction)StatementObject_bind_in,    METH_VARARGS | METH_KEYWORDS,  "Bind a query input (name, val, val_type, size)"},
   {"bind_out",      (PyCFunction)StatementObject_bind_out,   METH_VARARGS | METH_KEYWORDS,  "Bind/define an output variable type."},

   {"dynamic",       (PyCFunction)StatementObject_dynamic,    METH_VARARGS,   "Add dynamic(injected) sql to the statement (dynId, dynVal)"},
   {"destroy",       (PyCFunction)StatementObject_destroy,    METH_NOARGS ,   "Frees all handles"},
   {"free",          (PyCFunction)StatementObject_free,       METH_NOARGS,    "Same as destroy statement"},
   {"initialize",    (PyCFunction)StatementObject_initialize, METH_NOARGS,    "Initialize the statement"},
   {"name",          (PyCFunction)StatementObject_name,       METH_NOARGS,    "Return the statement name"},
   {"sql",           (PyCFunction)StatementObject_sql,        METH_VARARGS,   "Sets the sql for this db statement(sql)"},

   {0}  /* Sentinel */
};

static PyMemberDef ConnectObject_Members[] = {
   {"STMNT_TYPE_NA",   T_INT, offsetof(ConnectObject, stmnt_type_na),   READONLY, (char*) "Statement type unknown/not applicable"},
   {"STMNT_TYPE_READ", T_INT, offsetof(ConnectObject, stmnt_type_read), READONLY, (char*) "Statement type read (select)"},
   {"STMNT_TYPE_CUD",  T_INT, offsetof(ConnectObject, stmnt_type_cud),  READONLY, (char*) "Statement type create/update/delete"},

   {0}  /* Sentinel */
};


static PyMemberDef StatementObject_Members[] = {
   {(char*)"columns", T_OBJECT_EX, offsetof(StatementObject, columns), 0, (char*)"column names"},
   {(char*)"result",  T_OBJECT_EX, offsetof(StatementObject, result),  0, (char*)"result list"},
   {0}
};

static PyMethodDef _Module_Methods[] = {
   {0, 0}
};

static struct PyModuleDef _Module_Def = {
   PyModuleDef_HEAD_INIT,
   _MODULE_NAME,   /* name of module */
   0,              /* module documentation, may be NULL */
   -1,             /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
   _Module_Methods
};


_EXTERN_C_BEGIN

/*------------------------------------------------------------------------------
   maint entry point
------------------------------------------------------------------------------*/
#if defined (_METTLE_DB_POSTGRESQL)
  PyMODINIT_FUNC PyInit_mettledb_postgresql9(void)
#elif defined (_METTLE_DB_SQLITE)
  PyMODINIT_FUNC PyInit_mettledb_sqlite3(void)
#endif
{
   PyObject* m;

   if ((datetimeModule = PyImport_ImportModule("datetime")) == 0)
      return 0;

   if ((datetimeClass = PyObject_GetAttrString(datetimeModule, "datetime")) == 0 ||
       (dateClass     = PyObject_GetAttrString(datetimeModule, "date"))     == 0 ||
       (timeClass     = PyObject_GetAttrString(datetimeModule, "time"))     == 0)
      goto CLEANUP_Module;

   if ((jsonModule = PyImport_ImportModule("json")) == 0)
      goto CLEANUP_Module;

   if ((jsonDumps = PyObject_GetAttrString(jsonModule, "dumps")) == 0 ||
       (jsonLoads = PyObject_GetAttrString(jsonModule, "loads")) == 0)
      goto CLEANUP_Module;

   if ((uuidModule = PyImport_ImportModule("uuid")) == 0)
      goto CLEANUP_Module;

   if ((uuidClass = PyObject_GetAttrString(uuidModule, "UUID")) == 0)
      goto CLEANUP_Module;

   if ((mettleLibModule = PyImport_ImportModule("mettle.lib")) == 0)
      goto CLEANUP_Module;

   if ((xMettleClass = PyObject_GetAttrString(mettleLibModule, "xMettle")) == 0)
      goto CLEANUP_Module;

   ConnectObjectType.tp_dealloc = (destructor) ConnectObject_dealloc;
   ConnectObjectType.tp_methods = ConnectObject_Methods;
   ConnectObjectType.tp_members = ConnectObject_Members;
   ConnectObjectType.tp_init    = (initproc) ConnectObject_init;
   ConnectObjectType.tp_new     = ConnectObject_new;

   if (PyType_Ready(&ConnectObjectType) < 0)
      goto CLEANUP_Module;

   StatementObjectType.tp_dealloc = (destructor) StatementObject_dealloc;
   StatementObjectType.tp_methods = StatementObject_Methods;
   StatementObjectType.tp_members = StatementObject_Members;
   StatementObjectType.tp_init    = (initproc) StatementObject_init;
   StatementObjectType.tp_new     = StatementObject_new;

   if (PyType_Ready(&StatementObjectType) < 0)
      goto CLEANUP_Module;

   m = PyModule_Create(&_Module_Def);

   Py_INCREF(&ConnectObjectType);
   Py_INCREF(&StatementObjectType);

   if (PyModule_AddObject(m, "Connect",   (PyObject*) &ConnectObjectType) < 0)
      goto CLEANUP_Module;

   if (PyModule_AddObject(m, "Statement", (PyObject*) &StatementObjectType) < 0)
      goto CLEANUP_Module;

   return m;

CLEANUP_Module:

   if (mettleLibModule)
      Py_DECREF(mettleLibModule);

   if (uuidModule)
      Py_DECREF(uuidModule);

   if (jsonModule)
      Py_DECREF(jsonModule);

   if (datetimeModule)
      Py_DECREF(datetimeModule);

   return 0;
}

_EXTERN_C_END
