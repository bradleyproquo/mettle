/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/db/postgresql/dbconnect.h"

#include "mettle/db/sqlbuilder.h"
#include "mettle/db/sqlparams.h"
#include "mettle/db/statement.h"

#include "mettle/lib/datetime.h"
#include "mettle/lib/common.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libpq-fe.h"

#if defined (COMPILER_MSC)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

#elif defined (COMPILER_GNU)

   #if defined(OS_MS_WINDOWS)

      #define WIN32_LEAN_AND_MEAN
      #include <winsock2.h>

   #else

      #include <netinet/in.h>
      #include <arpa/inet.h>

   #endif

#endif


#define RAISE(msg, stmnt) raiseException(__FILE__, __LINE__, msg, stmnt, 0, 0)
#define RAISE_STMNT(msg, stmnt, pgstmnt, result) raiseException(__FILE__, __LINE__, msg, stmnt, pgstmnt, result)
#define PG_SUCCESS(res) ((PQresultStatus(res) == PGRES_COMMAND_OK || PQresultStatus(res) == PGRES_NONFATAL_ERROR))

#define MODULE  "PostgreSQL"

using namespace Mettle::Lib;
using namespace Mettle::DB;

namespace Mettle { namespace DB { namespace PostgreSQL {

// server/catalog/pg_type.h, SELECT * FROM pg_attribute WHERE attrelid = 'mytable'::regclass;

#define TEXTOID           25
#define JSONOID           114
#define JSONBOID          3802
#define BPCHAROID         1042
#define VARCHAROID        1043
#define BITOID            1560
#define VARBITOID         1562
#define CSTRINGOID        2275
#define BYTEAOID          17
#define BOOLOID           16
#define CHAROID           18
#define UNKNOWNOID        705
#define INT2OID           21
#define INT4OID           23
#define INT8OID           20
#define FLOAT4OID         700
#define FLOAT8OID         701
#define CASHOID           790
#define NUMERICOID        1700
#define TIMESTAMPOID      1114
#define TIMESTAMPTZOID    1184
#define DATEOID           1082
#define ABSTIMEOID        702
#define TINTERVALOID      704
#define TIMEOID           1083
#define TIMETZOID         1266
#define UUIDOID           2950


struct _PostgreSQLHandle
{
   PGconn* conn;
   bool    inTransaction;
   bool    tranModeEnabled;
   int     stmntCount;
};


struct _PostgreSQLDataBind
{
   int     numParams;
   char**  paramValues;
   Oid*    paramTypes;
   int8_t* pgFree;
};


struct _PostgreSQLStmntHandle
{
   DBConnect* pgConn;
   PGresult * fetchRes;
   int        fetchIdx;
   int        fetchMaxRows;

   _PostgreSQLDataBind in;
};

class AutoPQFree
{
public:
   unsigned char* _mem;
   size_t         _size;

   AutoPQFree()
   {
      _mem  = 0;
      _size = 0;
   }

   ~AutoPQFree()
   {
      if (_mem)
         PQfreemem(_mem);
   }
};


class DBStatement : public Statement
{
public:
                 DBStatement(const char* name, const int stmntType = 0);

   virtual      ~DBStatement();

   void          initialize();

   void          destroy();

   void          allocateParams(void *bind, const unsigned int count);

   _PostgreSQLStmntHandle *_handle;

private:

   void          destroyParams(void *bind);
};

static void mapInData(SqlParams::Binding   *bind,
                      char                *&param,
                      Oid                  *ptype,
                      int8_t               *pgFree,
                      PGconn               *conn)
{
   switch (bind->type)
   {
      case SqlParams::STRING:
      case SqlParams::STRING_C_STR:
         *ptype = TEXTOID;

         if (*(char* )bind->ptr)
         {
            param = (char* ) Common::memoryAlloc(bind->ptrSize);
            String::safeCopy(param, (char* )bind->ptr, bind->ptrSize);
         }
         break;

      case SqlParams::CHAR:
         *ptype = CHAROID;

         if (*bind->asChar() != 0)
         {
            param = (char* ) Common::memoryAlloc(2);

            param[0] = *bind->asChar();
            param[1] = 0;
         }

         break;

      case SqlParams::INT8:
         *ptype = INT2OID;

         if (!bind->isNull)
         {
           param = (char* ) Common::memoryAlloc(32);
           sprintf(param, "%d", *bind->asInt8());
         }
         break;

      case SqlParams::INT16:
         *ptype = INT2OID;

         if (!bind->isNull)
         {
            param = (char* ) Common::memoryAlloc(64);
            sprintf(param, "%d", *bind->asInt16());
         }
         break;

      case SqlParams::INT32:
         *ptype = INT4OID;

         if (!bind->isNull)
         {
            param = (char* ) Common::memoryAlloc(64);
            sprintf(param, "%d", *bind->asInt32());
         }
         break;

      case SqlParams::INT64:
         *ptype = INT8OID;

         if (!bind->isNull)
         {
            param = (char* ) Common::memoryAlloc(64);
            sprintf(param, "%ld", *bind->asInt64());
         }
         break;

      case SqlParams::FLOAT:
         *ptype = FLOAT4OID;
         param  = (char* ) Common::memoryAlloc(64);
         sprintf(param, "%f", *bind->asFloat());
         break;

      case SqlParams::DOUBLE:
         *ptype = FLOAT8OID;
         param  = (char* ) Common::memoryAlloc(64);
         sprintf(param, "%lf", *bind->asDouble());
         break;

      case SqlParams::BOOL:
         *ptype = BOOLOID;
          param = (char* ) Common::memoryAlloc(8);
          if (!bind->isNull && *bind->asBool())
             strcpy(param, "true");
          else
             strcpy(param, "false");
         break;

      case SqlParams::DATE:
         *ptype = DATEOID;

         if (bind->orig.origDatePtr &&
             !bind->orig.origDatePtr->isNull())
         {
            param = (char* ) Common::memoryAlloc(16);
            sprintf(param,
                    "%4.4d-%2.2d-%2.2d",
                    bind->orig.origDatePtr->year(),
                    bind->orig.origDatePtr->month(),
                    bind->orig.origDatePtr->day());
         }
         break;

      case SqlParams::TIME:
         *ptype = TIMEOID;

         if (bind->orig.origTimePtr &&
             !bind->orig.origTimePtr->isNull())
         {
            param = (char* ) Common::memoryAlloc(16);
            sprintf(param,
                    "%2.2d:%2.2d:%2.2d",
                    bind->orig.origTimePtr->hour(),
                    bind->orig.origTimePtr->minute(),
                    bind->orig.origTimePtr->second());
         }
         break;

      case SqlParams::DATETIME:
         *ptype = TIMESTAMPOID;

         if (bind->orig.origDateTimePtr &&
             !bind->orig.origDateTimePtr->isNull())
         {
            param = (char* ) Common::memoryAlloc(32);
            sprintf(param,
                    "%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d",
                    bind->orig.origDateTimePtr->date.year(),
                    bind->orig.origDateTimePtr->date.month(),
                    bind->orig.origDateTimePtr->date.day(),
                    bind->orig.origDateTimePtr->time.hour(),
                    bind->orig.origDateTimePtr->time.minute(),
                    bind->orig.origDateTimePtr->time.second());
         }
         break;

      case SqlParams::UUID:
         *ptype = UUIDOID;

         if (!bind->isNull &&
             bind->orig.origGuidPtr &&
             !bind->orig.origGuidPtr->isNull())
         {
            param = (char* ) Common::memoryAlloc(38);
            bind->orig.origGuidPtr->toString(param);
         }
         break;

      case SqlParams::MEMBLOCK:
         *ptype = BYTEAOID;

         if (!bind->isNull && !bind->orig.origMemBlockPtr->isEmpty())
         {
            size_t paramSize = 0;

            param = (char* ) PQescapeByteaConn(conn,
                                               (const unsigned char* ) bind->orig.origMemBlockPtr->block(),
                                               bind->orig.origMemBlockPtr->size(),
                                               &paramSize);

            *pgFree = 1;

            if (param == 0)
               throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "Error escaping memory block!- %d (%s)", bind->type, bind->name);
         }
         break;

      case SqlParams::JSON:
         *ptype = JSONOID;

         if (!bind->isNull && *(char* )bind->ptr)
         {
            param = (char* ) Common::memoryAlloc(bind->ptrSize);
            String::safeCopy(param, (char* )bind->ptr, bind->ptrSize);
         }
         break;

      default :
         throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "DataType Map Error - %d (%s)", bind->type, bind->name);
   }
}

static void mapOutData(SqlParams::Binding   *bind,
                       char                 *val)
{
   switch (bind->type)
   {
      case SqlParams::STRING:
      case SqlParams::JSON:
         if (_STR_IS_NULL(val))
         {
            bind->orig.origStringPtr->clear();
            bind->isNull = true;
         }
         else
         {
            unsigned int len = strlen(val);

            bind->isNull = false;

            if (len < bind->ptrSize)
               bind->orig.origStringPtr->copyTruncate(val, len);
            else
            {
              (*bind->orig.origStringPtr) = val;
              bind->ptr                   = (char* ) bind->orig.origStringPtr->c_str();
              bind->ptrSize               = bind->orig.origStringPtr->bytesUsed();
            }
         }
         break;

      case SqlParams::CHAR:
         if (_STR_IS_NULL(val))
         {
            bind->isNull = true;
            *((char* ) bind->orig.origPtr) = 0;
         }
         else
         {
            bind->isNull = false;
            *((char* ) bind->orig.origPtr) = *val;
         }
         break;

      case SqlParams::INT8:
         if (_STR_IS_NULL(val))
         {
            bind->isNull            = true;
            *((int8_t *) bind->ptr) = 0;
         }
         else
         {
            bind->isNull            = false;
            *((int8_t *) bind->ptr) = (int16_t) String::toInt(val);
         }
         break;

      case SqlParams::INT16:
         if (_STR_IS_NULL(val))
         {
            bind->isNull             = true;
            *((int16_t *) bind->ptr) = 0;
         }
         else
         {
            bind->isNull             = false;
            *((int16_t *) bind->ptr) = (int16_t) String::toInt(val);
         }
         break;

      case SqlParams::INT32:
         if (_STR_IS_NULL(val))
         {
            bind->isNull             = true;
            *((int32_t *) bind->ptr) = 0;
         }
         else
         {
            bind->isNull             = false;
            *((int32_t *) bind->ptr) = String::toInt32(val);
         }
         break;

      case SqlParams::INT64:
         if (_STR_IS_NULL(val))
         {
            bind->isNull             = true;
            *((int64_t *) bind->ptr) = 0;
         }
         else
         {
            bind->isNull             = false;
            *((int64_t *) bind->ptr) = String::toInt64(val);
         }
         break;

      case SqlParams::DOUBLE:
         if (_STR_IS_NULL(val))
         {
            bind->isNull = true;
            *((double *) bind->ptr) = 0.0;
         }
         else
         {
            bind->isNull = false;
            *((double *) bind->ptr) = String::toDouble(val);
         }
         break;

      case SqlParams::BOOL:
         bind->isNull = false;
         if (_STR_IS_NULL(val) || *val != 't')
            *((bool *) bind->ptr) = false;
         else
            *((bool *) bind->ptr) = true;
         break;

      case SqlParams::DATE:
         if (_STR_IS_NULL(val))
         {
            bind->orig.origDatePtr->nullify();
            bind->isNull = true;
         }
         else
         {
            char yyyymmdd[9];

            sprintf(yyyymmdd, "%4.4s%2.2s%2.2s", val, val + 5, val + 8);

            *bind->orig.origDatePtr = yyyymmdd;
            bind->isNull = false;
         }

         break;

      case SqlParams::TIME:
         if (_STR_IS_NULL(val))
         {
            bind->orig.origTimePtr->nullify();
            bind->isNull = true;
         }
         else
         {
            char hhmmss[7];

            sprintf(hhmmss, "%2.2s%2.2s%2.2s", val, val + 3, val + 6);

            *bind->orig.origTimePtr = hhmmss;
            bind->isNull = false;
         }
         break;

      case SqlParams::DATETIME:
         if (_STR_IS_NULL(val))
         {
            bind->orig.origDateTimePtr->nullify();
            bind->isNull = true;
         }
         else
         {
            char yyyymmddhhmmss[15];

            sprintf(yyyymmddhhmmss, "%4.4s%2.2s%2.2s%2.2s%2.2s%2.2s", val, val + 5, val + 8, val + 11, val + 14, val + 17);

            *bind->orig.origDateTimePtr = yyyymmddhhmmss;
            bind->isNull = false;
         }
         break;

      case SqlParams::UUID:
         if (_STR_IS_NULL(val))
         {
            bind->orig.origGuidPtr->nullify();
            bind->isNull = true;
         }
         else
         {
            bind->orig.origGuidPtr->parse(val);
            bind->isNull = false;
         }
         break;

      case SqlParams::MEMBLOCK:
         bind->orig.origMemBlockPtr->clear();

         if (val)
         {
            AutoPQFree af;

            af._mem = PQunescapeBytea((const unsigned char* ) val, &af._size);

            if (af._mem == 0)
               throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "Error unescaping memory block!- %d (%s)", bind->type, bind->name);

            bind->orig.origMemBlockPtr->allocate(af._size);
            bind->isNull = false;

            memcpy(bind->orig.origMemBlockPtr->_block, af._mem, af._size);
         }
         else
            bind->isNull = false;

         break;

      default :
         throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "DataType Map Error - %d (%s)", bind->type, bind->name);
   }

   if (bind->_nullPtr)
      *bind->_nullPtr = bind->isNull;
}

DBConnect::DBConnect()
{
   _handle = 0;
}

DBConnect::~DBConnect()
{
   destroy();
}

void DBConnect::destroy()
{
   close();
   _DELETE(_handle)
}

void DBConnect::initialize()
{
   destroy();

   _handle = new _PostgreSQLHandle();

   memset(_handle, 0, sizeof(_PostgreSQLHandle));
}

void DBConnect::close()
{
   if (!_handle)
      return;

   if (!_handle->conn)
      return;

   PQfinish(_handle->conn);

   _handle->conn = 0;
}

void DBConnect::connect(const char* connectionStr)
{
   initialize();

   _handle->tranModeEnabled = true;
   _handle->inTransaction   = false;
   _handle->conn            = PQconnectdb(connectionStr);

   if (!_handle->conn)
      throw xTerminate(__FILE__, __LINE__, MODULE, "Not enough memory available to create a PGconn object!");

   if (PQstatus(_handle->conn) != CONNECTION_OK)
      RAISE("PQconnectdb", connectionStr);
}

void DBConnect::connect(const char* user,
                        const char* password,
                        const char* db_name,
                        const char* host,
                        const int   port,
                        const char* schema)
{
   String connStr;

   if (!_STR_IS_NULL(user))
      connStr += String().format(" user=%s", user);

   if (!_STR_IS_NULL(password))
      connStr += String().format(" password=%s", password);

   if (!_STR_IS_NULL(db_name))
      connStr += String().format(" dbname=%s", db_name);

   if (!_STR_IS_NULL(host))
   {
      if (String(host[0]).isInt())
        connStr += String().format(" hostaddr=%s", host);
      else
        connStr += String().format(" host=%s", host);
   }

   if (port)
        connStr += String().format(" port=%d", port);

   connect(connStr.strip().c_str());
}

bool DBConnect::reset(const unsigned int retrys, const unsigned int miliSeconds)
{
   if (_handle->inTransaction)
      _handle->inTransaction = false;

   for (int32_t idx = 0; idx <= retrys; idx++)
   {
      PQreset(_handle->conn);

      if (PQstatus(_handle->conn) == CONNECTION_OK)
         return true;

      if (miliSeconds > 0)
         Mettle::Lib::Common::sleepMili(miliSeconds);
   }

   return false;
}

const char* DBConnect::name() const throw ()
{
   return MODULE;
}

const char* DBConnect::dateToStr(const Mettle::Lib::Date   *srcDate,
                                       Mettle::Lib::String *destDate) const
{
   char jot[16];

   if (srcDate == 0 || srcDate->isNull())
      destDate->format("NULL");
   else
      destDate->format("to_date('%s', 'YYYYMMDD')", srcDate->toString(jot));

   return destDate->c_str();
}

const char* DBConnect::dateTimeToStr(const Mettle::Lib::DateTime  *srcDate,
                                           Mettle::Lib::String    *destDate) const
{
   char jot1[16];
   char jot2[16];

   if (srcDate == 0 || srcDate->isNull())
      destDate->format("NULL");
   else
      destDate->format("to_timestamp('%s%s', 'YYYYMMDDHH24MISS')", srcDate->date.toString(jot1), srcDate->time.toString(jot2));

   return destDate->c_str();
}

int DBConnect::substInBindParams(const unsigned int  bindIndex,
                                 const char         *bindname,
                                 char                jotter[256])
{
   return sprintf(jotter, "$%d", bindIndex + 1);
}

bool DBConnect::isSelectStatement(const char* sql) noexcept
{
   char* p;

   for (p = (char* ) sql; *p; *p++)
     if ((*p >= 'a' && *p <= 'z') || (*p >= 'A' && *p <= 'Z'))
     {
       if (String::strnicmp(p, "select", 6) == 0 ||
           String::strnicmp(p, "values", 6) == 0)
          return true;

       break;
     }

   return false;
}

void DBConnect::execute(Statement *statement)
{
   DBStatement  *stmnt = (DBStatement *) statement;
   bool          isSelect = false;

   stmnt->sql->subst(substInBindParams, stmnt->in);

   if (stmnt->stmntType == 1)
      isSelect = true;
   else if (stmnt->stmntType == 0)
      isSelect = isSelectStatement(stmnt->sql->value);

   bindInput(stmnt);

   stmnt->_handle->fetchRes = PQexecParams(_handle->conn,
                                           stmnt->sql->value,
                                           stmnt->_handle->in.numParams,
                                           stmnt->_handle->in.paramTypes,
                                           stmnt->_handle->in.paramValues,
                                           0,
                                           0,
                                           0);

   if (stmnt->_handle->fetchRes == 0)
      RAISE_STMNT("PQexecParams", stmnt->sql->value, stmnt, stmnt->_handle->fetchRes);

   int pqres = PQresultStatus(stmnt->_handle->fetchRes);

   if (isSelect)
   {
      if (pqres != PGRES_TUPLES_OK)
         RAISE_STMNT("PQexecParams", stmnt->sql->value, stmnt, stmnt->_handle->fetchRes);

      bindOutput(stmnt);
   }
   else
   {
      if (!PG_SUCCESS(stmnt->_handle->fetchRes))
      {
         RAISE_STMNT("PQexecParams", stmnt->sql->value, stmnt, stmnt->_handle->fetchRes);
      }
   }
}

void DBConnect::bindInput(DBStatement *stmnt)
{
   if (stmnt->sql->mapCount < 1)
     return;

   unsigned int         i;
   SqlParams::Binding  *bind;
   _PostgreSQLDataBind *stmntParam;

   stmnt->allocateParams(&stmnt->_handle->in, stmnt->sql->mapCount);

   stmntParam = &stmnt->_handle->in;

   for (i = 0; i < stmnt->sql->mapCount; i++)
   {
      bind = &stmnt->in->bindings[stmnt->sql->map[i]];

      mapInData(bind,
                stmntParam->paramValues[stmnt->sql->map[i]],
                &stmntParam->paramTypes[stmnt->sql->map[i]],
                &stmntParam->pgFree[stmnt->sql->map[i]],
                _handle->conn);
   }
}

void DBConnect::bindOutput(DBStatement *stmnt)
{
   if (stmnt->out->count < 1)
      autoAllocOutput(stmnt);
}

bool DBConnect::fetch(Statement *stmnt)
{
   return dbFetch((DBStatement *) stmnt);
}

bool DBConnect::dbFetch(DBStatement *stmnt)
{
   unsigned int            i;
   SqlParams::Binding     *bind;
   char                   *val;
   _PostgreSQLStmntHandle *handle = stmnt->_handle;

   if (handle->fetchMaxRows == 0)
   {
      handle->fetchIdx = 0;

      if ((handle->fetchMaxRows = PQntuples(handle->fetchRes)) < 1)
         return false;
   }

   if (handle->fetchIdx >= handle->fetchMaxRows)
      return false;

   for (i = 0; i < stmnt->out->count; ++i)
   {
      bind   = &stmnt->out->bindings[i];
      val    = PQgetvalue(handle->fetchRes,  handle->fetchIdx, i);

      mapOutData(bind, val);
   }

   ++handle->fetchIdx;

   return true;
}

void DBConnect::autoAllocOutput(DBStatement *stmnt)
{
   PGresult              *res = stmnt->_handle->fetchRes;
   int                    noFields;
   int                    idx;

   noFields = PQnfields(stmnt->_handle->fetchRes);

   for (idx = 0; idx < noFields; idx++)
   {
      switch (PQftype(res, idx))
      {
         case TEXTOID           :
         case VARCHAROID        :
         case VARBITOID         :
         case CSTRINGOID        :
         case JSONOID           :
         case JSONBOID          :
         case BITOID            :
         case UNKNOWNOID        :
            stmnt->out->Allocate(SqlParams::STRING, PQfname(res, idx), 0/*PQfsize(res, idx)*/);
            break;

         case CHAROID           :
         case BPCHAROID         :
            stmnt->out->Allocate(SqlParams::CHAR, PQfname(res, idx), sizeof(char));
            break;

         case INT2OID           :
            stmnt->out->Allocate(SqlParams::INT16, PQfname(res, idx), sizeof(int16_t));
            break;

         case INT4OID           :
            stmnt->out->Allocate(SqlParams::INT32, PQfname(res, idx), sizeof(int32_t));
            break;

         case INT8OID           :
            stmnt->out->Allocate(SqlParams::INT64, PQfname(res, idx), sizeof(int64_t));
            break;

         case FLOAT4OID         :
            stmnt->out->Allocate(SqlParams::FLOAT, PQfname(res, idx), sizeof(float));
            break;

         case FLOAT8OID         :
         case CASHOID           :
         case NUMERICOID        :
            stmnt->out->Allocate(SqlParams::DOUBLE, PQfname(res, idx), sizeof(double));
            break;

         case BOOLOID            :
            stmnt->out->Allocate(SqlParams::BOOL, PQfname(res, idx), sizeof(bool));
            break;

         case TIMESTAMPOID      :
         case TIMESTAMPTZOID    :
            stmnt->out->Allocate(SqlParams::DATETIME, PQfname(res, idx));
            break;

         case DATEOID           :
            stmnt->out->Allocate(SqlParams::DATE, PQfname(res, idx));
            break;

         case ABSTIMEOID        :
         case TINTERVALOID      :
         case TIMEOID           :
         case TIMETZOID         :
            stmnt->out->Allocate(SqlParams::TIME, PQfname(res, idx));
            break;

         case BYTEAOID          :
            stmnt->out->Allocate(SqlParams::MEMBLOCK, PQfname(res, idx), 0);
            break;

         case UUIDOID          :
            stmnt->out->Allocate(SqlParams::UUID, PQfname(res, idx), 0);
            break;

         default :
            throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "DataType Map Error FFF - %d (%s)", PQftype(res, idx), PQfname(res, idx));
      }
   }
}

void DBConnect::raiseException(const char         *file,
                               const int           line,
                               const char         *msg,
                               const char         *sqlStmnt,
                                     DBStatement  *pgStmnt,
                                     void         *result)
{
   xMettle::eCode   exceptionCode = xMettle::DBStandardError;

   if (strcmp(msg, "PQconnectdb"))
   {
      if (PQstatus(_handle->conn) != CONNECTION_OK)
      {
         reset(0, 0); // Auto attempt a reset

         throw xMettle(file, line, xMettle::DBBokenConnection, MODULE, "%s - Connection lost (%d/%s) [%s]",
            msg,
            PQstatus(_handle->conn),
            PQerrorMessage(_handle->conn),
            sqlStmnt);
      }
   }
   else
   {
      throw xTerminate(file, line, xMettle::DBBokenConnection, MODULE, "%s - Could not connect to database - (%d)",
         msg,
         PQstatus(_handle->conn));
   }

   if (_handle->inTransaction)
   {
      execSingleStatement("ROLLBACK TO _MX");
      execSingleStatement("SAVEPOINT _MX");
   }

   if (result)
   {
      ExecStatusType  resStatus = PQresultStatus((PGresult *) result);
      char           *resMsg    = PQresultErrorMessage((PGresult *)result);

      if (pgStmnt && pgStmnt->sql->mapCount > 0)
      {
         unsigned int   idx;
         char          *fieldMsg = 0;

         for (idx = 0; idx < pgStmnt->sql->mapCount; idx++)
           if ((fieldMsg = PQresultErrorField((PGresult *) result, idx)) != 0)
              break;

        if (fieldMsg)
           throw xMettle(file, line, exceptionCode, MODULE, "%s - For Field (%d/%s) - Status (%d/%s) [%s]",
              msg,
              idx + 1,
              fieldMsg,
              resStatus,
              resMsg,
              sqlStmnt);
      }

      if (strstr(resMsg, "could not obtain lock on row"))
         exceptionCode = xMettle::DBLockNoWaitFailed;
      else if (strstr(resMsg, "violates foreign key constraint"))
         exceptionCode = xMettle::DBForeignKeyViolation;
      else if (strstr(resMsg, "duplicate key value violates unique constraint") && strstr(resMsg, "_pkey"))
         exceptionCode = xMettle::DBPrimaryKeyViolation;
      else if (strstr(resMsg, "duplicate key value violates unique constraint"))
         exceptionCode = xMettle::DBUniqueKeyViolation;

      throw xMettle(file, line, exceptionCode, MODULE, "%s - Sql Error (%d/%s) [%s]",
         msg,
         resStatus,
         resMsg,
         sqlStmnt);
   }

   if (pgStmnt && !result)
      throw xTerminate(file, line, MODULE, "%s - OUT OF MEMORY (%s) [%s]",
         msg,
         PQerrorMessage(_handle->conn),
         sqlStmnt);

   throw xMettle(file, line, exceptionCode, MODULE, "%s - Sql Error (%s) [%s]",
      msg,
      PQerrorMessage(_handle->conn),
      sqlStmnt);
}

void DBConnect::commit()
{
   if (!_handle->inTransaction)
     return;

   execSingleStatement("COMMIT");
   _handle->inTransaction = false;
}

void DBConnect::rollback()
{
   if (!_handle->inTransaction)
     return;

   execSingleStatement("ROLLBACK");
   _handle->inTransaction = false;
}

Statement *DBConnect::createStatement(const char* name, const int stmntType)
{
   if (_handle->tranModeEnabled && !_handle->inTransaction)
   {
     execSingleStatement("BEGIN TRANSACTION", true);
     execSingleStatement("SAVEPOINT _MX");
     _handle->inTransaction = true;
   }

   DBStatement *s = new DBStatement(name, stmntType);

   s->initialize();

   s->_handle->pgConn = this;

   return s;
}

void DBConnect::execSingleStatement(const char* stmnt, const bool attemp_reset_on_fail)
{
   PGresult   *res;

   if ((res = PQexec(_handle->conn, stmnt)) == 0)
   {
      if (!attemp_reset_on_fail || !reset(0, 0))
         RAISE_STMNT("PQexec", stmnt, 0, res);

      if ((res = PQexec(_handle->conn, stmnt)) == 0)
         RAISE_STMNT("PQexec", stmnt, 0, res);
   }

   if (!PG_SUCCESS(res))
   {
      PQclear(res);

      RAISE_STMNT("PQexec", stmnt, 0, res);
   }

   PQclear(res);
}

/* DEPRECATED - Use the post sequence instead */
bool DBConnect::preGetSequence(      void          *seqNo,
                               const unsigned int   seqNoSize,
                                     Statement     *stmnt,
                               const char          *table,
                               const char          *col)
{
   Statement::Safe st(createStatement("PreGetSequence", 1));

   st.obj->sql->add(String().format("SELECT nextval('%s_%s_seq');", table, col).c_str());

   if (seqNoSize == sizeof(int32_t))
   {
      int32_t i = 0;

      st.obj->out->Bind("SeqNo", &i);

      execute(st.obj);

      if (!fetch(st.obj))
         throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "PreGetSequence() - No rows returned");

      *((int32_t *) seqNo) = i;
   }
   else if (seqNoSize == sizeof(int64_t))
   {
      int64_t i = 0;

      st.obj->out->Bind("SeqNo", &i);

      execute(st.obj);

      if (!fetch(st.obj))
         throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "PreGetSequence() - No rows returned");

      *((int64_t *) seqNo) = i;
   }
   else
      throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "SeqNoSize (%u) is not expected!", seqNoSize);

   return true;
}

bool DBConnect::postGetSequence(      void          *seqNo,
                                const unsigned int   seqNoSize,
                                      Statement     *stmnt,
                                const char          *table,
                                const char          *col)
{
   Statement::Safe st(createStatement("PostGetSequence", 1));

   st.obj->sql->add(String().format("SELECT currval('%s_%s_seq');", table, col).c_str());

   if (seqNoSize == sizeof(int32_t))
   {
      int32_t i = 0;

      st.obj->out->Bind("SeqNo", &i);

      execute(st.obj);

      if (!fetch(st.obj))
         throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "PostGetSequence() - No rows returned");

      *((int32_t *) seqNo) = i;
   }
   else if (seqNoSize == sizeof(int64_t))
   {
      int64_t i = 0;

      st.obj->out->Bind("SeqNo", &i);

      execute(st.obj);

      if (!fetch(st.obj))
         throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "PostGetSequence() - No rows returned");

      *((int64_t *) seqNo) = i;
   }
   else
      throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "SeqNoSize (%u) is not expected!", seqNoSize);

   return true;
}

void DBConnect::getTimeStamp(Mettle::Lib::DateTime &dt)
{
   Statement::Safe st(createStatement("GetTimeStamp", 1));

   st.obj->sql->add("SELECT clock_timestamp()");

   st.obj->out->Bind("TMStamp", &dt);

   execute(st.obj);

   if (!fetch(st.obj))
      throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "GetTimeStamp() - No rows returned");
}

void DBConnect::lock(Statement* stmnt)
{
   execSingleStatement("SAVEPOINT _MX");
}

bool DBConnect::transactionModeGet() const
{
   return _handle->tranModeEnabled;
}

void DBConnect::transactionModeSet(const bool mode)
{
   if (mode == _handle->tranModeEnabled)
      return;

   if (_handle->inTransaction)
      throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "Cannot toggle transaction mode while there is a transaction in progress, commit or rollback first.");

   _handle->tranModeEnabled = mode;
}

int DBConnect::backendPID() const
{
   if (!_handle)
      return 0;

   return PQbackendPID(_handle->conn);
}

DBStatement::DBStatement(const char* name, const int stmntType)
             :Statement(name, stmntType)
{
   _handle = 0;
}

DBStatement::~DBStatement()
{
   destroy();
}

void DBStatement::initialize()
{
   Statement::initialize();

   in      = new SqlParams();
   out     = new SqlParams();
   _handle = new _PostgreSQLStmntHandle();

   memset(_handle, 0, sizeof(_PostgreSQLStmntHandle));
}

void DBStatement::destroy()
{
   if (_handle)
   {
      if (_handle->fetchRes)
      {
         PQclear(_handle->fetchRes);
         _handle->fetchRes = 0;
      }

      destroyParams(&_handle->in);

      delete _handle;
      _handle = 0;
   }

   Statement::destroy();
}

void DBStatement::allocateParams(void               *bind,
                                 const unsigned int  count)
{
   destroyParams(bind);

   _PostgreSQLDataBind *b = (_PostgreSQLDataBind *) bind;

   b->numParams       = count;
   b->paramValues     = 0;
   b->pgFree          = 0;

   if (count < 1)
      return;

   b->paramValues  = (char**)  Common::memoryAlloc(sizeof(char* ) * count);
   b->paramTypes   = (Oid*)    Common::memoryAlloc(sizeof(Oid)    * count);
   b->pgFree       = (int8_t*) Common::memoryAlloc(sizeof(int8_t) * count);

   memset(b->paramValues,  0, sizeof(char* ) * count);
   memset(b->paramTypes,   0, sizeof(Oid)    * count);
   memset(b->pgFree,       0, sizeof(int8_t) * count);
}

void DBStatement::destroyParams(void *bind)

{
   _PostgreSQLDataBind *b = (_PostgreSQLDataBind *) bind;

   if (b->numParams < 1)
     return;

   if (b->paramValues)
   {
      for (int idx = 0; idx < b->numParams; idx++)
      {
         if (b->pgFree[idx])
         {
            if (b->paramValues[idx])
            {
               PQfreemem(b->paramValues[idx]);
               b->paramValues[idx] = 0;
            }
         }
         else
         {
            _FREE(b->paramValues[idx])
         }
      }
   }

   _FREE(b->paramValues)
   _FREE(b->paramTypes)
   _FREE(b->pgFree)

   b->numParams = 0;
}

#undef TEXTOID
#undef JSONOID
#undef JSONBOID
#undef BPCHAROID
#undef VARCHAROID
#undef BITOID
#undef BOOLOID
#undef VARBITOID
#undef CSTRINGOID
#undef BYTEAOID
#undef CHAROID
#undef INT2OID
#undef INT4OID
#undef INT8OID
#undef FLOAT4OID
#undef FLOAT8OID
#undef CASHOID
#undef NUMERICOID
#undef TIMESTAMPOID
#undef TIMESTAMPTZOID
#undef DATEOID
#undef ABSTIMEOID
#undef TINTERVALOID
#undef TIMEOID
#undef TIMETZOID
#undef UUIDOID

}}}

#undef RAISE
#undef RAISE_STMNT
#undef PG_SUCCESS
#undef MODULE
