/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_DB_DBSCHEMA_H_
#define __METTLE_DB_DBSCHEMA_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/collection.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace DB {

/** \class DBSchema dbschema.h mettle/db/dbschema.h
 * \brief An object that represents a relational database table schema.
 */
class DBSchema
{
public:

   /** \brief The standard column types that this object expects.
    *
    * This is used to define what column type that table columns can have.
    */
   enum ColumnType
   {
     eInt8    = 1,
     eInt16,
     eInt32,
     eInt64,
     eSeq32,
     eSeq64,
     eChar,
     eString,
     eDate,
     eTime,
     eDateTime,
     eTimeStamp,
     eDouble,
     eMemoryBlock,
     eBool,
     eUUID
   };

   class Table;

   /** \class Column dbschema.h mettle/db/dbschema.h
    * \brief An object that represent a relational database table column.
    */
   class Column
   {
   public:

      DECLARE_COLLECTION_LIST_CLASS(Column);

      /** \brief Constructor.
       *  \param colNo the column index number.
       *  \param name the column name.
       *  \param type the column type.
       *  \param size the column size if it is of a varying type like a string.
       *  \param nullable indicate if this column is nullable.
       */
      Column(const int32_t    colNo,
             const char      *name,
             const ColumnType type,
             const int32_t    size,
             const bool       nullable);

      /** \brief Destructor.
       */
      virtual ~Column();

      /** \brief Get if this column type is a digit.
       *  \returns true if the column type is compatible with a digit.
       */
      bool isDigit() const;

      /** \brief Get if this column type is a integer.
       *  \returns true if the column type is compatible with a integer.
       */
      bool isInteger() const;

      /** \brief Get if this column type is a sequence.
       *  \returns true if the column type is compatible with a sequence.
       */
      bool isSequence() const;

      /** \brief Get if this column type is a double.
       *  \returns true if the column type is compatible with a double.
       */
      bool isDouble() const;

      /** \brief Get if this column type is a char.
       *  \returns true if the column type is compatible with a char.
       */
      bool isChar() const;

      /** \brief Get if this column type is a string.
       *  \returns true if the column type is compatible with a string.
       */
      bool isString() const;

      /** \brief Get if this column type is a date.
       *  \returns true if the column type is compatible with a date.
       */
      bool isDate() const;

      /** \brief Get if this column type is a time.
       *  \returns true if the column type is compatible with a time.
       */
      bool isTime() const;

      /** \brief Get if this column type is a datetime.
       *  \returns true if the column type is compatible with a datetime.
       */
      bool isDateTime() const;

      /** \brief Get if this column type is a timestamp.
       *  \returns true if the column type is compatible with a timestamp.
       */
      bool isTimeStamp() const;

      /** \brief Get if this column type is a dateordatetime.
       *  \returns true if the column type is compatible with a dateordatetime.
       */
      bool isDateOrDateTime() const;

      /** \brief Get if this column type is a memoryblock.
       *  \returns true if the column type is compatible with a memoryblock.
       */
      bool isMemoryBlock() const;

      /** \brief Get if this column type is a bool.
       *  \returns true if the column type is compatible with a bool.
       */
      bool isBool() const;

      /** \brief Get if this column type is a universal unique identifier.
       *  \returns true if the column type is compatible with a uuid.
       */
      bool isUUID() const;

      /** \brief Get the column name.
       *  \returns The column name.
       */
      const Mettle::Lib::String &name() const;

      /** \brief Public access to _colNo.
       */
      int32_t _colNo;

      /** \brief Public access to _name.
       */
      Mettle::Lib::String _name;

      /** \brief Public access to _type.
       */
      ColumnType _type;

      /** \brief Public access to _size.
       */
      int32_t _size;

      /** \brief Public access to _nullable.
       */
      bool _nullable;

   };

   /** \class PrimaryKey dbschema.h mettle/db/dbschema.h
    * \brief An object that represent a relational database primary key.
    */
   class PrimaryKey
   {
   public:

      /** \brief Constructor.
       *  \param table the table object this primary key belongs to.
       */
      PrimaryKey(Table *table);

      /** \brief Destructor.
       */
      virtual           ~PrimaryKey();

      /** \brief Check if the primary key is a pure sequence column.
       *  \param returns true if the only primary key column is a sequence.
       */
      bool isSequence() const;

      /** \brief Public access to the column names that make up this primary key.
       */
      Mettle::Lib::String::List _columns;

      /** \brief Public access to the table object.
       */
      Table *_table;
   };

   /** \class ForeignKey dbschema.h mettle/db/dbschema.h
    * \brief An object that represent a relational database foreign key.
    */
   class ForeignKey
   {
   public:
      DECLARE_COLLECTION_LIST_CLASS(ForeignKey);

      /** \brief Constructor.
       *  \param table the table object this foreign key belongs to.
       *  \param name the name of this foriegn key.
       *  \param fkTable the name table this foreign key links to.
       */
      ForeignKey(Table *table, const char *name, const char *fkTable);

      /** \brief Destructor.
       */
      virtual ~ForeignKey();

      /** \brief Get the column name.
       *  \returns The column name.
       */
      const Mettle::Lib::String &name() const;

      /** \brief Public access to _name.
       */
      Mettle::Lib::String _name;

      /** \brief Public access to _fkTable.
       */
      Mettle::Lib::String _fkTable;

      /** \brief Public access to _columns.
       */
      Mettle::Lib::String::List _columns;

      /** \brief Public access to the table object.
       */
      Table *_table;
   };

   /** \class UniqueKey dbschema.h mettle/db/dbschema.h
    * \brief An object that represent a relational database unique key.
    */
   class UniqueKey
   {
   public:

      DECLARE_COLLECTION_LIST_CLASS(UniqueKey);

      /** \brief Constructor.
       *  \param table the table object this uqniue key belongs to.
       *  \param name the name of this unique key.
       */
      UniqueKey(Table *table, const char *name);

      /** \brief Destructor.
       */
      virtual ~UniqueKey();

      /** \brief Get the column name.
       *  \returns The column name.
       */
      const Mettle::Lib::String &name() const;

      /** \brief Public access to _name.
       */
      Mettle::Lib::String _name;

      /** \brief Public access to the unique key columns.
       */
      Mettle::Lib::String::List  _columns;

      /** \brief Public access to the table object.
       */
      Table *_table;
   };

   /** \class Index dbschema.h mettle/db/dbschema.h
    * \brief An object that represent a relational database index.
    */
   class Index
   {
   public:

      DECLARE_COLLECTION_LIST_CLASS(Index);

      /** \brief Constructor.
       *  \param table the table object this uqniue key belongs to.
       *  \param name the name of this index.
       */
      Index(Table *table, const char *name);

      /** \brief Destructor.
       */
      virtual ~Index();

      /** \brief Get the column name.
       *  \returns The column name.
       */
      const Mettle::Lib::String &name() const;

      /** \brief Public access to _name.
       */
      Mettle::Lib::String _name;

      /** \brief Public access to the unique key columns.
       */
      Mettle::Lib::String::List  _columns;

      /** \brief Public access to the table object.
       */
      Table *_table;
   };


   /** \class Table dbschema.h mettle/db/dbschema.h
    * \brief An object that represent a relational database table.
    */
   class Table
   {
   public:
      DECLARE_COLLECTION_LIST_CLASS(Table);

      /** \brief Constructor.
       *  \param schema the schema object this table falls into.
       *  \param name the name of this table.
       */
      Table(DBSchema *schema, const char *name);

      /** \brief Destructor.
       */
      virtual           ~Table();

      /** \brief Gets a column object by name.
       *  \param name the name of the column to get.
       *  \param throwIfMissing raise an exception if the column could not be found.
       *  \returns the column object found or NULL if the column could not be found.
       *  \throws xMettle if the column object could not be found.
       */
      const Column *getColumn(const char *name, const bool throwIfMissing = true) const;

      /** \brief Gets a foreign key object by name.
       *  \param name the name of the foreign key to get.
       *  \param throwIfMissing raise an exception if the foreign key could not be found.
       *  \returns the foreign key object found or NULL if the foreign key could not be found.
       *  \throws xMettle if the foreign key object could not be found.
       */
      const ForeignKey *getForeignKey(const char *name, const bool throwIfMissing = true) const;

      /** \brief Gets a unique key object by name.
       *  \param name the name of the unique key to get.
       *  \param throwIfMissing raise an exception if the unique key could not be found.
       *  \returns the unique key object found or NULL if the unique key could not be found.
       *  \throws xMettle if the unique key object could not be found.
       */
      const UniqueKey *getUniqueKey(const char *name, const bool throwIfMissing = true) const;

      /** \brief Gets an index object by name.
       *  \param name the name of the index to get.
       *  \param throwIfMissing raise an exception if the index could not be found.
       *  \returns the index object found or NULL if the index could not be found.
       *  \throws xMettle if the index object could not be found.
       */
      const Index *getIndex(const char *name, const bool throwIfMissing = true) const;

      /** \brief Check if this table has a primary key.
       *  \returns true if this table has a primary key.
       */
      bool hasPrimaryKey() const;

      /** \brief Check if this table has a foreign keys.
       *  \returns true if this table has a foreign keys.
       */
      bool hasForeignKeys() const;

      /** \brief Check if this table has a unique keys.
       *  \returns true if this table has a unique keys.
       */
      bool hasUniqueKeys() const;

      /** \brief Check if this table has a indexes.
       *  \returns true if this table has a indexes.
       */
      bool hasIndexes() const;

      /** \brief Public access to the table name.
       */
      Mettle::Lib::String _name;

      /** \brief Public access to the _columms.
       */
      Column::List _columns;

      /** \brief Public access to the table primar key.
       */
      PrimaryKey _primaryKey;

      /** \brief Public access to the foreign keys.
       */
      ForeignKey::List _foreignKeys;

      /** \brief Public access to the unique keys.
       */
      UniqueKey::List _uniqueKeys;

      /** \brief Public access to the indexes.
       */
      Index::List _indexes;

      /** \brief Public access to the schema object.
       */
      DBSchema *_schema;
   };


   /** \brief Constructor.
    */
   DBSchema();

   /** \brief Destructor.
    */
   virtual ~DBSchema();

   /** \brief Gets a table object by name.
    *  \param name the name of the table to get.
    *  \param throwIfMissing raise an exception if the table could not be found.
    *  \returns the table object found or NULL if the table could not be found.
    *  \throws xMettle if the table object could not be found.
    */
   const Table *getTable(const char *name, const bool throwIfMissing = true) const;

   /** \brief Public access to the schema name.
    */
   Mettle::Lib::String _name;

   /** \brief Public access to the tables.
    */
   Table::List _tables;
};

}}

#endif
