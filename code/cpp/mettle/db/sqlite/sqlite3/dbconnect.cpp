/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/db/sqlite/dbconnect.h"

#include "mettle/db/sqlbuilder.h"
#include "mettle/db/sqlparams.h"
#include "mettle/db/statement.h"

#include "mettle/lib/common.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sqlite3.h"

#define RAISE(msg, stmnt) raiseException(__FILE__, __LINE__, msg, stmnt, 0, 0)
#define RAISE_STMNT(msg, stmnt, sqlstmnt, result) raiseException(__FILE__, __LINE__, msg, stmnt, sqlstmnt, result)

#define MODULE  "SQLite"

#define DT_FMT    "%Y-%m-%d %H:%M:%S"
#define DATE_FMT  "%Y-%m-%d"
#define TIME_FMT  "%H:%M:%S"


using namespace Mettle::Lib;
using namespace Mettle::DB;

namespace Mettle { namespace DB { namespace SQLite {


struct _SQLiteHandle
{
   sqlite3* conn;
   bool     inTransaction;
   bool     tranModeEnabled;
};


struct _SQLiteStmntHandle
{
   DBConnect*    sqlConn;
   sqlite3_stmt* stmnt;
   int           sqlRC;
};


class DBStatement : public Statement
{
public:
                 DBStatement(const char* name, const int stmntType = 0);

   virtual      ~DBStatement();

   void          initialize();

   void          destroy();

   void          allocateParams(void* bind, const unsigned int count);

   _SQLiteStmntHandle *_handle;
};


static void freeTextInput(void* targ)
{
   _FREE(targ)
}


DBConnect::DBConnect()
{
   _handle = 0;
}

DBConnect::~DBConnect()
{
   destroy();
}

void DBConnect::destroy()
{
   close();
   _DELETE(_handle)
}

void DBConnect::initialize()
{
   destroy();

   _handle = new _SQLiteHandle();

   memset(_handle, 0, sizeof(_SQLiteHandle));
}

void DBConnect::close()
{
   if (!_handle)
      return;

   if (!_handle->conn)
      return;

   if (sqlite3_close(_handle->conn) != SQLITE_OK)
      RAISE("sqlite3_close", "");

   _handle->conn = 0;
}

void DBConnect::connect(const char* connectionStr)
{
   initialize();

   _handle->tranModeEnabled = true;
   _handle->inTransaction   = false;

   if (sqlite3_open(connectionStr, &_handle->conn) != SQLITE_OK)
   {
      if (!_handle->conn)
         throw xTerminate(__FILE__, __LINE__, MODULE, "Not enough memory avalible to create a SQLite connection object!");

      RAISE("sqlite3_open", connectionStr);
   }
}

void DBConnect::connect(const char* user,
                        const char* password,
                        const char* db_name,
                        const char* host,
                        const int   port,
                        const char* schema)

{
   String connStr;

   if (!_STR_IS_NULL(user))
      connStr += user;

   if (!_STR_IS_NULL(password))
      connStr += password;

   if (!_STR_IS_NULL(db_name))
      connStr += schema;

   if (!_STR_IS_NULL(host))
      connStr += host;

   connect(connStr.strip().c_str());
}

bool DBConnect::reset(const unsigned int retrys, const unsigned int miliSeconds)
{
   return true;
}

const char* DBConnect::name() const throw ()
{
   return MODULE;
}

const char* DBConnect::dateToStr(const Mettle::Lib::Date*   srcDate,
                                       Mettle::Lib::String* destDate) const
{
   char jot[16];

   if (srcDate == 0 || srcDate->isNull())
      destDate->format("NULL");
   else
      destDate->format("date('%4.4d-%2.2d-%2.2d')", srcDate->year(), srcDate->month(), srcDate->day());

   return destDate->c_str();
}

const char* DBConnect::dateTimeToStr(const Mettle::Lib::DateTime* srcDate,
                                           Mettle::Lib::String*   destDate) const
{
   char jot1[16];
   char jot2[16];

   if (srcDate == 0 || srcDate->isNull())
      destDate->format("NULL");
   else
      destDate->format("datetime('%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d')", srcDate->date.year(), srcDate->date.month(), srcDate->date.day(), srcDate->time.hour(), srcDate->time.minute(), srcDate->time.second());

   return destDate->c_str();
}

void DBConnect::execute(Statement* dbStatement)
{
   DBStatement* stmnt = (DBStatement*) dbStatement;
   const char*  tmp   = 0;

   stmnt->sql->subst(0, stmnt->in);

   stmnt->_handle->sqlRC = sqlite3_prepare_v2(_handle->conn,
                                              stmnt->sql->value,
                                              (int) stmnt->sql->used,
                                              &stmnt->_handle->stmnt,
                                              &tmp);

   bindInput(stmnt);

   if (stmnt->_handle->sqlRC != SQLITE_OK)
      RAISE_STMNT("sqlite3_prepare_v2", stmnt->sql->value, stmnt, 0);

   stmnt->_handle->sqlRC = sqlite3_step(stmnt->_handle->stmnt);

   if (stmnt->_handle->sqlRC == SQLITE_DONE)
      return;

   if (stmnt->_handle->sqlRC != SQLITE_ROW)
      RAISE_STMNT("sqlite_step", stmnt->sql->value, stmnt, 0);

   bindOutput(stmnt);
}

void DBConnect::bindInput(DBStatement* stmnt)
{
   if (stmnt->sql->mapCount < 1)
     return;

   unsigned int         i;
   int                  colIdx;
   int                  rc;
   const char*          sqliteName;
   SqlParams::Binding*  bind;
   char*                jotter = 0;
   char                 colJotter[256];

   for (i = 0; i < stmnt->sql->mapCount; i++)
   {
      bind = &stmnt->in->bindings[stmnt->sql->map[i]];

      sprintf(colJotter, ":%s", bind->name);

      if ((colIdx = sqlite3_bind_parameter_index(stmnt->_handle->stmnt, colJotter)) == 0)
         RAISE_STMNT("sqlite3_bind_parameter_index", bind->name, stmnt, 0);

      jotter = 0;

      switch (bind->type)
      {
         case SqlParams::STRING :
         case SqlParams::JSON   :
            if ((rc = sqlite3_bind_text(stmnt->_handle->stmnt,
                                        colIdx,
                                        bind->orig.origStringPtr->c_str(),
                                        -1,
                                        SQLITE_STATIC)) != SQLITE_OK)
               RAISE_STMNT("sqlite3_bind_text", bind->name, stmnt, 0);
            break;

         case SqlParams::STRING_C_STR :
            if ((rc = sqlite3_bind_text(stmnt->_handle->stmnt,
                                        colIdx,
                                        (char* ) bind->ptr,
                                        -1,
                                        SQLITE_STATIC)) != SQLITE_OK)
               RAISE_STMNT("sqlite3_bind_text", bind->name, stmnt, 0);
            break;

         case SqlParams::CHAR :
            jotter = (char*) Common::memoryAlloc(4);

            if (*bind->asChar() == 0)
            {
               jotter[0] = 0;
            }
            else
            {
               jotter[0] = *bind->asChar();
               jotter[1] = 0;
            }

            if ((rc = sqlite3_bind_text(stmnt->_handle->stmnt,
                                        colIdx,
                                        jotter,
                                        -1,
                                        freeTextInput)) != SQLITE_OK)
            {
               freeTextInput(jotter);
               RAISE_STMNT("sqlite3_bind_text", bind->name, stmnt, 0);
            }
            break;

         case SqlParams::BOOL     :
            if (!bind->isNull)
            {
               if ((rc = sqlite3_bind_int(stmnt->_handle->stmnt,
                                          colIdx,
                                          (int) *bind->asBool())) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_int", bind->name, stmnt, 0);
            }
            else
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            break;

         case SqlParams::INT8     :
            if (!bind->isNull)
            {
               if ((rc = sqlite3_bind_int(stmnt->_handle->stmnt,
                                          colIdx,
                                          (int) *bind->asInt8())) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_int", bind->name, stmnt, 0);
            }
            else
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            break;

         case SqlParams::INT16    :
            if (!bind->isNull)
            {
               if ((rc = sqlite3_bind_int(stmnt->_handle->stmnt,
                                          colIdx,
                                          (int) *bind->asInt16())) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_int", bind->name, stmnt, 0);
            }
            else
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            break;

         case SqlParams::INT32    :
            if (!bind->isNull)
            {
               if ((rc = sqlite3_bind_int(stmnt->_handle->stmnt,
                                          colIdx,
                                          (int) *bind->asInt32())) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_int", bind->name, stmnt, 0);
            }
            else
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            break;

         case SqlParams::INT64    :
            if (!bind->isNull)
            {
               if ((rc = sqlite3_bind_int64(stmnt->_handle->stmnt,
                                            colIdx,
                                            (sqlite3_int64)
                                            *bind->asInt64())) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_int64", bind->name, stmnt, 0);
            }
            else
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            break;

         case SqlParams::DOUBLE   :
            if ((rc = sqlite3_bind_double(stmnt->_handle->stmnt,
                                          colIdx,
                                          *bind->asDouble())) != SQLITE_OK)
               RAISE_STMNT("sqlite3_bind_double", bind->name, stmnt, 0);
            break;

         case SqlParams::DATETIME :
            if (bind->orig.origDateTimePtr->isNull())
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            else
            {
               jotter = (char* ) Common::memoryAlloc(20);
               bind->orig.origDateTimePtr->format(jotter, DT_FMT);

               if ((rc = sqlite3_bind_text(stmnt->_handle->stmnt,
                                           colIdx,
                                           jotter,
                                           19,
                                           freeTextInput)) != SQLITE_OK)
               {
                  freeTextInput(jotter);
                  RAISE_STMNT("sqlite3_bind_text", bind->name, stmnt, 0);
               }
            }
            break;

         case SqlParams::DATE     :
            if (bind->orig.origDatePtr->isNull())
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            else
            {
               jotter = (char* ) Common::memoryAlloc(11);
               bind->orig.origDatePtr->format(jotter, DATE_FMT);

               if ((rc = sqlite3_bind_text(stmnt->_handle->stmnt,
                                           colIdx,
                                           jotter,
                                           10,
                                           freeTextInput)) != SQLITE_OK)
               {
                  freeTextInput(jotter);
                  RAISE_STMNT("sqlite3_bind_text", bind->name, stmnt, 0);
               }
            }
            break;

         case SqlParams::TIME     :
            if (bind->orig.origTimePtr->isNull())
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            else
            {
               jotter = (char* ) Common::memoryAlloc(9);
               bind->orig.origTimePtr->format(jotter, TIME_FMT);

               if ((rc = sqlite3_bind_text(stmnt->_handle->stmnt,
                                           colIdx,
                                           jotter,
                                           8,
                                           freeTextInput)) != SQLITE_OK)
               {
                  freeTextInput(jotter);
                  RAISE_STMNT("sqlite3_bind_text", bind->name, stmnt, 0);
               }
            }
            break;

         case SqlParams::UUID     :
            if (bind->isNull)
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            else
            {
               Guid *goo = bind->orig.origGuidPtr;

               if (goo->isNull())
               {
                  if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                     RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
               }
               else
               {
                  jotter = (char* ) Common::memoryAlloc(37);

                  goo->toString(jotter);

                  if ((rc = sqlite3_bind_text(stmnt->_handle->stmnt,
                                              colIdx,
                                              jotter,
                                              -1,
                                              freeTextInput)) != SQLITE_OK)
                  {
                     freeTextInput(jotter);
                     RAISE_STMNT("sqlite3_bind_text", bind->name, stmnt, 0);
                  }
               }
            }
            break;

         case SqlParams::MEMBLOCK:
            if (bind->orig.origMemBlockPtr->isEmpty())
            {
               if ((rc = sqlite3_bind_null(stmnt->_handle->stmnt, colIdx)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_null", bind->name, stmnt, 0);
            }
            else
            {
               if ((rc = sqlite3_bind_blob(stmnt->_handle->stmnt,
                                           colIdx,
                                           bind->orig.origMemBlockPtr->_block,
                                           bind->orig.origMemBlockPtr->_size,
                                           SQLITE_STATIC)) != SQLITE_OK)
                  RAISE_STMNT("sqlite3_bind_blob", bind->name, stmnt, 0);

            }
            break;

         default :
            throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "DataType Map Error - %d (%s)", bind->type, bind->name);
      }

      if (bind->_nullPtr)
         *bind->_nullPtr = bind->isNull;
   }
}

void DBConnect::bindOutput(DBStatement* stmnt)
{
   if (stmnt->out->count > 0)
      return;

   sqlite3_stmt  *res      = stmnt->_handle->stmnt;
   int            noFields = sqlite3_column_count(res);
   int            idx;

   for (idx = 0; idx < noFields; idx++)
   {
      switch (sqlite3_column_type(res, idx))
      {
         case SQLITE3_TEXT      :
            stmnt->out->Allocate(SqlParams::STRING, sqlite3_column_name(res, idx), sqlite3_column_bytes(res, idx) + 1);
            break;

         case SQLITE_INTEGER    :
            stmnt->out->Allocate(SqlParams::INT32, sqlite3_column_name(res, idx), sizeof(int32_t));
            break;

         case SQLITE_FLOAT         :
            stmnt->out->Allocate(SqlParams::DOUBLE, sqlite3_column_name(res, idx), sizeof(double));
            break;

         case SQLITE_BLOB :
            stmnt->out->Allocate(SqlParams::MEMBLOCK, sqlite3_column_name(res, idx), 0);
            break;

         case SQLITE_NULL :
         default          :
            throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "DataType Map Error - %d (%s)", sqlite3_column_type(res, idx), sqlite3_column_name(res, idx));
      }
   }
}

bool DBConnect::fetch(Statement* stmnt)
{
   return dbFetch((DBStatement*) stmnt);
}

bool DBConnect::dbFetch(DBStatement* stmnt)
{
   unsigned int            i;
   SqlParams::Binding    *bind;
   const char             *sval;
   int                     ival;
   sqlite3_stmt           *res = stmnt->_handle->stmnt;
   int                     sql3type;

   if (stmnt->_handle->sqlRC == SQLITE_DONE)
      return false;

   for (i = 0; i < stmnt->out->count; ++i)
   {
      bind     = &stmnt->out->bindings[i];
      sql3type = sqlite3_column_type(res, (int) i);

      switch (bind->type)
      {
         case SqlParams::STRING :
         case SqlParams::JSON   :
            if (sql3type == SQLITE_NULL)
            {
               bind->orig.origStringPtr->clear();
               bind->isNull = true;
            }
            else
            {
               const char* val = (const char* ) sqlite3_column_text(res, (int) i);
               int         len = strlen(val);

               bind->isNull = false;

               if (len < bind->ptrSize)
                  bind->orig.origStringPtr->copyTruncate(val, len);
               else
               {
                 (*bind->orig.origStringPtr) = val;
                 bind->ptr                   = (char* ) bind->orig.origStringPtr->c_str();
                 bind->ptrSize               = bind->orig.origStringPtr->bytesUsed();
               }
            }

            break;

         case SqlParams::CHAR :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull = true;
               *((char* ) bind->orig.origPtr) = 0;
            }
            else
            {
               bind->isNull = false;
               *((char* ) bind->orig.origPtr) = *((const char* ) sqlite3_column_text(res, (int) i));
            }
            break;

         case SqlParams::BOOL :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull          = true;
               *((bool *) bind->ptr) = 0;
            }
            else
            {
               bind->isNull          = false;
               *((bool *) bind->ptr) = (bool) sqlite3_column_int(res, (int) i);
            }
            break;

         case SqlParams::INT8     :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull            = true;
               *((int8_t *) bind->ptr) = 0;
            }
            else
            {
               bind->isNull            = false;
               *((int8_t *) bind->ptr) = sqlite3_column_int(res, (int) i);
            }
            break;

         case SqlParams::INT16    :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull             = true;
               *((int16_t *) bind->ptr) = 0;
            }
            else
            {
               bind->isNull             = false;
               *((int16_t *) bind->ptr) = sqlite3_column_int(res, (int) i);
            }
            break;

         case SqlParams::INT32    :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull             = true;
               *((int32_t *) bind->ptr) = 0;
            }
            else
            {
               bind->isNull             = false;
               *((int32_t *) bind->ptr) = sqlite3_column_int(res, (int) i);
            }
            break;

         case SqlParams::INT64    :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull             = true;
               *((int64_t *) bind->ptr) = 0;
            }
            else
            {
               bind->isNull             = false;
               *((int64_t *) bind->ptr) = (int64_t) sqlite3_column_int64(res, (int) i);
            }
            break;

         case SqlParams::DOUBLE   :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull = true;
               *((double *) bind->ptr) = 0.0;
            }
            else
            {
               bind->isNull = false;
               *((double *) bind->ptr) = sqlite3_column_double(res, (int) i);
            }
            break;

         case SqlParams::DATE     :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull = true;
               bind->orig.origDatePtr->nullify();
            }
            else
            {
               DateTime dt;

               dt.parse((const char* ) sqlite3_column_text(res, (int) i), DATE_FMT);

               bind->isNull = false;
               *bind->orig.origDatePtr = dt.date;
            }

            break;

         case SqlParams::DATETIME :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull = true;
               bind->orig.origDateTimePtr->nullify();
            }
            else
            {
               bind->isNull = false;
               bind->orig.origDateTimePtr->parse((const char* ) sqlite3_column_text(res, (int) i), DT_FMT);
            }
            break;

         case SqlParams::TIME     :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull = true;
               bind->orig.origTimePtr->nullify();
            }
            else
            {
               DateTime dt;

               dt.parse((const char* ) sqlite3_column_text(res, (int) i), TIME_FMT);

               bind->isNull = false;
               *bind->orig.origTimePtr = dt.time;
            }
            break;

         case SqlParams::UUID :
            if (sql3type == SQLITE_NULL)
            {
               bind->isNull = true;
               bind->orig.origGuidPtr->nullify();
            }
            else
            {
               bind->isNull = false;
               *bind->orig.origGuidPtr = (const char* ) sqlite3_column_text(res, (int) i);
            }
            break;

         case SqlParams::MEMBLOCK:
            bind->orig.origMemBlockPtr->clear();

            if (sql3type == SQLITE_BLOB)
            {
               const void* tmp     = sqlite3_column_blob(res, (int) i);
               int         tmpSize = sqlite3_column_bytes(res, (int) i);

               if (tmp && tmpSize)
               {
                  bind->orig.origMemBlockPtr->allocate(tmpSize);
                  memcpy(bind->orig.origMemBlockPtr->_block, tmp, tmpSize);
                  bind->isNull = false;
               }
               else
                  bind->isNull = true;
            }
            else
               bind->isNull = true;
            break;

         default :
            throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "DataType Map Error - %d (%s)", bind->type, bind->name);
      }
   }

   stmnt->_handle->sqlRC = sqlite3_step(stmnt->_handle->stmnt);

   if (stmnt->_handle->sqlRC != SQLITE_ROW && stmnt->_handle->sqlRC != SQLITE_DONE)
      RAISE_STMNT("sqlite_step", stmnt->sql->value, stmnt, 0);

   return true;
}

void DBConnect::raiseException(const char*   file,
                               const int     line,
                               const char*   msg,
                               const char*   sqlStmnt,
                               DBStatement*  sqlLiteStmnt,
                               void*         result)
{
   xMettle::eCode  exceptionCode = xMettle::DBStandardError;

   throw xMettle(file, line, exceptionCode, MODULE, "%s - Sql Error (%d/%s) [%s]",
      msg,
      sqlite3_errcode(_handle->conn),
      sqlite3_errmsg(_handle->conn),
      sqlStmnt);
}

void DBConnect::commit()
{
   if (!_handle->inTransaction)
     return;

   execSingleStatement("COMMIT TRANSACTION");
   _handle->inTransaction = false;
}

void DBConnect::rollback()
{
   if (!_handle->inTransaction)
     return;

   execSingleStatement("ROLLBACK TRANSACTION");
   _handle->inTransaction = false;
}

Statement* DBConnect::createStatement(const char* name, const int stmntType)
{
   if (_handle->tranModeEnabled && !_handle->inTransaction)
   {
     execSingleStatement("BEGIN TRANSACTION");
     _handle->inTransaction = true;
   }

   DBStatement* s = new DBStatement(name, stmntType);

   s->initialize();

   s->_handle->sqlConn = this;

   return s;
}

void DBConnect::execSingleStatement(const char* stmnt)
{
   sqlite3_stmt* res = 0;
   const char*   tmp = 0;
   int           rc;

   rc = sqlite3_prepare_v2(_handle->conn,
                           stmnt,
                           -1,
                           &res,
                           &tmp);

   if (rc != SQLITE_OK)
      RAISE_STMNT("sqlite3_prepare_v2", stmnt, 0, 0);

   rc = sqlite3_step(res);

   if (rc != SQLITE_DONE)
   {
      sqlite3_finalize(res);

      RAISE_STMNT("sqlite3_step", stmnt, 0, 0);
   }

   sqlite3_finalize(res);
}

bool DBConnect::preGetSequence(      void         *seqNo,
                               const unsigned int  seqNoSize,
                                     Statement    *stmnt,
                               const char         *table,
                               const char         *col)
{
   return false;
}

bool DBConnect::postGetSequence(      void*         seqNo,
                                const unsigned int  seqNoSize,
                                      Statement*    stmnt,
                                const char*         table,
                                const char*         col)
{
   sqlite3_int64 seq = sqlite3_last_insert_rowid(_handle->conn);

   if (seqNoSize == sizeof(int32_t))
      *((int32_t *) seqNo) = (int32_t) seq;
   else
      *((int64_t *) seqNo) = (int64_t) seq;

   return true;
}

void DBConnect::getTimeStamp(DateTime &dt)
{
   // NIM - Note this is applying a UTC conversion, we don't want that
   // Statement::Safe st(createStatement("GetTimeStamp", 1));

   // st.obj->sql->add("SELECT strftime('%Y%m%d%H%M%S')");

   // st.obj->out->Bind("TMStamp", &dt);

   // execute(st.obj);

   // if (!fetch(st.obj))
   //    throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "GetTimeStamp() - No rows returned");
   dt = DateTime::now();
}

void DBConnect::lock(Statement* stmnt)
{
}

bool DBConnect::transactionModeGet() const
{
   return _handle->tranModeEnabled;
}

void DBConnect::transactionModeSet(const bool mode)
{
   if (mode == _handle->tranModeEnabled)
      return;

   if (_handle->inTransaction)
      throw xMettle(__FILE__, __LINE__, xMettle::DBStandardError, MODULE, "Cannot toggle transaction mode while there is a transaction in progress, commit or rollback first.");

   _handle->tranModeEnabled = mode;
}

int DBConnect::backendPID() const
{
   return -1;
}

DBStatement::DBStatement(const char* name, const int stmntType)
            :Statement(name, stmntType)
{
   _handle = 0;
}

DBStatement::~DBStatement()
{
   destroy();
}

void DBStatement::initialize()
{
   Statement::initialize();

   in      = new SqlParams();
   out     = new SqlParams();
   _handle = new _SQLiteStmntHandle();

   memset(_handle, 0, sizeof(_SQLiteStmntHandle));

   _handle->sqlRC = SQLITE_OK;
}

void DBStatement::destroy()
{
   if (_handle)
   {
      if (_handle->stmnt)
      {
         sqlite3_finalize(_handle->stmnt);
         _handle->stmnt = 0;
      }

      delete _handle;
      _handle = 0;
   }

   Statement::destroy();
}


#undef RAISE
#undef RAISE_STMNT
#undef MODULE

}}}
