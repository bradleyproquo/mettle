/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_DB_SQLITE_DBCONNECT_H_
#define __METTLE_DB_SQLITE_DBCONNECT_H_

#include "mettle/db/iconnect.h"

namespace Mettle { namespace DB { namespace SQLite {

struct _SQLiteHandle;

class DBStatement;

class DBConnect : public Mettle::DB::IConnect
{
   friend class DBStatement;

public:

   DBConnect();

   virtual ~DBConnect();

   virtual void connect(const char* user,
                        const char* password,
                        const char* db_name,
                        const char* host,
                        const int   port = 0,
                        const char* schema = 0);

   virtual void connect(const char* connectionStr);

   bool reset(const unsigned int retrys, const unsigned int miliSeconds);

   void commit();

   void rollback();

   void close();

   bool fetch(Statement* stmnt);

   void execute(Statement* stmnt);

   virtual Statement* createStatement(const char* name, const int stmntType = 0);

   virtual bool preGetSequence(void* seqNo, const unsigned int seqNoSize, Statement* stmnt, const char* table, const char* col);

   virtual bool postGetSequence(void* seqNo, const unsigned int seqNoSize, Statement* stmnt, const char* table, const char* col);

   virtual void getTimeStamp(Mettle::Lib::DateTime &dt);

   virtual void lock(Statement *stmnt = 0);

   virtual const char* name() const noexcept;

   virtual const char* dateToStr(const Mettle::Lib::Date   *srcDate,
                                       Mettle::Lib::String *destDate) const;

   virtual const char* dateTimeToStr(
      const Mettle::Lib::DateTime* srcDate,
            Mettle::Lib::String*   destDate) const;

   virtual bool transactionModeGet() const;

   virtual void transactionModeSet(const bool mode);

   int backendPID() const;

protected:

   _SQLiteHandle *_handle;

   void initialize();

   void destroy();

   void raiseException(
      const char*   file,
      const int     line,
      const char*   msg,
      const char*   sqlStmnt,
      DBStatement*  sqlLiteStmnt,
      void*         result);

   void execSingleStatement(const char* stmnt);

   bool dbFetch(DBStatement* stmnt);

   void bindInput(DBStatement* stmnt);

   void bindOutput(DBStatement* stmnt);
};

}}}

#endif
