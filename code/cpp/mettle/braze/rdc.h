/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

/*
   RDC NOTES   (Ross's Data Compression)
   ---------

   Original compression/uncompresseion design and code by Ed Ross, 1/92

*/

#ifndef __METTLE_BRAZE_RDC_H_
#define __METTLE_BRAZE_RDC_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/xmettle.h"

namespace Mettle {

namespace IO
{
   class MemoryStream;
}

namespace Braze {

/** \class RDC rdc.h mettle/lib/rdc.h
 * \brief Ed Ross's data compression.
 */
class RDC
{
public:

   /** \brief Data state enum.
    *
    * Used to tell what data the internal memory stream is in.
    */
   enum DataState
   {
      None = 0,
      Compressed,
      Uncompressed
   };

   /** \brief Constructor.
    */
   RDC() noexcept;

   /** \brief Defualt Destructor.
    */
   ~RDC() noexcept;

   /** \brief Eats a memory stream taking over the memory ownership.
    *  \param in the memroy stream to eat.
    *  \param length the uncompressed length of the memory stream.
    *  \param compressedLength the comrpessed length of the memory stream.
    */
   void eat(Mettle::IO::MemoryStream  *in,
            const uint32_t             length,
            const uint32_t             compressedLength);

   /** \brief Attempts to compress the internal memory stream.
    *  \throws a xMettle exception if out of memory or if the data has already been compressed.
    */
   void compress();

   /** \brief Attempts to uncompress the internal memory stream.
    *  \throws a xMettle exception if out of memory, data has already been un-compressed or if there stream is corrupt.
    */
   void uncompress();

   /** \brief Clears this objects, freeing all memory.
    */
   void clear() noexcept;

   /** \brief Get the current internal memory state.
    *  \returns the internal memory state.
    */
   DataState state() const noexcept;

   /** \brief Public access to the internal data/memory buffer, treat this as read only.
    */
   Mettle::IO::MemoryStream *_data;

   /** \brief Public access to the internal _state value, treat this as read only.
    */
   DataState _state;

   /** \brief Public access to the internal compressed length value, treat this as ready only.
    */
   uint32_t _compressedLength;

   /** \brief Public access to the internal length value, treat this as ready only.
    */
   uint32_t _length;
};

}}


#endif
