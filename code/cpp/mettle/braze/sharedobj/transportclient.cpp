/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/sharedobj/transportclient.h"

#include "mettle/braze/sharedobj/sharedobjheader.h"

#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"

#include "mettle/io/streamreader.h"
#include "mettle/io/streamwriter.h"
#include "mettle/io/memorystream.h"

#include <string.h>
#include <stdlib.h>

#if defined (OS_MS_WINDOWS)


#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#endif

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace SharedObj {

#define MODULE "SharedObj::ClientTransport"

TransportClient::Settings::Settings(const char *path,
                                    const char *so)
               :ITransport::ISettings()
{
   _path = (char *) malloc(strlen(path) + 1);
   _so   = (char *) malloc(strlen(so)   + 1);

   if (!_path || !_so)
   {
      _FREE(_path)
      _FREE(_so)

      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Could not allocate memory for path/so strings - '%s'/'%s'", path, so);
   }

   strcpy(_path,  path);
   strcpy(_so,    so);
}


TransportClient::Settings::~Settings() noexcept
{
   _FREE(_path)
   _FREE(_so)
}


TransportClient::TransportClient()
{
   _token          = -1;
   _msBuff         = new MemoryStream();

   _soTokenAcquire = 0;
   _soTokenRelease = 0;
   _soSend         = 0;
   _soReceive      = 0;

#if defined (OS_MS_WINDOWS)

     _dllPtr = 0;

#endif
}


TransportClient::~TransportClient() noexcept
{
   destruct();

   _DELETE(_msBuff)
}


void TransportClient::construct(ITransport::ISettings *settings)
{
   destruct();

   Settings *ts = (Settings *) settings;

#if defined (OS_MS_WINDOWS)

   _dllPtr  = loadDLL(ts->_path, ts->_so);

   _soTokenAcquire  = (int32_t (*)())                            getMethod("soTokenAcquire");
   _soTokenRelease  = (int32_t (*)(int32_t))                     getMethod("soTokenRelease");
   _soSend          = (int32_t (*)(int32_t, uint8_t *, int32_t)) getMethod("soSend");
   _soReceive       = (int32_t (*)(int32_t, uint8_t *, int32_t)) getMethod("soReceive");

   _token = _soTokenAcquire();

#endif
}


void TransportClient::destruct() noexcept
{
#if defined (OS_MS_WINDOWS)

   if (_dllPtr)
   {
      if (_token >= 0 && _soTokenRelease != 0)
         _soTokenRelease(_token);

      FreeLibrary(_dllPtr);
      _dllPtr = 0;
   }

#endif

   _token = -1;
}


int32_t TransportClient::signature() const
{
   return 0x0B700000;
}


IReader *TransportClient::newReader(Mettle::IO::IStream *s) const
{
   return new StreamReader(s);
}


IWriter *TransportClient::newWriter(Mettle::IO::IStream *s) const
{
   return new StreamWriter(s);
}


RpcHeader *TransportClient::createHeader()
{
   return new SharedObjHeader();
}


void TransportClient::send(RpcHeader *header,
                           IStream   *data)
{
   _msBuff->clear();

   SharedObjHeader *head = (SharedObjHeader*) header;
   StreamWriter     sr(_msBuff);
   MemoryStream    *msData = (MemoryStream *) data;

   head->_serialize(&sr, head->_name());
   _msBuff->write(msData->_memory, msData->size());

   int32_t outSize = _soSend(_token, _msBuff->_memory, _msBuff->size());

   if (outSize < 0)
     throw xMettle(__FILE__, __LINE__, MODULE, "Unexpected error code ({0}) encountered during Transport Send", outSize);

   _msBuff->clear();
   _msBuff->addMemory(outSize);
}


void TransportClient::sendErrorMessage(RpcHeader  *header,
                                       String     *errMsg)
{
}

void TransportClient::receiveHeader(RpcHeader *header)
{
   if (_msBuff->size() == 0)
      throw xMettle(__FILE__, __LINE__, MODULE, "Internal error, cannot receive before sending.");

   int32_t rc = _soReceive(_token, _msBuff->_memory, _msBuff->size());

   if (rc < 0)
     throw xMettle(__FILE__, __LINE__, MODULE, "Unexpected error code (%d) encountered during Transport Receive", rc);

   _msBuff->positionStart();

   SharedObjHeader *head = (SharedObjHeader*) header;
   StreamReader     sr(_msBuff);

   head->_deserialize(&sr, head->_name());
}

void TransportClient::receive(RpcHeader *header,
                              IStream   *data)
{
   SharedObjHeader *head = (SharedObjHeader*) header;

   if (_msBuff->size() - _msBuff->position() == 0)
      return;

   data->write(_msBuff->_memory + _msBuff->position(), _msBuff->size() - _msBuff->position());

   _msBuff->clear();
}

void TransportClient::receiveErrorMessage(RpcHeader   *head,
                                          String      *errMsg)
{
   StreamReader  sr(_msBuff);

   sr.readStart("ServerException");
   sr.read("ErrorMessage", *errMsg);
   sr.readEnd("ServerException");
}

void TransportClient::terminate()
{
}

void TransportClient::houseKeeping()
{
}

bool TransportClient::waitForConnection(const int32_t timeOut)
{
   return true;
}

String *TransportClient::clientAddress(String *buf)
{
   return buf;
}


#if defined(OS_MS_WINDOWS)

HMODULE TransportClient::loadDLL(const char *path, const char *soname)
{
   HMODULE  dllPtr = 0;

   String fullPath;

   if (path && *path)
      fullPath.joinDirs(path, soname, 0);
   else
      fullPath = soname;

   dllPtr = LoadLibraryEx(fullPath.c_str(), 0, LOAD_WITH_ALTERED_SEARCH_PATH);

   if (dllPtr == 0)
     throw xMettle(__FILE__, __LINE__, MODULE, "Error loading dll '{0}'", fullPath.c_str());

  return dllPtr;

}

FARPROC TransportClient::getMethod(const char *name)
{
   FARPROC func;

   func = GetProcAddress(_dllPtr, name);

   if (func == 0)
     throw xMettle(__FILE__, __LINE__, MODULE, "Could not load dll method '{0}'", name);

  return func;
}

#endif

#undef MODULE

}}}
