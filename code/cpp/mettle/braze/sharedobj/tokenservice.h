/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_SHAREDOBJ_TOKENSERVICE_H_
#define __METTLE_BRAZE_SHAREDOBJ_TOKENSERVICE_H_

#include "mettle/braze/rpcheader.h"

namespace Mettle {

namespace Lib
{
   class Logger;
   class Mutex;
   class String;
}

namespace IO
{
   class IStream;
}


namespace Braze {

class  IServerMarshaler;
class  ITransport;
class  RpcHeader;
class  Server;


namespace SharedObj {

class LibServer;

class TokenService
{
public:

   TokenService(const unsigned int maxServers);

   virtual ~TokenService() noexcept;

   virtual int32_t tokenAcquire();

   virtual int32_t tokenRelease(const int32_t token);

   virtual int32_t send(const int32_t  token,
                        const void    *data,
                        const int32_t  dataLen);

   virtual int32_t receive(const int32_t  token,
                                 void    *data,
                           const int32_t  dataLen);

protected:

   virtual LibServer *newServer() = 0;

   LibServer           **_servers;
   unsigned int          _maxServers;
   Mettle::Lib::Mutex   *_mutex;

};

}}}

#endif
