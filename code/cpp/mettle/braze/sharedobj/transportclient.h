/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_SHAREDOBJ_TRANSPORTCLIENT_H_
#define __METTLE_BRAZE_SHAREDOBJ_TRANSPORTCLIENT_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

#include "mettle/braze/itransport.h"

#if defined (OS_MS_WINDOWS)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#endif

namespace Mettle
{

namespace IO
{
   class MemoryStream;
}

namespace Braze { namespace SharedObj {

class TransportClient : public Mettle::Braze::ITransport
{
public:

   class Settings : public Mettle::Braze::ITransport::ISettings
   {
   public:
      Settings(const char *path,
               const char *so);

      virtual ~Settings() noexcept;

      char    *_path;
      char    *_so;
   };


   TransportClient();

   virtual ~TransportClient() noexcept;

   virtual void      construct(Mettle::Braze::ITransport::ISettings *settings);

   virtual void      destruct() noexcept;

   virtual int32_t   signature() const;

   virtual Mettle::IO::IReader    *newReader(Mettle::IO::IStream *s) const;

   virtual Mettle::IO::IWriter    *newWriter(Mettle::IO::IStream *s) const;

   virtual RpcHeader *createHeader();

   virtual void      send(Mettle::Braze::RpcHeader *header,
                          Mettle::IO::IStream      *data);

   virtual void      sendErrorMessage(RpcHeader           *head,
                                       Mettle::Lib::String *errMsg);

   virtual void      receiveHeader(RpcHeader *head);

   virtual void      receive(RpcHeader           *head,
                             Mettle::IO::IStream *data);

   virtual void      receiveErrorMessage(RpcHeader            *head,
                                          Mettle::Lib::String *errMsg);

   virtual void      terminate();

   virtual void      houseKeeping();

   virtual bool      waitForConnection(const int32_t timeOut);

   virtual Mettle::Lib::String *clientAddress(Mettle::Lib::String *buf);

protected:

#if defined (OS_MS_WINDOWS)

   HMODULE  _dllPtr;

   HMODULE  loadDLL(const char *path, const char *soname);

   FARPROC  getMethod(const char *name);

#endif

   int32_t _token;

   Mettle::IO::MemoryStream *_msBuff;

   int32_t (*_soTokenAcquire)();

   int32_t (*_soTokenRelease)(int32_t);

   int32_t (*_soSend)(int32_t, uint8_t *, int32_t);

   int32_t (*_soReceive)(int32_t, uint8_t *, int32_t);
};

}}}

#endif
