/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_SHAREOBJ_SERVER_H_
#define __METTLE_BRAZE_SHAREOBJ_SERVER_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/xmettle.h"

#include "mettle/braze/itransport.h"

namespace Mettle
{

namespace IO
{
   class MemoryStream;
}

namespace Braze { namespace SharedObj {


class TransportServer : public Mettle::Braze::ITransport
{
public:

   class Settings : public Mettle::Braze::ITransport::ISettings
   {
   public:
      Settings();

      ~Settings() noexcept;
   };


   TransportServer();

   virtual ~TransportServer() noexcept;

   virtual void      construct(Mettle::Braze::ITransport::ISettings *settings);

   virtual void      destruct() noexcept;

   virtual int32_t   signature() const;

   virtual Mettle::IO::IReader    *newReader(Mettle::IO::IStream *s) const;

   virtual Mettle::IO::IWriter    *newWriter(Mettle::IO::IStream *s) const;

   virtual RpcHeader *createHeader();

   virtual void      send(Mettle::Braze::RpcHeader *header,
                          Mettle::IO::IStream      *data);

   virtual void      sendErrorMessage(RpcHeader           *head,
                                       Mettle::Lib::String *errMsg);

   virtual void      receiveHeader(RpcHeader *head);

   virtual void      receive(RpcHeader           *head,
                             Mettle::IO::IStream *data);

   virtual void      receiveErrorMessage(RpcHeader            *head,
                                          Mettle::Lib::String *errMsg);

   virtual void      terminate();

   virtual void      houseKeeping();

   virtual bool      waitForConnection(const int32_t timeOut);

   virtual Mettle::Lib::String *clientAddress(Mettle::Lib::String *buf);

   Mettle::IO::MemoryStream *streamReceive() const;

   Mettle::IO::MemoryStream *streamSend()    const;


protected:

   Mettle::IO::MemoryStream *_recv;
   Mettle::IO::MemoryStream *_send;

};

}}}

#endif
