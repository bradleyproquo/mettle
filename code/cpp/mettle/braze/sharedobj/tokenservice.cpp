/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/sharedobj/tokenservice.h"

#include "mettle/io/ireader.h"
#include "mettle/io/iwriter.h"
#include "mettle/io/memorystream.h"

#include "mettle/braze/sharedobj/libserver.h"
#include "mettle/braze/sharedobj/sharedobjheader.h"
#include "mettle/braze/sharedobj/transportserver.h"

#include "mettle/braze/iservermarshaler.h"
#include "mettle/braze/server.h"

#include "mettle/lib/common.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/mutex.h"
#include "mettle/lib/xmettle.h"

using namespace Mettle::Lib;
using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace SharedObj {

#define MODULE "TokenService"

TokenService::TokenService(const unsigned int maxServers)
{
   _mutex      = 0;
   _servers    = 0;
   _maxServers = maxServers;

   _mutex = new Mettle::Lib::Mutex();

   if (_maxServers < 1)
      _maxServers = 1;

   _servers = (LibServer **) Common::memoryAlloc(sizeof(LibServer *) * maxServers);

   memset(_servers, 0, sizeof(LibServer *) * maxServers);
}

TokenService::~TokenService() noexcept
{
   if (_servers)
   {
      for (unsigned int idx = 0; idx < _maxServers; idx++)
      {
         _DELETE(_servers[idx])
      }

      _FREE(_servers)
   }

   _DELETE(_mutex)
}

int32_t TokenService::tokenAcquire()
{
   Mutex::Auto lock(_mutex);

   for (unsigned int idx = 0; idx < _maxServers; idx++)
      if (_servers[idx] == 0)
      {
         _servers[idx] = newServer();
         return (int32_t) idx;
      }

   return -1;
}

int32_t TokenService::tokenRelease(const int32_t token)
{
   if (token < 0 || token >= _maxServers)
      return -1;

   Mettle::Lib::Mutex::Auto lock(_mutex);

   _DELETE(_servers[token])

   return 0;
}


int32_t TokenService::send(const int32_t  token,
                           const void    *data,
                           const int32_t  dataLen)
{
   if (token < 0 || token >=_maxServers || _servers[token] == 0)
      return -1;

   LibServer                *server = _servers[token];
   TransportServer          *trans  = (TransportServer *) server->transport();
   Mettle::IO::MemoryStream *recv   = trans->streamReceive();
   Mettle::IO::MemoryStream *send   = trans->streamSend();

   recv->clear();
   recv->write(data, dataLen);
   recv->positionStart();

   server->run(0);

   return send->size();
}

int32_t TokenService::receive(const int32_t  token,
                                    void    *data,
                              const int32_t  dataLen)
{
   if (token < 0 || token >=_maxServers || _servers[token] == 0)
      return -1;

   LibServer                *server = _servers[token];
   TransportServer          *trans  = (TransportServer *) server->transport();
   Mettle::IO::MemoryStream *send   = trans->streamSend();

   if (send->size() != dataLen)
      return -2;

   memcpy(data, send->_memory, dataLen);

   return 0;
}


#undef MODULE

}}}
