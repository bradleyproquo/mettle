/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/sentinelserver.h"

#include "mettle/braze/tcp/sentinelclientconnect.h"
#include "mettle/braze/tcp/transportclient.h"
#include "mettle/braze/tcp/tcpheader.h"

#include "mettle/lib/common.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"

#include "mettle/io/bigendianreader.h"
#include "mettle/io/bigendianwriter.h"
#include "mettle/io/memorystream.h"

#include "mettle/braze/client.h"
#include "mettle/braze/iserverinterface.h"
#include "mettle/braze/iservermarshaler.h"
#include "mettle/braze/rpcheader.h"
#include "mettle/braze/itransport.h"

#include "mettle/braze/tcp/sockettcpserver.h"

#include <string.h>
#include <stdlib.h>

#if defined (OS_MS_WINDOWS)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#endif

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace TCP {


SentinelServer::ChildServerDetail::ChildServerDetail()
{
   socketServer = 0;
}


SentinelServer::ChildServerDetail::~ChildServerDetail()
{
   _DELETE(socketServer)
}


SentinelServer::SentinelServer(
   Mettle::Lib::Logger* logger,
   IServerMarshaler*    marshaler,
   ITransport*          trans)
   :Mettle::Braze::Server(logger, marshaler, trans)
{
   _log                  = logger;
   _processManager       = 0;
   _sentinelCallDelegate = 0;
}


SentinelServer::~SentinelServer() noexcept
{
   destroy();
}


void SentinelServer::destroy()
{
   if (_processManager)
      stopChildServers();

   _DELETE(_processManager)
   _childServerDetails.purge();
}


const char *SentinelServer::sentinelSignature()
{
   return "0x05337134";
}


int32_t SentinelServer::requestSocketSignature()
{
   return 0x00000001;
}


void SentinelServer::addChildServer(
   const String& name,
   const String& path,
   const String& args,
   const String& service,
   const int32_t numToRun,
   const int32_t timeOut,
   const bool    manageSocket)
{
   ChildServerDetail* rec = _childServerDetails.append(new ChildServerDetail());

   rec->name         = name;
   rec->path         = path;
   rec->args         = args;
   rec->service      = service;
   rec->numToRun     = numToRun;
   rec->timeOut      = timeOut;
   rec->numRunning   = 0;
   rec->manageSocket = manageSocket;
}


void SentinelServer::receive(
   Mettle::Lib::String& remoteSignature,
   Mettle::IO::IStream* input,
   int32_t&             errorCode,
   String*              errMsg)
{
   try
   {
      RpcHeader::Safe head(_trans->createHeader());

      _trans->receiveHeader(head.obj);

      if (head.obj->serverSignature == sentinelSignature())
      {
         _log->debug("Sentinel Message Received");

         _isSentinelCall = true;
      }
      else
      {
        if (head.obj->serverSignature != _marshaler->_signature())
           throw xMettle(__FILE__, __LINE__, xMettle::ComsInvalidHeader, "TCP/Client", "Invalid server signature received from client (%d), expected (%d)", head.obj->serverSignature, _marshaler->_signature());
      }

      remoteSignature = head.obj->clientSignature;

      if (head.obj->transportSignature != _trans->signature())
         throw xMettle(__FILE__, __LINE__, xMettle::ComsInvalidHeader, "TCP/Client", "Invalid transport signature received from client (%d), expected (%d)", head.obj->transportSignature, _trans->signature());

      _trans->receive(head.obj, input);
   }
   catch (xMettle &x)
   {
      _log->spot(__FILE__, __LINE__, x);
      errorCode = (int32_t) x.errorCode;
      *errMsg   = x.errorMsg;
   }
}


void SentinelServer::send(
   const Mettle::Lib::String& remoteSignature,
         Mettle::IO::IStream* output,
   const int32_t              errorCode,
         String*              errorMessage)
{
   RpcHeader::Safe head(_trans->createHeader());

   if (_isSentinelCall)
   {
      head.obj->serverSignature = sentinelSignature();
   }
   else
   {
      head.obj->serverSignature = _marshaler->_signature();
   }

   head.obj->clientSignature     = remoteSignature;
   head.obj->transportSignature  = _trans->signature();
   head.obj->errorCode           = errorCode;

   if (errorCode)
   {
      _trans->sendErrorMessage(head.obj, errorMessage);
      return;
   }

   _trans->send(head.obj, output);
}


void SentinelServer::run()
{
   int          exceptionCountDown = 10;
   unsigned int startChildIdx = 0;

   if (!_processManager)
      _processManager = new ProcessManager();

   construct();

   _log->info("%s[Sentinel]Server started", serverID());

   String remoteSignature;

   for (;;)
   {
      try
      {
         MemoryStream  header;
         MemoryStream  input;
         MemoryStream  output;
         int32_t       errorCode = 0;
         String        errorMessage;

         houseKeeping();

         if (!waitForConnection(1000))
         {
            if (shutdownServer())
               break;

            slackTimeHandler();

            while (startChildIdx < _childServerDetails.count())
            {
               startChildServers(_childServerDetails.get(startChildIdx++)->name);
               break;
            }

            continue;
         }

         _isSentinelCall = false;

         receive(remoteSignature,
                 &input,
                 errorCode,
                 &errorMessage);

         if (errorCode)
         {
            send(remoteSignature, &output, errorCode, &errorMessage);
            continue;
         }

         if (_isSentinelCall)
         {
            processSentinelCall(remoteSignature,
                                &input,
                                &output,
                                errorCode,
                                &errorMessage);
         }
         else
         {
           _marshaler->_serve(this,
                              remoteSignature,
                              &input,
                              &output,
                              errorCode,
                              &errorMessage);
         }

         send(remoteSignature, &output, errorCode, &errorMessage);

         exceptionCountDown = 10;
      }
      catch (xMettle &x)
      {
         if (shutdownServer())
         {
            _log->error("%sException caught & shutdown signal received: ignoring exception [%s]", serverID(), x.errorMsg);
            destruct();
            return;
         }

         exceptionCountDown--;

         if (exceptionCountDown <= 0)
         {
            _log->spot(__FILE__, __LINE__, x);
            _log->error("%sCountdown reached [%d]: blowing server...", serverID(), exceptionCountDown);
            throw;
         }

         if (x.errorCode == xMettle::TerminalException)
         {
            _log->spot(__FILE__, __LINE__, x);
            _log->error("%sTerminal exception encountered: blowing server...", serverID());
            throw;
         }

         _log->warning("%sServer exception [count:%d, code:%d, source:%s, signature:%s]: %s",
            serverID(),
            exceptionCountDown,
            x.errorCode,
            x.source,
            remoteSignature.c_str(),
            x.errorMsg);

         _trans->terminate();

         continue;
      }
      catch (std::exception &x)
      {
         if (shutdownServer())
         {
            _log->error("%sException caught & shutdown signal received: ignoring exception [%s]", serverID(), x.what());
            destruct();
            return;
         }

         exceptionCountDown--;

         if (exceptionCountDown <= 0)
         {
            _log->spot(__FILE__, __LINE__, x);
            _log->error("%sCountdown reached [%d]: blowing server....", serverID(), exceptionCountDown);
            throw;
         }

         _log->warning("%sServer exception [count:%d, signature:%s, std::exception]: %s",
            serverID(),
            exceptionCountDown,
            remoteSignature.c_str(),
            x.what());

         _trans->terminate();

         continue;
      }
      catch (...)
      {
         _log->spot(__FILE__, __LINE__);
         throw;
      }
   }

   _log->info("%s[Sentinel]Server shutting down", serverID());

   destruct();
}


void SentinelServer::processSentinelCall(
   const Mettle::Lib::String& remoteSignature,
   Mettle::IO::IStream*       input,
   Mettle::IO::IStream*       output,
   int32_t&                   errorCode,
   String*                    errMsg)
{
   Mettle::IO::MemoryStream ims;
   Mettle::IO::MemoryStream oms;

   errorCode = 0;

   input->poop(&ims, 0);

   try
   {
      if (remoteSignature == requestSocketSignature())
      {
         requestSocketInfo(&ims, &oms);
      }
      else if (_sentinelCallDelegate)
      {
         if (!_sentinelCallDelegate(remoteSignature, &ims, &oms))
            throw xMettle(__FILE__, __LINE__, "SentinelServer", "processSentinelCall(SentinelCallDelegate) - Remote Signature (%d) is not known!", remoteSignature);
      }
      else
      {
         throw xMettle(__FILE__, __LINE__, "SentinelServer", "processSentinelCall() - Remote Signature (%d) is not known!", remoteSignature);
      }
   }
   catch (xTerminate &x)
   {
      _log->spot(__FILE__, __LINE__, x);
      throw;
   }
   catch (xMettle &x)
   {
      _log->spot(__FILE__, __LINE__, x);
      errorCode = (int32_t) x.errorCode;
      *errMsg   = x.errorMsg;
   }
   catch (std::exception &x)
   {
      _log->spot(__FILE__, __LINE__, x);
      errorCode = (int32_t) xMettle::StandardException;
      *errMsg   = x.what();
   }
   catch (...)
   {
      _log->spot(__FILE__, __LINE__);
      throw xTerminate(__FILE__, __LINE__, "SentinelServer", "processSentinelCall() - Unknown exception caught!");
   }

   if (!errorCode)
      output->eat(&oms);
}


void SentinelServer::initializeChildSocket(ChildServerDetail *c)
{
   if (!c->manageSocket || c->socketServer)
      return;

   _log->info("%s - Initializing socket (%s) for (%s)", serverID(), c->service.c_str(), c->path.c_str());

   c->socketServer = new SocketTcpServer(c->timeOut);

   c->socketServer->initialize(c->service.c_str(), 0, true);

   _log->info("%s - Initializing Child Socket - Done", serverID());
}


void SentinelServer::startChildServers(const char *name)
{
   unsigned int               idx;
   int                        tdx;
   int                        toStart;
   ChildServerDetail*         c;
   ProcessManager::ChildProc* proc;
   ProcessManager::ChildProc* listProc;

   _log->info(" - Start Child Servers - Start(%s)", name ? name : "");

   for (idx = 0; idx < _childServerDetails.count(); idx++)
   {
      c = _childServerDetails.get(idx);

      if (name && c->name != name)
         continue;

      toStart = c->numToRun - c->numRunning;

      if (toStart <= 0)
      {
         _log->info(" - Server (%s) already has (%d) instances running", c->name.c_str(), c->numRunning);
         continue;
      }

      initializeChildSocket(c);

      _log->info(" - Starting (%d) %s servers", toStart, c->name.c_str());

      for (tdx = 0; tdx < toStart; tdx++)
      {
         proc      = _processManager->spawnChild(c->path, c->args);
         listProc  = c->instances.add(new ProcessManager::ChildProc());
         *listProc = *proc;

         _log->info("Server (%s) - PID (%d) started", c->name.c_str(), proc->pid);

         c->numRunning++;
      }
   }

   _log->info(" - Start Child Servers - Done");
}


void SentinelServer::stopChildServers(const char *name)
{
   unsigned int               idx;
   ChildServerDetail*         c;
   ProcessManager::ChildProc* proc;

   _log->info(" - Stop Child Servers - Start(%s)", name ? name : "");

   for (idx = 0; idx < _childServerDetails.count(); idx++)
   {
      c = _childServerDetails.get(idx);

      if (name && c->name != name)
         continue;

      if (c->numRunning < 1)
      {
         _log->info(" - 0 running (%s) servers", c->name.c_str());
         continue;
      }

      _log->info(" - Stopping (%d) (%s) servers", c->numRunning, c->name.c_str());

      for (c->instances.start(); c->instances.current(); c->instances.next())
      {
         proc = c->instances.current();

         _log->info(" - terminated PID (%d)", proc->pid);

         _processManager->terminate(proc);
      }
   }

   houseKeeping();

   _log->info(" - Stop Child Servers - Done");
}


void SentinelServer::houseKeeping()
{
   ProcessManager::ChildProc  proc;
   ChildServerDetail*         c;
   unsigned int               idx;

   _processManager->houseKeeping();

   while (_processManager->fetchChildExit(proc))
   {
      c = 0;

      for (idx = 0; idx < _childServerDetails.count(); idx++)
      {
         c = _childServerDetails.get(idx);

         for (c->instances.start(); c->instances.current(); c->instances.next())
            if (c->instances.current()->pid == proc.pid)
               break;

         if (c->instances.current() && c->instances.current()->pid == proc.pid)
            break;

         c = 0;
      }

      if (c)
      {
         c->numRunning--;
         c->instances.remove();
         _log->info("Server (%s) - PID (%d) terminated with rc (%d)", c->name.c_str(), proc.pid, proc.rc);
      }
      else
      {
         _log->info("???Unknown Child??? - PID (%d) terminated with rc (%d)", proc.pid, proc.rc);
      }
   }

   closeWaitingSocketsForDeadChildren();
}


void SentinelServer::closeWaitingSocketsForDeadChildren()
{
   unsigned int       idx;
   unsigned int       max;
   ChildServerDetail *c;

   for (idx = 0; idx < _childServerDetails.count(); idx++)
   {
      c = _childServerDetails.get(idx);

      if (!c->manageSocket || c->numRunning > 0 || !c->socketServer)
         continue;

      for (max = 0; c->socketServer->waitForConnection(0) && max < 10; max++)
      {
         c->socketServer->closeAccepted();
         _log->info("%s - Closed accepted socket for dead child server (%s)", serverID(), c->name.c_str());
      }
   }
}


void SentinelServer::requestSocketInfo(
   MemoryStream *in,
   MemoryStream *out)
{
   unsigned int       idx;
   int32_t            pid;
   ChildServerDetail* c;
   BigEndianReader    reader(in);
   BigEndianWriter    writer(out);

   reader.readStart("requestSocketInfo_IN");
   reader.read("pid", pid);
   reader.readEnd("requestSocketInfo_IN");

   for (idx = 0; idx < _childServerDetails.count(); idx++)
   {
      c = _childServerDetails.get(idx);

      for (c->instances.start(); c->instances.current(); c->instances.next())
         if (c->instances.current()->pid == pid)
         {
            writer.writeStart("requestSocketInfo_OUT");

#if defined (OS_MS_WINDOWS)

            MemoryBlock info;

            info.allocate(sizeof(WSAPROTOCOL_INFO));

            if (WSADuplicateSocket(c->socketServer->_socket, pid, (WSAPROTOCOL_INFO *) info.block()) == SOCKET_ERROR)
               SocketTcp::throwSocketError(__LINE__, "WSADuplicateSocket(%d)", pid);

            writer.write("info", &info);

#else

            writer.write("socket", c->socketServer->_socket);

#endif
            writer.writeEnd("requestSocketInfo_OUT");

            return;
         }
   }

   throw xMettle(__FILE__, __LINE__, "SentinelServer", "requestSocketInfo() - Socket request from unknown child PID (%d)", pid);
}

}}}
