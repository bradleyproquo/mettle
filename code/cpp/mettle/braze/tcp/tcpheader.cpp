/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/tcpheader.h"

#include "mettle/io/ireader.h"
#include "mettle/io/iwriter.h"

using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace TCP {

TcpHeader::TcpHeader(): Mettle::Braze::RpcHeader()
{
   messageSize    =
   compressedSize = 0;
}

TcpHeader::~TcpHeader() noexcept
{
}


const char *TcpHeader::_name() const
{
   return "TCP Header";
}

unsigned int TcpHeader::_serialize(Mettle::IO::IWriter *w, const char *name) const
{
   unsigned int sizeWritten = 0;

   sizeWritten = RpcHeader::_serialize(w, RpcHeader::_name());

   sizeWritten += w->writeStart(name);
   sizeWritten += w->write("MessageSize",       messageSize);
   sizeWritten += w->write("CompressedSize",    compressedSize);
   sizeWritten += w->writeEnd(name);

   return sizeWritten;
}

unsigned int TcpHeader::_deserialize(Mettle::IO::IReader *r, const char *name)
{
   unsigned int sizeRead = 0;

   sizeRead = RpcHeader::_deserialize(r, RpcHeader::_name());

   sizeRead += r->readStart(name);
   sizeRead += r->read("MessageSize",       messageSize);
   sizeRead += r->read("CompressedSize",    compressedSize);
   sizeRead += r->readEnd(name);

   return sizeRead;
}

}}}
