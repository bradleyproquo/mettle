/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_TCP_SENTINELSERVER_H_
#define __METTLE_BRAZE_TCP_SENTINELSERVER_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/collection.h"
#include "mettle/lib/linklist.h"
#include "mettle/lib/string.h"

#include "mettle/braze/server.h"

#include "mettle/lib/processmanager.h"

namespace Mettle
{

namespace Lib
{
   class Logger;
}

namespace IO
{
   class MemoryStream;
}

namespace Braze { namespace TCP {

class SocketTcpServer;

class SentinelServer : public Mettle::Braze::Server
{
public:

   class ChildServerDetail
   {
   public:

       ChildServerDetail();
      ~ChildServerDetail();

      Mettle::Lib::String   name;
      Mettle::Lib::String   path;
      Mettle::Lib::String   args;
      Mettle::Lib::String   service;
      int32_t               numToRun;
      int32_t               timeOut;
      int32_t               numRunning;
      bool                  manageSocket;
      SocketTcpServer*      socketServer;

      Mettle::Lib::ProcessManager::ChildProc::LinkList instances;

      DECLARE_COLLECTION_LIST_CLASS(ChildServerDetail);
   };


   typedef bool (*SentinelCallDelegate)(const Mettle::Lib::String& remoteSignature,
                                        Mettle::IO::MemoryStream*  in,
                                        Mettle::IO::MemoryStream*  out);

                      SentinelServer(Mettle::Lib::Logger*             logger,
                                     Mettle::Braze::IServerMarshaler* marshaler,
                                     Mettle::Braze::ITransport*       trans);

   virtual           ~SentinelServer() noexcept;

   void               addChildServer(const Mettle::Lib::String& name,
                                     const Mettle::Lib::String& path,
                                     const Mettle::Lib::String& args,
                                     const Mettle::Lib::String& service,
                                     const int32_t              numToRun,
                                     const int32_t              timeOut      = 1000,
                                     const bool                 manageSocket = true);

   void               startChildServers(const char* name=0);

   void               stopChildServers(const char* name=0);

   void               houseKeeping();

   virtual void       run();

   ChildServerDetail::List       _childServerDetails;
   Mettle::Lib::ProcessManager  *_processManager;
   SentinelCallDelegate          _sentinelCallDelegate;

protected:

   void                destroy();

   static const char * sentinelSignature();

   static int32_t      requestSocketSignature();

   virtual void        receive(Mettle::Lib::String& remoteSignature,
                               Mettle::IO::IStream* input,
                               int32_t&             errorCode,
                               Mettle::Lib::String* errMsg);

   virtual void        send(const Mettle::Lib::String& remoteSignature,
                                  Mettle::IO::IStream* output,
                            const int32_t              errorCode,
                                  Mettle::Lib::String* errorMessage);

   virtual void        processSentinelCall(const Mettle::Lib::String& remoteSignature,
                                           Mettle::IO::IStream*       input,
                                           Mettle::IO::IStream*       output,
                                           int32_t&                   errorCode,
                                           Mettle::Lib::String*       errMsg);

   virtual void        initializeChildSocket(ChildServerDetail* c);

   virtual void        requestSocketInfo(Mettle::IO::MemoryStream* in,
                                         Mettle::IO::MemoryStream* out);

   virtual void        closeWaitingSocketsForDeadChildren();

   bool                 _isSentinelCall;
   Mettle::Lib::Logger* _log;
};

}}}

#endif
