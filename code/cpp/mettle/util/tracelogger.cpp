/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/util/tracelogger.h"

#include <time.h>
#include <string.h>
#include <stdio.h>

using namespace Mettle::Lib;

namespace Mettle { namespace Util {


void TraceFileLogger::TraceItem::Clear()
{
   file        = 0;
   line        = 0;
   datetime[0] = 0;
   lvl[0]      = 0;
   msg[0]      = 0;
}


TraceFileLogger::TraceFileLogger(const char*          logFile,
                                 const Level          level,
                                 const bool           display,
                                 const unsigned short maxTraces)
                :LoggerStdFile(logFile, level, display)
{
   _maxTraces = maxTraces;
   _currItem  = 0;
   _itemIdx   = 0;
   _items     = new TraceItem[_maxTraces]();

   for (unsigned int idx = 0; idx < _maxTraces; ++idx)
      _items[idx].Clear();
}


TraceFileLogger::~TraceFileLogger() noexcept
{
   if (_items)
      delete [] _items;
}


void TraceFileLogger::TraceReset() noexcept
{
   for (unsigned int idx = 0; idx < _maxTraces; ++idx)
      _items[idx].Clear();

   _currItem = 0;
   _itemIdx  = 0;
}


void TraceFileLogger::GetDate(char* dest) noexcept
{
   time_t now;
   tm*    nowTm;

   time(&now);

   nowTm = localtime(&now);

   sprintf(dest,
           "%04d%02d%02d%02d%02d%02d",
           nowTm->tm_year + 1900,
           nowTm->tm_mon + 1,
           nowTm->tm_mday,
           nowTm->tm_hour,
           nowTm->tm_min,
           nowTm->tm_sec);
}

Logger *TraceFileLogger::Trace(const char*        file,
                               const unsigned int line) noexcept
{
   TraceItem *item = NextItem();

   item->file = (char*)file;
   item->line = line;

   strcpy(item->lvl, "TRC");
   GetDate(item->datetime);
   item->msg[0] = 0;

   return this;
}


void TraceFileLogger::error(const char* fmt, ...) noexcept
{
   va_list ap;

   va_start(ap, fmt);
   TraceLog(fmt, ap, "ERR");
   va_end(ap);

   if (_level > Error)
      return;

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, _logFH);
      va_end(ap);
   }
}


void TraceFileLogger::warning(const char* fmt, ...) noexcept
{
   va_list ap;

   va_start(ap, fmt);
   TraceLog(fmt, ap, "ERR");
   va_end(ap);

   if (_level > Warning)
      return;

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, _logFH);
      va_end(ap);
   }
}


void TraceFileLogger::info(const char* fmt, ...) noexcept
{
   va_list ap;

   va_start(ap, fmt);
   TraceLog(fmt, ap, "INF");
   va_end(ap);

   if (_level > Info)
      return;

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, _logFH);
      va_end(ap);
   }
}


void TraceFileLogger::debug(const char* fmt, ...) noexcept
{
   va_list ap;

   va_start(ap, fmt);
   TraceLog(fmt, ap, "DBG");
   va_end(ap);

   if (_level > Debug)
      return;

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, stdout);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      FileLog(fmt, ap, _logFH);
      va_end(ap);
   }
}


void TraceFileLogger::TraceLog(const char* fmt,
                               va_list     ap,
                               const char* lvl) noexcept
{
   TraceItem* item = NextLogItem();

   strcpy(item->lvl, lvl);
   GetDate(item->datetime);
   vsnprintf(item->msg, sizeof(item->msg) -1, fmt, ap);
}


void TraceFileLogger::FileLog(const char* fmt,
                              va_list     ap,
                              FILE       *fp) noexcept
{
   fprintf(fp, "%s %s ", _currItem->datetime, _currItem->lvl);
   vfprintf(fp, fmt, ap);
   fputc('\n', fp);
   fflush(fp);
}


TraceFileLogger::TraceItem* TraceFileLogger::NextItem() noexcept
{
   _currItem = &_items[_itemIdx++];

   _currItem->Clear();

   if (_itemIdx >= _maxTraces)
      _itemIdx = 0;

   return _currItem;
}


TraceFileLogger::TraceItem* TraceFileLogger::NextLogItem() noexcept
{
   if (_currItem == 0)
      return NextItem();

   if (_currItem->msg[0] == 0)
      return _currItem;

   return NextItem();
}

void TraceFileLogger::TraceUnwind() const noexcept
{
   if (_currItem == 0)
   {
      UnwindMsg("!! Nothing to Unwind");
      return;
   }

   unsigned int  idx = _itemIdx;
   unsigned int  cnt = 0;
   TraceItem*    item;

   if (idx + 1 == _maxTraces)
      idx = 0;
   else
      idx++;

   UnwindMsg("!! Unwinding Stack Trace");

   for (;;)
   {
      item = &_items[idx++];

      if (item->file || item->msg[0])
      {
         if (item->file && item->msg[0])
            UnwindMsg("[%u](%s/%u): %s %s %s", ++cnt, item->file, item->line, item->datetime, item->lvl, item->msg);
         else if (item->file)
            UnwindMsg("[%u](%s/%u): %s %s", ++cnt, item->file, item->line, item->datetime, item->lvl);
         else
            UnwindMsg("[%u]: %s %s %s", ++cnt, item->datetime, item->lvl, item->msg);
      }

      if (idx == _maxTraces)
         idx = 0;

      if (idx == _itemIdx)
         break;
   }
}


void TraceFileLogger::UnwindMsg(const char* fmt, ...) const noexcept
{
   va_list ap;

   if (!_logFH || _display)
   {
      va_start(ap, fmt);
      UnwindMsg(stdout, fmt, ap);
      va_end(ap);
   }

   if (_logFH)
   {
      va_start(ap, fmt);
      UnwindMsg(_logFH, fmt, ap);
      va_end(ap);
   }
}


void TraceFileLogger::UnwindMsg(FILE* fp, const char* fmt, va_list ap) const noexcept
{
   vfprintf(fp, fmt, ap);
   fputc('\n', fp);
   fflush(fp);
}


}}
