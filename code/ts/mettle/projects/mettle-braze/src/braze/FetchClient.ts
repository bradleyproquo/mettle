/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Client }        from './Client';
import { MettlePromise } from '@bitsmiths/mettle-lib';
import { ListStream }    from '@bitsmiths/mettle-io';

/**
 * The braze client object that implements comms using fetch (may require polyfills on older browsers)
 */
export class FetchClient extends Client {
  /**
   * The braze client object that implements comms using fetch (may require polyfills on older browsers)
   *
   * @param _url          Endpoint to hit.
   * @param _corsMode     Cors mode (no-cors, cors, same-origin) (usually no-cors) one of FetchCorsMode
   * @param _credentials  Credential cookie mode (omit, same-origin, include) (usually include) one of FetchCredentials
   * @param _headers      Optional headers to add to the fetch/braze calls
   * @param _sigUrl       If set to true, fetch client will add the server and rpc signatures to the url.
   */
  constructor(
    public _url: string,
    public _corsMode: RequestMode,
    public _credentials: RequestCredentials,
    public _headers: {} = undefined,
    public _sigUrl: boolean = false) {
    super();
  }

  /**
   * Sends the input stream to the server.
   * @param prom      The promise to break on failure.
   * @param marsh     The generated braze client marshaler.
   * @param sig       The call signature.
   * @param instream  The input stream to send.
   * @param recv      The function to call with the data received
   */
  send(
    prom:     MettlePromise,
    marsh:    any,
    sig:      string,
    instream: ListStream,
    recv:     (prom: MettlePromise, marsh: any, sig: string, stream: ListStream) => any): void {
    instream._list.unshift(marsh._signature());
    instream._list.unshift(sig);
    instream._list.unshift(0);
    instream._list.unshift(parseInt('0x08779915', 16));

    var url: string;

    if (this._sigUrl === true) {
      url = `${this._url}/${marsh._signature()}/${sig}`;
    } else {
      url = this._url;
    }

    fetch(url, {
      method:      'POST',
      body:        JSON.stringify(instream._list),
      credentials: this._credentials,
      mode:        this._corsMode,
      headers:     this._headers || {},
    })
    .then((response: any) =>
    {
      if (response.ok)
        return response.json();

      throw new Error('Network response was not ok.');
    })
    .then ((jsonData: any[]) =>
    {
      // Note input stream should look like: [trans_sig, error_code, client_sig, server_sigh, error_msg]
      const stream = new ListStream(prom, jsonData);

      if (stream.read() !== parseInt('0x08779915', 16))
        prom.break('Transport signature mismatch!');

      const errCode = stream.read();

      if (stream.read() !== sig)
        prom.break('Reponse client signature mismatch!');
      else if (stream.read() !== marsh._signature())
        prom.break('Reponse server signature mismatch!');

      if (errCode !== 0)
      {
        prom.break(stream.read(), errCode);
        return;
      }

      if (prom.broken())
        return;

      recv(prom, marsh, sig, stream);
    })
    .catch (function (error)
    {
      prom.break(error.message);
    });
  }
}
