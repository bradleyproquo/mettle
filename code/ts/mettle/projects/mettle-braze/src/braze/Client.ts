/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

 import { MettlePromise } from '@bitsmiths/mettle-lib';
 import { ListStream }    from '@bitsmiths/mettle-io';
 import { Writer }        from '@bitsmiths/mettle-io';
 import { Reader }        from '@bitsmiths/mettle-io';
 import { StreamWriter }  from '@bitsmiths/mettle-io';
 import { StreamReader }  from '@bitsmiths/mettle-io';

/**
 * The braze client object that needs to be overloaded depending on what
 * framework is being used.
 */
export class Client {

  constructor() {
  }

  /**
   * Get the writer object to be used for the client.
   *
   * @param prom      The promise to break on failure.
   * @param instream  The input stream to send.
   * @returns         New writer object.
   */
  newWriter(prom: MettlePromise, instream: ListStream): Writer {
    return new StreamWriter(prom, instream);
  }

  /**
   * Get the reader object to be used for the client.
   *
   * @param prom       The promise to break on failure.
   * @param outstream  The output stream to receive.
   * @returns          New reader object.
   */
  newReader(prom: MettlePromise, outstream: ListStream): Reader {
    return new StreamReader(prom, outstream);
  }

  /**
   * Sends the input stream to the server, this is a virtual method.
   *
   * @param prom      The promise to break on failure.
   * @param marsh     The generated braze client marshaler.
   * @param sig       The call signature.
   * @param instream  The input stream to send.
   */
  send(
    prom:     MettlePromise,
    marsh:    any,
    sig:      string,
    instream: ListStream,
    recv:     (prom: MettlePromise, marsh: any, sig: string, _o: ListStream) => any): void {
    // Virtual method.
  }
}
