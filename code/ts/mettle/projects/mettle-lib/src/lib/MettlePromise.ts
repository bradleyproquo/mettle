/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { xMettle }      from './xMettle';
import { xMettleECode } from './xMettle';

/**
 * Promise handling class.
 */
export class MettlePromise {
  _onSucc : (obj: any, httpCode: number)=>any;
  _onFail : (ex: xMettle)=>any;
  _onExcp : (ex: xMettle)=>any;
  _stack  : number  = 0;
  _broken : boolean = false;

  /**
   * constructor.
   * @param onSucc Function to call on success, function should look like, func(succObj, httpCode), succObj will the value.
   * @param onFail Function to call on failure, function should look like, func(xmettle), xmettle will the an xMettle instance.
   * @param onExcp Function to call on exception, not required, but useful for seperateing intenral and user errors.
   */
  constructor(onSucc: (obj: any, httpCode: number)=>any, onFail: (ex: xMettle)=>any, onExcp: (ex: xMettle)=>any) {
    this._onSucc = onSucc;
    this._onFail = onFail;
    this._onExcp = onExcp;
    this._stack  = 0;
    this._broken = false;
  }

  /**
   * Check to see if this is a broken promise.
   * @returns True if this is a broken promise.
   */
  broken():boolean {
    return this._broken;
  }

  /**
   * Push the promise to another level of logic.  Prevents it being kept 1 time.
   *  For every push(), there must also be a keep() call.
   * @returns Returns itself.
   */
  push():this {
    this._stack++;
    return this;
  }

  /**
   * Keep the promise signalling a success if this is the last keep() call in the stack.
   * @param pobj      The object to send to with the promise.
   * @param httpCode  Optionall also send the http code.
   * @returns         pobj if the keep reduced the stack else, returns undefined;
   */
  keep(pobj: any, httpCode?: number): any {
    if (this._broken) {
      let xm = new xMettle("Unhandled broken promise!", xMettleECode.InternalException);

      if (this._onExcp !== undefined)
        this._onExcp(xm);
      else if (this._onFail !== undefined)
        this._onFail(xm);

      return undefined;
    }

    if (this._stack <= 0) {
       this._onSucc(pobj, (httpCode === undefined ? 200 : httpCode));
       return undefined;
    }

    this._stack--;
    return pobj;
  }

  /**
   * Break a promise with a msg and http code.  By default raises standard exceptions if type is not specified..
   * @param err      An xMettle exception or a message.
   * @param errCode  Optionall pass in the xMettleECode error code.
   * @param httpCode Optionally pass in the http code if err is a string.
   */
  break(err: xMettle | string, errCode?: number, httpCode?: number) {
    this._broken = true;

    let xm;

    if (typeof(err) === "object" && err instanceof xMettle)
      xm = (<xMettle>err);
    else
      xm = new xMettle((<string>err), (errCode === undefined ? xMettleECode.InternalException : errCode), httpCode);

    if (xm.getErrorCode() !== xMettleECode.StandardException && this._onExcp !== undefined)
      this._onExcp(xm);
    else
      this._onFail(xm);
  }

  /**
   * Break a promise with a msg and http code.  By default raises internal exception if type is not specified.
   * @param err      Error message.
   * @param errCode  Optionall pass in the xMettleECode error code.
   * @param httpCode Optionally pass in the http code if err is a string.
   */
  raise(err: xMettle | string, errCode?: number, httpCode?: number) {
    this._broken = true;

    if (typeof(err) === "object" && err instanceof xMettle) {
      if (this._onExcp != undefined)
        this._onExcp(err);
      else
        this._onFail(err);
    }

    let xm;

    if (typeof(err) === "object" && err instanceof xMettle)
      xm = (<xMettle>err);
    else
      xm = new xMettle((<string>err), (errCode === undefined ? xMettleECode.InternalException : errCode), httpCode);

    if (this._onExcp !== undefined)
      this._onExcp(xm);
    else
      this._onFail(xm);
  }
}
