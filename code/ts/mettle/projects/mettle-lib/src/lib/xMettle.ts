/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

export const xMettleECode = {
  NoError    : 0,

  UnknownException   : -101,
  StandardException  : -102,
  TerminalException  : -103,
  OutOfMemory        : -104,
  SocketTimeout      : -105,
  InternalException  : -106,
  DavException       : -107,
  CredExpired        : -108,
  CredInvalid        : -109,
  CredDenied         : -110,
  TokenExpired       : -111,
  TokenInvalid       : -112,
  STANDARD_MAX_ERROR : -199,

  ComsStandardError : -200,
  ComsTimeOut       : -201,
  ComsInvalidHeader : -202,
  ComsServerError   : -203,
  ComsReceiveError  : -204,
  ComsSignature     : -205,
  COMSLASTERROR     : -299,

  DBStandardError       : -300,
  DBForeignKeyViolation : -301,
  DBPrimaryKeyViolation : -302,
  DBUniqueKeyViolation  : -303,
  DBLockNoWaitFailed    : -304,
  DBBokenConnection     : -305,
  DBLASTERROR           : -399
};

 /**
  * Exception class.
  */
export class xMettle {
  _msg   : string = '';
  _code  : number = xMettleECode.StandardException;
  _hcode : number = 599;

 /**
  * @param errMsg   The generated error message.
  * @param errCode  The exception code.
  * @param httpCode The http code.
  */
  constructor (errMsg: string, errCode?: number, httpCode?: number) {
    this._msg   = errMsg;
    this._code  = errCode  === undefined ? xMettleECode.StandardException : errCode;
    this._hcode = httpCode === undefined ? 599 : httpCode;
  }

  /**
   * @returns The exception description.
   */
  toString(): string {
    return "xMettle [msg:" + this._msg + ", code:" + this._code + "]";
  }

  /**
   * Get the error code.
   * @returns The exception enum/xMettleECode.
   */
  getErrorCode(): number  {
    return this._code;
  }

  /**
   * Get the error message.
   * @returns The exception message.
   */
  getMsg(): string {
    return this._msg;
  }

  /**
   * Get the http response code.
   * @returns The http code.
   */
  getHttpCode(): number {
    return this._hcode;
  }
}
