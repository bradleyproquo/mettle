/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { MettlePromise } from '@bitsmiths/mettle-lib'

/**
  * Standard writer interface for serializable mettle objects
  */
export interface Writer {

  _prom: MettlePromise;

  /**
   * Clears the writer and stream.
   */
  clear(): void;

  /**
   * Start writing a new object.
   * @param name  The name of the object being written.
   */
  writeStart(name: string): void;

  /**
   * Start writing a new list object.
   * @param name      Name of the object being written.
   * @param listSize  The length of the list.
   */
  writeStartList(name: string, listSize: number): void;

  /**
   * End writing an object.
   * @param name  The name of the object.
   */
  writeEnd(name: string): void;

  /**
   * Write a boolean
   * @param name  The name of the value.
   * @param v     The boolen being written.
   */
  writeBool(name: string, v: boolean): void;

  /**
   * Write a string.
   * @param name  The name of the value.
   * @param v     The string being written.
   */
  writeString(name: string, v: string): void;

  /**
   * Write a char.
   * @param name  The name of the value.
   * @param v     The char being written.
   */
  writeChar(name: string, v: string): void;

  /**
   * Write an integer.
   * @param name  The name of the value.
   * @param v     The int being written.
   */
  writeInt(name: string, v: number): void;

  /**
   * Write a double.
   * @param name  The name of the value.
   * @param v     The int being written.
   */
  writeFloat(name: string, v: number): void;

  /**
   * Write an integer.
   * @param name  The name of the value.
   * @param v     The int being written.
   */
  writeNumber(name: string, v: number): void;

  /**
   * Write a datetime.
   * @param name  The name of the value.
   * @param v     The datetime being written.
   */
  writeDateTime(name: string, v: Date): void;

  /**
   * Write a date.
   * @param name  The name of the value.
   * @param v     The date being written.
   */
  writeDate(name: string, v: Date): void;

  /**
   * Write a time.
   * @param name  The name of the value.
   * @param v     The date being written.
   */
  writeTime(name: string, v: Date): void;

  /**
   * Write a byte array.
   * @param name  The name of the value.
   * @param v     The byte array being written.
   */
  writeByteArray(name: string, v: string): void;

  /**
   * Write a guid.
   * @param name  The name of the value.
   * @param v     The guid being written.
   */
  writeGuid(name: string, v: string): void;
}
