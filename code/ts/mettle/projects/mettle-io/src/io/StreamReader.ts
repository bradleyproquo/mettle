/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { MettlePromise } from '@bitsmiths/mettle-lib';
import { ListStream }    from './ListStream';
import { Reader }        from './Reader';
import { Util }          from './Util';

/**
 * Standard reader object
 */
export class StreamReader implements Reader {

  /**
   * Constructor
   * @param prom    The promise to break if anything goes wrong.
   * @param stream  The stream object to read from.
   */
   constructor(public _prom: MettlePromise, public _stream: ListStream) { }

  /**
   * Clears the reader and stream.
   */
  clear() {
    this._stream.clear();
  }

  /**
   * Start reading a new object.
   * @param name  The name of the object being read.
   */
  readStart(name: string) {
  }

  /**
   * Start reading a new list object.
   * @param name  The name of the object being written.
   * @returns     The length of the list.
   */
  readStartList(name: string): number {
    return this.readInt(name);
  }

  /**
   * End reading an object.
   * @param name  The name of the object.
   */
  readEnd(name: string) {
  }

  /**
   * Read a boolean
   * @param name  The name of the value.
   * @returns     The boolen being read.
   */
  readBool(name: string): boolean {
    const res = this._stream.read();

    if (this._prom.broken())
      return false;

    if (typeof(res) !== 'boolean') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a bool');
      return false;
    }

    return res;
  }

  /**
   * Read a string.
   * @param name  The name of the value.
   * @returns     The string being read.
   */
  readString(name: string): string {
    const res = this._stream.read();

    if (this._prom.broken())
      return '';

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a string');
      return '';
    }

    return res;
  }

  /**
   * Read a char.
   * @param name  The name of the value.
   * @returns     The char being read.
   */
  readChar(name: string): string {
    const res = this._stream.read();

    if (this._prom.broken())
      return '';

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a char/string');
      return '';
    }

    if (res.length !== 1) {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a char - invalid length: ' + res.length);
      return '';
    }

    return res;
  }

  /**
   * Read an integer.
   * @param name  The name of the value.
   * @returns     T: the number being read.
   */
  readInt(name: string): number {
    const res = this._stream.read();

    if (this._prom.broken())
      return 0;

    if (!res && res !== 0) {
      return undefined;
    }

    if (typeof(res) !== 'number') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a number');
      return 0;
    }

    return res;
  }

  /**
   * Read a float.
   * @param name  The name of the value.
   * @returns     T: the number being read.
   */
  readFloat(name: string): number {
    const res = this._stream.read();

    if (this._prom.broken())
      return 0;

    if (typeof(res) !== 'number') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a number');
      return 0;
    }

    return res;
  }

  /**
   * Read a ageneric js/ts number.
   * @param name  The name of the value.
   * @returns     T: the number being read.
   */
  readNumber(name: string): number {
    const res = this._stream.read();

    if (this._prom.broken())
      return 0;

    if (!res && res !== 0) {
      return undefined;
    }

    if (typeof(res) !== 'number') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a number');
      return 0;
    }

    return res;
  }

  /**
   * Read a datetime.
   * @param name string   : name of the value.
   * @returns     T : the datetime being read.
   */
  readDateTime(name: string): Date {
    try {
      const res = this._stream.read();

      if (this._prom.broken())
        return undefined;

      if (!res) {
        return undefined;
      }

      return Util.stringToDateTime(res);
    } catch (e) {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream: ' + e.message);
      return undefined;
    }
  }

  /**
   * Read a date.
   * @param name string   : name of the value.
   * @returns     T : the date being read.
   */
  readDate(name: string): Date {
    try {
      const res = this._stream.read();

      if (this._prom.broken())
        return undefined;

      if (!res) {
        return undefined;
      }

      return Util.stringToDate(res);
    } catch (e) {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream: ' + e.message);
      return undefined;
    }
  }

  /**
   * Read a time.
   * @param name string   : name of the value.
   * @returns     T : the time being read.
   */
  readTime(name: string): Date {
    try {
      const res = this._stream.read();

      if (this._prom.broken())
        return undefined;

      if (!res) {
        return undefined;
      }

      return Util.stringToTime(res);
    } catch (e) {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream: ' + e.message);
      return undefined;
    }
  }

  /**
   * Read a byte array.
   * @param name  The name of the value.
   * @returns     The base64 encoded byte array.
   */
  readByteArray(name: string): string {
    const res = this._stream.read();

    if (this._prom.broken())
      return '';

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a byte array (string)');
      return '';
    }

    return res;
  }

  /**
   * Read a guid.
   * @param name  The name of the value.
   * @returns     The guid being read.
   */
  readGuid(name: string): string {
    const res = this._stream.read();

    if (this._prom.broken())
      return '';

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.Reader: Cannot read [' + name + '] from stream type is not a string/guid');
      return '';
    }

    if (res.length !== 0 && res.length !== 36) {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, guid length [' + res.length + '] is not valid, expected [36]');
      return '';
    }

    return res;
  }
}
