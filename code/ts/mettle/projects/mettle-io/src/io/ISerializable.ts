/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Writer } from './Writer';
import { Reader } from './Reader';

/**
 * This is the mettle.io Serializable interface.
 */
export class ISerializable {

  constructor() { }

  /**
   * Serializes this base object
   *
   * @param _w     The writer object that will perform the serialization.
   * @param _name  The name of the object, if left null, the _name() will be used.
   */
  _serialize(_w: Writer, _name?: string) {

  }

  /**
   * Deserializes this base object
   *
   * @param _r     The reader object that will perform the deserialization.
   * @param _name  The name of the object, if left null, the _name() will be used.
   */
  _deserialize(_r: Reader, _name?: string) {

  }

  /**
   * Gets the name of the base object.
   *
   * @returns The name of the object.
   */
  _name(): string {
    return '--UNKNOWN--';
  }

}
