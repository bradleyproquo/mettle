/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { MettlePromise } from '@bitsmiths/mettle-lib';
import { Writer }        from './Writer';
import { Util }          from './Util';

/**
 * Object writer object
 */
export class ObjWriter implements Writer {

  /**
   * constructor
   * @param _prom   The promise to use.
   * @param _object The object to write to.
   */
  constructor(public _prom: MettlePromise, public _object: Object) { }

  /**
   * Clears the writer and stream.
   */
  clear() {
    for (const key in this._object) {
      if (this._object.hasOwnProperty(key)) {
        delete this._object[key];
      }
    }
  }

  /**
   * Start writing a new object.
   * @param name  The name of the object being written.
   */
  writeStart(name: string) {
  }

  /**
   * Start writing a new list object.
   * @param name      Name of the object being written.
   * @param listSize  The length of the list.
   */
  writeStartList(name: string, listSize: number) {
  }

  /**
   * End writing an object.
   * @param name  The name of the object.
   */
  writeEnd(name: string) {
  }

  /**
   * Write a boolean
   * @param name  The name of the value.
   * @param v     The boolen being written.
   */
  writeBool(name: string, v: boolean): void {
    if (typeof(v) !== 'boolean')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a bool');

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }

  /**
   * Write a string.
   * @param name  The name of the value.
   * @param v     The string being written.
   */
  writeString(name: string, v: string): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    if (typeof(v) !== 'string')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a string');

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }

  /**
   * Write a char.
   * @param name  The name of the value.
   * @param v     The char being written.
   */
  writeChar(name: string, v: string): void {
    if (typeof(v) !== 'string')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a string');

    if (v.length !== 1)
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, char has invalid length:' + v.length);

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }

  /**
   * Write an integer.
   * @param name  The name of the value.
   * @param v     The int being written.
   */
  writeInt(name: string, v: number): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    if (typeof(v) !== 'number')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a number');

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }

  /**
   * Write a double.
   * @param name  The name of the value.
   * @param v     The int being written.
   */
  writeFloat(name: string, v: number): void {
    if (typeof(v) !== 'number')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a number');

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }

  /**
   * Write an integer.
   * @param name  The name of the value.
   * @param v     The int being written.
   */
  writeNumber(name: string, v: number): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    if (typeof(v) !== 'number')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a number');

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }

  /**
   * Write a datetime.
   * @param name  Name of the value.
   * @param v     The datetime being written.
   */
  writeDateTime(name: string, v: Date): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    try {
      if (!(v instanceof Date))
        return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a Date (datetime)');

      Object.defineProperty(this._object, name, {
        value: Util.dateTimeToString(v),
        writable: true,
        enumerable: true
      });
    } catch (e) {
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object: ' + e.message);
    }
  }

  /**
   * Write a date.
   * @param name  Name of the value.
   * @param v     The date being written.
   */
  writeDate(name: string, v: Date): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    try {
      if (!(v instanceof Date))
        return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a Date (date)');

      Object.defineProperty(this._object, name, {
        value: Util.dateToString(v),
        writable: true,
        enumerable: true
      });
    } catch (e) {
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object: ' + e.message);
    }
  }

  /**
   * Write a time.
   * @param name  Name of the value.
   * @param v     The date being written.
   */
  writeTime(name: string, v: Date): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    try {
      if (!(v instanceof Date))
        return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a Date (time)');

      Object.defineProperty(this._object, name, {
        value: Util.timeToString(v),
        writable: true,
        enumerable: true
      });
    } catch (e) {
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object: ' + e.message);
    }
  }

  /**
   * Write a byte array.
   * @param name  The name of the value.
   * @param v     The byte array being written.
   */
  writeByteArray(name: string, v: string): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    if (typeof(v) !== 'string')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a byte array');

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }

  /**
   * Write a guid.
   * @param name  The name of the value.
   * @param v     The guid being written.
   */
  writeGuid(name: string, v: string): void {
    if (v === undefined) {
      Object.defineProperty(this._object, name, {
        value: v,
        writable: true,
        enumerable: true
      });
      return;
    }

    if (typeof(v) !== 'string')
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, type is not a string/guid');

    if (v.length !== 0 && v.length !== 36)
      return this._prom.raise('mettle.ObjWriter: Cannot write [' + name + '] to object, guid has invalid length [' + v.length + '], expected [36]');

    Object.defineProperty(this._object, name, {
      value: v,
      writable: true,
      enumerable: true
    });
  }
}
