/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { MettlePromise } from '@bitsmiths/mettle-lib';
import { Reader }        from './Reader';
import { Util }          from './Util';

/**
 * Object reader object
 */
export class ObjReader implements Reader {

  /**
   * Constructor
   * @param object The object to read from.
   */
   constructor(public _prom: MettlePromise, public _object: Object) { }

  /**
   * Clears the reader and stream.
   */
  clear() {
    for (const key in this._object) {
      if (this._object.hasOwnProperty(key)) {
        delete this._object[key];
      }
    }
  }

  /**
   * Start reading a new object.
   * @param name The name of the object being read.
   */
  readStart(name: string) {
  }

  /**
   * Start reading a new list object.
   * @param name  The name of the object being written.
   * @returns     The length of the list.
   */
  readStartList(name: string): number {
    this._exists(name);

    if (this._prom.broken())
      return 0;

    if (this._object[name] instanceof Array)
      return this._object[name].length;

    this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not an Array');
    return 0;
  }

  /**
   * End reading an object.
   * @param name The name of the object.
   */
  readEnd(name: string) {
  }

  /**
   * Read a boolean
   * @param name  The name of the value.
   * @returns     The boolen being read.
   */
  readBool(name: string): boolean {
    this._exists(name);

    if (this._prom.broken())
      return false;

    const res = this._object[name];

    if (typeof(res) !== 'boolean') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not a bool');
      return false;
    }

    return res;
  }

  /**
   * Read a string.
   * @param name  The name of the value.
   * @returns     The string being read.
   */
  readString(name: string): string {
    this._exists(name);

    if (this._prom.broken())
      return '';

    const res = this._object[name];

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not a string');
      return '';
    }

    return res;
  }

  /**
   * Read a char.
   * @param name  The name of the value.
   * @returns     The char being read.
   */
  readChar(name: string): string {
    this._exists(name);

    if (this._prom.broken())
      return '';

    const res = this._object[name];

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object type is not a char/string');
      return '';
    }

    if (res.length !== 1) {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object type is not a char - invalid length: ' + res.length);
      return '';
    }

    return res;
  }

  /**   * Read an integer.
   * @param name  The name of the value.
   * @returns     The number being read.
   */
  readInt(name: string): number {
    this._exists(name);

    if (this._prom.broken())
      return 0;

    const res = this._object[name];

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'number') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not a number');
      return 0;
    }

    return res;
  }

  /**   * Read a float.
   * @param name  The name of the value.
   * @returns     The number being read.
   */
  readFloat(name: string): number {
    this._exists(name);

    if (this._prom.broken())
      return 0.0;

    const res = this._object[name];

    if (typeof(res) !== 'number') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not a number');
      return 0.0;
    }

    return res;
  }

  /** Read a ageneric js/ts number.
   * @param name  The name of the value.
   * @returns     The number being read.
   */
  readNumber(name: string): number {
    this._exists(name);

    if (this._prom.broken())
      return 0.0;

    const res = this._object[name];

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'number') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not a number');
      return 0.0;
    }

    return res;
  }

  /**
   * Read a datetime.
   * @param name  Name of the value.
   * @returns     The datetime being read.
   */
  readDateTime(name: string): Date {
    try {
      this._exists(name);

      if (this._prom.broken())
        return undefined;

      const res = this._object[name];

      if (!res) {
        return undefined;
      }

      return Util.stringToDateTime(res);
    } catch (e) {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object: ' + e.message);
      return undefined;
    }
  }

  /**
   * Read a date.
   * @param name  Name of the value.
   * @returns     The date being read.
   */
  readDate(name: string): Date {
    try {
      this._exists(name);

      if (this._prom.broken())
        return undefined;

      const res = this._object[name];

      if (!res) {
        return undefined;
      }

      return Util.stringToDate(res);
    } catch (e) {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object: ' + e.message);
      return undefined;
    }
  }

  /**
   * Read a time.
   * @param  name  Name of the value.
   * @returns      The time being read.
   */
  readTime(name: string): Date {
    try {
      this._exists(name);

      if (this._prom.broken())
        return undefined;

      const res = this._object[name];

      if (!res) {
        return undefined;
      }

      return Util.stringToTime(res);
    } catch (e) {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object: ' + e.message);
      return undefined;
    }
  }

  /**
   * Read a byte array.
   * @param name  The name of the value.
   * @returns     Tyte array.
   */
  readByteArray(name: string): string {
    this._exists(name);

    if (this._prom.broken())
      return '';

    const res = this._object[name];

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not a byte array (string)');
      return '';
    }

    return res;
  }

  /**
   * Read a guid.
   * @param name  The name of the value.
   * @returns     The guid being read.
   */
  readGuid(name: string): string {
    this._exists(name);

    if (this._prom.broken())
      return '';

    const res = this._object[name];

    if (!res) {
      return undefined;
    }

    if (typeof(res) !== 'string') {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, type is not a string');
      return '';
    }

    if (res.length !== 0 && res.length !== 36) {
      this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, guid length [' + res.length + '] is not valid, expected [36]');
      return '';
    }

    return res;
  }

  /**
   * Check that the property exists on the object
   * @param name The name of the value.
   */
  _exists(name: string): void {
    if (this._object.hasOwnProperty(name))
      return;

    this._prom.raise('mettle.ObjReader: Cannot read [' + name + '] from object, property does not exist');
  }
}
