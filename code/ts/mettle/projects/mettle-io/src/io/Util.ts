/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import * as moment_ from 'moment';

const moment = moment_;

/**
 * Static Util class
 */
export abstract class Util {

  /**
   * convert a datetime to a mettle string.
   * @param v   The datetime being written.
   * @returns   The datetime as a string.
   */
  static dateTimeToString(v: Date): string {

    if (!v) {
      return undefined;
    }

    if (typeof(v) !== 'object' || !(v instanceof Date)) {
      throw new Error('mettle.Util: Cannot convert to datetime to string, type is not a Date()');
    }

    const p4 = '0000';
    const p2 = '00';
    let s  = Util._padLeft(v.getFullYear(),  p4) +
             '-' +
             Util._padLeft(v.getMonth() + 1, p2) +
             '-' +
             Util._padLeft(v.getDate(),      p2) +
             'T' +
             Util._padLeft(v.getHours(),     p2) +
             ':' +
             Util._padLeft(v.getMinutes(),   p2) +
             ':' +
             Util._padLeft(v.getSeconds(),   p2);

    let tzAbs = v.getTimezoneOffset();
    let tzNeg = false;

    if (tzAbs < 0)
      tzNeg = true;

    tzAbs = Math.abs(tzAbs);

    const tzhr  = Math.floor(tzAbs / 60);
    const tzmin = tzAbs % 60;

    // this seems counter intuitive by js dates return the offset to get to UTC not from UTC to the current.
    if (tzNeg === true) {
      s += '+';
    }
    else {
      s += '-';
    }

    s += Util._padLeft(tzhr,  p2);
    s += Util._padLeft(tzmin, p2);

    return s;
  }

  /**
   * convert a date to a mettle string.
   * @param v   The date being written.
   * @returns   The date as a string.
   */
  static dateToString(v: Date): string {

    if (!v) {
      return undefined;
    }

    if (typeof(v) !== 'object' || !(v instanceof Date)) {
      throw new Error('mettle.Util: Cannot convert to date to string, type is not a Date()');
    }

    const p4 = '0000';
    const p2 = '00';

    return Util._padLeft(v.getFullYear(),  p4) +
           '-' +
           Util._padLeft(v.getMonth() + 1, p2) +
           '-' +
           Util._padLeft(v.getDate(),      p2);
  }

  /**
   * Convert a time to a mettle string
   * @param v   The time being written.
   * @returns   The time as a string.
   */
  static timeToString(v: Date): string {

    if (!v) {
      return undefined;
    }

    if (typeof(v) !== 'object' || !(v instanceof Date)) {
      throw new Error('mettle.Util: Cannot convert to time string, type is not a Date()');
    }

    const p2 = '00';

    return Util._padLeft(v.getHours(),     p2) +
           ':' +
           Util._padLeft(v.getMinutes(),   p2) +
           ':' +
           Util._padLeft(v.getSeconds(),   p2);
  }

  /**
   * Convert a mettle string to a Date.
   * @param v string  : datetime to convert.
   * @return  Date    : the datetime being read.
   */
  static stringToDateTime(v: string): Date {
    if (v === undefined) {
      return undefined;
    }

    if (v.length !== 24) {
      throw new Error('mettle.Util: Cannot convert to Date, datetime string is invalid length [' + v.length + ']');
    }

    if (v === '0001-01-01T00:00:00+0000') {
      return undefined;
    }

    const dt = moment(v).toDate();

    if (!dt) {
      throw new Error('mettle.Util: Cannot convert to Date, invalid datetime');
    }

    return dt;
  }

  /**
   * Convert a mettle date string to a Date
   * @param v  Date to convert.
   * @returns  The date being read.
   */
  static stringToDate(v: string): Date {
    if (v === undefined) {
      return undefined;
    }

    if (v.length !== 10) {
      throw new Error('mettle.Util: Cannot convert to Date, date string is invalid length [' + v.length + ']');
    }

    if (v === '0001-01-01') {
      return undefined;
    }

    const yyyy = Number(v.slice( 0,  4));
    const mm   = Number(v.slice( 5,  7));
    const dd   = Number(v.slice( 8,  10));
    const dt   = new Date(yyyy, mm - 1, dd);

    if (!dt) {
      throw new Error('mettle.Util: Cannot convert to Date, invalid date');
    }

    return dt;
  }

  /**
   * Convert a mettle time string to a Date.
   * @param v  Time to convert to Date.
   * @returns  The time being read.
   */
  static stringToTime(v: string): Date {
    if (v === undefined) {
      return undefined;
    }

    if (v.length !== 8) {
      throw new Error('mettle.Util: Cannot convert to Date, time string is invalid length [' + v.length + ']');
    }

    const hh   = Number(v.slice( 0, 2));
    const mmm  = Number(v.slice( 3, 5));
    const ss   = Number(v.slice( 6, 8));
    const dt   = new Date(0, 0, 1, hh, mmm, ss);

    if (!dt) {
      throw new Error('mettle.Util: Cannot convert to Date, invalid time');
    }

    return dt;
  }

  /**
   * Pads to the left of a number and returns the string.
   * @param num  The number to pad.
   * @param pad  The padding string to use.
   * @returns    The padded result.
   */
  static _padLeft(num: number, pad: string): string {
    const str = num.toString();

    return pad.substring(0, pad.length - str.length) + str;
  }
}
