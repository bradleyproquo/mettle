/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { MettlePromise } from '@bitsmiths/mettle-lib';

import { ISerializable } from './ISerializable';
import { ObjReader }     from './ObjReader';
import { ObjWriter }     from './ObjWriter';

/**
 * Static Util class
 */
export abstract class JSONUtil {

  /**
   * Converts a Mettle serializable object into a json string (for embedding in a database)
   * @param obj  The object that is going to be embedded.
   * @returns    The JSON string of the object.
   */
  static convertToJSON(obj: ISerializable): string {

    const prom = new MettlePromise((data, httpCode) => {
    }, (exc) => {
      throw new Error(exc.getMsg());
    }, (exc) => {
      throw new Error(exc.getMsg());
    });

    const outRec = {};

    const writer = new ObjWriter(prom, outRec);

    obj._serialize(writer);

    prom.keep(outRec);

    return JSON.stringify(outRec, function(k, v) { return v === undefined ? null : v; });
  }

  /**
   * Converts a JSON string to a Mettle serializable object (data embedded in a database)
   * @param data The JSON object that is going to be read.
   * @param obj  The Mettle serializable object that is going to be filled.
   */
  static convertToObj(data: string, obj: ISerializable): void {

    const prom = new MettlePromise((inData, httpCode) => {
    }, (exc) => {
      throw new Error(exc.getMsg());
    }, (exc) => {
      throw new Error(exc.getMsg());
    });

    const jo = JSON.parse(data);

    const reader = new ObjReader(prom, jo);

    obj._deserialize(reader);

    prom.keep(obj);
  }
}
