/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { MettlePromise } from '@bitsmiths/mettle-lib'

/**
 * Standard reader interface for serializable mettle objects
 */
export interface Reader {

  _prom: MettlePromise;

  /**
   * Clears the reader and stream.
   */
  clear();

  /**
   * Start reading a new object.
   * @param name  The name of the object being read.
   */
  readStart(name: string);

  /**
   * Start reading a new list object.
   * @param name The name of the object being written.
   * @returns    The length of the list.
   */
  readStartList(name: string): number;

  /**
   * End reading an object.
   * @param name The name of the object.
   */
  readEnd(name: string);

  /**
   * Read a boolean
   * @param name The name of the value.
   * @returns    The boolen being read.
   */
  readBool(name: string): boolean;

  /**
   * Read a string.
   * @param name The name of the value.
   * @returns    The string being read.
   */
  readString(name: string): string;

  /**
   * Read a char.
   * @param name The name of the value.
   * @returns    The char being read.
   */
  readChar(name: string): string;

  /**   * Read an integer.
   * @param name The name of the value.
   * @returns    The number being read.
   */
  readInt(name: string): number;

  /**   * Read a float.
   * @param name The name of the value.
   * @returns    The number being read.
   */
  readFloat(name: string): number;

  /** Read a ageneric js/ts number.
   * @param name The name of the value.
   * @returns    The number being read.
   */
  readNumber(name: string): number;

  /**
   * Read a datetime.
   * @param name The name of the value.
   * @returns    The datetime being read.
   */
  readDateTime(name: string): Date;

  /**
   * Read a date.
   * @param name The name of the value.
   * @returns    The date being read.
   */
  readDate(name: string): Date;

  /**
   * Read a time.
   * @param name The name of the value.
   * @returns    The time being read.
   */
  readTime(name: string): Date;

  /**
   * Read a byte array.
   * @param name The name of the value.
   * @returns    Tyte array.
   */
  readByteArray(name: string): string;

  /**
   * Read a guid.
   * @param name The name of the value.
   * @returns    The guid being read.
   */
  readGuid(name: string): string;
}
