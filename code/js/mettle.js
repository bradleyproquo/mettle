/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

var mettle = {

  lib : {

    /**
      * Util function that pads a number with zeros.
      * @param num      number : The number to pad.
      * @param numZeros number : How many zeros to pad with.
      * @return         string : The padded number as a string.
      */
    zeroPad : function(num, numZeros) {
      var n          = Math.abs(num);
      var zeros      = Math.max(0, numZeros - Math.floor(n).toString().length);
      var zeroString = Math.pow(10, zeros).toString().substr(1);

      if (num < 0)
        zeroString = '-' + zeroString;

      return zeroString + n;
    },

    /**
     * Ref class, constructor takes the object to be referened.
     * @param obj object : the object to reference.
     */
    Ref : function (obj) {
      this.ref = obj;

      /**
       * Get the object in the reference.
       * @return object: the ref object.
       */
      this.get = function() {
        return this.ref;
      }

      /**
       * Sets a new object to be referenced.
       * @param obj object : the object.
       */
      this.set = function(obj) {
        return this.ref = obj;
      }
    },

    xMettleECode : {
      NoError    : 0,

      UnknownException   : -101,
      StandardException  : -102,
      TerminalException  : -103,
      OutOfMemory        : -104,
      SocketTimeout      : -105,
      InternalException  : -106,
      DavException       : -107,
      CredExpired        : -108,
      CredInvalid        : -109,
      CredDenied         : -110,
      TokenExpired       : -111,
      TokenInvalid       : -112,
      STANDARD_MAX_ERROR : -199,

      ComsStandardError : -200,
      ComsTimeOut       : -201,
      ComsInvalidHeader : -202,
      ComsServerError   : -203,
      ComsReceiveError  : -204,
      ComsSignature     : -205,
      COMSLASTERROR     : -299,

      DBStandardError       : -300,
      DBForeignKeyViolation : -301,
      DBPrimaryKeyViolation : -302,
      DBUniqueKeyViolation  : -303,
      DBLockNoWaitFailed    : -304,
      DBBokenConnection     : -305,
      DBLASTERROR           : -399
    },

    /**
     * Exception class.
     * @param errMsg  string : the generated error message.
     * @param errCode int    : the exception code.
     */
    xMettle : function(errMsg, errCode, httpCode) {
      this._msg   = errMsg.toString();
      this._code  = errCode  == null ? mettle.lib.xMettleECode.StandardException : errCode;
      this._hcode = httpCode == null ? 599 : httpCode;

      this.toString = function() {
        return "xMettle [msg:" + this._msg + ", code:" + this._code + "]";
      }

      /**
       * Get the error code.
       * @return int : the exception enum/xMettleECode.
       */
      this.getErrorCode = function() {
        return this._code;
      }

      /**
       * Get the error message.
       * @return string : the exception message.
       */
      this.getMsg = function() {
        return this._msg;
      }

      /**
       * Get the http response code.
       * @return int : the http code.
       */
      this.getHttpCode = function() {
        return this._hcode;
      }
    },

    /**
     * Promise handling class.
     * @param onSucc  function : function to call on success, function should look like, func(succObj, httpCode), succObj will the value.
     * @param onFail  function : function to call on failure, function should look like, func(xmettle), xmettle will the an xMettle instance.
     * @param onExcp  function : function to call on exception, not required, but useful for seperateing intenral and user errors.
     */
    Promise : function(onSucc, onFail, onExcp) {
      this._onSucc = onSucc;
      this._onFail = onFail;
      this._onExcp = onExcp;
      this._stack  = 0;
      this._broken = false;

      /**
       * Check to see if this is a broken promise.
       * @return bool : true if this is a broken promise.
       */
      this.broken = function() {
        return this._broken;
      }

      /**
       * Push the promise to another level of logic.  Prevents it being kept 1 time.
       *  For every push(), there must also be a keep() call.
       * @return Promise : returns itself.
       */
      this.push = function() {
        this._stack++;
        return this;
      }

      /**
       * Keep the promise signalling a success if this is the last keep() call in the stack.
       * @param pobj     object : the object to send to with the promise.
       * @param httpCode int    : optionally also send the http code.
       * @return         object : pobj if the keep simply reduced it stack else, returns null;
       */
      this.keep = function(pobj, httpCode) {
        if (this._broken) {
          xm = new mettle.lib.xMettle("Unhandled broken promise!", mettle.lib.xMettleECode.InternalException);

          if (_onExcp != null)
            this._onExcp(xm);
          else if (_onFail != null)
            this._onFail(xm);

          return null;
        }

        if (this._stack <= 0) {
           this._onSucc(pobj, (httpCode == null ? 200 : httpCode));
           return null;
        }

        this._stack--;
        return pobj;
      }

      /**
       * Break a promise with a msg and http code.  By default raises standard exceptions if type is not specified..
       * @parama err      xMettle/str : a xMettle exception or a message.
       * @param  errCode  int         : optionall pass in the xMettleECode error code.
       * @param  httpCode int         : optionally pass in the http code if err is a string.
       */
      this.break = function(err, errCode, httpCode) {
        this._broken = true;

        var xm;

        if (typeof(err) == "object" && err instanceof xMettle)
          xm = err;
        else
          xm = new mettle.lib.xMettle(err, errCode, httpCode);

        if (xm.getErrorCode() != mettle.lib.xMettleECode.StandardException && this._onExcp != null)
          this._onExcp(xm);
        else
          this._onFail(xm);
      }

      /**
       * Break a promise with a msg and http code.  By default raises internal exception if type is not specified.
       * @parama err      xMettle/str : error message.
       * @param  errCode  int         : optionall pass in the xMettleECode error code.
       * @param  httpCode int         : optionally pass in the http code if err is a string.
       */
      this.raise = function(err, errCode, httpCode) {
        this._broken = true;

        if (typeof(err) == "object" && err instanceof xMettle) {
          if (this._onExcp != null)
            this._onExcp(err);
          else
            this._onFail(err);
        }

        var xm = new mettle.lib.xMettle(err, (errCode == null ? mettle.lib.xMettleECode.InternalException : errCode), httpCode);

        if (this._onExcp != null)
          this._onExcp(xm);
        else
          this._onFail(xm);
      }
    }

  },

  io : {

    /**
     * List Stream class, for using an array as a stream of data.
     * @param prom Promise : the promise to break if anything goes wrong.
     * @param list array   : an optional param to initialize the object with.
     */
    ListStream : function(prom, list) {
      this._prom = prom;
      this._list = list == null ? [] : list;
      this._pos  = 0;

      /**
        * Clear the internal list.
        */
      this.clear = function() {
        this._list.length = 0;
      }

      /**
       * Reads the next object from the list stream.
       * @ereturn object : the next object from the list..
       */
      this.read = function() {
        if (this._pos >= this._list.length)
          return this._prom.raise("mettle.ListStream: Attempted to read stream beyond the boundry!");

        return this._list[this._pos++];
      }

      /**
       * Writes an object to end of the list stream.
       * @paaram obj object : the object to be written.
       * @return     object : the object that was written
       */
      this.write = function(obj) {
        this._list.push(obj);

        return obj;
      }

      /**
       * Gets the length/size of the internal list.
       * @return int : the length of the list.
       */
      this.size = function() {
        return this._list.length;
      }

      /**
       * Moves the internal stream position to the start of the stream.
       */
      this.positionStart = function() {
        this._pos = 0;
      }

      /**
       * Moves the internal stream position to the end of the stream.
       */
      this.positionEnd = function() {
        this._pos = this.length;
      }

      /**
       * Moves the internal stream position forward in indexes.
       * @param offset int: the number of indexes to move forward from the current position.
       */
      this.positionMove = function(offset) {
        this._pos += offset;
      }

      /**
       * Gets the current position of the stream.
       * @returns int: the current position of the stream.
       */
      this.position = function() {
        return this._pos;
      }
    },

    /**
     * Standard writer object
     * @param prom   Promise : the promise to break if anything goes wrong.
     * @param stream Stream  : the stream object to write to.
     */
    Writer : function(prom, stream) {
      this._prom   = prom;
      this._stream = stream;

      /**
       * Clears the writer and stream.
       */
      this.clear = function() {
        this._stream.clear();
      }

      /**
       * Start writing a new object.
       * @param name string : name of the object being written.
       */
      this.writeStart = function(name) {
      }

      /**
       * Start writing a new list object.
       * @param name     string : name of the object being written.
       * @param listSize int    : the length of the list.
       */
      this.writeStartList = function(name, listSize) {
        this.writeInt(name, listSize);
      }

      /**
       * End writing an object.
       * @param name string : name of the object.
       */
      this.writeEnd = function(name) {
      }

      /**
       * Write a boolean
       * @param name string : name of the value.
       * @param v    bool   : the boolen being written.
       */
      this.writeBool = function(name, v) {
        if (typeof(v) != "boolean")
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, type is not a bool");

        this._stream.write(v);
      }

      /**
       * Write a guid.
       * @param name string : name of the value.
       * @param v    string : the guid being written.
       */
      this.writeGuid = function(name, v) {
        if (typeof(v) != "string")
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, type is not a guid/string");

        if (res.length() != 0 && res.length() != 36)
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, length [ " + res.length() + "] is not valid for a guid, expected [36].");

        this._stream.write(v);
      }

      /**
       * Write a string.
       * @param name string : name of the value.
       * @param v    string : the string being written.
       */
      this.writeString = function(name, v) {
        if (typeof(v) != "string")
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, type is not a string");

        this._stream.write(v);
      }

      /**
       * Write a char.
       * @param name string : name of the value.
       * @param v    string : the char being written.
       */
      this.writeChar = function(name, v) {
        if (typeof(v) != "string")
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, type is not a string");

        if (v.length != 1)
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, char has invalid length:" + v.length);

        this._stream.write(v);
      }

      /**
       * Write an integer.
       * @param name string : name of the value.
       * @param v    int     : the int being written.
       */
      this.writeInt = function(name, v) {
        if (typeof(v) != "number")
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, type is not a number");

        this._stream.write(v);
      }

      /**
       * Write a float.
       * @param name string : name of the value.
       * @param v    float  : the float being written.
       */
      this.writeFloat = function(name, v) {
        if (typeof(v) != "number")
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, type is not a number");

        this._stream.write(v);
      }

      /**
       * Write a datetime.
       * @param name string   : name of the value.
       * @param v    datetime : the datetime being written.
       */
      this.writeDateTime = function(name, v) {
        if (typeof(v) != "object" || !(v instanceof Date))
          return this._prom.raise("mettle.Reader: Cannot write [" + name + "] to stream, type is not a date");

        var s = mettle.lib.zeroPad(v.getFullYear(),  4) +
                mettle.lib.zeroPad(v.getMonth() + 1, 2) +
                mettle.lib.zeroPad(v.getDate(),      2) +
                mettle.lib.zeroPad(v.getHours(),     2) +
                mettle.lib.zeroPad(v.getMinutes(),   2) +
                mettle.lib.zeroPad(v.getSeconds(),   2);

        var tzhr  = Math.floor(v.getTimezoneOffset()/60);
        var tzmin = v.getTimezoneOffset() % 60;

        if (tzhr >= 0)
          s += "+";

        s += mettle.lib.zeroPad(tzhr,  2);
        s += mettle.lib.zeroPad(tzmin, 2);

        this._stream.write(s);
      }

      /**
       * Write a date.
       * @param name string   : name of the value.
       * @param v    datetime : the date being written.
       */
      this.writeDate = function(name, v) {
        if (typeof(v) != "object" || !(v instanceof Date))
          return this._prom.raise("mettle.Reader: Cannot write [" + name + "] to stream, type is not a date");

        var s = mettle.lib.zeroPad(v.getFullYear(),  4) +
                mettle.lib.zeroPad(v.getMonth() + 1, 2) +
                mettle.lib.zeroPad(v.getDate(),      2);

        this._stream.write(s);
      }

      /**
       * Write a time.
       * @param name string   : name of the value.
       * @param v    datetime : the time being written.
       */
      this.writeTime = function(name, v) {
        if (typeof(v) != "object" || !(v instanceof Date))
          return this._prom.raise("mettle.Reader: Cannot write [" + name + "] to stream, type is not a date");

        var s = mettle.lib.zeroPad(v.getHours(),     2) +
                mettle.lib.zeroPad(v.getMinutes(),   2) +
                mettle.lib.zeroPad(v.getSeconds(),   2);

        this._stream.write(v.toJSON());
      }

      /**
       * Write a byte array.
       * @param name string : name of the value.
       * @param v    string : the byte array being written.
       */
      this.writeByteArray = function(name, v) {
        if (typeof(v) != "string")
          return this._prom.raise("mettle.Writer: Cannot write [" + name + "] to stream, type is not a byte array");

        this._stream.write(v);
      }
    },

    /**
     * Standard reader object
     * @param prom   Promise : the promise to break if anything goes wrong.
     * @param stream Stream  : the stream object to read from.
     */
    Reader : function(prom, stream) {
      this._prom   = prom;
      this._stream = stream;

      /**
       * Clears the reader and stream.
       */
      this.clear = function() {
        this._stream.clear();
      }

      /**
       * Start reading a new object.
       * @param name string : name of the object being read.
       */
      this.readStart = function(name) {
      }

      /**
       * Start reading a new list object.
       * @param name  string : name of the object being written.
       * @return       int   : the length of the list.
       */
      this.readStartList = function(name) {
        return this.readInt(name);
      }

      /**
       * End reading an object.
       * @param name string : name of the object.
       */
      this.readEnd = function(name) {
      }

      /**
       * Read a boolean
       * @param name string : name of the value.
       * @return     bool   : the boolen being read.
       */
      this.readBool = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "boolean")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a bool");

        return res;
      }

      /**
       * Read a guid.
       * @param name string : name of the value.
       * @return     string : the guid being read.
       */
      this.readGuid = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "string")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a guid");

        if (res.length() != 0 and res.length() != 36)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream, length [ " + res.length() + "] is not valid for a guid, expected [36].");

        return res;
      }

      /**
       * Read a string.
       * @param name string : name of the value.
       * @return     string : the string being read.
       */
      this.readString = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "string")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a string");

        return res;
      }

      /**
       * Read a char.
       * @param name string : name of the value.
       * @return     string : the char being read.
       */
      this.readChar = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "string")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a char/string");

        if (res.length != 1)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a char - invalid length: " + res.length);

        return res;
      }

      /**
       * Read an integer.
       * @param name string : name of the value.
       * @return     int    : the int being read.
       */
      this.readInt = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "number")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not an int");

        return res;
      }

      /**
       * Read a float.
       * @param name string : name of the value.
       * @return     float  : the float being read.
       */
      this.readFloat = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "number")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a float");

        return res;
      }

      /**
       * Read a datetime.
       * @param name string   : name of the value.
       * @return     Date     : the datetime being read.
       */
      this.readDateTime = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "string")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a datetime string");

        if (res.length != 19)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type, datetime string is invalid length [" + res.length + "]");

        var yyyy = Number(res.slice( 0,  4));
        var mm   = Number(res.slice( 4,  6));
        var dd   = Number(res.slice( 6,  8));
        var hh   = Number(res.slice( 8, 10));
        var mmm  = Number(res.slice(10, 12));
        var ss   = Number(res.slice(12, 14));
        var tzhr = Number(res.slice(15, 17));
        var tzmn = Number(res.slice(17, 19));
        var tzo  = (tzhr * 60) + tzmn;
        var dt   = new Date(yyyy, mm - 1, dd, hh, mmm, ss);

        if (dt == null || dt == NaN)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a datetime");

        if (res[14] == '-')
          tzo = 0 - tzo;

        var tzlo = dt.getTimezoneOffset();

        if (tzlo != tzo)
        {
          if (tzo > tzlo)
            tzo = tzo - tzlo;
          else
            tzo = tzlo - tzo;

          dt = new Date(dt.getTime() + (tzo * 60));

          if (dt == null || dt == NaN)
            return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a datetime, timezone error!");
        }

        return dt;
      }

      /**
       * Read a date.
       * @param name string   : name of the value.
       * @return     Date     : the date being read.
       */
      this.readDate = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "string")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a date string");

        if (res.length != 8)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type, date string is invalid length [" + res.length + "]");

        var yyyy = Number(res.slice( 0,  4));
        var mm   = Number(res.slice( 4,  6));
        var dd   = Number(res.slice( 6,  8));
        var dt   = new Date(yyyy, mm - 1, dd, 0, 0, 0);

        if (dt == null || dt == NaN)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a date");

        return dt;
      }

      /**
       * Read a time.
       * @param name string   : name of the value.
       * @return     Date     : the time being read.
       */
      this.readTime = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "string")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a time string");

        if (res.length != 6)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type, time string is invalid length [" + res.length + "]");

        var hh   = Number(res.slice( 8, 10));
        var mmm  = Number(res.slice(10, 12));
        var ss   = Number(res.slice(12, 14));
        var dt   = new Date(0, 0, 1, hh, mmm, ss);

        if (dt == null || dt == NaN)
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a time");

        return dt;
      }

      /**
       * Read a byte array.
       * @param name string : name of the value.
       * @return     string : byte array.
       */
      this.readByteArray = function(name) {
        res = this._stream.read(name)

        if (this._prom.broken())
          return;

        if (typeof(res) != "string")
          return this._prom.raise("mettle.Reader: Cannot read [" + name + "] from stream type is not a byte array (string)");

        return res;
      }
    }
  },

  braze : {

    BrazeType : {
      Bool      : "Bool",
      Char      : "Char",
      String    : "String",
      Date      : "Date",
      DateTime  : "DateTime",
      Time      : "Time",
      Int8      : "Int8",
      Int16     : "Int16",
      Int32     : "Int32",
      Int64     : "Int64",
      Uint8     : "Uint8",
      Uint16    : "Uint16",
      Uint32    : "Uint32",
      Uint64    : "Uint64",
      Double    : "Double",
      Memblock  : "Memblock",
      UUID      : "UUID"
    },

    /**
     * Standard braze list / array class.
     */
    List : function(listType) {
      this._listType = listType;

      /**
       * Clear the list.
       */
      this.clear = function() {
        this.length = 0;
      }

      /**
       * Get the default list name.
       * @return string : the defualt list name.
       */
      this._name = function() {
        if (typeof(this._listType) == "function")
          return this._listType.name + ".List";

        return this._listType  + "List";
      }

      /**
       * Serialize the list with a writer.
       * @param _w     mettle.lib.Writer  : the writer object to use.
       * @param _oname string             : optionally pass in a diff name for the object.
       */
      this._serialize = function(_w, _oname) {
        if (_oname == null)
          _oname = this._name();

        _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

        var _idx = 0;

        for (; _idx < this.length; ++_idx) {
          if (typeof(this._listType) == "function") {
            this[_idx]._serialize(_w, _oname);
          }
          else if (this._listType.startsWith("String")) {
            _w.writeString(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("Char")) {
            _w.writeChar(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("Int") || this._listType.startsWith("Uint")) {
            _w.writeInt(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("Bool")) {
            _w.writeBool(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("Double")) {
            _w.writeFloat(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("DateTime")) {
            _w.writeDateTime(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("Date")) {
            _w.writeDate(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("Time")) {
            _w.writeTime(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("UUID")) {
            _w.writeGuid(this._listType, this[idx]);
          }
          else if (this._listType.startsWith("Memblock")) {
            _w.writeByteArray(this._listType, this[idx]);
          }

          if (_w._prom.broken())
            return;
        }

        _w.writeEnd(_oname); if (_w._prom.broken()) return;
      }

      /**
       * Derialize the list with a reader.
       * @param _w     mettle.lib.Writer  : the writer object to use.
       * @param _oname string             : optionally pass in a diff name for the object.
       */
      this._deserialize = function(_r, _oname) {
        if (_oname == null)
          _oname = this._name();

        var _idx     = 0;
        var _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

        this.length = _listLen;

        for (; _idx < this.length; ++_idx) {
          if (typeof(this._listType) == "function") {
            obj = new this._listType();
            obj._deserialize(_r, _oname)
            this[_idx] = obj;
          }
          else if (this._listType.startsWith("String")) {
            this[_idx] = _r.readString(this._listType);
          }
          else if (this._listType.startsWith("Char")) {
            this[_idx] = _r.readChar(this._listType);
          }
          else if (this._listType.startsWith("Int") || this._listType.startsWith("Uint")) {
            this[_idx] = _r.readInt(this._listType);
          }
          else if (this._listType.startsWith("Bool")) {
            this[_idx] = _r.readBool(this._listType);
          }
          else if (this._listType.startsWith("Double")) {
            this[_idx] = _r.readFloat(this._listType);
          }
          else if (this._listType.startsWith("DateTime")) {
            this[_idx] = _r.readDateTime(this._listType);
          }
          else if (this._listType.startsWith("Date")) {
            this[_idx] = _r.readDate(this._listType);
          }
          else if (this._listType.startsWith("Time")) {
            this[_idx] = _r.readTime(this._listType);
          }
          else if (this._listType.startsWith("UUID")) {
            this[_idx] = _r.readGuid(this._listType);
          }
          else if (this._listType.startsWith("Memblock")) {
            this[_idx] = _r.readByteArray(this._listType);
          }

          if (_r._prom.broken())
            return;
        }

        _r.readEnd(_oname); if (_r._prom.broken()) return;
      }
    },

    /**
     * The braze client object that needs to be overloaded depending on what
     * framework is being used.
     */
    Client : function() {

      /**
       * Get the writer object to be used for the client.
       * @param prom      mettle.lib.Promise   : the promise to break on failure.
       * @param instream  mettle.io.ListStream : the input stream to send.
       */
      this.newWriter = function(prom, instream) {
        return new mettle.io.Writer(prom, instream);
      }

      /**
       * Get the reader object to be used for the client.
       * @param prom      mettle.lib.Promise   : the promise to break on failure.
       * @param outstream mettle.io.ListStream : the output stream to receive.
       */
      this.newReader = function(prom, outstream) {
        return new mettle.io.Reader(prom, outstream);
      }

      /**
       * Sends the input stream to the server.
       * @param prom      mettle.lib.Promise   : the promise to break on failure.
       * @param marsh     Braze Client Marsh   : the generated braze client marshaler.
       * @param sig       string               : the call signature.
       * @param instream  mettle.io.ListStream : the input stream to send.
       */
      this.send = function(prom, marsh, sig, instream) {
        // Virtual method.
      }

      /**
       * Receives the output stream from the server.
       * @param prom      mettle.lib.Promise   : the promise to break on failure.
       * @param marsh     Braze Client Marsh   : the generated braze client marshaler.
       * @param sig       string               : the call signature.
       * @return          mettle.io.ListStream : the output stream if there is output.
       */
      this.receive = function(prom, marsh, sig) {
        // virtual method.
        return null;
      }
    },

    /**
     * The braze client object that implements comms using fetch (may require polyfills on older browsers)
     *
     * @param url         string   : endpoint to hit.
     * @param marsh       string   : cors mode (no-cors, cors, same-origin) (usually no-cors)
     * @param credentials string   : credential cookie mode (omit, same-origin, include) (usually include)
     */
    FetchClient : function(url, corsMode, credentials) {

      mettle.braze.Client.call(this);

      this._url         = url;
      this._mode        = corsMode;
      this._credentials = credentials;

      /**
       * Sends the input stream to the server.
       * @param prom      mettle.lib.Promise   : the promise to break on failure.
       * @param marsh     Braze Client Marsh   : the generated braze client marshaler.
       * @param sig       string               : the call signature.
       * @param instream  mettle.io.ListStream : the input stream to send.
       * @param receive   function             : the function to call with the data received
       */
      this.send = function(prom, marsh, sig, instream, receive) {
        instream._list.unshift(0);
        instream._list.unshift(parseInt('0x08779915', 16));
        instream._list.unshift(marsh._signature());
        instream._list.unshift(sig);

        fetch(this._url, {
          method: 'POST',
          body: JSON.stringify(instream._list),
          credentials: this._credentials,
          mode: this._mode,
        }).then(function (response) {
          if(response.ok) {
            return response.json();
          }

          throw new Error('Network response was not ok.');
        }).then (function (jsonData) {
          var stream = new mettle.io.ListStream(prom, jsonData);

          if (stream.read() != sig)
            prom.break("Reponse client signature mismatch!");
          else if (stream.read() != marsh._signature())
            prom.break("Reponse server signature mismatch!");
          else if (stream.read() != parseInt('0x08779915', 16))
            prom.break("Transport signature mismatch!");

          if (prom.broken())
            return;

          var errCode = stream.read();

          if (errCode != 0) {
            prom.break(stream.read());
            return;
          }

          receive(marsh, prom, stream);
        }).catch (function (error) {
          prom.break(error.message);
        });
      }
    }
  }
};

mettle.braze.List.prototype = new Array();
