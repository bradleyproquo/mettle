#!/bin/bash

#******************************************************************************#
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
#******************************************************************************#
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
#******************************************************************************#

export LOCAL_PATH=`pwd`
export LOCAL_HOST=`hostname`
export LOCAL_USER=`whoami`

INIT_ENV_FORCE_REFRESH=0

INIT_ENV_VER_PYTHON="3.7"

# -- Environment Variables
export METTLE_VER_CODENAME=N/A
export METTLE_VER_MAJ=0
export METTLE_VER_MED=0
export METTLE_VER_MIN=0
export METTLE_VERSION="${METTLE_VER_MAJ}.${METTLE_VER_MED}.${METTLE_VER_MIN}"


source "$LOCAL_PATH/code/bash/mettle-lib.sh"
source "$LOCAL_PATH/code/bash/mettle-platform.sh"

export PYTHONPATH=$LOCAL_PATH/code/python3\
:$LOCAL_PATH/bin/pyd/gnu


if [ -f .local.secrets ]; then
  source .local.secrets
fi


function checkEnvironment()
{
  ! mplat_isLinux &&\
  ! mplat_isMac &&\
    mlib_raise "$0" "$LINENO" "Detected platform is [${MLIB_PLATFORM}], only linux or mac is currently supported"

  local rc
  local ver

  ver=`make --version | head -n 1 2>/dev/null`
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[make] not installed or configured correctly." "$rc"
  fi

  if [[ "$ver" != 'GNU Make'* ]]; then
    mlib_raise "$0" "$LINENO" "Detected make [${ver}], only GNU Make is currently supported."
  fi

  ver=`git --version 2>/dev/null`
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[git] not installed or configured correctly." "$rc"
  fi

  wget --version 2>/dev/null 1>/dev/null
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[wget] not installed or configured correctly." "$rc"
  fi

  g++ --version 2>/dev/null 1>/dev/null
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[g++] not installed or configured correctly." "$rc"
  fi

  ver=$(python${INIT_ENV_VER_PYTHON} --version 2>/dev/null)
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[python${INIT_ENV_VER_PYTHON}] not installed or configured correctly." "$rc"
  fi

  if [[ "$ver" != "Python ${INIT_ENV_VER_PYTHON}."* ]]; then
    mlib_raise "$0" "$LINENO" "Detected Python 3 version [${ver}], only ${INIT_ENV_VER_PYTHON}.X is currently supported."
  fi

  virtualenv --version 2>/dev/null 1>/dev/null
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[virtualenv] not installed or configured correctly." "$rc"
  fi

  npm --version 2>/dev/null 1>/dev/null
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[npm] not installed or configured correctly." "$rc"
  fi

  yarn --version 2>/dev/null 1>/dev/null
  rc=$?

  if [ $rc -ne 0 ]; then
    mlib_raise "$0" "$LINENO" "[yarn] not installed or configured correctly." "$rc"
  fi
}


function initPythonVirtualEnv()
{
  cd "$LOCAL_PATH"

  local installPip=0

  if [ ! -d mettle-venv ]; then
    echo ""
    echo " - Initializing python${INIT_ENV_VER_PYTHON} virtual environment for mettle..."
    echo ""

    mlib_exec "$0" "$LINENO" "python${INIT_ENV_VER_PYTHON} -m venv ./mettle-venv"

    installPip=1
  fi

  source "${LOCAL_PATH}/mettle-venv/bin/activate"

  if [ $installPip -eq 1 ] || [ $INIT_ENV_FORCE_REFRESH -eq 1 ]; then
    mlib_exec "$0" "$LINENO" "pip install wheel"
    mlib_exec "$0" "$LINENO" "pip install --upgrade pip"
    mlib_exec "$0" "$LINENO" "pip install -r \"$LOCAL_PATH/code/python3/pip-requirements.txt\""
  fi
}


function initLocalConfigs()
{
  mlib_exec "$0" "$LINENO" "cd \"${LOCAL_PATH}/externs\""

  if [ ! -f "./config.mk" ]; then
    mlib_exec "$0" "$LINENO" "cp ./_template.config.mk ./config.mk"
  fi
}


function initExternals()
{
  mlib_exec "$0" "$LINENO" "cd \"${LOCAL_PATH}/externs/external_srcs\""
  mlib_exec "$0" "$LINENO" "./init-srcs.sh"
  mlib_exec "$0" "$LINENO" "cd \"${LOCAL_PATH}/externs\""
  mlib_exec "$0" "$LINENO" "make -f ./makefile clean 1>/dev/null"
  mlib_exec "$0" "$LINENO" "make -f ./makefile 1>/dev/null"
}


function initVersionInfo()
{
  local vparts=(`cat "${LOCAL_PATH}/build/package-version.txt" | tr "." "\n"`)

  export METTLE_VER_CODENAME=${vparts[0]}
  export METTLE_VER_MAJ=${vparts[1]}
  export METTLE_VER_MED=${vparts[2]}
  export METTLE_VER_MIN=${vparts[3]}
  export METTLE_VERSION="${METTLE_VER_MAJ}.${METTLE_VER_MED}.${METTLE_VER_MIN}"
}


function usageAndExit()
{
  echo ""
  echo " Init Mettle Environment"
  echo ""
  echo "Usage: $0 [ -r -? ]"
  echo "---------------------------------------------------------------------"
  echo " -r    : Force virtual environment refreshes"
  echo " -?    : This help"
  echo ""
  echo ""
  echo " To intialize your environment run >> . ./`basename $0`"
  echo ""

  exit 2
}


function main()
{
  checkEnvironment

  initLocalConfigs

  initExternals

  initVersionInfo

  initPythonVirtualEnv

  mlib_exec "$0" "$LINENO" "pre-commit install"

  alias sql_pg='psql -U mettle'

  echo ""
  echo " - Mettle Envirnoment Initialized : ${METTLE_VERSION} - ${METTLE_VER_CODENAME}"
  echo ""
}


while getopts "r" opt; do
  case $opt in
    r)  INIT_ENV_FORCE_REFRESH=1;;
    \?) usageAndExit;;
    *)  usageAndExit;;
  esac
done

main
