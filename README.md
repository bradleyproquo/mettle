# METTLE #

Mettle is a code generation tool with a library that supports that code generation.  Mettle is primarily coded in python, but
it has alway generated code for multiple languages and databases.

**We currently generated 3 things**

- Database: Table code, DAO (data access objects) code, and table SQL generation
- RPCs (Braze): Server and client code, DTO (data transfer objects)
- Makefiles: C++, mettle, angular, solution makefiles

## What is this all about? ##

Code generation greatly speeds up development for any client, server, database, style application.  This is because you define your
database tables and RPC (remote procedure calls/RESTful API's) in one place. Then the code generator will generate the back-end
(server) python, C++, or Java code (or whatever language you want) and it will also generate all the front-end (client) angular,
CSharp, C++ code.

This means our code is always 100% in sync whenever we make changes to database tables, RPC calls, or RPC structures. In addition
the code generator also generates data quality checks, security checks etc.

This sounds pretty good, but where the magic happens is that the generated code is generated against the mettle libraries which we
have for each supported language. These library are highly extensible and are meant to be overloaded by developers to put in
their own data transport layers, security layers without have to fiddle with the code generation at all.

If that was not cool enough, the code generation and libraries themselves protect your code from the databases and data
transport. If you code project properly, you could change databases and so long as you are not using some obscure custom
feature set you won't have to change a line of your business code. If you change your TCP/IP stack to something else, you just
overload the Transport code and nothing in your business/server code is effected.

Finally, the code generators themselves can be quite easily extended to add any custom stuff that you want to them.

## Currently Supported Languages, Libraries, and Generators ##

We have recently removed support for Java and C#, as nobody was using it and it was just a headache to keep the code up to date
for something we were not using. The old generators are still in the git repo, so we can alway put them back.

**LEGEND**

- ✔️ : Fully supported
- ❌ : Not supported
- ⚠️ : Supported, but very old or not fully tested
- 🛑 : Deprecated, not longer supported
- 🔷 : Not applicable


### Database Driver Support ###

*As you can see, we primarily work with PostgreSQL, though i would like to add support
for more databases.*

| Languages    |  PostreSQL        | SQLite      | Microsoft SQL  | Oracle    |
| ------------ | -------------     | ----------- | -------------- | --------- |
| C++          | ✔️ (Native)       | ✔️ (Native) | ❌             | ❌       |
| Python       | ✔️ (C++ Extended, psycopg2, Sql Alchemy) | ✔️ (Native, C++ Extended) | ❌             | ❌       |
| Python Async | ✔️ (databases.io) | ✔️ (databases.io)   | ❌             | ❌       |
| TypeScript   | ❌                | ❌          | ❌             | ❌       |
| JavaScript   | ❌                | ❌          | ❌             | ❌       |
| Java         | 🛑                | ❌          | ❌             | ❌       |
| C#           | 🛑                | ❌          | ❌             | ❌       |

### Database Generators ###

| Languages         |  PostreSQL | SQLite     | Microsoft SQL | Oracle     |
| ------------------| ---------- | ---------- | ------------- | ---------- |
| SQL Files         | ✔️         | ✔️         | ⚠️            | ⚠️         |
| C++ Tables        | ✔️         | ✔️         | ✔️            | ✔️         |
| C++ DAO           | ✔️         | ✔️         | ⚠️            | ⚠️         |
| Python Tables     | ✔️         | ✔️         | ✔️            | ✔️         |
| Python DAO        | ✔️         | ✔️         | ⚠️            | ⚠️         |
| TypeScript Tables | ✔️         | ✔️         | ✔️            | ✔️         |
| TypeScript DAO    | ❌         | ❌         | ❌            | ❌         |
| JavaScript Tables | ✔️         | ✔️         | ✔️            | ✔️         |
| JavaScript DAO    | ❌         | ❌         | ❌            | ❌         |
| Java Tables       | 🛑         | ❌         | ❌            | ❌         |
| Java DAO          | ❌         | ❌         | ❌            | ❌         |
| C# Tables         | 🛑         | ❌         | ❌            | ❌         |
| C# DAO            | ❌         | ❌         | ❌            | ❌         |

### Data Transport Support ###

| Languages    |  TCP/IP       | RESTful |
| ------------ | ------------- | ----------- |
| C++          | ✔️            | ❌ |
| Python       | ✔️            | ✔️ |
| TypeScript   | ❌            | ✔️ |
| JavaScript   | ❌            | ✔️ |
| Java         | 🛑            | ❌ |
| C#           | 🛑            | ❌ |


### Braze (RPC/RESTful) Generators ###

| Languages   |  Server Code  | Async Server Code | Client Code | DTO Code  |
| ------------| ------------- |-------------------| ----------- | --------- |
| C++         | ✔️            | ❌                |  ✔️          | ✔️        |
| Python      | ✔️            | ✔️                |  ✔️          | ✔️        |
| TypeScript  | ❌            | ❌                |  ✔️          | ✔️        |
| JavaScript  | ❌            | ❌                |  ✔️          | ✔️        |
| Java        | 🛑            | ❌                |  🛑          | 🛑        |
| C#          | 🛑            | ❌                |  🛑          | 🛑        |


### Makefile Generators ###

| Languages   |  Executables  | Libraries   | Shared Objects | Projects  |
| ------------| ------------- | ----------- | -------------- | --------- |
| C++         | ✔️            | ✔️          | ✔️             | 🔷       |
| Python      | ✔️            | ✔️          | 🔷             | 🔷       |
| TypeScript  | 🔷            | ✔️          | ✔️             | 🔷       |
| JavaScript  | 🔷            | ✔️          | ✔️             | 🔷       |
| Java        | 🛑            | 🛑          | 🛑             | 🔷       |
| C#          | 🛑            | 🛑          | 🛑             | 🔷       |
| Mettle Mk   | 🔷            | 🔷          | 🔷             | ✔️       |
| <Solution>  | 🔷            | 🔷          | 🔷             | ✔️       |


## How do I get set up? ##

### Linux/OSX ###

- Python 3.7.X
- Gnu Make
- Gnu C++, (packages: uuid-dev)
- PostreSQL 9/10.x libraries (the current default setup expects you to have them)
- npm
- yarn (note you might have `sudo apt-get remove cmdtest` see: `https://yarnpkg.com/lang/en/docs/install/`)
- wget
- rsync

Commands to install environment:
```console
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

sudo apt remove cmdtest

sudo apt install postgresql-11 pgadmin4 libpq-dev nodejs yarn python3-setuptools uuid-dev yarn

pip3 install virtualenv
```

### Windows ###

Note that windows is no longer offically supported, but here is what you need.

- Python 3.7.X
- Gnu Make
- MinGW-64
- PostreSQL 9/10.x libraries (the current default setup expects you to have them)
- npm
- yarn
- junction
- wget


### How its put together ###

Mettle assumes that the build system have all of the above in your path and in their
default locations. If stuff is not in their default locations you may have
to tweak your env with some symbolic links or tweak Mettle's make files.

The initialize scripts also assume you are running in bash.

Mettle uses a make file generator that will generate makefiles to build itself. Additionally
it also auto downloads external libraries (JSMN, YAML) for the C++ into the `externs` folder.

The `externs` folder is where mettle references it's external dependencies. This
includes postgres, jsmn, yaml, and MingGW for windows back in the day. Generally you
can alter the symbolic links/makefile in this folder to suit your local environment.

## First Time Setup ##

1. After a fresh checkout, you will need to the run the `init-env.sh` bash script that is in the root of the repository.
2. This script will do a cursory check to ensure all the dependencies are installed, pathed, and are at the minimum required version.
3. The script will then go to the `externs/external_srcs` folder and run the local `init-srcs.sh` script which will download and uncompress the C++ JSMN and YAML libraries Mettle requires. Note there is a `init-srcs.yml` file there that shows exactly what the script will attempt to download.
4. The script now creates a copy of the `_template.config.mk` file to `config.mk` in the `externs` folder. In this folder you can change the default paths of your local system for PostgreSQL and Python.
5. Once this is successful, the script then runs make in the `externs` folder which will create symbolic links to the downloaded sources as we as the default locations. Note that you can run `make clean` and `make` again to un-link and re-link these symbolic links manually.
6. Now the script will create a local Python virtual environment in the root of the repo and install pythons dependencies.
7. Mettle should be ready to build when you see the message: ` - Mettle Environment Initialized - `

## Building Mettle ##

1. The first thing to always do, is run `. ./init-env.sh` in the root of the mettle repo. This command sets up the python virtual environment and checks everything is good to go.
2. Please __NOTE__ the `.` in front of the command. Without that nothing works. As a side note if you run this command and your terminal closes, it because something is raising an exception. You will need to run the command without the `.` to see what it is.
3. Anyway, to do a build is really simple, go to the `build` directory and type `make full`
4. That will generate all the make files, compile all the C++, Python, and Angular code.
5. Note, that full builds always cleans, builds release binaries, and packages a release zip.
6. The targets in the make file are `make clean`, `make debug`, `make release`, and `make package`. Please view the makefile for additional information.

### Advance Building ###

There are two ways that you can go and build/rebuild a specific component in Mettle:

#### [1] Solution File ####
1. In the `build` folder run the `gen.py` script by typing `python gen.py`
2. This script generates all the makefiles for mettle. If you inspect the build directory you will see a local folder `_mk_sln` has been created.
3. Navigate to the folder and type `make` and `make list`
4. This will show you all the various targets that you can use to build specific components of the script.
5. If you want, you can simply type `make` here and it will by default build a `release` version of mettle.

#### [2] Running Makefiles ####
1. As above you must first run `gen.py` script to generate all the makefiles.
2. Every module/library/executable in mettle has it's very own make file generated for it. This means you can navigate to the language and module of your choice and build it.

Example - Build Mettle C++ Library:

```console
<from mettle root>
cd code/cpp/mettle/lib/_mk_cpp_lib
make
```

This makes fixing errors/problems in one place much easier than trying to build everything from the main `build/makefile`


## Unittest Mettle ##

This section is a TODO. There is a `testing` folder in the root of mettle. But its a bit complex and convoluted and needs to be refactored.

I need to change it all to `pytest` and come up with a better way of doing the C++, JavaScript and TypeScript tests.

## Publishing to npm... ##

After the full build with updated versions:

```console
<from mettle root>
cd code/ts/mettle/dist/bitsmiths-mettle-lib
npm login
npm publish --access public
cd ../bitsmiths-mettle-db
npm publish --access public
cd ../bitsmiths-mettle-io
npm publish --access public
cd ../bitsmiths-mettle-braze
npm publish --access public
```

## Publishing to PYPI... ##

When you are ready to push a new python mettle version do the following:

- Check the `setup.cfg` file has the correct version set. It should usually be correct if you used the `prep-verion.sh` shell script that is in `~/build` directory.
- We clean old build directories if they exist.
- Do the build, and the twine upload the repository (Note the example below uploads to the test pypi url).

```console
<from mettle root>
cd code/python3
rm -f -r build dist bitsmiths_mettle.egg-info
python3 -m build
twine upload dist/*
<testing...> python3 -m twine upload --repository testpypi dist/*
```
