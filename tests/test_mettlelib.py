# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import copy
import pytest


pytestmark = [ pytest.mark.mettle_libs ]


def test_sql_statement_subst():
    from mettle.db.statement import Statement


    def mettle_repl(idx: int, name: str, var: object = None):
        return f':{name}'


    def psyco_repl(idx: int, name: str, var: object = None):
        return f'%({name})s'


    def misc_repl(idx: int, name: str, var: object = None):
        return '${%s}' % name


    def num_repl(idx: int, name: str, var: object = None):
        return f'$${idx}'


    mettle_qry = 'select * from demo d where d.id = :demo_id and d.name = :demo_name'
    psyco_qry  = 'select * from demo d where d.id = %(demo_id)s and d.name = %(demo_name)s'
    misc_qry   = 'select * from demo d where d.id = ${demo_id} and d.name = ${demo_name}'
    num_qry    = 'select * from demo d where d.id = $$1 and d.name = $$2'

    st = Statement('test')
    st.bind_in('demo_id',   1)
    st.bind_in('demo_name', 'testing-subst')
    st.sql(mettle_qry)

    mettle_st = copy.copy(st)
    psyco_st  = copy.copy(st)
    misc_st   = copy.copy(st)
    num_st    = copy.copy(st)

    assert mettle_qry == mettle_st.sql_subst(mettle_repl)
    assert psyco_qry  == psyco_st.sql_subst(psyco_repl, {'%': '%%'})
    assert misc_qry   == misc_st.sql_subst(misc_repl)
    assert num_qry    == num_st.sql_subst(num_repl)

    comm_qry = """select * from demo /* :test_comment */ d where -- :another_comment
d.id = :demo_id and d.name = :demo_name and 'some:str' == 'some:str' and d.name like 'Foo%'"""

    comm_pg2 = """select * from demo /* :test_comment */ d where -- :another_comment
d.id = %(demo_id)s and d.name = %(demo_name)s and 'some:str' == 'some:str' and d.name like 'Foo%%'"""


    st.sql(comm_qry)

    assert comm_qry == st.sql_subst(mettle_repl)
    assert comm_pg2 == st.sql_subst(psyco_repl, {'%': '%%'})
