# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import asyncio
import copy
import datetime
import pytest
import pytest_asyncio.plugin  # noqa
import uuid

from mettle.lib import xMettle
from mettle.db  import IAConnect

from _test_python3.db import tables

pytestmark = [ pytest.mark.databases_async ]

g_encode_io_database = None


@pytest.fixture(scope='module')
def event_loop():
    """
    A module-scoped event loop.
    """
    return asyncio.new_event_loop()


@pytest.fixture(scope='module')
async def _init_encoded_io_database(db_url, db_driver) -> bool:

    if not db_driver.startswith('encode_io'):
        return False

    from databases     import Database

    global g_encode_io_database

    if not g_encode_io_database:
        g_encode_io_database = Database(db_url, min_size=1, max_size=4)

    if not g_encode_io_database.is_connected:
        await g_encode_io_database.connect()
        assert g_encode_io_database.is_connected

    return True


@pytest.fixture(scope='module')
async def db_conn(db_url, db_target, db_driver, _init_encoded_io_database) -> IAConnect:
    conn = None

    if db_driver.startswith('encode_io'):
        from databases.core import Connection

        global g_encode_io_database

        if db_target == 'postgresql':
            from mettle.db.encode_io_databases.aconnect_postgres import AConnectPostgres

            async with Connection(g_encode_io_database._backend) as en_con:
                conn = AConnectPostgres(en_con)
                yield conn

    assert conn is not None


@pytest.fixture(scope='module')
async def db_conn2(db_url, db_target, db_driver, _init_encoded_io_database) -> IAConnect:
    conn = None

    if db_driver.startswith('encode_io'):
        from databases.core import Connection

        global g_encode_io_database

        if db_target == 'postgresql':
            from mettle.db.encode_io_databases.aconnect_postgres import AConnectPostgres

            async with Connection(g_encode_io_database._backend) as en_con:
                conn = AConnectPostgres(en_con)
                yield conn

    assert conn is not None


@pytest.mark.asyncio
async def test_check_db_is_empty(aiodb_dao, db_conn):
    """
    Ensure all the tables are empty before we start out tests.
    """
    recs = []

    qry1 = aiodb_dao.dAllTypesSelectAll(db_conn)
    qry2 = aiodb_dao.dParentTypeSelectAll(db_conn)
    qry3 = aiodb_dao.dParentSelectAll(db_conn)
    qry4 = aiodb_dao.dChildSelectAll(db_conn)

    assert 0 == await (await qry1.exec()).fetch_all(recs)
    assert 0 == await (await qry2.exec()).fetch_all(recs)
    assert 0 == await (await qry3.exec()).fetch_all(recs)
    assert 0 == await (await qry4.exec()).fetch_all(recs)

    await db_conn.rollback()


@pytest.mark.asyncio
async def test_all_types_crud(aiodb_dao, db_conn):
    """
    Test we can insert, select, update all possible data types.
    """
    atc = aiodb_dao.dAllTypes(db_conn)
    atr = aiodb_dao.dAllTypes(db_conn)
    atu = aiodb_dao.dAllTypes(db_conn)

    await atc.insert_deft(
        8,
        16,
        32,
        64,
        123.45,
        True,
        'This is a string',
        'X',
        b'12345678901010101',
        uuid.uuid4(),
        {"foo": 1, "bar": 2},
        datetime.date.today(),
        datetime.time(23, 59, 59),
        datetime.datetime.now())

    await db_conn.commit()

    await atr.select_one_deft(atc.rec.type_seq64)

    atc.rec.type_datetime = atc.rec.type_datetime.replace(microsecond=0)
    atr.rec.type_datetime = atr.rec.type_datetime.replace(microsecond=0)

    assert atc.rec.type_seq64       == atr.rec.type_seq64
    assert atc.rec.type_int8        == atr.rec.type_int8
    assert atc.rec.type_int16       == atr.rec.type_int16
    assert atc.rec.type_int32       == atr.rec.type_int32
    assert atc.rec.type_int64       == atr.rec.type_int64
    assert atc.rec.type_double      == atr.rec.type_double
    assert atc.rec.type_bool        == atr.rec.type_bool
    assert atc.rec.type_string      == atr.rec.type_string
    assert atc.rec.type_char        == atr.rec.type_char
    assert atc.rec.type_memblock    == atr.rec.type_memblock
    assert atc.rec.type_uuid        == atr.rec.type_uuid
    assert atc.rec.type_date        == atr.rec.type_date
    assert atc.rec.type_time        == atr.rec.type_time
    assert atc.rec.type_datetime    == atr.rec.type_datetime
    assert isinstance(atr.rec.type_json, dict)
    assert atc.rec.type_json['foo'] == atr.rec.type_json.get('foo')
    assert atc.rec.type_json['bar'] == atr.rec.type_json.get('bar')

    await atc.insert_deft(
        -8,
        -16,
        -32,
        -64,
        -123.45,
        False,
        'This is a string',
        'X',
        b'abcdefghijklmonopqrstuvwxyz',
        uuid.uuid4(),
        {"foo": 123.34, "bar": 'BARBAR'},
        datetime.date.today(),
        datetime.time(23, 59, 59),
        datetime.datetime.now())

    await db_conn.commit()

    await atr.select_one_deft(atc.rec.type_seq64)

    atc.rec.type_datetime = atc.rec.type_datetime.replace(microsecond=0)
    atr.rec.type_datetime = atr.rec.type_datetime.replace(microsecond=0)

    assert atc.rec.type_seq64       == atr.rec.type_seq64
    assert atc.rec.type_int8        == atr.rec.type_int8
    assert atc.rec.type_int16       == atr.rec.type_int16
    assert atc.rec.type_int32       == atr.rec.type_int32
    assert atc.rec.type_int64       == atr.rec.type_int64
    assert atc.rec.type_double      == atr.rec.type_double
    assert atc.rec.type_bool        == atr.rec.type_bool
    assert atc.rec.type_string      == atr.rec.type_string
    assert atc.rec.type_char        == atr.rec.type_char
    assert atc.rec.type_memblock    == atr.rec.type_memblock
    assert atc.rec.type_uuid        == atr.rec.type_uuid
    assert atc.rec.type_date        == atr.rec.type_date
    assert atc.rec.type_time        == atr.rec.type_time
    assert atc.rec.type_datetime    == atr.rec.type_datetime
    assert isinstance(atr.rec.type_json, dict)
    assert atc.rec.type_json['foo'] == atr.rec.type_json.get('foo')
    assert atc.rec.type_json['bar'] == atr.rec.type_json.get('bar')

    atu.rec = copy.copy(atr.rec)

    await atu.update_deft(
        atu.rec.type_seq64,
        -18,
        -116,
        -132,
        -164,
        -1123.45,
        True,
        'This is not a string',
        'Y',
        b'9876543210',
        uuid.uuid4(),
        {"foo": 987.65, "bar": 'SHEEP SHEEP'},
        datetime.date.today() + datetime.timedelta(days=-2),
        datetime.time(11, 23, 23),
        datetime.datetime.now() + datetime.timedelta(hours=20))

    await db_conn.commit()

    await atr.select_one_deft(atu.rec.type_seq64)

    atu.rec.type_datetime = atu.rec.type_datetime.replace(microsecond=0)
    atr.rec.type_datetime = atr.rec.type_datetime.replace(microsecond=0)

    assert atu.rec.type_seq64       == atr.rec.type_seq64
    assert atu.rec.type_int8        == atr.rec.type_int8
    assert atu.rec.type_int16       == atr.rec.type_int16
    assert atu.rec.type_int32       == atr.rec.type_int32
    assert atu.rec.type_int64       == atr.rec.type_int64
    assert atu.rec.type_double      == atr.rec.type_double
    assert atu.rec.type_bool        == atr.rec.type_bool
    assert atu.rec.type_string      == atr.rec.type_string
    assert atu.rec.type_char        == atr.rec.type_char
    assert atu.rec.type_memblock    == atr.rec.type_memblock
    assert atu.rec.type_uuid        == atr.rec.type_uuid
    assert atu.rec.type_date        == atr.rec.type_date
    assert atu.rec.type_time        == atr.rec.type_time
    assert atu.rec.type_datetime    == atr.rec.type_datetime
    assert isinstance(atr.rec.type_json, dict)
    assert atu.rec.type_json['foo'] == atr.rec.type_json.get('foo')
    assert atu.rec.type_json['bar'] == atr.rec.type_json.get('bar')

    await atr.delete_one()

    assert not await atr.try_select_one()

    await db_conn.rollback()


@pytest.mark.asyncio
async def test_rollback(aiodb_dao, db_conn):
    """
    Ensure all the tables are empty before we start out tests.
    """
    at = aiodb_dao.dAllTypes(db_conn)

    await at.select_one_deft(1)

    bef               = at.rec.type_int64
    at.rec.type_int64 = 6969

    await at.update()
    await at.select_one_deft(1)
    assert at.rec.type_int64 == 6969

    await db_conn.rollback()

    await at.select_one_deft(1)
    assert at.rec.type_int64 == bef


@pytest.mark.asyncio
async def test_inserts(aiodb_dao, db_conn):
    """
    Test inserting into all tables work.
    """
    pt     = aiodb_dao.dParentType(db_conn)
    pa     = aiodb_dao.dParent(db_conn)
    ch     = aiodb_dao.dChild(db_conn)
    tlist  = []
    usr_id = f'[mettle_ut_{db_conn.name()}'.lower()

    await pt.insert_deft('Single Mother', tables.tParentType.Role_Couplet.key_mother, usr_id)
    assert 1 == pt.rec.id

    await pt.insert_deft('Married Father', tables.tParentType.Role_Couplet.key_father, usr_id)
    assert 2 == pt.rec.id

    await pt.insert_deft('Drunken Aunt', tables.tParentType.Role_Couplet.key_aunt, usr_id)
    assert 3 == pt.rec.id

    await pt.insert_deft('Awesome Uncle', tables.tParentType.Role_Couplet.key_uncle, usr_id)
    assert 4 == pt.rec.id

    await db_conn.commit()

    ptall = aiodb_dao.dParentTypeSelectAll(db_conn)

    assert 4 == await (await(ptall.exec())).fetch_all(tlist)

    await pa.insert_deft(2, 'Chunks', 'Charles Wageslave Worksalot', 987.654, 951, datetime.date(1981, 12, 2),
                         False, uuid.uuid4(), usr_id)
    assert 1 == pa.rec.id

    await pa.insert_deft(1, 'Sue', 'Susan Shagsalotof Chads', 100.12, None, datetime.date(1985, 4, 22), True,
                         uuid.uuid4(), usr_id)
    assert 2 == pa.rec.id

    await pa.insert_deft(4, 'Evilin', 'Everly Drinkstomuch Booze', 12.3456, 654, datetime.date(1990, 2, 28),
                         True, None, usr_id)
    assert 3 == pa.rec.id

    await pa.insert_deft(3, 'Mark', 'Mark Liveslife Well', 32.98, 9000, datetime.date(1980, 5, 1), False, None, usr_id)
    assert 4 == pa.rec.id

    await pa.insert_deft(3, 'Möller', 'Möller UTF8', 222.111, 8000, datetime.date(1970, 6, 6), False, None, usr_id)
    assert 5 == pa.rec.id

    await db_conn.commit()

    pall = aiodb_dao.dParentSelectAll(db_conn)

    assert 5 == await (await pall.exec()).fetch_all(tlist)

    bed_time  = datetime.time(20, 0, 0)
    male      = tables.tChild.Gender_Couplet.key_male
    female    = tables.tChild.Gender_Couplet.key_female

    await ch.insert_deft(1, None, 'Bobby',  'Robert Chads',    male,   datetime.date(2002, 3,  17), bed_time, None, b'')
    await ch.insert_deft(1, None, 'Shela',  'Shelly Chads',    female, datetime.date(2004, 10, 30), bed_time, None, bytearray(b'SHELLY HAS A PIC'))  # noqa
    await ch.insert_deft(1, None, 'Chuck',  'Charles Chads',   male,   datetime.date(2005, 11,  9), bed_time, None, None)
    await ch.insert_deft(1, None, 'Lizzy',  'Elizabeth Chads', female, datetime.date(2007, 1,  14), bed_time, None, b'')

    await ch.insert_deft(2, None, 'Zack',  'Jacob Worksalot',   male,   datetime.date(2003, 5,  15), bed_time,  None, b'JACOB HAS A PIC')  # noqa
    await ch.insert_deft(2, None, 'Bels',  'Belinda Worksalot', female, datetime.date(2001, 7,  8),  None,     None, b'')

    await ch.insert_deft(3, None, 'Ann',   'Anneliese Booze',   female, datetime.date(2006, 6,  6),  bed_time, None, None)

    await ch.insert_deft(4, None, 'Mary',  'Mary Well',      female, datetime.date(2011,  3, 18), None, None, None)
    await ch.insert_deft(4, None, 'Alex',  'Alexander Well', male,   datetime.date(2013,  9,  4), None, None, b'ALEX HAS A PIC')  # noqa
    await ch.insert_deft(4, None, 'Vicky', 'Victoria Well',  female, datetime.date(2014, 12, 20), None, None, b'')

    await db_conn.commit()


@pytest.mark.asyncio
async def test_selects(aiodb_dao, db_conn):
    """
    Test selecting the records brings back the correct data.
    """
    pt     = aiodb_dao.dParentType(db_conn)
    pa     = aiodb_dao.dParent(db_conn)
    usr_id = f'[mettle_ut_{db_conn.name()}'.lower()

    await pt.select_one_deft(1)

    assert pt.rec.id         == 1
    assert pt.rec.descr      == 'Single Mother'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_mother
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    await pt.select_one_deft(2)

    assert pt.rec.id         == 2
    assert pt.rec.descr      == 'Married Father'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_father
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    await pt.select_one_deft(3)

    assert pt.rec.id         == 3
    assert pt.rec.descr      == 'Drunken Aunt'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_aunt
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    await pt.select_one_deft(4)

    assert pt.rec.id         == 4
    assert pt.rec.descr      == 'Awesome Uncle'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_uncle
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    await pa.select_one_deft(1)

    assert pa.rec.id             == 1
    assert pa.rec.parent_type_id == 2
    assert pa.rec.short_name     == 'Chunks'
    assert pa.rec.full_name      == 'Charles Wageslave Worksalot'
    assert pa.rec.amount         == 987.654
    assert pa.rec.code           == 951
    assert pa.rec.do_b           == datetime.date(1981, 12, 2)
    assert pa.rec.mother         is False
    assert pa.rec.reg_id is not None
    assert isinstance(pa.rec.reg_id, uuid.UUID)
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    await pa.select_one_deft(4)

    assert pa.rec.id             == 4
    assert pa.rec.parent_type_id == 3
    assert pa.rec.short_name     == 'Mark'
    assert pa.rec.full_name      == 'Mark Liveslife Well'
    assert pa.rec.amount         == 32.98
    assert pa.rec.code           == 9000
    assert pa.rec.do_b           == datetime.date(1980, 5, 1)
    assert pa.rec.mother         is False
    assert pa.rec.reg_id is None
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    await pa.select_one_deft(3)

    assert pa.rec.id             == 3
    assert pa.rec.parent_type_id == 4
    assert pa.rec.short_name     == 'Evilin'
    assert pa.rec.full_name      == 'Everly Drinkstomuch Booze'
    assert pa.rec.amount         == 12.3456
    assert pa.rec.code           == 654
    assert pa.rec.do_b           == datetime.date(1990, 2, 28)
    assert pa.rec.mother         is True
    assert pa.rec.reg_id is None
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    await pa.select_one_deft(2)

    assert pa.rec.id             == 2
    assert pa.rec.parent_type_id == 1
    assert pa.rec.short_name     == 'Sue'
    assert pa.rec.full_name      == 'Susan Shagsalotof Chads'
    assert pa.rec.amount         == 100.12
    assert pa.rec.code           is None
    assert pa.rec.do_b           == datetime.date(1985, 4, 22)
    assert pa.rec.mother         is True
    assert pa.rec.reg_id is not None
    assert isinstance(pa.rec.reg_id, uuid.UUID)
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    await pa.select_one_deft(5)

    assert pa.rec.id         == 5
    assert pa.rec.short_name == 'Möller'
    assert pa.rec.full_name  == 'Möller UTF8'
    assert pa.rec.amount     == 222.111
    assert pa.rec.code       == 8000
    assert pa.rec.do_b       == datetime.date(1970, 6, 6)


@pytest.mark.asyncio
async def test_updates(aiodb_dao, db_conn):
    """
    Test we can update records.
    """
    ch     = aiodb_dao.dChild(db_conn)
    recs   = tables.tChild.List()
    dtnow  = datetime.datetime.now().replace(microsecond=0)
    cqry   = aiodb_dao.dChildByParent(db_conn)

    await (await cqry.exec_deft(2)).fetch_all(recs)

    for x in recs:
        x.last_spanked = datetime.datetime.now()
        await ch.update(x)

    await db_conn.commit()

    recs.clear()

    await (await cqry.exec_deft(2)).fetch_all(recs)

    for x in recs:
        assert x.last_spanked.replace(microsecond=0) == dtnow
        assert x.tm_stamp >= dtnow


@pytest.mark.asyncio
async def test_dynamic_wildcards(aiodb_dao, db_conn):
    """
    Test that dynamic % wild cards don't cause problems with some db drivers.
    """
    qry = aiodb_dao.dParentByCriteria(db_conn)
    res = []

    qry.irec.code     = 1
    qry.irec.criteria = " and p.shortname like 'M%'"

    await qry.exec()
    await qry.fetch_all(res)

    assert len(res) == 2


@pytest.mark.asyncio
async def test_nullable_strings(aiodb_dao, db_conn):
    """
    Test that nullable strings go in as empty or Nones, but always come back as empty strings.
    """
    ch   = aiodb_dao.dChild(db_conn)
    male = tables.tChild.Gender_Couplet.key_male
    tod  = datetime.date.today()

    await ch.insert_deft(2, None, '',  'Null String',   male, tod, None, None, b'')
    await ch.select_one()

    assert ch.rec.id
    assert not ch.rec.short_name
    assert type(ch.rec.short_name) == str
    assert ch.rec.short_name == ''

    ch.rec.short_name = 'Not null'
    await ch.update()
    await ch.select_one()

    assert ch.rec.short_name == 'Not null'

    ch.rec.short_name = ''
    await ch.update()
    await ch.select_one()

    assert ch.rec.short_name == ''


@pytest.mark.asyncio
async def test_locks_and_waits(aiodb_dao, db_conn, db_conn2):
    """
    Test that the lock ones and waits are working
    """
    if db_conn.name().lower().find('postgres') < 0:
        return

    from mettle.db import DBLock

    pt1     = aiodb_dao.dParentType(db_conn)
    pt2     = aiodb_dao.dParentType(db_conn2)
    dblock  = DBLock(50, 2)  # 50 miliseconds, 2 retry

    await pt1.lock_one_deft(1, dblock)

    with pytest.raises(xMettle) as ex1:
        await pt2.lock_one_deft(1, dblock)

    assert ex1.value.get_error_code() == xMettle.eCode.DBLockNoWaitFailed

    await db_conn.rollback()
    await pt2.lock_one_deft(1, dblock)

    with pytest.raises(xMettle) as ex2:
        await pt1.lock_one_deft(1, dblock)

    assert ex2.value.get_error_code() == xMettle.eCode.DBLockNoWaitFailed

    await db_conn.rollback()
    await db_conn2.rollback()


@pytest.mark.asyncio
async def test_multi_table_procs(aiodb_dao, db_conn):
    """
    Ensure mutlie selects work as intended.
    """
    qry  = aiodb_dao.dChildParentAndChild(db_conn)
    recs = tables.oChildParentAndChild.List()

    await (await qry.exec_deft(1)).fetch_all(recs)
    assert 4 == len(recs)

    for x in recs:
        assert x.cnt == x.par.id + x.child.id

    dp = aiodb_dao.dParentType(db_conn)

    await dp.select_one_deft(3)

    qry  = aiodb_dao.dParentTypeMyParents(db_conn)
    recs = tables.oParentTypeMyParents.List()

    await (await qry.exec_deft(5, dp.rec.id)).fetch_all(recs)

    for x in recs:
        assert type(x.par)  == tables.tParent
        assert 3            == x.par.parent_type_id
        assert 3 + x.par.id == x.cnt

    await db_conn.rollback()
