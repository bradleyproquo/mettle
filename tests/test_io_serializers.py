# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import pytest
import uuid

from mettle.io.big_endian_reader import BigEndianReader
from mettle.io.big_endian_writer import BigEndianWriter
from mettle.io.memory_stream     import MemoryStream

from mettle.io.py_list_reader      import PyListReader
from mettle.io.py_list_writer      import PyListWriter
from mettle.io.py_json_list_reader import PyJsonListReader
from mettle.io.py_json_list_writer import PyJsonListWriter
from mettle.io.list_stream         import ListStream

from mettle.io.py_dict_reader      import PyDictReader
from mettle.io.py_dict_writer      import PyDictWriter
from mettle.io.py_json_dict_reader import PyJsonDictReader
from mettle.io.py_json_dict_writer import PyJsonDictWriter

pytestmark = [ pytest.mark.io_serializers ]


@pytest.fixture(scope="session")
def io_data_sets() -> list:
    res = []

    res.append({
        'name'           : 'Test Basic',
        'type_bool'      : False,
        'type_char'      : 'X',
        'type_int8'      : -8,
        'type_int16'     : -16,
        'type_int32'     : -32,
        'type_int64'     : -64,
        'type_uint8'     : 8,
        'type_uint16'    : 16,
        'type_uint32'    : 32,
        'type_uint64'    : 64,
        'type_double'    : 1234.456,
        'type_float'     : 987.654,
        'type_string'    : 'This is a string',
        'type_bytearray' : b'Some bytes data',
        'type_datetime'  : datetime.datetime.now().replace(microsecond=0),
        'type_date'      : datetime.date.today(),
        'type_time'      : datetime.datetime.now().replace(microsecond=0).time(),
        'type_json'      : { 'foo': 'bar', 'value': 1234.5, 'lst': [ 1, 2, 'a', 'b', 'c'] },
        'type_guid'      : uuid.uuid4(),
    })

    res.append({
        'name'           : 'None Test',
        'type_bool'      : True,
        'type_char'      : 'n',
        'type_int8'      : None,
        'type_int16'     : None,
        'type_int32'     : None,
        'type_int64'     : None,
        'type_uint8'     : None,
        'type_uint16'    : None,
        'type_uint32'    : None,
        'type_uint64'    : None,
        'type_double'    : 0.0,
        'type_float'     : 0.0,
        'type_string'    : None,
        'type_bytearray' : None,
        'type_datetime'  : None,
        'type_date'      : None,
        'type_time'      : None,
        'type_json'      : None,
        'type_guid'      : None,
    })

    return res


@pytest.fixture(scope="session")
def io_read_writers() -> list:
    res          = []
    bigend       = {}
    pylist       = {}
    pyjsonlist   = {}
    pydict       = {}
    pyjsondict   = {}

    bigend['stream'] = MemoryStream()
    bigend['reader'] = BigEndianReader(bigend['stream'])
    bigend['writer'] = BigEndianWriter(bigend['stream'])

    pylist['stream'] = ListStream()
    pylist['reader'] = PyListReader(pylist['stream'])
    pylist['writer'] = PyListWriter(pylist['stream'])

    pylist['stream'] = ListStream()
    pylist['reader'] = PyListReader(pylist['stream'])
    pylist['writer'] = PyListWriter(pylist['stream'])

    pyjsonlist['stream'] = ListStream()
    pyjsonlist['reader'] = PyJsonListReader(pyjsonlist['stream'])
    pyjsonlist['writer'] = PyJsonListWriter(pyjsonlist['stream'])

    pydict['stream'] = {}
    pydict['reader'] = PyDictReader(pydict['stream'])
    pydict['writer'] = PyDictWriter(pydict['stream'])

    pyjsondict['stream'] = {}
    pyjsondict['reader'] = PyJsonDictReader(pyjsondict['stream'])
    pyjsondict['writer'] = PyJsonDictWriter(pyjsondict['stream'])

    res.append(bigend)
    res.append(pylist)
    res.append(pyjsonlist)
    res.append(pydict)
    res.append(pyjsondict)

    return res


def test_serialize_and_deserialize(io_data_sets, io_read_writers):
    """
    Test that we can serialize and deserialize all types with all known reader/writers
    """
    for rw in io_read_writers:
        st = rw['stream']
        wr = rw['writer']
        re = rw['reader']

        for ds in io_data_sets:
            st.clear()

            wr.write_start(ds['name'])
            wr.write_bool('type_bool',           ds['type_bool'])
            wr.write_char('type_char',           ds['type_char'])
            wr.write_int8('type_int8',           ds['type_int8'])
            wr.write_int16('type_int16',         ds['type_int16'])
            wr.write_int32('type_int32',         ds['type_int32'])
            wr.write_int64('type_int64',         ds['type_int64'])
            wr.write_uint8('type_uint8',         ds['type_uint8'])
            wr.write_uint16('type_uint16',       ds['type_uint16'])
            wr.write_uint32('type_uint32',       ds['type_uint32'])
            wr.write_uint64('type_uint64',       ds['type_uint64'])
            wr.write_double('type_double',       ds['type_double'])
            wr.write_float('type_float',         ds['type_float'])
            wr.write_string('type_string',       ds['type_string'])
            wr.write_bytearray('type_bytearray', ds['type_bytearray'])
            wr.write_datetime('type_datetime',   ds['type_datetime'])
            wr.write_date('type_date',           ds['type_date'])
            wr.write_time('type_time',           ds['type_time'])
            wr.write_json('type_json',           ds['type_json'])
            wr.write_guid('type_guid',           ds['type_guid'])
            wr.write_end(ds['name'])

            if not isinstance(st, dict):
                st.position_start()

            re.read_start(ds['name'])
            assert re.read_bool('type_bool')                 == ds['type_bool']
            assert re.read_char('type_char')                 == ds['type_char']
            assert re.read_int8('type_int8')                 == ds['type_int8']
            assert re.read_int16('type_int16')               == ds['type_int16']
            assert re.read_int32('type_int32')               == ds['type_int32']
            assert re.read_int64('type_int64')               == ds['type_int64']
            assert re.read_uint8('type_uint8')               == ds['type_uint8']
            assert re.read_uint16('type_uint16')             == ds['type_uint16']
            assert re.read_uint32('type_uint32')             == ds['type_uint32']
            assert re.read_uint64('type_uint64')             == ds['type_uint64']
            assert round(re.read_double('type_double'), 3)   == round(ds['type_double'], 3)
            assert round(re.read_float('type_float'), 3)     == round(ds['type_float'], 3)
            assert re.read_string('type_string')             == ds['type_string']
            assert re.read_bytearray('type_bytearray')       == ds['type_bytearray']
            assert re.read_datetime('type_datetime')         == ds['type_datetime']
            assert re.read_date('type_date')                 == ds['type_date']
            assert re.read_time('type_time')                 == ds['type_time']
            assert re.read_json('type_json')                 == ds['type_json']
            assert re.read_guid('type_guid')                 == ds['type_guid']
            re.read_end(ds['name'])
