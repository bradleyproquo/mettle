#!/bin/bash

# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

set -m
set -e

export TEST_PATH="$LOCAL_PATH/tests"
export LOG_PATH="$LOCAL_PATH/tests/log"

declare -a _arg_skip_steps
declare -a _arg_run_steps
_arg_verbose=0
_arg_list=


declare -a _test_steps=(\
  pytest_mettle_libs\
  pytest_generators\
  pip_installs\
  start_braze_service\
  pytest_braze_rpc\
  db_postgres_mettle\
  db_postgres_psycopg2\
  db_postgres_sql_alchemy\
  db_postgres_async_encode_io\
  db_sqlite3_mettle\
  db_sqlite3_native\
)

declare -a _test_steps_descr=(\
  "Test the mettle libraries"\
  "Run pytest for the code generators"\
  "Python pip install required test packages (uvicorn, fastapi)"\
  "Start the braze test (FastAPI) service"\
  "Run pytest for braze RPC server calls"\
  "Test PostgreSQL database code (mettledb driver)"\
  "Test PostgreSQL database code (psycopg2 driver)"\
  "Test PostgreSQL database code (sql alchemy driver)"\
  "Test async PostgreSQL database code (encode io async driver)"\
  "Test Sqlite database code (mettledb driver)"\
  "Test Sqlite database code(native python driver)"\
)

function vcho()
{
  if [ $_arg_verbose -eq 1 ]; then
    echo "${@}"
  fi
}

function _kill_child_processes()
{
  set +e

  vcho "kill start"

  while kill %% 2>/dev/null; do sleep 0; done

  vcho "kill done"

  set +m
}

function _test_pytest()
{
  cd "$LOCAL_PATH"

  if [ ! -d test-results ]; then
    mkdir test-results
  fi

  if [[ $_arg_verbose -eq 1 ]]; then
    local vbflag="--capture=no"
  fi

  pytest --junitxml=test-results/results.xml ${vbflag} -m ${@}
}

function pytest_mettle_libs()
{
  _test_pytest mettle_libs
  _test_pytest io_serializers
}

function pytest_generators()
{
  _test_pytest generators
}

function pytest_braze_rpc()
{
  _test_pytest braze_rpc
}

function pip_installs()
{
  set +e

  pip show uvicorn --version 2>/dev/null 1>/dev/null

  if [ $? -ne 0 ]; then
    set -e
    pip install uvicorn
    set +e
  fi

  pip show fastapi --version 2>/dev/null 1>/dev/null

  if [ $? -ne 0 ]; then
    set -e
    pip install fastapi
    set +e
  fi

  set -e
}

function _test_python_db()
{
  local targ_db="${1}"
  local targ_driver="${2}"
  local async_flag="${3}"

  vcho " Testing Python Database Code [database: $targ_db, driver: $targ_driver]"

  cd "${TEST_PATH}/_makefiles"

  if [ $_arg_verbose -eq 1 ]; then
    make -f "localdb.${targ_db}.makefile" rebuild
  else
    make -f "localdb.${targ_db}.makefile" rebuild > "${LOG_PATH}/create_db_${targ_db}.out" 2>&1
  fi

  _test_pytest "databases${async_flag}" --db_target ${targ_db} --db_driver ${targ_driver} ${vbflag}
}

function db_sqlite3_native()
{
  _test_python_db sqlite native_sqlite3
}

function db_sqlite3_mettle()
{
  _test_python_db sqlite mettledb_sqlite3
}

function db_postgres_async_encode_io()
{
  _test_python_db postgresql encode_io_postgresql _async
}

function db_postgres_sql_alchemy()
{
  _test_python_db postgresql sql_alchemy
}

function db_postgres_psycopg2()
{
  _test_python_db postgresql psycopg2
}

function db_postgres_mettle()
{
  _test_python_db postgresql mettledb_postgresql
}

function start_braze_service()
{
  cd "$LOCAL_PATH/tests"

  local out_file="${LOG_PATH}/_run-braze-fastpi-service.out"

  ./_run-braze-fastpi-service.sh > "$out_file" 2>&1 &
  sleep 2

  set +e

  local running=$(jobs | grep Running)

  if [ $_arg_verbose -eq 1 ] || [ ! "$running" ]; then
    echo "  --- Braze Service Output -- [Start]"
    cat "$out_file"
    echo "  --- Braze Service Output -- [End]"
  fi

  set -e

  if [ ! "$running" ]; then
    echo "TestBraze service not running after starting up!"
    return 1
  fi
}

function _list_steps()
{
  local idx=0

  echo ""

  for step in ${_test_steps[@]}; do
    printf "  %-32.32s : %s" "$step" "${_test_steps_descr[${idx}]}"
    echo ""
    idx=$((idx+1))
  done

  echo ""
}

function _usage_and_exit()
{
  echo ""
  echo " Run Mettle Tests"
  echo ""
  echo "Usage: $0 [option] [ -s skip | -r run | -v | -] [arg]..."
  echo "----------------------------------------------------------------------------"
  echo " -s, --skip SKIP : Skip a step in the test *"
  echo " -r, --run RUN   : Run only a specfic step in the test *"
  echo " -l, --list      : List all the steps instead of running the test"
  echo " -v, --verbose   : Turn on verbose messages"
  echo " -h, -?, --help  : This help"
  echo ""
  echo "  * These arguments can be use multiple times"
  echo ""

  exit 2
}

function main()
{
  if [ "$_arg_list" ]; then
    _list_steps
    exit 2
  fi

  if [ "$_arg_skip_steps" ]; then vcho " - NOTE! Skipping steps: ${_arg_skip_steps[@]}"; fi
  if [ "$_arg_run_steps" ];  then vcho " - NOTE! Running steps: ${_arg_run_steps[@]}"; fi
  if [ "$_arg_verbose" ];    then vcho " - NOTE! Verbose ON"; fi

  vcho ""
  vcho "Mettle Test Starting..."
  vcho ""

  local idx=0
  local skip
  local is_in
  local ele

  for step in "${_test_steps[@]}"; do
    skip=0

    for ele in "${_arg_skip_steps[@]}"; do
      if [[ "$ele" == "$step" ]]; then
        skip=1
        break
      fi
    done

    if [ "$_arg_run_steps" ]; then
      skip=1

      for ele in "${_arg_run_steps[@]}"; do
        if [[ "$ele" == "$step" ]]; then
          skip=0
          break
        fi
      done
    fi

    if [[ "$skip" -eq 0 ]]; then
      echo "Running  : $step - ${_test_steps_descr[${idx}]}"

      $step

    else
      vcho "Skipping : $step - ${_test_steps_descr[${idx}]}"
    fi

    idx=$((idx+1))
  done

  echo ""
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -s|--skip)      _arg_skip_steps+=("$2"); shift ;;
        -r|--run)       _arg_run_steps+=("$2"); shift ;;
        -l|--list)      _arg_list=1 ;;
        -v|--verbose)   _arg_verbose=1 ;;
        -h|--help|-\?) _usage_and_exit ;;
        *) echo "Unknown parameter passed: $1"; _usage_and_exit;;
    esac
    shift
done

for x in "${_arg_skip_steps[@]}"; do
  echo " - skip $x"
done

for sig in INT QUIT HUP TERM ALRM USR1; do
  trap "
    _kill_child_processes
    trap - $sig EXIT
    kill -s $sig "'"$$"' "$sig"
done

trap _kill_child_processes EXIT

main
