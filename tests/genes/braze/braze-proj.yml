file-type : Mettle.Braze
version   : 2.1
name      : TestBraze
sig-mode  : named

language-generators:
  - target    : C++
    enabled   : False
    dest-path : ../../cpp/mettlebraze
    options   :
      braze.struct       : 'b|'
      braze.couplet      : '|_Couplet'
      server.marshaler   : '|ServerMarshaler'
      server.interface   : '|ServerInterface'
      client.marshaler   : '|ClientMarshaler'
      client.interface   : '|ClientInterface'
      includepath.braze  : mettlebraze
      includepath.tables : mettledb
      namespace          : TestBraze
      casing             :
        class  : pascal
        method : pascal
        member : pascal
        file   : lower

  - target    : JavaScript
    enabled   : False
    dest-path : ../../js/mettlebraze
    options   :
      braze.struct      : 'b|'
      braze.couplet     : '|_Couplet'
      client.marshaler  : '|ClientMarshaler'
      client.interface  : '|ClientInterface'
      namespace         : mettlebraze
      jsonifyFriendly   : 'on'
      casing         :
        class  : pascal
        method : camel
        member : camel
        file   : pascal

  - target    : TypeScript
    enabled   : True
    dest-path : ../../_test_ts/tapp/src/app/mettlebraze
    options   :
      braze.struct     : 'b|'
      braze.couplet    : '|_Couplet'
      client.marshaler : '|ClientMarshaler'
      client.interface : '|ClientInterface'
      import.path      : '../../../../../../code/ts'
      import.dbpath    : 'TestDB:../mettledb/tables;'
      file.couplet     : '|_couplet'
      file.struct      :
      file.class       :
      casing         :
        class  : pascal
        method : camel
        member : camel
        file   : camel

  - target    : Python3
    enabled   : True
    dest-path : ../../_test_python3/braze
    options   :
      braze.struct      : 'b|'
      braze.couplet     : '|_Couplet'
      client.marshaler  : '|ClientMarshaler'
      client.interface  : '|ClientInterface'
      client.serverimpl : '|ClientServerImpl'
      server.marshaler  : '|ServerMarshaler'
      server.interface  : '|ServerInterface'
      file.couplet      : '|_couplet'
      file.struct       : 'b|'
      file.class        :
      namespace         : _test_python3.db
      casing        :
        class  : pascal
        method : snake
        member : snake
        file   : snake


db-projects:
  TestDB  : ../db/db-proj.yml

dicts:
  statusTypes :
    A  : Active
    D  : Disabled
    S  : Suspended

  numberList:
    1  : One
    2  : Two
    3  : Three

couplets:
  - name : Status
    type : char
    vals : statusTypes

  - name : Numbers
    type : int32
    vals : numberList

  - name : AddrType
    type : string
    vals :
      PYS  : Physical
      POST : Postal
      EM   : Email


structs:
  - name   : TestRec
    meta   : { descr: This is the basic test record }
    fields :
      - { name: aint8,     type: int8, descr: 'This tests a byte, smallest int possible' }
      - { name: aint16,    type: int16 }
      - { name: aint32,    type: int32 }
      - { name: aint64,    type: int64 }
      - { name: astring,   type: string }
      - { name: adouble,   type: double }
      - { name: adatetime, type: datetime }
      - { name: aguid,     type: uuid }

  - name   : TestDbRec
    fields :
      - { name: abc,       type: int64 }
      - { name: clientRec, type: "db:TestDB.Parent" }
      - { name: efg,       type: int64 }

  - name   : TestMultiDbRec
    fields :
      - { name: parentRec, type: "db:TestDB.Parent" }
      - { name: childRec,  type: "db:TestDB.Child" }
      - { name: efg,       type: int64 }

  - name   : TestArrays
    fields :
      - { name: recList1, type: "braze:TestRec[]" }
      - { name: intList1, type: "int32[]" }
      - { name: recList2, type: "braze:TestRec[]" }
      - { name: intList2, type: "int32[]" }

  - name   : TestMemoryBlock
    fields :
      - { name: name,         type: string, min: 10, max: 50, descr: "The name of who the finger prints belong too." }
      - { name: image,        type: memblock, descr: The actual png/jpg image of the finger prints. }
      - { name: fingerprints, type: "memblock[]", descr: "Series of encoded finger prints from the image."}

  - name   : TestAuth
    fields :
      - { name: uname, type: string }
      - { name: color, type: string }

  - name   : TestDav
    fields :
      - { name: aint8,     type: int8,    min: 1,  max: 127 }
      - { name: aint32,    type: int32,   min: 20, max: 9999 }
      - { name: astring,   type: string,  null: False, max: 255 }
      - { name: adate,     type: date,    null: False, min: "1980-01-01" }
      - { name: status,    type: char,    in: Status }
      - { name: email,     type: string,  regex: validEmail, func: notDuplicate }

  - name   : TestDbCouplRef
    fields :
      - { name: childId,   type: int64 }
      - { name: gender,    type: char, in: 'db:TestDB.Child.Gender' }

# { type: int32, descr: The same value as the out arg }

remote-calls:
  - name: CallVoid

  - name     : CallAuthTest
    auth     : WhoMatchesName
    returns  : { type: int32, descr: The same value as the args-out value parameter }
    args-in  :
      - { name: who,  type: 'braze:TestAuth', descr: The person who is making the call }
      - { name: name, type: string, null: False, max: 50, descr: The name of the who person }
    args-out :
      - { name: value, type: int32, descr: Sets this value to a non zero value }
    meta     :
      descr : Call the method with an auth test

  - name     : CallDavBasic
    meta     :
      descr : Call a method with basic DAV validations
    returns  : bool
    args-in  :
      - { name: num,     type: 'int32',  min:  10,    max: 90 }
      - { name: status,  type: 'char',   in: Status }
      - { name: name,    type: 'string', null: False, max: 20 }
      - { name: amt,     type: 'double', min:  0.01,  max: 999.99 }

  - name    : CallDavTest
    returns : bool
    meta     :
      descr : Call a method with more complex dav validations
    args-in :
      - { name: validBraze,   type: 'braze:TestDav' }
      - { name: invalidBraze, type: 'braze:TestDav',  cmds : clear }
      - { name: extraBraze,   type: 'braze:TestDav',   'status:notin': ['x', 'y'], email+regex: extraEmailCheck, 'aDate:null': null }
      - { name: validDb,      type: 'db:TestDB.Child' }
      - { name: invalidDb,    type: 'db:TestDB.Child', cmds: clear }
      - { name: extraDb,      type: 'db:TestDB.Child', ShortName+min: 10, FullName+regex: nameCheck }
      - { name: insertDb,     type: 'db:TestDB.Child', cmds: insert }
      - { name: keyOnlyDb,    type: 'db:TestDB.Child', cmds: key }

  - name     : CallBraze
    meta     :
      descr : A braze call testing with braze structures
    returns  : 'braze:TestRec'
    args-in  :
      - { name: inRec,       type: 'braze:TestRec' }
      - { name: inRecArray,  type: 'braze:TestRec[]' }
    args-out :
      - { name: outRec,      type: 'braze:TestRec' }
      - { name: outRecArray, type: 'braze:TestRec[]' }

  - name     : CallDb
    meta     :
      descr : A braze call testing with database structures
    returns  : 'db:TestDB.Parent'
    args-in  :
      - { name: inRec,       type: 'db:TestDB.Parent' }
      - { name: inRecArray,  type: 'db:TestDB.Parent[]' }
    args-out :
      - { name: outRec,      type: 'db:TestDB.Parent' }
      - { name: outRecArray, type: 'db:TestDB.Parent[]' }

  - name     : CallDbCritQuery
    returns  : 'db:TestDB.Parent[]'
    args-in  :
      - { name: crit,  type: 'db:TestDB.Parent.ByCriteria.in' }

  - name     : CallDbNullInt
    returns  : 'db:TestDB.Parent.GetCode.out'
    args-in  :
      - { name: crit,  type: 'db:TestDB.Parent.GetCode.in' }

  - name     : CallDbLists
    returns  : 'db:TestDB.Parent[]'
    args-in  :
      - { name: inList,    type: 'db:TestDB.Parent[]' }
    args-ref :
      - { name: inoutList, type: 'db:TestDB.Parent[]' }
    args-out :
      - { name: outList,   type: 'db:TestDB.Parent[]' }

  - name     : CallBrazeLists
    returns  : 'braze:TestRec[]'
    args-in  :
      - { name: inList,    type: 'braze:TestRec[]' }
    args-ref :
      - { name: inoutList, type: 'braze:TestRec[]' }
    args-out :
      - { name: outList,   type: 'braze:TestRec[]' }

  - name     : CallDates
    returns  : date
    args-in  :
      - { name: inDate,    type: date }
      - { name: inDateT,   type: datetime }
    args-ref :
      - { name: inoutDate,  type: date }
      - { name: inoutDateT, type: datetime }
    args-out :
      - { name: outDate,   type: date }
      - { name: outDateT,  type: datetime }

  - name     : CallStrings
    returns  : string
    args-in  :
      - { name: inStr,    type: string }
    args-ref :
      - { name: inoutStr, type: string }
    args-out :
      - { name: outStr,   type: string }

  - name     : CallDoubles
    returns  : double
    args-in  :
      - { name: inDbl,    type: double }
    args-ref :
      - { name: inoutDbl, type: double }
    args-out :
      - { name: outDbl,   type: double }

  - name     : CallMemoryBlocks
    returns  : 'memblock'
    args-in  :
      - { name: inBlock,    type: 'memblock' }
    args-ref :
      - { name: inoutBlock, type: 'memblock' }
    args-out :
      - { name: outBlock,   type: 'memblock' }

  - name     : CallDateLists
    returns  : 'date[]'
    args-in  :
      - { name: inDate,    type: 'date[]' }
    args-ref :
      - { name: inoutDate, type: 'date[]' }
    args-out :
      - { name: outDate,   type: 'date[]' }

  - name     : CallStringLists
    returns  : 'string[]'
    args-in  :
      - { name: inStr,    type: 'string[]' }
    args-ref :
      - { name: inoutStr, type: 'string[]' }
    args-out :
      - { name: outStr,   type: 'string[]' }

  - name     : CallDoubleLists
    returns  : 'double[]'
    args-in  :
      - { name: inDbl,    type: 'double[]' }
    args-ref :
      - { name: inoutDbl, type: 'double[]' }
    args-out :
      - { name: outDbl,   type: 'double[]' }

  - name     : CallIntLists
    returns  : 'int64[]'
    args-in  :
      - { name: inInt,    type: 'int64[]' }
    args-ref :
      - { name: inoutInt, type: 'int64[]' }
    args-out :
      - { name: outInt,   type: 'int64[]' }

  - name     : CallMemoryBlockLists
    returns  : 'memblock[]'
    args-in  :
      - { name: inBlock,    type: 'memblock[]' }
    args-ref :
      - { name: inoutBlock, type: 'memblock[]' }
    args-out :
      - { name: outBlock,   type: 'memblock[]' }

  - name     : CallUuidLists
    returns  : 'uuid[]'
    args-in  :
      - { name: inUuid,    type: 'uuid[]' }
    args-ref :
      - { name: inoutUuid, type: 'uuid[]' }
    args-out :
      - { name: outUuid,   type: 'uuid[]' }
