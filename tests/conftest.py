# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import pytest
import os
import sys


sys.path.append(os.path.join(os.getenv('LOCAL_PATH'), 'tests'))


def pytest_addoption(parser):
    parser.addoption("--db_target", action="store")
    parser.addoption("--db_driver", action="store")


@pytest.fixture(scope="session")
def ut_genes_data() -> dict:
    res = {
        'root-path'  : os.getenv('LOCAL_PATH'),
        'tests-path' : os.path.join(os.getenv('LOCAL_PATH'), 'tests'),
        'genes-path' : os.path.join(os.getenv('LOCAL_PATH'), 'tests', 'genes'),
    }

    res['projects'] = {
        'db-proj'    : os.path.join(res['genes-path'], 'db',    'db-proj.yml'),
        'braze-proj' : os.path.join(res['genes-path'], 'braze', 'braze-proj.yml')
    }

    res['gen-cmd']    = 'python3 %s' % os.path.join(res['root-path'], 'code', 'python3', 'mettle')

    res['gen-paths'] = {
        'sqldef'           : os.path.join(res['genes-path'], 'db', 'sqldef'),
        'py_db_dao'        : os.path.join(res['tests-path'], '_test_python3', 'db', 'dao'),
        'py_db_dao_async'  : os.path.join(res['tests-path'], '_test_python3', 'db', 'dao_async'),
        'py_db_tbl'        : os.path.join(res['tests-path'], '_test_python3', 'db', 'tables'),
    }

    return res


@pytest.fixture(scope="session")
def braze_client():
    from _test_python3.braze                          import TestBrazeClientMarshaler
    from mettle.braze                                 import Client
    from mettle.braze.http                            import TransportClientSettings
    from _test_python3.api.testbraze_transport_client import TestBrazeTransportClient

    trans = TestBrazeTransportClient()
    trans.construct(TransportClientSettings('http://127.0.0.1:3771/braze', True))

    client = Client(trans)
    res    = TestBrazeClientMarshaler(client)

    return res


@pytest.fixture(scope='session')
def db_supported_targets() -> tuple:
    return ('postgresql', 'sqlite')


@pytest.fixture(scope='session')
def db_supported_drivers() -> tuple:
    return (
        'mettledb_postgresql',
        'mettledb_sqlite3',
        'psycopg2',
        'sql_alchemy',
        'native_sqlite3',
        'encode_io_postgresql',
    )


@pytest.fixture(scope='session')
def db_connection_strings() -> dict:
    return {
        'postgresql' : {
            '*' : 'postgresql://mettle:dev@127.0.0.1/mettle',
        },
        'sqlite' : {
            '*' : f'{os.path.join(os.getenv("LOCAL_PATH"), "tests", "mettle_sqlite3.db")}',
        }
    }


@pytest.fixture(scope='session')
def db_target(request, db_supported_targets) -> str:
    res = request.config.option.db_target

    if res not in db_supported_targets:
        raise Exception(f'Database target [{res}] not supported. Supported targets are: {db_supported_targets}')

    return res


@pytest.fixture(scope='session')
def db_driver(request, db_supported_drivers) -> str:
    res = request.config.option.db_driver

    if res not in db_supported_drivers:
        raise Exception(f'Database driver [{res}] not supported. Supported drivers are: {db_supported_drivers}')

    return res


@pytest.fixture(scope='session')
def db_dao(db_target):
    from mettle.genes import common

    dao_path = f'_test_python3.db.dao.{db_target}'
    dao, exc = common.import_dyn_pluggin(dao_path, True)

    return dao


@pytest.fixture(scope='session')
def aiodb_dao(db_target):
    from mettle.genes import common

    dao_path = f'_test_python3.db.dao_async.{db_target}'
    dao, exc = common.import_dyn_pluggin(dao_path, True)

    return dao


@pytest.fixture(scope='session')
def db_url(db_connection_strings, db_target, db_driver) -> str:
    conn_str = db_connection_strings[db_target].get(db_driver)

    if not conn_str:
        conn_str = db_connection_strings[db_target].get('*')

    assert conn_str is not None

    return conn_str
