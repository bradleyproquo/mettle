# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #


import copy
import datetime
import pytest
import uuid

from mettle.lib import xMettle
from mettle.db  import IConnect

from _test_python3.db import tables

_db_data = {}

pytestmark = [ pytest.mark.databases ]


def _get_new_dbcon(url: str, db_target: str, db_driver: str) -> IConnect:

    if db_target == 'postgresql':
        if db_driver == 'mettledb_postgresql':
            import mettledb_postgresql9
            conn = mettledb_postgresql9.Connect()
        elif db_driver == 'psycopg2':
            from mettle.db.psycopg2.connect import Connect as Psycopg2Connect
            conn = Psycopg2Connect()

        elif db_driver == 'sql_alchemy':
            from mettle.db.sqlalc.connect_postgres import ConnectPostgres as SqlAlcConnect
            global _db_data
            conn = SqlAlcConnect(_db_data['session']())


    elif db_target == 'sqlite':
        if db_driver == 'mettledb_sqlite3':
            import mettledb_sqlite3
            conn = mettledb_sqlite3.Connect()
        elif db_driver == 'native_sqlite3':
            from mettle.db.sqlite.connect import Connect as SqliteConnect
            conn = SqliteConnect()

    assert conn is not None

    if db_driver != 'sql_alchemy':
        conn.connect(url)

    return conn


@pytest.fixture(scope='session')
def _init_sql_alc(db_url, db_target, db_driver) -> bool:
    if db_driver != 'sql_alchemy':
        return False

    import sqlalchemy
    import sqlalchemy.orm
    import sqlalchemy.dialects.postgresql

    global _db_data

    if not _db_data.get('engine'):
        options = {
            'pool_recycle'      : 3600,
            'pool_size'         : 10,
            'pool_timeout'      : 30,
            'max_overflow'      : 30,
            'echo'              : False,
            'execution_options' : {
                'autocommit'        : False,
                'autoflush'         : False,
                'expire_on_commit'  : False,
                'stream_results'    : False,
            },
        }

        _db_data['engine'] = sqlalchemy.create_engine(db_url, **options)
        _db_data['session'] = sqlalchemy.orm.sessionmaker(bind=_db_data['engine'])

    return True


@pytest.fixture(scope='session')
def db_conn(db_url, db_target, db_driver, _init_sql_alc) -> IConnect:
    return _get_new_dbcon(db_url, db_target, db_driver)


@pytest.fixture(scope='session')
def db_conn2(db_url, db_target, db_driver, _init_sql_alc) -> IConnect:
    return _get_new_dbcon(db_url, db_target, db_driver)



def test_dao_and_connection_ok(db_dao, db_conn):
    """
    Ensure the dao is not none, and that the db connector is ok.
    """
    assert db_dao is not None
    assert db_conn is not None
    assert db_conn.name() is not None


def test_check_db_is_empty(db_dao, db_conn):
    """
    Ensure all the tables are empty before we start out tests.
    """
    recs = []

    assert 0 == db_dao.dAllTypesSelectAll(db_conn).exec().fetch_all(recs)
    assert 0 == db_dao.dParentTypeSelectAll(db_conn).exec().fetch_all(recs)
    assert 0 == db_dao.dParentSelectAll(db_conn).exec().fetch_all(recs)
    assert 0 == db_dao.dChildSelectAll(db_conn).exec().fetch_all(recs)

    db_conn.rollback()


def test_all_types_crud(db_dao, db_conn):
    """
    Test we can insert, select, update all possible data types.
    """
    atc = db_dao.dAllTypes(db_conn)
    atr = db_dao.dAllTypes(db_conn)
    atu = db_dao.dAllTypes(db_conn)

    atc.insert_deft(
        8,
        16,
        32,
        64,
        123.45,
        True,
        'This is a string',
        'X',
        b'12345678901010101',
        uuid.uuid4(),
        {"foo": 1, "bar": 2},
        datetime.date.today(),
        datetime.time(23, 59, 59),
        datetime.datetime.now())

    db_conn.commit()

    atr.select_one_deft(atc.rec.type_seq64)

    atc.rec.type_datetime = atc.rec.type_datetime.replace(microsecond=0)
    atr.rec.type_datetime = atr.rec.type_datetime.replace(microsecond=0)

    assert atc.rec.type_seq64       == atr.rec.type_seq64
    assert atc.rec.type_int8        == atr.rec.type_int8
    assert atc.rec.type_int16       == atr.rec.type_int16
    assert atc.rec.type_int32       == atr.rec.type_int32
    assert atc.rec.type_int64       == atr.rec.type_int64
    assert atc.rec.type_double      == atr.rec.type_double
    assert atc.rec.type_bool        == atr.rec.type_bool
    assert atc.rec.type_string      == atr.rec.type_string
    assert atc.rec.type_char        == atr.rec.type_char
    assert atc.rec.type_memblock    == atr.rec.type_memblock
    assert atc.rec.type_uuid        == atr.rec.type_uuid
    assert atc.rec.type_date        == atr.rec.type_date
    assert atc.rec.type_time        == atr.rec.type_time
    assert atc.rec.type_datetime    == atr.rec.type_datetime
    assert isinstance(atr.rec.type_json, dict)
    assert atc.rec.type_json['foo'] == atr.rec.type_json.get('foo')
    assert atc.rec.type_json['bar'] == atr.rec.type_json.get('bar')

    atc.insert_deft(
        -8,
        -16,
        -32,
        -64,
        -123.45,
        False,
        'This is a string',
        'X',
        b'abcdefghijklmonopqrstuvwxyz',
        uuid.uuid4(),
        {"foo": 123.34, "bar": 'BARBAR'},
        datetime.date.today(),
        datetime.time(23, 59, 59),
        datetime.datetime.now())

    db_conn.commit()

    atr.select_one_deft(atc.rec.type_seq64)

    atc.rec.type_datetime = atc.rec.type_datetime.replace(microsecond=0)
    atr.rec.type_datetime = atr.rec.type_datetime.replace(microsecond=0)

    assert atc.rec.type_seq64       == atr.rec.type_seq64
    assert atc.rec.type_int8        == atr.rec.type_int8
    assert atc.rec.type_int16       == atr.rec.type_int16
    assert atc.rec.type_int32       == atr.rec.type_int32
    assert atc.rec.type_int64       == atr.rec.type_int64
    assert atc.rec.type_double      == atr.rec.type_double
    assert atc.rec.type_bool        == atr.rec.type_bool
    assert atc.rec.type_string      == atr.rec.type_string
    assert atc.rec.type_char        == atr.rec.type_char
    assert atc.rec.type_memblock    == atr.rec.type_memblock
    assert atc.rec.type_uuid        == atr.rec.type_uuid
    assert atc.rec.type_date        == atr.rec.type_date
    assert atc.rec.type_time        == atr.rec.type_time
    assert atc.rec.type_datetime    == atr.rec.type_datetime
    assert isinstance(atr.rec.type_json, dict)
    assert atc.rec.type_json['foo'] == atr.rec.type_json.get('foo')
    assert atc.rec.type_json['bar'] == atr.rec.type_json.get('bar')

    atu.rec = copy.copy(atr.rec)

    atu.update_deft(
        atu.rec.type_seq64,
        -18,
        -116,
        -132,
        -164,
        -1123.45,
        True,
        'This is not a string',
        'Y',
        b'9876543210',
        uuid.uuid4(),
        {"foo": 987.65, "bar": 'SHEEP SHEEP'},
        datetime.date.today() + datetime.timedelta(days=-2),
        datetime.time(11, 23, 23),
        datetime.datetime.now() + datetime.timedelta(hours=20))

    db_conn.commit()

    atr.select_one_deft(atu.rec.type_seq64)

    atu.rec.type_datetime = atu.rec.type_datetime.replace(microsecond=0)
    atr.rec.type_datetime = atr.rec.type_datetime.replace(microsecond=0)

    assert atu.rec.type_seq64       == atr.rec.type_seq64
    assert atu.rec.type_int8        == atr.rec.type_int8
    assert atu.rec.type_int16       == atr.rec.type_int16
    assert atu.rec.type_int32       == atr.rec.type_int32
    assert atu.rec.type_int64       == atr.rec.type_int64
    assert atu.rec.type_double      == atr.rec.type_double
    assert atu.rec.type_bool        == atr.rec.type_bool
    assert atu.rec.type_string      == atr.rec.type_string
    assert atu.rec.type_char        == atr.rec.type_char
    assert atu.rec.type_memblock    == atr.rec.type_memblock
    assert atu.rec.type_uuid        == atr.rec.type_uuid
    assert atu.rec.type_date        == atr.rec.type_date
    assert atu.rec.type_time        == atr.rec.type_time
    assert atu.rec.type_datetime    == atr.rec.type_datetime
    assert isinstance(atr.rec.type_json, dict)
    assert atu.rec.type_json['foo'] == atr.rec.type_json.get('foo')
    assert atu.rec.type_json['bar'] == atr.rec.type_json.get('bar')

    atr.delete_one()

    assert not atr.try_select_one()

    db_conn.rollback()


def test_rollback(db_dao, db_conn):
    """
    Ensure all the tables are empty before we start out tests.
    """
    at = db_dao.dAllTypes(db_conn)

    at.select_one_deft(1)

    bef               = at.rec.type_int64
    at.rec.type_int64 = 6969

    at.update()
    at.select_one_deft(1)
    assert at.rec.type_int64 == 6969

    db_conn.rollback()

    at.select_one_deft(1)
    assert at.rec.type_int64 == bef


def test_inserts(db_dao, db_conn):
    """
    Test inserting into all tables work.
    """
    pt     = db_dao.dParentType(db_conn)
    pa     = db_dao.dParent(db_conn)
    ch     = db_dao.dChild(db_conn)
    tlist  = []
    usr_id = f'[mettle_ut_{db_conn.name()}'.lower()

    pt.insert_deft('Single Mother', tables.tParentType.Role_Couplet.key_mother, usr_id)
    assert 1 == pt.rec.id

    pt.insert_deft('Married Father', tables.tParentType.Role_Couplet.key_father, usr_id)
    assert 2 == pt.rec.id

    pt.insert_deft('Drunken Aunt', tables.tParentType.Role_Couplet.key_aunt, usr_id)
    assert 3 == pt.rec.id

    pt.insert_deft('Awesome Uncle', tables.tParentType.Role_Couplet.key_uncle, usr_id)
    assert 4 == pt.rec.id

    db_conn.commit()

    assert 4 == db_dao.dParentTypeSelectAll(db_conn).exec().fetch_all(tlist)

    pa.insert_deft(2, 'Chunks', 'Charles Wageslave Worksalot', 987.654, 951, datetime.date(1981, 12, 2),
                   False, uuid.uuid4(), usr_id)
    assert 1 == pa.rec.id

    pa.insert_deft(1, 'Sue', 'Susan Shagsalotof Chads', 100.12, None, datetime.date(1985, 4, 22), True,
                   uuid.uuid4(), usr_id)
    assert 2 == pa.rec.id

    pa.insert_deft(4, 'Evilin', 'Everly Drinkstomuch Booze', 12.3456, 654, datetime.date(1990, 2, 28),
                   True, None, usr_id)
    assert 3 == pa.rec.id

    pa.insert_deft(3, 'Mark', 'Mark Liveslife Well', 32.98, 9000, datetime.date(1980, 5, 1), False, None, usr_id)
    assert 4 == pa.rec.id

    pa.insert_deft(3, 'Möller', 'Möller UTF8', 222.111, 8000, datetime.date(1970, 6, 6), False, None, usr_id)
    assert 5 == pa.rec.id

    db_conn.commit()

    assert 5 == db_dao.dParentSelectAll(db_conn).exec().fetch_all(tlist)

    bed_time  = datetime.time(20, 0, 0)
    male      = tables.tChild.Gender_Couplet.key_male
    female    = tables.tChild.Gender_Couplet.key_female

    ch.insert_deft(1, None, 'Bobby',  'Robert Chads',    male,   datetime.date(2002, 3,  17), bed_time, None, b'')
    ch.insert_deft(1, None, 'Shela',  'Shelly Chads',    female, datetime.date(2004, 10, 30), bed_time, None, bytearray(b'SHELLY HAS A PIC'))  # noqa
    ch.insert_deft(1, None, 'Chuck',  'Charles Chads',   male,   datetime.date(2005, 11,  9), bed_time, None, None)
    ch.insert_deft(1, None, 'Lizzy',  'Elizabeth Chads', female, datetime.date(2007, 1,  14), bed_time, None, b'')

    ch.insert_deft(2, None, 'Zack',  'Jacob Worksalot',   male,   datetime.date(2003, 5,  15), bed_time,  None, b'JACOB HAS A PIC')  # noqa
    ch.insert_deft(2, None, 'Bels',  'Belinda Worksalot', female, datetime.date(2001, 7,  8),  None,     None, b'')

    ch.insert_deft(3, None, 'Ann',   'Anneliese Booze',   female, datetime.date(2006, 6,  6),  bed_time, None, None)

    ch.insert_deft(4, None, 'Mary',  'Mary Well',      female, datetime.date(2011,  3, 18), None, None, None)
    ch.insert_deft(4, None, 'Alex',  'Alexander Well', male,   datetime.date(2013,  9,  4), None, None, b'ALEX HAS A PIC')
    ch.insert_deft(4, None, 'Vicky', 'Victoria Well',  female, datetime.date(2014, 12, 20), None, None, b'')

    db_conn.commit()


def test_selects(db_dao, db_conn):
    """
    Test selecting the records brings back the correct data.
    """
    pt     = db_dao.dParentType(db_conn)
    pa     = db_dao.dParent(db_conn)
    usr_id = f'[mettle_ut_{db_conn.name()}'.lower()

    pt.select_one_deft(1)

    assert pt.rec.id         == 1
    assert pt.rec.descr      == 'Single Mother'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_mother
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    pt.select_one_deft(2)

    assert pt.rec.id         == 2
    assert pt.rec.descr      == 'Married Father'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_father
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    pt.select_one_deft(3)

    assert pt.rec.id         == 3
    assert pt.rec.descr      == 'Drunken Aunt'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_aunt
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    pt.select_one_deft(4)

    assert pt.rec.id         == 4
    assert pt.rec.descr      == 'Awesome Uncle'
    assert pt.rec.role       == tables.tParentType.Role_Couplet.key_uncle
    assert pt.rec.usr_id     == usr_id
    assert isinstance(pt.rec.tm_stamp, datetime.datetime)

    pa.select_one_deft(1)

    assert pa.rec.id             == 1
    assert pa.rec.parent_type_id == 2
    assert pa.rec.short_name     == 'Chunks'
    assert pa.rec.full_name      == 'Charles Wageslave Worksalot'
    assert pa.rec.amount         == 987.654
    assert pa.rec.code           == 951
    assert pa.rec.do_b           == datetime.date(1981, 12, 2)
    assert pa.rec.mother         is False
    assert pa.rec.reg_id is not None
    assert isinstance(pa.rec.reg_id, uuid.UUID)
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    pa.select_one_deft(4)

    assert pa.rec.id             == 4
    assert pa.rec.parent_type_id == 3
    assert pa.rec.short_name     == 'Mark'
    assert pa.rec.full_name      == 'Mark Liveslife Well'
    assert pa.rec.amount         == 32.98
    assert pa.rec.code           == 9000
    assert pa.rec.do_b           == datetime.date(1980, 5, 1)
    assert pa.rec.mother         is False
    assert pa.rec.reg_id is None
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    pa.select_one_deft(3)

    assert pa.rec.id             == 3
    assert pa.rec.parent_type_id == 4
    assert pa.rec.short_name     == 'Evilin'
    assert pa.rec.full_name      == 'Everly Drinkstomuch Booze'
    assert pa.rec.amount         == 12.3456
    assert pa.rec.code           == 654
    assert pa.rec.do_b           == datetime.date(1990, 2, 28)
    assert pa.rec.mother         is True
    assert pa.rec.reg_id is None
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    pa.select_one_deft(2)

    assert pa.rec.id             == 2
    assert pa.rec.parent_type_id == 1
    assert pa.rec.short_name     == 'Sue'
    assert pa.rec.full_name      == 'Susan Shagsalotof Chads'
    assert pa.rec.amount         == 100.12
    assert pa.rec.code           is None
    assert pa.rec.do_b           == datetime.date(1985, 4, 22)
    assert pa.rec.mother         is True
    assert pa.rec.reg_id is not None
    assert isinstance(pa.rec.reg_id, uuid.UUID)
    assert pa.rec.usr_id         == usr_id
    assert isinstance(pa.rec.tm_stamp, datetime.datetime)

    pa.select_one_deft(5)

    assert pa.rec.id         == 5
    assert pa.rec.short_name == 'Möller'
    assert pa.rec.full_name  == 'Möller UTF8'
    assert pa.rec.amount     == 222.111
    assert pa.rec.code       == 8000
    assert pa.rec.do_b       == datetime.date(1970, 6, 6)


def test_updates(db_dao, db_conn):
    """
    Test we can update records.
    """
    ch     = db_dao.dChild(db_conn)
    recs   = tables.tChild.List()
    dtnow  = datetime.datetime.now().replace(microsecond=0)

    db_dao.dChildByParent(db_conn).exec_deft(2).fetch_all(recs)

    for x in recs:
        x.last_spanked = datetime.datetime.now()
        ch.update(x)

    db_conn.commit()

    recs.clear()

    db_dao.dChildByParent(db_conn).exec_deft(2).fetch_all(recs)

    for x in recs:
        assert x.last_spanked.replace(microsecond=0) == dtnow
        assert x.tm_stamp >= dtnow


def test_dynamic_wildcards(db_dao, db_conn):
    """
    Test that dynamic % wild cards don't cause problems with some db drivers.
    """
    qry = db_dao.dParentByCriteria(db_conn)
    res = []

    qry.irec.code     = 1
    qry.irec.criteria = " and p.shortname like 'M%%'"

    qry.exec().fetch_all(res)

    assert len(res) == 2


def test_nullable_strings(db_dao, db_conn):
    """
    Test that nullable strings go in as empty or Nones, but always come back as empty strings.
    """
    ch   = db_dao.dChild(db_conn)
    male = tables.tChild.Gender_Couplet.key_male
    tod  = datetime.date.today()

    ch.insert_deft(2, None, '',  'Null String',   male, tod, None, None, b'')
    ch.select_one()

    assert ch.rec.id
    assert not ch.rec.short_name
    assert type(ch.rec.short_name) == str
    assert ch.rec.short_name == ''

    ch.rec.short_name = 'Not null'
    ch.update()
    ch.select_one()

    assert ch.rec.short_name == 'Not null'

    ch.rec.short_name = ''
    ch.update()
    ch.select_one()

    assert ch.rec.short_name == ''


def test_locks_and_waits(db_dao, db_conn, db_conn2):
    """
    Test that the lock ones and waits are working
    """
    if db_conn.name().lower().find('postgres') < 0:
        return

    from mettle.db import DBLock

    pt1     = db_dao.dParentType(db_conn)
    pt2     = db_dao.dParentType(db_conn2)
    dblock  = DBLock(50, 2)  # 50 miliseconds, 2 retry

    pt1.lock_one_deft(1, dblock)

    with pytest.raises(xMettle) as ex1:
        pt2.lock_one_deft(1, dblock)

    assert ex1.value.get_error_code() == xMettle.eCode.DBLockNoWaitFailed

    db_conn.rollback()
    pt2.lock_one_deft(1, dblock)

    with pytest.raises(xMettle) as ex2:
        pt1.lock_one_deft(1, dblock)

    assert ex2.value.get_error_code() == xMettle.eCode.DBLockNoWaitFailed

    db_conn.rollback()
    db_conn2.rollback()


def test_multi_table_procs(db_dao, db_conn):
    """
    Ensure mutli selects work as intended.
    """
    qry  = db_dao.dChildParentAndChild(db_conn)
    recs = tables.oChildParentAndChild.List()

    qry.exec_deft(1).fetch_all(recs)

    assert 4 == len(recs)

    for x in recs:
        assert x.cnt == x.par.id + x.child.id

    dp = db_dao.dParentType(db_conn)

    dp.select_one_deft(3)

    qry  = db_dao.dParentTypeMyParents(db_conn)
    recs = tables.oParentTypeMyParents.List()

    qry.exec_deft(5, dp.rec.id).fetch_all(recs)

    for x in recs:
        assert type(x.par)  == tables.tParent
        assert 3            == x.par.parent_type_id
        assert 3 + x.par.id == x.cnt
