# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import pytest
import uuid


pytestmark = [ pytest.mark.braze_rpc ]


def test_basic_call_void(braze_client):
    """
    Test the most basic calls.
    """
    assert braze_client.call_void() is None


def test_strings_doubles_dates(braze_client):
    """
    Test that strings, doubles, dates are passed correctly with the calls.
    """
    from mettle.lib     import Ref

    in_str    = 'hello world Stévie'
    inout_str = Ref('foo')
    out_str   = Ref('')

    res = braze_client.call_strings(in_str, inout_str, out_str)

    assert res is not None

    assert inout_str.get() == 'foobar'
    assert out_str.get()   == 'get outer here'
    assert res             == 'dindu nuffin'

    in_dbl    = 123.456789
    inout_dbl = Ref(123.0)
    out_dbl   = Ref(0.0)

    res = braze_client.call_doubles(in_dbl, inout_dbl, out_dbl)

    assert res is not None

    assert inout_dbl.get() == 123.456
    assert out_dbl.get()   == 963.852
    assert res             == 258.147

    idt  = datetime.date.today()
    idtm = datetime.datetime.now().replace(microsecond=0)

    idtm.replace(microsecond=0)

    iodt  = Ref(idt)
    iodtm = Ref(idtm)

    iodt.set(idt)
    iodtm.set(idtm)

    odt  = Ref(datetime.date)
    odtm = Ref(datetime.datetime)

    res = braze_client.call_dates(idt, idtm, iodt, iodtm, odt, odtm)

    assert res is not None
    assert res == idt

    assert iodt.get() == idt + datetime.timedelta(days=1)
    assert odt.get()  == idt

    assert iodtm.get() == (idtm + datetime.timedelta(days=1, hours=1))
    assert odtm.get()  == idtm


def test_braze_lists(braze_client):
    """
    Tests that braze lists are passed correctly between the client and server.
    """
    from _test_python3.braze import bTestRec

    in_list    = bTestRec.List()
    inout_list = bTestRec.List()
    out_list   = bTestRec.List()

    in_list.append(bTestRec(
        aint8=8,  aint16=16, aint32=32, aint64=6464, astring = 'BrazeTestIn1',
        adouble = 987654.321, adatetime=datetime.datetime.now()))
    in_list.append(bTestRec(
        aint8=9,  aint16=17, aint32=33, aint64=6565, astring = 'BrazeTestIn2',
        adouble = 987654.322, adatetime=datetime.datetime.now(), aguid = uuid.uuid4()))
    in_list.append(bTestRec(
        aint8=10, aint16=18, aint32=34, aint64=6666, astring = 'BrazeTestIn3',
        adouble = 987654.323, adatetime=datetime.datetime.now()))

    inout_list.append(bTestRec(
        aint8=8,  aint16=16, aint32=32, aint64=6464, astring = 'BrazeTestInOut1',
        adouble = 987654.321, adatetime=datetime.datetime.now()))
    inout_list.append(bTestRec(
        aint8=9,  aint16=17, aint32=33, aint64=6565, astring = 'BrazeTestInOut2',
        adouble = 987654.322, adatetime=datetime.datetime.now()))
    inout_list.append(bTestRec(
        aint8=10, aint16=18, aint32=34, aint64=6666, astring = 'BrazeTestInOut3',
        adouble = 987654.323, adatetime=datetime.datetime.now()))

    ret = braze_client.call_braze_lists(in_list, inout_list, out_list)

    assert type(ret) == bTestRec.List
    assert len(ret)  == 1

    res = ret[0]

    assert type(res)            == bTestRec
    assert res.aint8            == 8
    assert res.aint16           == 1616
    assert res.aint32           == 3232
    assert res.aint64           == 646464
    assert res.astring          == 'Braze:ABC'
    assert res.adouble          == 123456.789
    assert res.adatetime.date() == datetime.datetime.now().date()

    assert len(inout_list) == 2
    assert len(out_list)   == 3


def test_db_structure_calls(braze_client):
    """
    Test the database structure calls all work
    """
    from _test_python3.db.tables import tParent
    from _test_python3.db.tables import iParentGetCode

    pirec   = tParent(5, 2, 'Mary', 'Sue', 1234.456, 23, None, True, None, '[nim]')
    pilist  = tParent.List()
    porec   = tParent()
    polist  = tParent.List()

    pilist.append(pirec)

    res = braze_client.call_db(pirec, pilist, porec, polist)

    assert res is not None
    assert res.id      == pirec.id
    assert len(polist) == 2
    assert porec.id    == pirec.id

    pirec2  = tParent(12, 1, 'John', 'Doe', 987.7654, 11, None, False, None, '[nim]')
    pilist  = tParent.List()
    piolist = tParent.List()
    polist  = tParent.List()

    pilist.append(pirec2)
    pilist.append(pirec)
    piolist.append(pirec2)

    res = braze_client.call_db_lists(pilist, piolist, polist)

    assert res is not None

    assert len(res)      == 3
    assert len(piolist)  == 2
    assert len(polist)   == 2

    assert piolist[1].id == polist[1].id
    assert pilist[1].id  == polist[1].id
    assert pilist[1].id  == res[1].id

    crit = iParentGetCode(2)
    res  = braze_client.call_db_null_int(crit)

    assert res is not None
    assert res.code == crit.id

    crit = iParentGetCode(None)
    res  = braze_client.call_db_null_int(crit)

    assert res is not None
    assert res.code == crit.id


def test_call_authenticate(braze_client):
    """
    Test that the braze authentication is working.
    """
    from mettle.lib          import Ref
    from _test_python3.braze import bTestAuth

    who     = bTestAuth('test.user', 'green')
    name    = 'test.user'
    out_val = Ref(0)
    ret_val = braze_client.call_auth_test(who, name, out_val)

    assert ret_val is not None
    assert ret_val == out_val.get()
    assert ret_val == 54321

    with pytest.raises(Exception):
        braze_client.call_auth_test(who, 'some other dude', out_val)

    with pytest.raises(Exception):
        who.color = 'red'
        ret_val    = braze_client.call_auth_test(who, name, out_val)


def test_call_int_lists(braze_client):
    """
    Test that native integer list calls are working.
    """
    from mettle.braze import Int64List

    in_int    = Int64List()
    inout_int = Int64List()
    out_int   = Int64List()

    in_int.append(-42)
    in_int.append(0)
    in_int.append(42)

    inout_int.append(-8012345)
    inout_int.append(0)
    inout_int.append(8012345)

    ret_val = braze_client.call_int_lists(in_int, inout_int, out_int)

    assert ret_val is not None
    assert len(ret_val) == 4
    assert len(inout_int) > 3
    assert len(out_int) > 0
