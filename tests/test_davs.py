# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import datetime
import pytest
import re

from mettle.lib  import Dav
from mettle.lib  import DavCache


pytestmark = [ pytest.mark.mettle_libs ]


def test_dav_not_null():
    d = Dav(Dav.eDavType.NotNull)

    assert not d.validate('local', None,                   False)
    assert not d.validate('local', '',                     False)
    assert not d.validate('local', b'',                    False)
    assert not d.validate('local', datetime.date.min,      False)
    assert not d.validate('local', datetime.time.min,      False)
    assert not d.validate('local', datetime.datetime.min,  False)

    assert d.validate('local',  'Not null',                     False)
    assert d.validate('local',  0,                              False)
    assert d.validate('local',  0.0,                            False)
    assert d.validate('local',  b'abcdefg',                     False)
    assert d.validate('local',  datetime.date.today(),          False)
    assert d.validate('local',  datetime.datetime.now().time(), False)
    assert d.validate('local',  datetime.datetime.now(),        False)


def test_dav_min():
    dnow = datetime.datetime.now()

    ds = Dav(Dav.eDavType.Min, 10)
    di = Dav(Dav.eDavType.Min, 100)
    df = Dav(Dav.eDavType.Min, 200.0)
    dx = Dav(Dav.eDavType.Min, dnow)
    dd = Dav(Dav.eDavType.Min, dnow.date())
    dt = Dav(Dav.eDavType.Min, dnow.time())

    assert not ds.validate('local', 'abcde', False)
    assert ds.validate('local', 'abcdefghij', False)
    assert ds.validate('local', 'abcdefghijklmnopqrst', False)

    assert not di.validate('local', 50, False)
    assert di.validate('local', 100, False)
    assert di.validate('local', 180, False)

    assert not df.validate('local', 50.5, False)
    assert df.validate('local', 200.0, False)
    assert df.validate('local', 222.22, False)

    assert not dx.validate('local', dnow - datetime.timedelta(days=5), False)
    assert dx.validate('local', dnow, False)
    assert dx.validate('local', dnow + datetime.timedelta(days=5), False)

    assert not dd.validate('local', (dnow - datetime.timedelta(days=5)).date(), False)
    assert dd.validate('local', dnow.date(), False)
    assert dd.validate('local', (dnow + datetime.timedelta(days=5)).date(), False)

    assert not dt.validate('local', (dnow - datetime.timedelta(seconds=5)).time(), False)
    assert dt.validate('local', dnow.time(), False)
    assert dt.validate('local', (dnow + datetime.timedelta(seconds=5)).time(), False)


def test_dav_max():
    dnow = datetime.datetime.now()

    ds = Dav(Dav.eDavType.Max, 10)
    di = Dav(Dav.eDavType.Max, 100)
    df = Dav(Dav.eDavType.Max, 200.0)
    dx = Dav(Dav.eDavType.Max, dnow)
    dd = Dav(Dav.eDavType.Max, dnow.date())
    dt = Dav(Dav.eDavType.Max, dnow.time())

    assert ds.validate('local', 'abcde', False)
    assert ds.validate('local', 'abcdefghij', False)
    assert not ds.validate('local', 'abcdefghijklmnopqrst', False)

    assert di.validate('local', 50, False)
    assert di.validate('local', 100, False)
    assert not di.validate('local', 180, False)

    assert df.validate('local', 50.5, False)
    assert df.validate('local', 200.0, False)
    assert not df.validate('local', 222.22, False)

    assert dx.validate('local', dnow - datetime.timedelta(days=5), False)
    assert dx.validate('local', dnow, False)
    assert not dx.validate('local', dnow + datetime.timedelta(days=5), False)

    assert dd.validate('local', (dnow - datetime.timedelta(days=5)).date(), False)
    assert dd.validate('local', dnow.date(), False)
    assert not dd.validate('local', (dnow + datetime.timedelta(days=5)).date(), False)

    assert dt.validate('local', (dnow - datetime.timedelta(seconds=5)).time(), False)
    assert dt.validate('local', dnow.time(), False)
    assert not dt.validate('local', (dnow + datetime.timedelta(seconds=5)).time(), False)


def test_dav_in():
    dnow = datetime.datetime.now()

    d = Dav(Dav.eDavType.In, ['abc', 10, 22.22, dnow])

    assert d.validate('local', 'abc', False)
    assert d.validate('local', 10, False)
    assert d.validate('local', 22.22, False)
    assert d.validate('local', dnow, False)

    assert not d.validate('local', 'efg', False)
    assert not d.validate('local', 20, False)
    assert not d.validate('local', 33.33, False)
    assert not d.validate('local', dnow + datetime.timedelta(seconds=10), False)


def test_dav_not_in():
    dnow = datetime.datetime.now()

    d = Dav(Dav.eDavType.NotIn, ['abc', 10, 22.22, dnow])

    assert not d.validate('local',  'abc', False)
    assert not d.validate('local',  10, False)
    assert not d.validate('local',  22.22, False)
    assert not d.validate('local',  dnow, False)

    assert d.validate('local',  'efg', False)
    assert d.validate('local',  20, False)
    assert d.validate('local',  33.33, False)
    assert d.validate('local',  dnow + datetime.timedelta(seconds=10), False)


def test_dav_regex():
    rl = {
        'email' : re.compile(r'[^@]+@[^@]+\.[^@]+'),
        'cell'  : re.compile(r'(\+[0-9]+\s*)?(\([0-9]+\))?[\s0-9\-]+[0-9]+'),
    }

    demail = Dav(Dav.eDavType.Regex, 'email')
    dcell  = Dav(Dav.eDavType.Regex, 'cell')

    assert not demail.validate('local', 'hello world', False, rl)
    assert not demail.validate('local', '@co.za', False, rl)
    assert not demail.validate('local', 'mettle@', False, rl)
    assert demail.validate('local', 'mettle@bitsmiths.co.za', False, rl)
    assert demail.validate('local', 'mettle@bitsmiths.co', False, rl)

    assert not dcell.validate('local', 'hello world', False, rl)
    assert dcell.validate('local', '077 187 5566', False, rl)
    assert dcell.validate('local', '(077) 187 5566', False, rl)


def test_dav_func():

    def ok_obj(src, obj, otype):
        if obj in ['ok', 1, True]:
            return True

        return False

    fl = {
        'ok' : ok_obj,
    }

    df = Dav(Dav.eDavType.Func, 'ok')

    assert df.validate('local',  'ok', False, func_lookup=fl)
    assert df.validate('local',  1, False, func_lookup=fl)
    assert df.validate('local',  True, False, func_lookup=fl)

    assert not df.validate('local',  None, False, func_lookup=fl)
    assert not df.validate('local',  '!ok', False, func_lookup=fl)
    assert not df.validate('local',  100, False, func_lookup=fl)
    assert not df.validate('local',  False, False, func_lookup=fl)


def test_dav_cache():
    def ok_func(src, obj, otype):
        return True

    def not_ok_func(src, obj, otype):
        return False


    dc  = DavCache()
    lst = []

    dc.set_regex_lookup({
        'email' : r'[^@]+@[^@]+\.[^@]+',
        'cell'  : r'(\+[0-9]+\s*)?(\([0-9]+\))?[\s0-9\-]+[0-9]+',
    })

    dc.set_func_lookup({
        'ok'  : ok_func,
        '!ok' : not_ok_func
    })

    dc.add_targ('name', Dav(Dav.eDavType.NotNull))
    dc.add_targ('name', Dav(Dav.eDavType.Max, 20))

    assert 1 == dc.validate_object('x', {'name' : ''}, lst, fail_fast=False)
    assert 1 == dc.validate_object('x', {'name' : 'this name is way to long for the the validator.'}, lst, fail_fast=False)
    assert 0 == dc.validate_object('x', {'name' : 'Nicolas'}, lst, fail_fast=False)

    assert 2 == len(lst)

    dc.clear()
    lst.clear()

    dc.add_type(int, [Dav(Dav.eDavType.Min, 5), Dav(Dav.eDavType.Max, 95)])

    assert 0 == dc.validate_object('x', {'age': 37, 'drivexp': 20, 'workyears': 18}, lst, fail_fast=False)
    assert 0 == len(lst)

    assert 3 == dc.validate_object('x', {'age': 1, 'drivexp': 1, 'workyears': 1}, lst, fail_fast=False)
    assert 3 == len(lst)

    lst.clear()
    assert 3 == dc.validate_object('x', {'age': 99, 'drivexp': 100, 'workyears': 101}, lst, fail_fast=False)
    assert 3 == len(lst)

    class ObjTest:
        def __init__(self, name, age):
            self.name  = name
            self.age   = age

    dc.add_targ('name', [Dav(Dav.eDavType.NotNull), Dav(Dav.eDavType.Max, 20)])
    dc.add_targ('age',  [Dav(Dav.eDavType.Min, 18), Dav(Dav.eDavType.Max, 110)])

    lst.clear()
    assert 0 == dc.validate_object('x', ObjTest('Nic', 37), lst, fail_fast=False)
    assert 0 == len(lst)

    assert 1 == dc.validate_object('x', ObjTest('Nic', 17), lst, fail_fast=False)
    assert 1 == len(lst)

    lst.clear()

    assert 1 == dc.validate_object('x', ObjTest('Nic Nic Nic Nic Nic Nic Nic Nic Nic Nic', 27), lst, fail_fast=False)
    assert 1 == len(lst)

    lst.clear()

    assert 0 == dc.validate_object('x', [ObjTest('Nic', 37), ObjTest('Nic', 37)], lst, fail_fast=False)
    assert 0 == len(lst)

    assert 2 == dc.validate_object('x', [ObjTest('Nic', 7), ObjTest('ASDF;LKJASDF;LKJASDF;LKJASFD;KLJ', 37)], lst, fail_fast=False)  # noqa
    assert 2 == len(lst)

    class LookupTest:
        def __init__(self, email, cell):
            self.email  = email
            self.cell   = cell

    dc.add_targ('email', [Dav(Dav.eDavType.NotNull), Dav(Dav.eDavType.Max, 30), Dav(Dav.eDavType.Regex, 'email')])
    dc.add_targ('cell',  [Dav(Dav.eDavType.NotNull), Dav(Dav.eDavType.Max, 12), Dav(Dav.eDavType.Regex, 'cell')])

    assert 0 == dc.validate_object('x', LookupTest('mettle@bitsmiths.co.za', '077 187 5566'), lst, False)
    assert 1 == dc.validate_object('x', LookupTest('mettle@bitsmiths.co.za', 'ABCDEF'), lst, False)
    assert 1 == dc.validate_object('x', LookupTest('mettle', '077 187 5566'), lst, False)
    assert 1 == dc.validate_object('x', LookupTest('', '077 187 5566'), lst, False)
    assert 1 == dc.validate_object('x', LookupTest('mettle@bitsmiths.co.za', '077 187 5566 077 187 5566'), lst, False)

    dc.add_targ('email', [Dav(Dav.eDavType.Func, 'ok')])

    assert 0 == dc.validate_object('x', LookupTest('mettle@bitsmiths.co.za', '077 187 5566'), lst, False)

    dc.add_targ('email', [Dav(Dav.eDavType.Func, '!ok')])
    assert 1 == dc.validate_object('x', LookupTest('mettle@bitsmiths.co.za', '077 187 5566'), lst, False)
