#!/bin/bash

#
# Run the FastAPI braze test server
#

PYTHONPATH="${PYTHONPATH}:`pwd`"

uvicorn --port 3771 --workers 2 --limit-concurrency 10 --backlog 1024 --loop asyncio --reload _test_python3.api.main:app
