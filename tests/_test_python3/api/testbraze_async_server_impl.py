# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import copy
import logging

import datetime
import mettle.braze
import mettle.lib

from _test_python3.braze import bTestAuth
from _test_python3.braze import bTestRec

from _test_python3.db.tables import tParent
from _test_python3.db.tables import oParentGetCode
from _test_python3.db.tables import iParentGetCode


from _test_python3.braze import TestBrazeAsyncServerInterface


SVR_ID = '[test-braze-server-async] '


class TestBrazeAsyncServerImplementation(TestBrazeAsyncServerInterface):


    def __init__(self):
        pass


    def _shutdown_server(self):
        return False


    def _slack_time_handler(self):
        pass


    def _set_impl_data(self, data: dict):
        if not data:
            return


    def _set_rpc_token_data(self, tok_data: dict):
        pass


    async def call_auth_test(self,
                             who: bTestAuth,
                             name: str,
                             value: mettle.lib.Ref) -> int:
        logging.info(f'{SVR_ID} call_dates()')
        logging.info(f'    who    : {bTestAuth}')
        logging.info(f'    name   : {name}')
        logging.info(f'    value  : {value.get()}')

        value.set(54321)

        logging.info('     ------------')
        logging.info(f'    result : {value.get()}')

        return value.get()


    async def call_void(self):
        logging.info(f'{SVR_ID} call_void()')


    async def call_dates(self,
                         in_date: datetime.date,
                         in_date_t: datetime.datetime,
                         inout_date: mettle.lib.Ref,
                         inout_date_t: mettle.lib.Ref,
                         out_date: mettle.lib.Ref,
                         out_date_t: mettle.lib.Ref) -> datetime.date:
        logging.info(f'{SVR_ID} call_dates()')

        logging.info(f'    in_date      : {in_date}')
        logging.info(f'    in_date_t    : {in_date_t}')
        logging.info(f'    inout_date   : {inout_date.get()}')
        logging.info(f'    inout_date_t : {inout_date_t.get()}')

        if inout_date.get():
            inout_date.set(inout_date.get() + datetime.timedelta(days=1))

        if inout_date_t.get():
            inout_date_t.set(inout_date_t.get() + datetime.timedelta(days=1, hours=1))

        out_date.set(in_date)
        out_date_t.set(in_date_t)

        logging.info('     ------------')
        logging.info(f'    inout_date   : {inout_date.get()}')
        logging.info(f'    inout_date_t : {inout_date_t.get()}')
        logging.info(f'    out_date     : {out_date.get()}')
        logging.info(f'    out_date_t   : {out_date_t.get()}')

        return in_date


    async def call_strings(self,
                           in_str: str,
                           inout_str: mettle.lib.Ref,
                           out_str: mettle.lib.Ref) -> str:
        logging.info(f'{SVR_ID} call_strings()')
        logging.info(f'    in_str    : {in_str}')
        logging.info(f'    inout_str : {inout_str.get()}')

        inout_str.set(inout_str.get() + 'bar')
        out_str.set('get outer here')
        res = 'dindu nuffin'

        logging.info('     ------------')
        logging.info(f'    inout_str : {inout_str.get()}')
        logging.info(f'    out_str   : {out_str.get()}')
        logging.info(f'    return    : {res}')

        return res


    async def call_int_lists(self,
                             in_int: mettle.braze.Int64List,
                             inout_int: mettle.braze.Int64List,
                             out_int: mettle.braze.Int64List) -> mettle.braze.Int64List:
        logging.info(f'{SVR_ID} call_int_lists()')
        logging.info(f'    in_int    : {in_int}')
        logging.info(f'    inout_int : {inout_int}')

        inout_int.append(151)
        inout_int.append(152)
        inout_int.append(153)
        out_int.append(601)
        out_int.append(702)
        out_int.append(803)

        res = mettle.braze.Int64List()
        res.append(-1025)
        res.append(-2035)
        res.append(-3045)
        res.append(-4055)

        logging.info('     ------------')
        logging.info(f'    inout_int : {inout_int}')
        logging.info(f'    out_int   : {out_int}')
        logging.info(f'    return    : {res}')

        return res


    async def call_doubles(self,
                           in_dbl: float,
                           inout_dbl: mettle.lib.Ref,
                           out_dbl: mettle.lib.Ref) -> float:
        logging.info(f'{SVR_ID} call_doubles()')
        logging.info(f'    in_dbl    : {in_dbl}')
        logging.info(f'    inout_dbl : {inout_dbl.get()}')

        inout_dbl.set(inout_dbl.get() + 0.456)
        out_dbl.set(963.852)
        res = 258.147

        logging.info('     ------------')
        logging.info(f'    inout_dbl : {inout_dbl.get()}')
        logging.info(f'    out_dbl   : {out_dbl.get()}')
        logging.info(f'    return    : {res}')

        return res


    async def call_braze_lists(self,
                               in_list: bTestRec.List,
                               inout_list: bTestRec.List,
                               out_list: bTestRec.List) -> bTestRec.List:
        logging.info(f'{SVR_ID} call_braze_lists()')
        logging.info(f'    in_list    : {in_list}')

        for item in in_list:
            logging.info(f'        - {item}')

        logging.info(f'    inout_list : {inout_list}')

        for item in inout_list:
            logging.info(f'        - {item}')

        res = bTestRec.List()
        res.append(bTestRec(aint8=8, aint16=1616, aint32=3232, aint64=646464,
                            astring='Braze:ABC', adouble=123456.789, adatetime=datetime.datetime.now()))

        for x in range(3):
            obj = copy.copy(res[0])
            obj.aint8 += x + 1
            out_list.append(obj)

        inout_list.pop()

        logging.info('     ------------')
        logging.info(f'    inout_list : {inout_list}')

        for item in inout_list:
            item.aint16 += 1
            logging.info(f'        - {item}')

        logging.info(f'    out_list   : {out_list}')

        for item in out_list:
            logging.info(f'        - {item}')

        logging.info(f'    return     : {res}')

        for item in res:
            logging.info(f'        - {item}')

        return res


    async def call_db_null_int(self,
                               crit: iParentGetCode) -> oParentGetCode:
        logging.info(f'{SVR_ID} call_db_null_int()')
        logging.info(f'    crit       : {crit}')

        res = oParentGetCode(crit.id)

        logging.info('     ------------')
        logging.info(f'    result     : {res}')

        return res


    async def call_db(self,
                      in_rec: tParent,
                      in_rec_array: tParent.List,
                      out_rec: tParent,
                      out_rec_array: tParent.List) -> tParent:
        logging.info(f'{SVR_ID} call_db()')
        logging.info(f'    in_rec        : {in_rec}')
        logging.info(f'    in_rec_array  : {in_rec_array}')

        for item in in_rec_array:
            logging.info(f'        - {item}')

        out_rec._copy_from(in_rec)

        logging.info('     ------------')
        logging.info(f'    out_rec       : {out_rec}')
        logging.info(f'    out_rec_array : {out_rec_array}')

        out_rec_array.append(in_rec)
        out_rec_array.append(in_rec)

        for item in out_rec_array:
            logging.info(f'        - {item}')

        logging.info(f'    result        : {in_rec}')

        return in_rec


    async def call_db_lists(self,
                            in_list: tParent.List,
                            inout_list: tParent.List,
                            out_list: tParent.List) -> tParent.List:
        logging.info(f'{SVR_ID} call_db_lists()')
        logging.info(f'    in_list       : {in_list}')

        for item in in_list:
            logging.info(f'        - {item}')

        logging.info(f'    inout_list    : {inout_list}')

        for item in inout_list:
            logging.info(f'        - {item}')

        inout_list.append(in_list[-1])
        out_list.append(in_list[0])
        out_list.append(in_list[-1])

        res = tParent.List()

        res.append(in_list[0])
        res.append(in_list[-1])
        res.append(in_list[-1])

        logging.info('     ------------')
        logging.info(f'    inout_list    : {inout_list}')

        for item in inout_list:
            logging.info(f'        - {item}')

        logging.info(f'    out_list      : {out_list}')

        for item in out_list:
            logging.info(f'        - {item}')

        logging.info(f'    result        : {res}')

        for item in res:
            logging.info(f'        - {item}')

        return res
