# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import logging

from mettle.braze import ITransport
from mettle.braze import ServerMultiMarsh


class TestBrazeServer(ServerMultiMarsh):
    """
    Test Braze server..

    To be used by a Fast API web framework.
    """
    MODULE         = '[TestBrazeServer]'
    AUTH_LOGGED_IN = '_LOGGED_IN_'


    def __init__(self, trans: ITransport):
        """
        Constructor.

        :param logger: The logger to use.
        :param trans: (mettle.braze.ITransport) The transport object this server will use.
        :param services: List of services to load, None means load them all.
        """
        self._tok       = None
        self._tbs_si    = None
        self._tbs_sm    = None

        sm_list = []

        from .testbraze_async_server_impl                           import TestBrazeAsyncServerImplementation
        from _test_python3.braze.test_braze_async_server_marshaler  import TestBrazeAsyncServerMarshaler
        self._tbs_si = TestBrazeAsyncServerImplementation()
        self._tbs_sm = TestBrazeAsyncServerMarshaler(self._tbs_si)

        sm_list.append(self._tbs_sm)

        ServerMultiMarsh.__init__(self, sm_list, trans)


    def __del__(self):
        """
        Destructor.
        """
        pass


    def construct(self):
        """
        Overload.
        """
        ServerMultiMarsh.construct(self)


    def token_set(self, tok):
        self._tok = tok


    def token_get(self):
        return '' if self._tok is None else self._tok


    async def auth_async(self, rpc_name: str, rpc_auth: str, rpc_args: dict) -> dict:
        """
        Overload base method.
        """
        from _test_python3.braze import bTestAuth

        logging.info(f'    [Auth] rpc_name: {rpc_name}, rpc_auth: {rpc_auth}, rpc_args: {rpc_args}')

        if rpc_auth != 'WhoMatchesName':
            raise Exception('Authentication error - Unknown authentication type!' % (rpc_auth))

        who = rpc_args.get('who')
        usr = rpc_args.get('name')

        if who is None or type(who) != bTestAuth:
            raise Exception('Authentication error - who [%s] not found/valid!' % (str(who)))

        if usr is None or type(usr) != str:
            raise Exception('Authentication error - name [%s] not found/valid!' % (str(usr)))

        if who.color != 'green':
            raise Exception('Authentication error - only green is a valid color, and you are [%s]' % who.color)

        if who.uname != usr:
            raise Exception('Authentication error - Who [%s] is not the same as [%s]' % (who.uname, usr))

        return None
