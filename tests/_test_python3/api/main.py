#!/usr/bin/env python

# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import fastapi
import logging
import os
import typing

from fastapi.responses         import PlainTextResponse
from starlette.middleware.cors import CORSMiddleware

from _test_python3.api       import deps


SYS_ID = '[mettle-braze ut]'

logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.DEBUG)

logging.info('%s Service Starting' % SYS_ID)


app = fastapi.FastAPI(
    title       = 'Mettle Braze Test Service',
    openapi_url = "/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_credentials = True,
    allow_origins     = ["*"],
    allow_methods     = ["*"],
    allow_headers     = ["*"])


@app.get("/ping", response_class=PlainTextResponse)
async def ping():
    """
    Always returns 'OK' - used for health checkers.
    """
    return 'OK'


@app.get("/")
async def root():
    """
    Returns the service name and the running environment name.
    """
    return {
        'service'  : SYS_ID,
        'version'  : os.getenv('METTLE_VERSION'),
        'codename' : os.getenv('METTLE_VER_CODENAME'),
    }


@app.post("/braze/{server}/{rpc}", response_model = typing.List)
async def business_to_business_braze(
    request: fastapi.Request,
    server: str,
    rpc: str,
    braze_data: list = fastapi.Body(..., embed=False),
    bt_server = fastapi.Depends(deps.get_testbraze_server),
) -> typing.List:
    """
    B2B Braze API
    """
    logging.debug('%s /braze/%s/%s - start %r' % (SYS_ID, server, rpc, braze_data))

    res = []

    bt_server.get_transport().set_req(request)

    bt_server.get_transport().set_stream_list(braze_data)

    impl_data = {}

    rc  = await bt_server.service_rpc_async(impl_data)
    res = bt_server.get_transport().get_stream_list()

    if rc != 0:
        logging.warning('%s error out: %s' % (SYS_ID, str(res)))
        raise Exception(str(res))

    logging.debug('%s /braze/%s/%s - done' % (SYS_ID, server, rpc))

    return res
