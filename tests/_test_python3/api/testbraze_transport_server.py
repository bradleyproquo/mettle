# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

"""
Overload of transport server that sets the timezone to always be UTC
"""

from mettle.io.py_json_list_reader import PyJsonListReader
from mettle.io.py_json_list_writer import PyJsonListWriter

from mettle.braze.http.transport_server import TransportServer
from mettle.lib.timezone                import TimeZone


class TestBrazeTransportServer(TransportServer):
    """
    Standard http server with timezone overload.
    Note that his is implemented to only use falcon because of the set_req method.
    Create a new one if you are using a different web server framework.
    """

    def __init__(self):
        """
        Constructor.
        """
        TransportServer.__init__(self)
        self._tzone = TimeZone(0, 0)
        self._req   = None


    def new_reader(self, stream):
        return PyJsonListReader(stream, self._tzone)


    def new_writer(self, stream):
        return PyJsonListWriter(stream, self._tzone)


    def set_req(self, req):
        """
        Sets the fast-api requestion object
        """
        self._req = req


    def client_address(self):
        if not self._req:
            return '<UNKNOWN>'

        return str(self._req.client.host)
