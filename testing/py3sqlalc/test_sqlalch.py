#!/usr/bin/env python

# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import os.path
import unittest
import datetime

import database

import _setup
import _dbtest_common

from mettledb import model

cfg     = None
DEBUG   = True
TEST_DB = 'postgres'


def setUpModule():
    global cfg
    global DEBUG

    cfg = _setup.readConfig()

#     if DEBUG:
#         return

#     user   = cfg['db-tests']['postgres']['user-admin']
#     passwd = cfg['db-tests']['postgres']['passwd-admin'] or ''
#     db     = cfg['db-tests']['postgres']['db-admin']
#     host   = cfg['db-tests']['postgres']['host'] or ''

#     print('host:%r, db:%r, user:%r, passwd:%r' % (host, db, user, passwd))

#     conn   = mettledb_postgresql9.Connect()

#     conn.connectDeft(host, db, user, passwd)
#     conn.transactionModeSet(False)

#     user   = cfg['db-tests']['postgres']['user-test']
#     passwd = cfg['db-tests']['postgres']['passwd-test'] or ''
#     db     = cfg['db-tests']['postgres']['db-test']

#     if None == _setup.sqlFetch(conn, "SELECT 1 FROM pg_database WHERE datname='%s'" % db, one=True):
# #         _setup.sqlRun(conn, "DROP DATABASE %s" % db)

#         if None != _setup.sqlFetch(conn, "SELECT 1 FROM pg_roles WHERE rolname='%s'" % user, one=True):
#             _setup.sqlRun(conn, "DROP OWNED BY %s" % user)
#             _setup.sqlRun(conn, "DROP USER %s" % user)

#         _setup.sqlRun(conn, "CREATE USER %s WITH PASSWORD '%s'" % (user, passwd))
#         _setup.sqlRun(conn, "CREATE DATABASE %s owner %s" % (db, user))
#         _setup.sqlRun(conn, "GRANT ALL PRIVILEGES ON DATABASE %s TO %s;" % (db, user))

#     conn.close()


def tearDownModule():
    pass


class TestPostgreSQLAlchemy(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.conn   = None
        cls.tables = _dbtest_common.allTestTables()
        cls.usrId  = 'TestPostgreSQLAlchemy'
        cls.data   = _setup._data
        cls.cfg    = _setup._cfg
        # cls.conn   = mettledb_postgresql9.Connect()
        # cls.debug  = DEBUG

    @classmethod
    def tearDownClass(cls):
        del cls.conn


    def test_001_CheckFiles(self):
        if DEBUG: return

        pth = os.path.join('..', 'generator', 'db', 'sqldef', 'postgresql')
        _dbtest_common.checkGeneratedFiles(self, self.tables, pth)


    def test_002_Connect(self):
        db_url = "%s://%s:%s@%s/%s" % (
            self.cfg['db-tests'][TEST_DB].get('driver') or '',
            self.cfg['db-tests'][TEST_DB].get('user') or '',
            self.cfg['db-tests'][TEST_DB].get('passwd') or '',
            self.cfg['db-tests'][TEST_DB].get('host') or '',
            self.cfg['db-tests'][TEST_DB].get('db') or '',
        )

        print('db_url: %s' % db_url)

        database.initialize(db_url)


    def test_xx(self):
        print()
        with database.session_scope() as dbs:
            x = dbs.execute("select * from parenttype where id = :pt", {'pt': 2})

            print(x)
            print(dir(x))
            print(x.keys)
            print(x.keys())
            print(x.rowcount)

            for row in x:
                print(row)

            # x = dbs.execute("select * from parenttype")

            # print(dir(x))
            # print(x.keys)
            # print(x.keys())
            # print(x.rowcount)

            # for row in x:
            #     print(row)

        print()


    def test_010_SelectParentTypeTests(self):
        if DEBUG: return

        with database.session_scope() as dbs:
            pt = model.ParentType.select_one(dbs, 1)

            self.assertEqual(pt.id,      1)
            self.assertEqual(pt.descr,  'Single Mother')
            self.assertEqual(pt.role,    model.ParentType.Role_Couplet.key_mother)
            self.assertEqual(type(pt.tm_stamp), datetime.datetime)
            self.assertNotEqual(pt.tm_stamp, datetime.datetime.min)

            pt = model.ParentType.select_one(dbs, 2)

            self.assertEqual(pt.id,     2)
            self.assertEqual(pt.descr,  'Married Father')
            self.assertEqual(pt.role,   model.ParentType.Role_Couplet.key_father)

            pt = model.ParentType.select_one(dbs, 3)

            self.assertEqual(pt.id,     3)
            self.assertEqual(pt.descr,  'Drunken Aunt')
            self.assertEqual(pt.role,   model.ParentType.Role_Couplet.key_aunt)

            pt = model.ParentType.select_one(dbs, 4)

            self.assertEqual(pt.id,     4)
            self.assertEqual(pt.descr,  'Awesome Uncle')
            self.assertEqual(pt.role,   model.ParentType.Role_Couplet.key_uncle)

            all_recs = model.ParentType.select_all(dbs)

            self.assertGreaterEqual(len(all_recs), 4)


    # def test_011_ParentTypeCrud(self):







    # def test_004_TestAllTypes(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao
    #     _dbtest_common.testAllTypes(self, dao)


    # def test_014_InsertRecords(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao
    #     _dbtest_common.testInsertRecords(self, dao, self.usrId)


    # def test_015_SelectRecords(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao
    #     _dbtest_common.testSelectRecords(self, dao, self.usrId)


    # def test_016_UpdateRecords(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao
    #     _dbtest_common.testUpdateRecords(self, dao, 'Spanker')


    # def test_020_TestLocksAndWaits(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao

    #     conn2 = mettledb_postgresql9.Connect()

    #     host   = self.cfg['db-tests']['postgres']['host'] or ''
    #     user   = self.cfg['db-tests']['postgres']['user-test']
    #     passwd = self.cfg['db-tests']['postgres']['passwd-test'] or ''
    #     db     = self.cfg['db-tests']['postgres']['db-test']

    #     conn2.connectDeft(host, db, user, passwd)

    #     _dbtest_common.testLocksAndWaits(self, conn2, dao)

    #     conn2.close()


    # def test_021_TestMultiTableProcs(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao

    #     _dbtest_common.testMultiTableProcs(self, dao)


    # def test_022_TestJson(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao

    #     res = _setup.sqlFetch(self.conn, '''select '{"foo":{"bar":33.3}}'::json->'foo'->'bar' as "jval"''', one=True)

    #     self.assertIsNotNone(res)
    #     self.assertEqual(res['jval'], '33.3')


    # def test_022_TestNullStrCriteria(self):
    #     if self.debug:
    #         return

    #     from mettledb.dao import postgresql as dao

    #     qry = dao.dChildSelectNullStr(self.conn)
    #     lst = []

    #     qry.execDeft(1, '').fetchAll(lst)

    #     self.assertEqual(len(lst), 4)

    #     qry.execDeft(1, 'Chuck').fetchAll(lst)

    #     self.assertEqual(len(lst), 1)



if __name__ == '__main__':
    unittest.main(verbosity=2, failfast=True)
