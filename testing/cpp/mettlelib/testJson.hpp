/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"
#include "mettle/json/parser.h"
#include "mettle/json/datanode2json.h"

using namespace Mettle::Lib;
using namespace Mettle::Json;

#define testSuite testJson


LT_BEGIN_SUITE(testSuite)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testSuite)

LT_BEGIN_TEST(testSuite, test_Reading)

Mettle::Json::Parser  jp;
DataNode::Safe        root(0);
DataNode             *dn;
String                val;

LT_ASSERT_NOTHROW(root.remember(jp.parseFile("../jsonTestFile.json")))
LT_ASSERT_NEQ(root.obj, 0)

dn = root.obj;

DataNode *dn_name;
DataNode *dn_surname;
DataNode *dn_age;
DataNode *dn_address;
DataNode *dn_contact;
DataNode *dn_objFoo;
DataNode *dn_msg;

LT_ASSERT_NEQ((dn_name    = dn->get("name")),        0)
LT_ASSERT_NEQ((dn_surname = dn->get("surname")),     0)
LT_ASSERT_NEQ((dn_age     = dn->get("age")),         0)
LT_ASSERT_NEQ((dn_address = dn->get("address")),     0)
LT_ASSERT_NEQ((dn_contact = dn->get("contact-nos")), 0)
LT_ASSERT_NEQ((dn_objFoo  = dn->get("objFoo")),      0)
LT_ASSERT_NEQ((dn_msg     = dn->get("msg")),         0)


DataNode *dn_line1;
DataNode *dn_line2;
DataNode *dn_pcode;
DataNode *dn_objBar;
DataNode *dn_val1;
DataNode *dn_val2;
DataNode *dn_val3;
DataNode *dn_arr;

LT_ASSERT_NEQ((dn_line1  = dn->get("address/line1")),        0)
LT_ASSERT_NEQ((dn_line2  = dn->get("address/line2")),        0)
LT_ASSERT_NEQ((dn_pcode  = dn->get("address/pcode")),        0)
LT_ASSERT_NEQ((dn_objBar = dn->get("objFoo/objBar")),        0)
LT_ASSERT_NEQ((dn_val1   = dn->get("objFoo/objBar/val1")),   0)
LT_ASSERT_NEQ((dn_val2   = dn->get("objFoo/objBar/val2")),   0)
LT_ASSERT_NEQ((dn_val3   = dn->get("objFoo/objBar/val3")),   0)
LT_ASSERT_NEQ((dn_arr    = dn->get("objFoo/arr")),           0)

LT_CHECK_EQ(dn_name->valueIsEmpty(),  false)

LT_CHECK_EQ(dn_name->value()->toString(val),    "Nicolas")
LT_CHECK_EQ(dn_surname->value()->toString(val), "Milicevic")
LT_CHECK_EQ(dn_age->value()->toString(val),     "36")

LT_CHECK_EQ(dn_line1->value()->toString(val),   "35 Somewhere Rd")
LT_CHECK_EQ(dn_line2->value()->toString(val),   "Out There Ville")
LT_CHECK_EQ(dn_pcode->value()->toString(val),   "2478")

LT_CHECK_EQ(dn_val1->value()->toString(val),    "one")
LT_CHECK_EQ(dn_val2->value()->toString(val),    "2")
LT_CHECK_EQ(dn_val3->value()->toString(val),    "3")

LT_CHECK_EQ(dn_msg->value()->toString(val),     "have a nice day")

LT_ASSERT_EQ(dn_contact->value()->valueType(), DataVar::tl_list)

DataVar::List *contList = (DataVar::List *) dn_contact->value()->valuePtr();

LT_ASSERT_EQ(contList->count(), 4)

LT_CHECK_EQ(contList->get(0)->toString(val), "011-555-1234")
LT_CHECK_EQ(contList->get(1)->toString(val), "074-555-5678")
LT_CHECK_EQ(contList->get(2)->toString(val), "252.23")
LT_CHECK_EQ(contList->get(3)->toString(val), "555-555-5555")

DataVar::List *arrList = (DataVar::List *) dn_arr->value()->valuePtr();

LT_ASSERT_EQ(arrList->count(), 9)

StringBuilder testTxt;

DataNode2Json(root.obj).print(&testTxt);

//printf("written:%s\n", testTxt.value);

LT_END_TEST(test_Reading)

#undef testSuite
