/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/string.h"

#include <limits.h>

using namespace Mettle::Lib;

#define testSuite testString


LT_BEGIN_SUITE(testSuite)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testSuite)

LT_BEGIN_TEST(testSuite, test_Assignments)

#define ASSIGN_STR      "abcdefg"
#define ASSIGN_CHAR     'x'
#define ASSIGN_INT      12345
#define ASSIGN_INT_STR  "12345"
#define ASSIGN_DBL      987.654
#define ASSIGN_DBL_STR  "987.654"

   String s1(ASSIGN_STR);
   String s2 = ASSIGN_STR;
   String s3(s1);
   String s4;
   String s5;

   s4 = s1;
   s5 = ASSIGN_STR;

   LT_CHECK_EQ(s1, ASSIGN_STR)
   LT_CHECK_EQ(s2, ASSIGN_STR)
   LT_CHECK_EQ(s3, ASSIGN_STR)
   LT_CHECK_EQ(s4, ASSIGN_STR)
   LT_CHECK_EQ(s5, ASSIGN_STR)

   String c1(ASSIGN_CHAR);
   String c2 = ASSIGN_CHAR;
   String c3(c1);

   LT_CHECK_EQ(c1.length(),       1)
   LT_CHECK_EQ(c1[0],             ASSIGN_CHAR)
   LT_CHECK_EQ(c1,                ASSIGN_CHAR)
   LT_CHECK_EQ(c2,                "x")
   LT_CHECK_EQ(c2,                c1)
   LT_CHECK_EQ(c3,                c1)

   String i1(ASSIGN_INT);
   String i2 = ASSIGN_INT;

   LT_CHECK_EQ(i1, ASSIGN_INT_STR)
   LT_CHECK_EQ(i2, ASSIGN_INT_STR)

   String d1(ASSIGN_DBL);
   String d2 = ASSIGN_DBL;

   LT_CHECK_EQ(d1, ASSIGN_DBL_STR)
   LT_CHECK_EQ(d2, ASSIGN_DBL_STR)

#undef ASSIGN_STR
#undef ASSIGN_CHAR
#undef ASSIGN_INT
#undef ASSIGN_INT_STR
#undef ASSIGN_DBL
#undef ASSIGN_DBL_STR

LT_END_TEST(test_Assignments)

LT_BEGIN_TEST(testSuite, test_Operators)

   String cmps1("abcdefg");
   String cmps2("zyxwvut");
   String cmpn1("50");
   String cmpn2("100");

   LT_CHECK_EQ(cmps1  == cmps1,         true)
   LT_CHECK_EQ(cmps1  == "abcdefg",     true)
   LT_CHECK_EQ(cmps1  != cmps2,         true)
   LT_CHECK_EQ(cmps1  != "zyxwvut",     true)

   LT_CHECK_EQ(cmps1  > cmps2,     false)
   LT_CHECK_EQ(cmps2  > cmps1,     true)

   LT_CHECK_EQ(cmps1  < cmps2,     true)
   LT_CHECK_EQ(cmps2  < cmps1,     false)

   LT_CHECK_EQ(cmps1  != cmps1,         false)
   LT_CHECK_EQ(cmps1  != "abcdefg",     false)
   LT_CHECK_EQ(cmps1  == cmps2,         false)
   LT_CHECK_EQ(cmps1  == "zyxwvut",     false)
   LT_CHECK_EQ(cmpn1  > cmpn2,          true)

   cmpn1 += "foo";
   cmpn2 += "bar";

   LT_CHECK_EQ(cmpn1 == "50foo",     true)
   LT_CHECK_EQ(cmpn2 == "100bar",    true)

   cmpn1 += cmpn2;

   LT_ASSERT_EQ(cmpn1 == "50foo100bar", true)

   String spch1 = cmpn1.splice(0, 0 - cmpn2.length());
   String spch2 = cmpn1.splice(5, 0);
   String spch3 = cmpn1.splice(2, 8);

   LT_ASSERT_EQ(spch1 == "50foo",   true)
   LT_ASSERT_EQ(spch2 == cmpn2,     true)
   LT_ASSERT_EQ(spch3 == "foo100",  true)

LT_END_TEST(test_Operators)

LT_BEGIN_TEST(testSuite, test_Split)

   String       toSplit("abc;efg;hij;klmn;");
   String::List dest;

   toSplit.split(";", dest);

   LT_ASSERT_EQ(dest.count(),     5)
   LT_ASSERT_EQ(dest[0],          "abc")
   LT_ASSERT_EQ(dest[1],          "efg")
   LT_ASSERT_EQ(dest[2],          "hij")
   LT_ASSERT_EQ(dest[3],          "klmn")
   LT_ASSERT_EQ(dest[4].isEmpty(), true)

LT_END_TEST(test_Split)

LT_BEGIN_TEST(testSuite, test_ToTypes)

   int32_t  i32    = INT32_MAX - 1;
   int64_t  i64    = INT64_MAX - 1;
   uint32_t u32    = UINT32_MAX - 1;
   uint64_t u64    = UINT64_MAX - 1;
   double   dv     = 123.456;
   bool     btrue  = true;
   bool     bfalse = false;
   String   jot;

   jot = i32;
   LT_CHECK_EQ(i32, jot.toInt32());

   jot = i64;
   LT_CHECK_EQ(i64, jot.toInt64());

   jot = u32;
   LT_CHECK_EQ(u32, jot.toUInt32());

   jot = u64;
   LT_CHECK_EQ(u64, jot.toUInt64());

   jot = btrue;
   LT_CHECK_EQ(btrue, jot.toBool());

   jot = bfalse;
   LT_CHECK_EQ(bfalse, jot.toBool());

   jot = dv;
   LT_CHECK_EQ(dv, jot.toDouble());

LT_END_TEST(test_ToTypes)

LT_BEGIN_TEST(testSuite, test_FromTypes)

   String strue  = "true";
   String sfalse = "false";
   String jotTrue("true");
   String jotFalse("false");
   String jot;

   jot = true;
   LT_CHECK_EQ(strue, jot);
   LT_CHECK_EQ(strue, jotTrue);

   jot = false;
   LT_CHECK_EQ(sfalse, jot);
   LT_CHECK_EQ(sfalse, jotFalse);

LT_END_TEST(test_FromTypes)


#undef testSuite
