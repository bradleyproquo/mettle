/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/datavar.h"
#include "mettle/lib/string.h"

using namespace Mettle::Lib;

#define testSuite testDateVar

LT_BEGIN_SUITE(testSuite)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testSuite)

LT_BEGIN_TEST(testSuite, test_detectTypes)

   DataVar   dv;
   String    jot;
   DateTime  dt1;
   DateTime  dt2;
   double    dbl;
   int32_t   i32;
   //int64_t   i64;
   //uint32_t  u64;

   dv.write("true");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_bool)
   dv.toString(jot);
   LT_CHECK_EQ(jot, "true")

   dv.write("yes");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_bool)
   dv.toString(jot);
   LT_CHECK_EQ(jot, "true")

   dv.write("no");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_bool)
   dv.toString(jot);
   LT_CHECK_EQ(jot, "false")

   dv.write("false");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_bool)
   dv.toString(jot);
   LT_CHECK_EQ(jot, "false")

   dv.write("2016-05-22");
   dt1.date = 20160522;
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_date)
   dv.read(dt2.date);
   LT_CHECK_EQ(dt1.date, dt2.date)

   dv.write("21:42:59");
   dt1.time = 214259;
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_time)
   dv.read(dt2.time);
   LT_CHECK_EQ(dt1.time, dt2.time)

   dv.write("1999-12-31 23:59:59");
   dt1.assign(1999, 12, 31, 23, 59, 59);
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_datetime)
   dv.read(dt2);
   LT_CHECK_EQ(dt1, dt2)

   dv.write("123456");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_int32)
   dv.read(i32);
   LT_CHECK_EQ(123456, i32)

   dv.write("-123456");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_int32)
   dv.read(i32);
   LT_CHECK_EQ(-123456, i32)

   dv.write("9876.54321");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_double)
   dv.read(dbl);
   LT_CHECK_EQ(9876.54321, dbl)

   dv.write("Foobar");
   dv.toBestType();
   LT_CHECK_EQ(dv.valueType(), DataVar::tl_string)
   dv.read(jot);
   LT_CHECK_EQ(jot, "Foobar")

LT_END_TEST(test_detectTypes)


#undef testSuite
