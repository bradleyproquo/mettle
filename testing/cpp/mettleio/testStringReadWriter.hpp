/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/date.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"
#include "mettle/lib/time.h"

#include "mettle/io/stringbuilderreader.h"
#include "mettle/io/stringbuilderwriter.h"

using namespace Mettle::Lib;
using namespace Mettle::IO;

#define testSuite testStringReadWriter

LT_BEGIN_SUITE(testSuite)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testSuite)

LT_BEGIN_TEST(testSuite, test_writer)

   StringBuilder sb;
   StringBuilderWriter sbw(&sb);

   DateTime::setLocalTZ(2, 0);

   bool       outvarbool;
   char       outvarchar;
   int8_t     outvarint8_t;
   int16_t    outvarint16_t;
   int32_t    outvarint32_t;
   int64_t    outvarint64_t;
   uint8_t    outvaruint8_t;
   uint16_t   outvaruint16_t;
   uint32_t   outvaruint32_t;
   uint64_t   outvaruint64_t;
   double     outvardouble;
   float      outvarfloat;
   String     outvarString;
   UString    outvarUString;
   DateTime   outvarDateTime;
   Date       outvarDate;
   Time       outvarTime;

   bool       invarbool     = true;
   char       invarchar     = 'X';
   int8_t     invarint8_t   = 8;
   int16_t    invarint16_t  = 16;
   int32_t    invarint32_t  = 3232;
   int64_t    invarint64_t  = 64646464;
   uint8_t    invaruint8_t  = 88;
   uint16_t   invaruint16_t = 9666;
   uint32_t   invaruint32_t = 899887;
   uint64_t   invaruint64_t = 128128128;
   double     invardouble   = 123.456789;
   float      invarfloat    = 987.654;
   String     invarString   = "Big brown cow";
   UString    invarUString  = "Another fuggen cow";
   DateTime   invarDateTime = DateTime::now();
   Date       invarDate     = invarDateTime.date;
   Time       invarTime     = invarDateTime.time;

   sbw.write("bool",     invarbool);
   sbw.write("char",     invarchar);
   sbw.write("int8_t",   invarint8_t);
   sbw.write("int16_t",  invarint16_t);
   sbw.write("int32_t",  invarint32_t);
   sbw.write("int64_t",  invarint64_t);
   sbw.write("uint8_t",  invaruint8_t);
   sbw.write("uint16_t", invaruint16_t);
   sbw.write("uint32_t", invaruint32_t);
   sbw.write("uint64_t", invaruint64_t);
   sbw.write("double",   invardouble);
   sbw.write("float",    invarfloat);
   sbw.write("String",   invarString);
   sbw.write("UString",  invarUString);
   sbw.write("DateTime", invarDateTime);
   sbw.write("Date",     invarDate);
   sbw.write("Time",     invarTime);

   String::List  strlist;
   StringBuilder res;
   char          jotter[16];

   strlist.append(new String(invarbool));
   strlist.append(new String(invarchar));
   strlist.append(new String(invarint8_t));
   strlist.append(new String(invarint16_t));
   strlist.append(new String(invarint32_t));
   strlist.append(new String(invarint64_t));
   strlist.append(new String(invaruint8_t));
   strlist.append(new String(invaruint16_t));
   strlist.append(new String(invaruint32_t));
   strlist.append(new String(invaruint64_t));
   strlist.append(new String(invardouble));
   strlist.append(new String(invarfloat));
   strlist.append(new String(invarString));
   strlist.append(new String(invarUString.c_str()));
   strlist.append(new String(invarDateTime.toString(jotter, true)));
   strlist.append(new String(invarDate.toString(jotter)));
   strlist.append(new String(invarTime.toString(jotter)));

   res.joinSep("|", strlist);

//   printf("res.value: %s\n",  res.value);
//   printf("sb.value : %s\n",  sb.value);

   StringBuilderReader sbr(&sb);

   sbr.read("bool",     outvarbool);
   LT_CHECK_EQ(outvarbool, invarbool);
   sbr.read("char",     outvarchar);
   LT_CHECK_EQ(outvarchar, invarchar);
   sbr.read("int8_t",   outvarint8_t);
   LT_CHECK_EQ(outvarint8_t, invarint8_t);
   sbr.read("int16_t",  outvarint16_t);
   LT_CHECK_EQ(outvarint16_t, invarint16_t);
   sbr.read("int32_t",  outvarint32_t);
   LT_CHECK_EQ(outvarint32_t, invarint32_t);
   sbr.read("int64_t",  outvarint64_t);
   LT_CHECK_EQ(outvarint64_t, invarint64_t);
   sbr.read("uint8_t",  outvaruint8_t);
   LT_CHECK_EQ(outvaruint8_t, invaruint8_t);
   sbr.read("uint16_t", outvaruint16_t);
   LT_CHECK_EQ(outvaruint16_t, invaruint16_t);
   sbr.read("uint32_t", outvaruint32_t);
   LT_CHECK_EQ(outvaruint32_t, invaruint32_t);
   sbr.read("uint64_t", outvaruint64_t);
   LT_CHECK_EQ(outvaruint64_t, invaruint64_t);
   sbr.read("double",   outvardouble);
   LT_CHECK_EQ(outvardouble, invardouble);
   sbr.read("float",    outvarfloat);
   LT_CHECK_EQ(outvarfloat, invarfloat);
   sbr.read("String",   outvarString);
   LT_CHECK_EQ(outvarString, invarString);
   sbr.read("UString",  outvarUString);
   LT_CHECK_EQ(outvarUString, invarUString);
   sbr.read("DateTime", outvarDateTime);
   LT_CHECK_EQ(outvarDateTime, invarDateTime);
   sbr.read("Date",     outvarDate);
   LT_CHECK_EQ(outvarDate, invarDate);
   sbr.read("Time",     outvarTime);
   LT_CHECK_EQ(outvarTime, invarTime);

   LT_CHECK_EQ(0, strcmp(res.value, sb.value));

LT_END_TEST(test_writer)


#undef testSuite
