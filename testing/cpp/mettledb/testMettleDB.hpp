/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/common.h"
#include "mettle/lib/filehandle.h"
#include "mettle/lib/filemanager.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include "mettle/db/iconnect.h"
#include "mettle/db/statement.h"
#include "mettle/db/sqlbuilder.h"
#include "mettle/db/sqlparams.h"

using namespace Mettle::Lib;

#if defined(METTLEDB_SQLITE)

   #define testSuite testSQLite
   #define DB_FILENAME "sqlite_mettle.db"

   #include "mettle/db/sqlite/dbconnect.h"

#elif defined(METTLEDB_POSTGRESQL9)

   #define testSuite testPostgreSQL9
   #define DB_CONN_STRING "user=mettletest dbname=mettletest"

   #include "mettle/db/postgresql/dbconnect.h"

#else
   #error DATABASE NOT FOUND
#endif

#include "dao/alltypesDAO.h"
#include "dao/childDAO.h"
#include "dao/parentDAO.h"
#include "dao/parenttypeDAO.h"

// #define VERBOSE

using namespace TestDB;


class Variables
{
public:
   const char                *dbName;
   const char                *dbDirName;
   const char                *usrId;
   Mettle::Lib::String::List  tables;
   Mettle::Lib::String::List  extList;
   Mettle::Lib::String        sqlDefPath;
   Mettle::DB::IConnect      *conn;

   Mettle::Lib::Guid _guidChunks;
   Mettle::Lib::Guid _guidSue;


   Variables()
   {
      tables.append(new Mettle::Lib::String("AllTypes"));
      tables.append(new Mettle::Lib::String("Child"));
      tables.append(new Mettle::Lib::String("Parent"));
      tables.append(new Mettle::Lib::String("ParentType"));

#if defined(METTLEDB_SQLITE)
      dbName    = "SQLite";
      dbDirName = "sqlite";
      usrId     = "SqliteMettle";
      conn      = new Mettle::DB::SQLite::DBConnect();
#elif defined(METTLEDB_POSTGRESQL9)
      dbName    = "PostgreSQL";
      dbDirName = "postgresql";
      usrId     = "PostgresMettle";
      conn      = new Mettle::DB::PostgreSQL::DBConnect();

      extList.append(new Mettle::Lib::String(".drop"));
#endif

      extList.append(new Mettle::Lib::String(".table"));
      extList.append(new Mettle::Lib::String(".constraint"));
      extList.append(new Mettle::Lib::String(".index"));

      _guidChunks = Mettle::Lib::Guid::newGuid();
      _guidSue    = Mettle::Lib::Guid::newGuid();
   }

   ~Variables()
   {
      delete conn;
   }
};

Variables vars;


LT_BEGIN_SUITE(testSuite)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testSuite)

LT_BEGIN_TEST(testSuite, test_001_CheckFiles)

   vars.sqlDefPath.format("..%c..%c..%c..%cgenerator%cdb%csqldef%c%s",
      Mettle::Lib::String::dirSep(),
      Mettle::Lib::String::dirSep(),
      Mettle::Lib::String::dirSep(),
      Mettle::Lib::String::dirSep(),
      Mettle::Lib::String::dirSep(),
      Mettle::Lib::String::dirSep(),
      Mettle::Lib::String::dirSep(),
      vars.dbDirName);

   // - seems to be a bug here :( vars.sqlDefPath.joinDirs("..", "..", "..", "..", "generator", "db", "sqldef", _dbDirName, 0);

   String pth;

   LT_ASSERT_EQ(true, Mettle::Lib::FileManager::pathExists(vars.sqlDefPath.c_str()))

   _FOREACH(Mettle::Lib::String, ext, vars.extList)
   {
      _FOREACH(Mettle::Lib::String, tbl, vars.tables)
      {
         pth.format("%s%c%s%s", vars.sqlDefPath.c_str(), Mettle::Lib::String::dirSep(), tbl.obj->c_str(), ext.obj->c_str());
         LT_ASSERT_EQ(true, Mettle::Lib::FileManager::pathExists(pth.c_str()))
      }
   }

LT_END_TEST(test_001_CheckFiles)


LT_BEGIN_TEST(testSuite, test_002_Connect)

#if defined(METTLEDB_SQLITE)
     LT_ASSERT_EQ(0, strcmp("SQLite", vars.conn->name()))
     vars.conn->connect(DB_FILENAME);
#elif defined(METTLEDB_POSTGRESQL9)
     LT_ASSERT_EQ(0, strcmp("PostgreSQL", vars.conn->name()))
     vars.conn->connect(DB_CONN_STRING);
#endif

LT_END_TEST(test_002_Connect)


LT_BEGIN_TEST(testSuite, test_003_CreateTables)

   Mettle::Lib::String        pth;
   Mettle::Lib::String        sql;
   Mettle::Lib::String::List  sqlParts;
   Mettle::Lib::FileHandle    fh;
   Mettle::Lib::MemoryBlock   data;
   unsigned int               len;

   vars.conn->transactionModeSet(false);

   data.allocate(8096);

   LT_CHECKPOINT();

   _FOREACH(Mettle::Lib::String, ext, vars.extList)
   {
      _FOREACH(Mettle::Lib::String, tbl, vars.tables)
      {
         pth.format("%s%c%s%s", vars.sqlDefPath.c_str(), Mettle::Lib::String::dirSep(), tbl.obj->c_str(), ext.obj->c_str());

         fh.open(pth.c_str(), Mettle::Lib::FileHandle::Text);
         len = fh.read(&data, false);
         sqlParts.clear();

         LT_ASSERT_LT(len, (data._size - 1))
         data._block[len] = 0;

         sql = (char *) data._block;

         int sidx = sql.find("---SQL START---");
         int eidx = sql.find("---SQL END---");

         LT_ASSERT_NEQ(0, sidx)
         LT_ASSERT_NEQ(0, eidx)
         LT_ASSERT_LT(sidx + 15, eidx)

         sql = sql.splice(sidx + 15, eidx);
         sql.split(";", sqlParts);

         _FOREACH(Mettle::Lib::String, sqlCmd, sqlParts)
         {
            sqlCmd.obj->strip();

            if (sqlCmd.obj->isEmpty())
               continue;

            Mettle::DB::Statement::Safe stmnt(vars.conn->createStatement("x"));

            stmnt.obj->sql->add(sqlCmd.obj->c_str());

            vars.conn->execute(stmnt.obj);
         }
      }
   }

   vars.conn->transactionModeSet(true);

LT_END_TEST(test_003_CreateTables)

LT_BEGIN_TEST(testSuite, test_004_TestAllTypes)

   dAllTypes atc(vars.conn);
   dAllTypes atr(vars.conn);

   Mettle::Lib::DateTime    dtval = Mettle::Lib::DateTime::now();
   Mettle::Lib::Date        dval  = Mettle::Lib::Date::now();
   Mettle::Lib::Time        tval(23, 59, 59);
   Mettle::Lib::MemoryBlock mval;
   Mettle::Lib::Guid        gval = Mettle::Lib::Guid::newGuid();

   Mettle::Lib::String      json("{\"foo\":1, \"bar\":2 }");

   mval.allocate(20);
   memset(mval.blockU8(), '1', 20);

   atc.Insert(
        8,
        16,
        32,
        16,
        123.45,
        true,
        "This is a string",
        'X',
        mval,
        gval,
        json,
        dval,
        tval,
        dtval);

   atr.SelectOne(atc.rec.TypeSeq64);

   LT_ASSERT_EQ(atr.rec.TypeInt8,     atc.rec.TypeInt8)
   LT_ASSERT_EQ(atr.rec.TypeInt16,    atc.rec.TypeInt16)
   LT_ASSERT_EQ(atr.rec.TypeInt32,    atc.rec.TypeInt32)
   LT_ASSERT_EQ(atr.rec.TypeInt64,    atc.rec.TypeInt64)
   LT_ASSERT_EQ(atr.rec.TypeDouble,   atc.rec.TypeDouble)
   LT_ASSERT_EQ(atr.rec.TypeBool,     atc.rec.TypeBool)
   LT_ASSERT_EQ(atr.rec.TypeString,   atc.rec.TypeString)
   LT_ASSERT_EQ(atr.rec.TypeChar,     atc.rec.TypeChar)
   LT_ASSERT_EQ(atr.rec.TypeUuid,     atc.rec.TypeUuid)
   LT_ASSERT_EQ(atr.rec.TypeDate,     atc.rec.TypeDate)
   LT_ASSERT_EQ(atr.rec.TypeTime,     atc.rec.TypeTime)
   LT_ASSERT_EQ(atr.rec.TypeDatetime, atc.rec.TypeDatetime)
   // Postgres seems randomize the dictionary entries LT_ASSERT_EQ(atr.rec.TypeJson,     atc.rec.TypeJson)

   atc.Insert(
        -8,
        -16,
        -32,
        -16,
        -123.45,
        false,
        "abcdefghijklmnopqrstuvwxyz",
        'X',
        mval,
        gval,
        json,
        dval,
        tval,
        dtval);

   atr.SelectOne(atc.rec.TypeSeq64);

   LT_ASSERT_EQ(atr.rec.TypeInt8,     atc.rec.TypeInt8)
   LT_ASSERT_EQ(atr.rec.TypeInt16,    atc.rec.TypeInt16)
   LT_ASSERT_EQ(atr.rec.TypeInt32,    atc.rec.TypeInt32)
   LT_ASSERT_EQ(atr.rec.TypeInt64,    atc.rec.TypeInt64)
   LT_ASSERT_EQ(atr.rec.TypeDouble,   atc.rec.TypeDouble)
   LT_ASSERT_EQ(atr.rec.TypeBool,     atc.rec.TypeBool)
   LT_ASSERT_EQ(atr.rec.TypeString,   atc.rec.TypeString)
   LT_ASSERT_EQ(atr.rec.TypeChar,     atc.rec.TypeChar)
   LT_ASSERT_EQ(atr.rec.TypeUuid,     atc.rec.TypeUuid)
   LT_ASSERT_EQ(atr.rec.TypeDate,     atc.rec.TypeDate)
   LT_ASSERT_EQ(atr.rec.TypeTime,     atc.rec.TypeTime)
   LT_ASSERT_EQ(atr.rec.TypeDatetime, atc.rec.TypeDatetime)


   Mettle::DB::Statement* stmnt;
   int                    row = 0;
   char                   jotter[128];


   stmnt = vars.conn->createStatement("DynFetch");

   stmnt->sql->add("select * from alltypes");


   vars.conn->execute(stmnt);

   while (vars.conn->fetch(stmnt))
   {
      LT_ASSERT_GT(stmnt->out->count, 0)

#ifdef VERBOSE

      const char* comma = "";

      if (row == 0)
      {
         printf("------colcount:%d\n", stmnt->out->count);


         for (int idx = 0; idx < stmnt->out->count; idx++)
         {
            Mettle::DB::SqlParams::Binding* bind = &stmnt->out->bindings[idx];

            printf("%s%s", comma, bind->name);

            comma = ", ";
         }

         printf("\n");
      }

      comma = "";

      for (int idx = 0; idx < stmnt->out->count; idx++)
      {
         printf("%s", comma);

         Mettle::DB::SqlParams::Binding* bind = &stmnt->out->bindings[idx];

         switch (bind->type)
         {
            case Mettle::DB::SqlParams::STRING   :
            case Mettle::DB::SqlParams::JSON     : printf("%s", bind->asString()->c_str()); break;
            case Mettle::DB::SqlParams::CHAR     : printf("%c", *bind->asChar()); break;
            case Mettle::DB::SqlParams::INT8     : printf("%d", *bind->asInt8()); break;
            case Mettle::DB::SqlParams::INT16    : printf("%d", *bind->asInt16()); break;
            case Mettle::DB::SqlParams::INT32    : printf("%d", *bind->asInt32()); break;
            case Mettle::DB::SqlParams::INT64    : printf("%ld", *bind->asInt64()); break;
            case Mettle::DB::SqlParams::DOUBLE   : printf("%.2lf", *bind->asDouble()); break;
            case Mettle::DB::SqlParams::BOOL     : printf("%s", (*bind->asBool() ? "true" : "false")); break;
            case Mettle::DB::SqlParams::DATE     : printf("%s", bind->asDate()->toString(jotter)); break;
            case Mettle::DB::SqlParams::TIME     : printf("%s", bind->asTime()->toString(jotter)); break;
            case Mettle::DB::SqlParams::DATETIME : printf("%s", bind->asDateTime()->toString(jotter)); break;
            case Mettle::DB::SqlParams::UUID     : printf("%s", bind->asGuid()->toString(jotter)); break;
            case Mettle::DB::SqlParams::MEMBLOCK : printf("<blob>"); break;

            default :
               printf("<UNKNOWN!>"); break;
         }

         comma = ", ";
      }

      printf("\n");


      row++;
   }


#endif

   delete stmnt;



LT_END_TEST(test_004_TestAllTypes)


LT_BEGIN_TEST(testSuite, test_014_InsertRecords)

   dParentType pt(vars.conn);
   dParent     pa(vars.conn);
   dChild      ch(vars.conn);

   pt.Insert("Single Mother", cParentType::Role_Couplet::KeyMother, vars.usrId);
   LT_ASSERT_EQ(1, pt.rec.Id)

   pt.Insert("Married Father", cParentType::Role_Couplet::KeyFather, vars.usrId);
   LT_ASSERT_EQ(2, pt.rec.Id)

   pt.Insert("Drunken Aunt", cParentType::Role_Couplet::KeyAunt, vars.usrId);
   LT_ASSERT_EQ(3, pt.rec.Id)

   pt.Insert("Awesome Uncle", cParentType::Role_Couplet::KeyUncle, vars.usrId);
   LT_ASSERT_EQ(4, pt.rec.Id)

   vars.conn->commit();

   pa.Insert(2, "Chunks", "Charles Wageslave Worksalot", 987.654, 951, Mettle::Lib::Date(1981, 12, 2), false, vars._guidChunks, vars.usrId);
   LT_ASSERT_EQ(1, pa.rec.Id)

   pa.Insert(1, "Sue", "Susan Shagsalotof Chads", 100.12, 23, Mettle::Lib::Date(1985, 4, 22), true, vars._guidSue, vars.usrId);
   LT_ASSERT_EQ(2, pa.rec.Id)

   pa.Insert(4, "Evilin", "Everly Drinkstomuch Booze", 12.3456, 654, Mettle::Lib::Date(1990, 2, 28), true, Mettle::Lib::Guid(), vars.usrId);
   LT_ASSERT_EQ(3, pa.rec.Id)

   pa.Insert(3, "Mark", "Mark Liveslife Well", 32.98, 9000, Mettle::Lib::Date(1980, 5, 1), false, Mettle::Lib::Guid(), vars.usrId);
   LT_ASSERT_EQ(4, pa.rec.Id)

   Mettle::Lib::DateTime    nulldt;
   Mettle::Lib::Time        bedTime;
   Mettle::Lib::MemoryBlock nopic;

   ch.Insert(1, 0, "Bobby", "Robert Chads",     cChild::Gender_Couplet::KeyMale,   Mettle::Lib::Date(2002, 3,  17), bedTime, nulldt, nopic, true);
   ch.Insert(1, 0, "Shela", "Shelly Chads",     cChild::Gender_Couplet::KeyFemale, Mettle::Lib::Date(2004, 10, 30), bedTime, nulldt, nopic, true);
   ch.Insert(1, 0, "Chuck", "Charles Chads",    cChild::Gender_Couplet::KeyMale,   Mettle::Lib::Date(2005, 11,  9), bedTime, nulldt, nopic, true);
   ch.Insert(1, 0, "Lizzy", "Elizabeth Chads",  cChild::Gender_Couplet::KeyFemale, Mettle::Lib::Date(2007, 1,  14), bedTime, nulldt, nopic, true);

   ch.Insert(2, 0, "Zack",  "Jacob Worksalot",    cChild::Gender_Couplet::KeyMale,   Mettle::Lib::Date(2003, 5,  15), bedTime, nulldt, nopic, true);
   ch.Insert(2, 0, "Bels",  "Belinda Worksalot",  cChild::Gender_Couplet::KeyFemale, Mettle::Lib::Date(2001, 7,  8),  bedTime, nulldt, nopic, true);

   ch.Insert(3, 0, "Ann",   "Anneliese Booze",    cChild::Gender_Couplet::KeyFemale, Mettle::Lib::Date(2006, 6,  6),  bedTime, nulldt, nopic, true);

   ch.Insert(4, 0, "Mary",  "Mary Well",      cChild::Gender_Couplet::KeyFemale, Mettle::Lib::Date(2011,  3, 18),  bedTime, nulldt, nopic, true);
   ch.Insert(4, 0, "Alex",  "Alexander Well", cChild::Gender_Couplet::KeyMale,   Mettle::Lib::Date(2013,  9,  4),  bedTime, nulldt, nopic, true);
   ch.Insert(4, 0, "Vicky", "Victoria Well",  cChild::Gender_Couplet::KeyFemale, Mettle::Lib::Date(2014, 12, 20),  bedTime, nulldt, nopic, true);

   vars.conn->commit();

LT_END_TEST(test_014_InsertRecords)

LT_BEGIN_TEST(testSuite, test_015_SelectRecords)

   dParentType       pt(vars.conn);
   dParent           pa(vars.conn);
   Mettle::Lib::Date tmpDate;

   pt.SelectOne(1);

   LT_ASSERT_EQ(pt.rec.Id,     1)
   LT_ASSERT_EQ(pt.rec.Descr,  "Single Mother")
   LT_ASSERT_EQ(pt.rec.Role,   cParentType::Role_Couplet::KeyMother)
   LT_ASSERT_EQ(pt.rec.UsrId,  vars.usrId)
   LT_ASSERT_NEQ(pt.rec.TmStamp.isNull(), true)

   pt.SelectOne(2);

   LT_ASSERT_EQ(pt.rec.Id,     2)
   LT_ASSERT_EQ(pt.rec.Descr,  "Married Father")
   LT_ASSERT_EQ(pt.rec.Role,   cParentType::Role_Couplet::KeyFather)
   LT_ASSERT_EQ(pt.rec.UsrId,  vars.usrId)
   LT_ASSERT_NEQ(pt.rec.TmStamp.isNull(), true)

   pt.SelectOne(3);

   LT_ASSERT_EQ(pt.rec.Id,     3)
   LT_ASSERT_EQ(pt.rec.Descr,  "Drunken Aunt")
   LT_ASSERT_EQ(pt.rec.Role,   cParentType::Role_Couplet::KeyAunt)
   LT_ASSERT_EQ(pt.rec.UsrId,  vars.usrId)
   LT_ASSERT_NEQ(pt.rec.TmStamp.isNull(), true)

   pt.SelectOne(4);

   LT_ASSERT_EQ(pt.rec.Id,     4)
   LT_ASSERT_EQ(pt.rec.Descr,  "Awesome Uncle")
   LT_ASSERT_EQ(pt.rec.Role,   cParentType::Role_Couplet::KeyUncle)
   LT_ASSERT_EQ(pt.rec.UsrId,  vars.usrId)
   LT_ASSERT_NEQ(pt.rec.TmStamp.isNull(), true)

   pa.SelectOne(1);
   tmpDate = 19811202;

   LT_ASSERT_EQ(pa.rec.Id,                         1)
   LT_ASSERT_EQ(pa.rec.ParentTypeId,               2)
   LT_ASSERT_EQ(pa.rec.ShortName,                  "Chunks")
   LT_ASSERT_EQ(pa.rec.FullName,                   "Charles Wageslave Worksalot")
   LT_ASSERT_EQ(pa.rec.Amount,                     987.654)
   LT_ASSERT_EQ(pa.rec.Code,                       951)
   LT_ASSERT_EQ(pa.rec.DoB.compare(&tmpDate),      0)
   LT_ASSERT_EQ(pa.rec.Mother,                     false)
   LT_ASSERT_EQ(pa.rec.RegId.isNull(),             false)
   LT_ASSERT_EQ(pa.rec.RegId.compare(&vars._guidChunks), 0)
   LT_ASSERT_EQ(pa.rec.UsrID,                      vars.usrId)
   LT_ASSERT_NEQ(pa.rec.TmStamp.isNull(),          true)

   pa.SelectOne(4);
   tmpDate = 19800501;

   LT_ASSERT_EQ(pa.rec.Id,                4)
   LT_ASSERT_EQ(pa.rec.ParentTypeId,      3)
   LT_ASSERT_EQ(pa.rec.ShortName,         "Mark")
   LT_ASSERT_EQ(pa.rec.FullName,          "Mark Liveslife Well")
   LT_ASSERT_EQ(pa.rec.Amount,            32.98)
   LT_ASSERT_EQ(pa.rec.Code,              9000)
   LT_ASSERT_EQ(pa.rec.DoB.compare(&tmpDate),  0)
   LT_ASSERT_EQ(pa.rec.Mother,            false)
   LT_ASSERT_EQ(pa.rec.RegId.isNull(),    true)
   LT_ASSERT_EQ(pa.rec.UsrID,             vars.usrId)
   LT_ASSERT_NEQ(pa.rec.TmStamp.isNull(), true)

   pa.SelectOne(3);
   tmpDate = 19900228;

   LT_ASSERT_EQ(pa.rec.Id,                3)
   LT_ASSERT_EQ(pa.rec.ParentTypeId,      4)
   LT_ASSERT_EQ(pa.rec.ShortName,         "Evilin")
   LT_ASSERT_EQ(pa.rec.FullName,          "Everly Drinkstomuch Booze")
   LT_ASSERT_EQ(pa.rec.Amount,            12.3456)
   LT_ASSERT_EQ(pa.rec.Code,              654)
   LT_ASSERT_EQ(pa.rec.DoB.compare(&tmpDate),  0)
   LT_ASSERT_EQ(pa.rec.Mother,            true)
   LT_ASSERT_EQ(pa.rec.RegId.isNull(),    true)
   LT_ASSERT_EQ(pa.rec.UsrID,             vars.usrId)
   LT_ASSERT_NEQ(pa.rec.TmStamp.isNull(), true)

   pa.SelectOne(2);
   tmpDate = 19850422;

   LT_ASSERT_EQ(pa.rec.Id,                      2)
   LT_ASSERT_EQ(pa.rec.ParentTypeId,            1)
   LT_ASSERT_EQ(pa.rec.ShortName,               "Sue")
   LT_ASSERT_EQ(pa.rec.FullName,                "Susan Shagsalotof Chads")
   LT_ASSERT_EQ(pa.rec.Amount,                  100.12)
   LT_ASSERT_EQ(pa.rec.Code,                    23)
   LT_ASSERT_EQ(pa.rec.DoB.compare(&tmpDate),   0)
   LT_ASSERT_EQ(pa.rec.Mother,                  true)
   LT_ASSERT_EQ(pa.rec.RegId.isNull(),          false)
   LT_ASSERT_EQ(pa.rec.RegId.compare(&vars._guidSue), 0)
   LT_ASSERT_EQ(pa.rec.UsrID,                   vars.usrId)
   LT_ASSERT_NEQ(pa.rec.TmStamp.isNull(),       true)

LT_END_TEST(test_015_SelectRecords)


LT_BEGIN_TEST(testSuite, test_016_UpdateRecords)

   dChild       ch(vars.conn);
   cChild::List recs;

   dChildByParent(vars.conn).Exec(2).FetchAll(recs);

   _FOREACH(cChild, x, recs)
   {
      x.obj->LastSpanked = Mettle::Lib::DateTime::now();
      ch.Update(x.obj);
   }

   vars.conn->commit();

LT_END_TEST(test_016_UpdateRecords)


LT_BEGIN_TEST(testSuite, test_017_TestReset)

   LT_ASSERT_EQ(true, vars.conn->reset(0, 0));

   // dParentType pt(vars.conn);

   // for (int idx = 0; idx < 10; idx++)
   // {
   //    Mettle::Lib::Common::sleepMili(1000);

   //    try
   //    {
   //       pt.SelectOne((idx % 3)+1);

   //       printf("\n[%d]  Parent type selected: %d - %s\n", idx, pt.rec.Id, pt.rec.Descr.c_str());

   //       vars.conn->rollback();

   //    }
   //    catch (Mettle::Lib::xMettle &x)
   //    {
   //       printf("\nException : %s\n", x.what());
   //    }
   // }


LT_END_TEST(test_017_TestReset)
