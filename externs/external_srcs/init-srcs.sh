#!/bin/bash

#******************************************************************************#
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
#******************************************************************************#
#  Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
#******************************************************************************#

source "../../code/bash/mettle-lib.sh"


function checkout_git_src()
{
  local gr
  local gr_name
  local git_repos=(`compgen -v | grep "cfg_git_*"`)
  local cmd

  for gr in "${git_repos[@]}"; do
    gr_name="${gr:8}"

    if [ -d "$gr_name" ]; then
      continue
    fi

    cmd="git clone ${!gr} $gr_name"

    mlib_exec "$0" "$LINENO" "$cmd"

    echo ""
  done
}

function checkout_targz_src()
{
  local tr
  local tr_name
  local targz_file=(`compgen -v | grep "cfg_targz_*"`)
  local cmd
  local dl_file
  local tr_dir

  for tr in "${targz_file[@]}"; do
    tr_name="${gr:8}"
    dl_file=`basename ${!tr}`
    tr_dir="${dl_file%.tar.gz}"

    if [ -d "$tr_dir" ]; then
      continue
    fi

    mlib_exec "$0" "$LINENO" "rm -f $dl_file"
    cmd="wget ${!tr}"
    mlib_exec "$0" "$LINENO" "$cmd"

    if [ ! -f "$dl_file" ]; then
      mlib_raise "$0" "$LINENO" "Downloaded file not found [${dl_file}] from url [${!tr}]"
    fi

    cmd="tar -xvzf $dl_file"
    mlib_exec "$0" "$LINENO" "$cmd"

    if [ ! -d "$tr_dir" ]; then
      mlib_raise "$0" "$LINENO" "Extracted directory not found [${tr_dir}] from tar.gz [${dl_file}]"
    fi

    echo ""
  done
}


function main()
{
  mlib_yaml_load "./init-srcs.yml" "cfg_"

  checkout_git_src

  checkout_targz_src
}

main
