# Note, that when you create your 'config.mk' files, any of the below
# entries that are not present or commented out are simply ignored. This
# may cause the compile process to ignore building certain libraries and show
# some warnings.

# windows specfic paths
# MINGW_PATH=C:\Program\ Files\MinGW

PYTHON3_PATH=/usr/include/python3.7
POSTGRESQL9_PATH=/usr/include/postgresq
JSMN_PATH=./external_srcs/jsmn
LIBYAML_PATH=./external_srcs/yaml-0.1.7
